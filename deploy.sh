#!/bin/bash

set -ex

echo "======== BEGIN `date` ========"

# Vérification de l'utilisateur
if [ "$(id -un)" != "www-data" ]; then
    echo "Ce script doit être lancé en tant qu'utilisateur www-data."
    exit 1
fi

php bin/console cache:clear
php bin/console doctrine:migrations:migrate
php bin/console agora:init-documents_ressources

echo "======== END `date` ========"
