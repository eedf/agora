FROM php:8.2-apache

# Debian packages
RUN apt-get update -y
RUN apt-get install -y git locales npm unzip libicu-dev libzip-dev zip libpng-dev logrotate librabbitmq-dev supervisor

# Locale
RUN echo "fr_FR.utf8" > /etc/locale.gen
RUN locale-gen

# PHP extensions
RUN docker-php-ext-install pdo_mysql intl zip gd
RUN pecl install amqp && docker-php-ext-enable amqp

# Apache modules
RUN a2enmod rewrite && a2enmod status

# Apache config
COPY vhost.conf /etc/apache2/sites-available/000-default.conf

# Supervisor config
COPY supervisor.conf /etc/supervisor/conf.d/agora.conf

# PHP config
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY php.ini /usr/local/etc/php/conf.d/roads.ini

# Code
COPY ./ ./

# Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install

# Npm
RUN npm install
RUN mkdir /var/www/html/public/build && chown www-data. /var/www/html/public/build
RUN npm run build

# Dir temp export
RUN mkdir /var/www/html/public/temp && chown www-data. /var/www/html/public/temp

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/agora.conf"]
