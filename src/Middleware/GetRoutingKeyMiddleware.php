<?php

namespace App\Middleware;

use App\Contracts\RetryCountSupportInterface;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpReceivedStamp;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\HandlerArgumentsStamp;

class GetRoutingKeyMiddleware implements MiddlewareInterface
{
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        # Check based on interface, class, stamp or something else
        if ($envelope->last(AmqpReceivedStamp::class))
            $envelope = $envelope->with(new HandlerArgumentsStamp([
                'routingKey' =>  $envelope->last(AmqpReceivedStamp::class)->getAmqpEnvelope()->getRoutingKey()
            ]));

        return $stack->next()->handle($envelope, $stack);
    }
}
