<?php

namespace App\Form;

use App\Entity\NombreVotantsSpecifique;
use App\Entity\Voeu;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VoeuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'required' => true,
                'label' => "Titre du voeu",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                "attr" => ["class" => "w-100"]
            ])
            ->add('contenu', CKEditorType::class, [
                'required' => true,
                'attr' => ['class' => 'ckeditor'],
                'label' => "Contenu du voeu",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])->add('vote', VoteType::class, [
                'label' => false,
                'categorie' => NombreVotantsSpecifique::CATEGORIE_VOEUX
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Voeu::class,
        ]);
    }
}
