<?php

namespace App\Form;

use App\Entity\DocumentRessource;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire de saisie d'un document ressource de ROADS (animation)
 * @author  Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DocumentRessourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class, [
                'required' => true,
                'label' => "Titre",
                'attr' => ["class" => "w-100"]
            ])
            ->add('typeDocument', ChoiceType::class, [
                'choices' => [
                    DocumentRessource::TYPE_DOCUMENT_FICHIER => DocumentRessource::TYPE_DOCUMENT_FICHIER,
                    DocumentRessource::TYPE_DOCUMENT_LIEN => DocumentRessource::TYPE_DOCUMENT_LIEN,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => false,
            ])
            ->add('lien', UrlType::class, [
                'required' => true,
                'label' => false,
                "help" => "Veuillez saisir l'URL du lien",
                'attr' => ["class" => "w-100"]
            ])
            ->add('document', DocumentFileType::class, [
                'required' => true,
                'label' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DocumentRessource::class,
        ]);
    }
}
