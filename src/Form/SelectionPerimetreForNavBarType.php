<?php

namespace App\Form;

use App\Entity\Personne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SelectionPerimetreForNavBarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        /** @var Personne $personne */
        $personne = $options['personne'];
        $perimetreActif = $options['perimetreActif'];
        $builder
            ->add('perimetre', ChoiceType::class, [
                'mapped' => false,
                'expanded' => false,
                'multiple' => false,
                'label' => false,
                'choices' => $personne->getPerimetresDisponibles(),
                'data' => $perimetreActif,
                'attr' => [
                    'class' => 'border-secondary bg-secondary text-light rounded-pill',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
            'personne' => null,
            'perimetreActif' => null,
            'doneRedirect' => null
        ]);
    }
}
