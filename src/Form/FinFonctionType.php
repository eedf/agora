<?php

namespace App\Form;

use App\Entity\AttributionFonctionElective;
use App\Entity\Personne;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FinFonctionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('personne', AdherentOuAncienAdherentAutocompleteField::class, [
                'label' => "Personne",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ]
            ])
            ->add('mandatElectif', EntityType::class, [
                'label' => "Mandat concerné",
                'choice_label' => function (attributionFonctionElective $attributionFonctionElective): string {
                    return $attributionFonctionElective->getStatus() . " (" . $attributionFonctionElective->getEquipe()->getStructure()->getNom() . ")";
                },
                'placeholder' => "Sélectionnez d'abord la personne",
                'label' => 'Sélectionnez le mandat à terminer',
                'class' => AttributionFonctionElective::class,
                'choices' => [],
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Arrêter ce mandat'
            ]);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event): void {

                // this would be your entity
                $data = $event->getData();
                $personne = null;
                $attributionFonctionElective = null;
                if (array_key_exists('personne', $data)) {
                    $personne = $data->get('personne');
                }
                if (array_key_exists('mandatElectif', $data)) {
                    $attributionFonctionElective = $data->get('mandatElectif');
                }


                $this->formModifierMandatElectif($event->getForm(), $personne);
                $this->formModifierDateFinMandat($event->getForm(), $attributionFonctionElective);
            }
        );

        $builder->get('personne')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event): void {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $personne = $event->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback function!
                $this->formModifierMandatElectif($event->getForm()->getParent(), $personne);
            }
        );


        // by default, action does not appear in the <form> tag
        // you can set this value by passing the controller route
        $builder->setAction($options['action']);
    }

    private function formModifierMandatElectif(FormInterface $form, Personne $personne = null): void
    {
        $attributionsDisponibles = null === $personne ? [] : $personne->getAttributionsFonctionElectiveActives();
        $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
            'mandatElectif',
            EntityType::class,
            null,
            [

                'label' => "Mandat concerné",
                'choice_label' => function (attributionFonctionElective $attributionFonctionElective): string {
                    return $attributionFonctionElective->getStatus() . " (" . $attributionFonctionElective->getEquipe()->getStructure()->getNom() . ")";
                },
                'placeholder' => null === $personne ? "Sélectionnez d'abord la personne" : "Sélectionnez le mandat",
                'label' => 'Sélectionnez le mandat à terminer',
                'auto_initialize' => false,
                'class' => AttributionFonctionElective::class,
                'choices' => $attributionsDisponibles,
                'expanded' => false,
                'multiple' => false,
            ]
        );
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event): void {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $form = $event->getForm();
                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback function!
                $this->formModifierDateFinMandat($form->getParent(), $form->getData());
            }
        );
        $form->add($builder->getForm());
    }
    private function formModifierDateFinMandat(FormInterface $form, AttributionFonctionElective $attributionFonctionElective = null): void
    {
        $dateDebut = null === $attributionFonctionElective ? null : $attributionFonctionElective->getDateDebut();
        /** @var DateTime  $dateFin */
        $dateFin = null === $attributionFonctionElective ? null : $attributionFonctionElective->getDateFin();
        $form->add('dateFinMandat', DateType::class, [
            'label' => "Date de fin de mandat",
            'widget' => 'single_text',
            'html5' => false,
            'format' => 'dd-MM-yyyy',
            'attr' => [
                'class' => 'form-control datepicker col',
                "value" => null === $attributionFonctionElective ? null : $dateFin->format('d-m-Y'),
                'data-date-start-date' => null === $attributionFonctionElective ? null : $dateDebut->format('d-m-Y'),
                'data-date-end-date' => null === $attributionFonctionElective ? null : $dateFin->format('d-m-Y'),
            ],
        ]);
    }
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
