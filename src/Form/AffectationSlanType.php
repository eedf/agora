<?php

namespace App\Form;

use App\Entity\Structure;
use App\Repository\StructureRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AffectationSlanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Structure $structureSlan */
        $builder

            ->add('structureRegionaleAffiliee', EntityType::class, [
                'placeholder' => 'Non défini',
                'required' => false,
                'choice_label' => 'nom',
                'class' => Structure::class,
                'query_builder' => function (EntityRepository $er) {
                    /** @var StructureRepository $er */
                    $er->findAllStructuresRegionalesActives();
                },
                'placeholder' => 'Sélectionner la structure',
                'expanded' => false,
                'multiple' => false
            ]);
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /** @var Structure $structureSlan */
            $structureSlan = $event->getData();
            $form = $event->getForm();
            if ($structureSlan) {
                $form->add('structureRegionaleAffiliee', EntityType::class, [
                    'label' => $structureSlan->getNom(),
                    'choice_label' => 'nom',
                    'data' => $structureSlan->getStructureRegionaleAffiliee() ? $structureSlan->getStructureRegionaleAffiliee() : null,
                    'class' => Structure::class,
                    'query_builder' => function (EntityRepository $er) {
                        /** @var StructureRepository $er */
                        return $er->createQueryBuilder('s')
                            ->innerJoin("s.structureParent", "structureParent")
                            ->where("structureParent.echelon = :echelonNational")
                            ->andWhere("s.type = :type")
                            ->andWhere('s.statut in (:statutsActifs)')
                            ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])
                            ->setParameter('echelonNational', Structure::ECHELON_STRUCTURE_NATIONAL)
                            ->setParameter('type', Structure::TYPE_STRUCTURE_REGION)
                            ->orderBy('s.nom', 'ASC');
                    },
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Structure::class,
        ]);
    }
}
