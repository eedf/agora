<?php

namespace App\Form;

use App\Entity\VoteInstance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VoteInstanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('vote', VoteType::class, [
                'label' => "Inconnu",
                'required' => false
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $voteInstance = $event->getData();
            $form = $event->getForm();
            if ($voteInstance) {
                $form->add('vote', VoteType::class, [
                    'label' => $voteInstance->getType(),
                    'categorie' => str_replace(["'", " "], "", $voteInstance->getType()),
                    'label_attr' => [
                        'class' => 'fw-bold text-primary text-center',
                    ],
                    'required' => false
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => VoteInstance::class,
        ]);
    }
}
