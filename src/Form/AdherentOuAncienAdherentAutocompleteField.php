<?php

namespace App\Form;

use App\Entity\Adherent;
use App\Entity\Personne;
use App\Repository\PersonneRepository;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Autocomplete\Form\AsEntityAutocompleteField;
use Symfony\UX\Autocomplete\Form\ParentEntityAutocompleteType;

#[AsEntityAutocompleteField]
class AdherentOuAncienAdherentAutocompleteField extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => Personne::class,
            'placeholder' => 'Sélectionner...',
            'choice_label' => function (Personne $personne): string {
                return $personne->getAffichagePourRechercheAuto();
            },
            'filter_query' => function (QueryBuilder $queryBuilder, string $query, EntityRepository $repository) {
                $queryBuilder->leftJoin("personne.adherent", "adh");
                $queryBuilder
                    ->andWhere('(
                        (adh.id IS NOT NULL)
                    )');


                if (!$query) {
                    return;
                }
                $query = trim($query);
                $pos_space = strpos($query, " ");


                //Cas de plusieurs mots: nom prenom ou prenom nom
                if ($pos_space) {
                    $mot1 = substr($query, 0, $pos_space);
                    $mot2 = substr($query, $pos_space + 1);
                    $queryBuilder
                        ->andWhere('(personne.nom LIKE :mot1 AND personne.prenom LIKE :mot2)
                    OR (personne.nom LIKE :mot2 AND personne.prenom LIKE :mot1)')
                        ->setParameter('mot1', '%' . $mot1 . '%')
                        ->setParameter('mot2', '%' . $mot2 . '%');
                    //Cas d'un numéro d'adhérent ou de salarié commençant par 0
                } else if (str_starts_with($query, "0")) {
                    // On supprime les 0 en début de chaine
                    while (str_starts_with($query, "0")) {
                        $query = substr($query, 1);
                    }
                    $queryBuilder->andWhere('adh.numeroAdherent LIKE :str')
                        ->setParameter('str', $query . '%');
                } else {
                    $queryBuilder
                        ->andWhere('(personne.nom LIKE :str)
                         OR (personne.prenom LIKE :str)
                         OR (adh.numeroAdherent LIKE :str)')
                        ->setParameter('str', '%' . $query . '%');
                }
            },
            'query_builder' => function (PersonneRepository $personneRepository) {
                return $personneRepository->createQueryBuilder('personne');
            },
            'no_results_found_text'  => "Aucun résultat",
            'no_more_results_text' => "Fin de la recherche"

            //'security' => 'ROLE_SOMETHING',
        ]);
    }

    public function getParent(): string
    {
        return ParentEntityAutocompleteType::class;
    }
}
