<?php

namespace App\Form;

use App\Entity\DocumentFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Form\Type\VichFileType;

class DocumentFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('documentFile', VichFileType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => true,
                'download_label' => new PropertyPath('documentName'),
                'allow_delete' => $options['allow_delete'],
                'delete_label' => 'Supprimer',
                'label' => false,
                'help' => "Taille limite des fichiers acceptés : 5 Mo.</br> Formats acceptés : .ods, .xls, .xlsx, .odt, .doc, .docx, .odp, .ppt, .pptx, .pdf, .zip, .rar, .jpg, .png.",
                'help_html' => true,
                'constraints' =>
                [
                    new File(
                        [
                            'maxSize' => "5M",
                            'mimeTypes' => [
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                "application/vnd.ms-excel",
                                "application/vnd.ms-excel.sheet.macroEnabled.12",
                                "application/vnd.oasis.opendocument.spreadsheet",
                                "application/vnd.oasis.opendocument.text",
                                "application/msword",
                                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                "application/vnd.oasis.opendocument.presentation",
                                "application/pdf",
                                "application/vnd.ms-powerpoint",
                                "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                                "application/x-rar-compressed",
                                "application/zip",
                                "image/jpeg",
                                "image/png"
                            ],
                            'mimeTypesMessage' => "Le type du fichier est invalide (\"{{ type }}\").  Les types autorisés sont : .xlsx / .xls / .xlsm / .ods / .odt / .doc / .docx / .odp / .pdf / .ppt / .pptx / .rar / .zip."
                        ]
                    ),
                ]
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DocumentFile::class,
            'required' => false,
            'allow_delete' => true,

        ]);
    }
}
