<?php

namespace App\Form;

use App\Entity\CompteRenduInstance;
use App\Entity\Structure;
use App\Security\Voter\StructureVoter;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/** @deprecated Modifications des statuts AG 2024 */
class CompteRenduAPLPreReformeType extends AbstractType
{
    public function __construct(private AuthorizationCheckerInterface $authorizationChecker) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            //Informations générales
            ->add('dateInstance', DateType::class, [
                'label' => "Date de l'APL",
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control datepicker col',
                    'placeholder' => "jj-mm-aaaa"
                ],
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center col',
                ],
                'invalid_message' => "La date doit être expirmée sous la forme jj-mm-aaaa"
            ])
            ->add('dateFinInstance', DateType::class, [
                'label' => "Date de fin",
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control datepicker col',
                    'placeholder' => "jj-mm-aaaa"
                ],
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center col',
                ],
                'invalid_message' => "La date doit être expirmée sous la forme jj-mm-aaaa"
            ])
            ->add('plusieursJourInstance', CheckboxType::class, [
                'mapped' => false,
                'label' => "A eu lieu sur plusieurs jours",
                'required' => false
            ])
            ->add('nombreVotantsTotal', IntegerType::class, [
                'label' => "Nombre de votant·e·s",
                'attr' => [
                    'min' => 0
                ],
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ]
            ])
            ->add('nombreVotesChangeant', CheckboxType::class, [
                'mapped' => false,
                'label' => "Ce nombre change selon les votes",
                'required' => false
            ])
            //Representant(s) échelon supérieur
            ->add('representantsEchelonSuperieur', CollectionType::class, [
                'entry_type' => AdherentOuSalarieAutocompleteField::class,
                'entry_options' => [
                    'label' => false,
                    'error_bubbling' => false
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'help' => null,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'error_bubbling' => false
            ])
            ->add('representantEchelonSuperieurAbsent', CheckboxType::class, [
                'mapped' => false,
                'label' => "Absent·e(s)",
                'required' => false,
            ])

            //Nombre Votants
            ->add('nombreVotantsSpecifiques', CollectionType::class, [
                'entry_type' => NombreVotantsSpecifiqueType::class,
                'entry_options' => ['label' => false],
                'allow_add' => false,
                'allow_delete' => false,
                'label' => false,
                'by_reference' => true,
            ])

            //Votes
            ->add('votesInstances', CollectionType::class, [
                // each entry in the array will be an "email" field
                'entry_type' => VoteInstanceType::class,
                'entry_options' => [
                    'error_bubbling' => false,
                ],
                'error_bubbling' => false,
            ])
            //Elections
            ->add('electionsNominatives', CollectionType::class, [
                // each entry in the array will be an "email" field
                'entry_type' => ElectionNominativeType::class,
                'entry_options' => [
                    'label' => false,
                    'error_bubbling' => false,
                    "mode" => $options["mode"]

                ],
                'error_bubbling' => false,
                'label' => false
            ])
            ->add('electionEquipe', ElectionEquipeType::class, [
                "label" => "Equipe de gestion et d'animation",
                "typeInstance" => CompteRenduInstance::TYPE_APL_SLA,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'error_bubbling' => false
            ])


            //Documents
            ->add('rapportActivites', DocumentFileType::class, [
                'required' => false,
                'label' => "Rapport d'activités",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('rapportFinancier', DocumentFileType::class, [
                'required' => false,
                'label' => "Rapport financier",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('calendrierPrevisionnel', DocumentFileType::class, [
                'required' => false,
                'label' => "Calendrier",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('planAction', DocumentFileType::class, [
                'required' => false,
                'label' => "Plan d'action local",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('budgetPrevisionnel', DocumentFileType::class, [
                'required' => false,
                'label' => "Budget prévisionnel",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('projetPedagogique', DocumentFileType::class, [
                'required' => false,
                'label' => "Projet pédagogique",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('rapportMoral', DocumentFileType::class, [
                'required' => false,
                'label' => "Rapport moral",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('listeEmargement', DocumentFileType::class, [
                'required' => false,
                'label' => "Liste d'émargement",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('fonctionnementStructure', DocumentFileType::class, [
                'required' => false,
                'label' => "Règles de fonctionnement",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('autresDocuments', CollectionType::class, [
                'entry_type' => AutreDocumentType::class,
                'entry_options' => [
                    'label' => false,
                    'error_bubbling' => false
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'help' => null,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'error_bubbling' => false,
                'by_reference' => false,
            ])

            ->add('commentaireTenueInstance', CKEditorType::class, [
                'required' => true,
                'label' => null,
            ])

            ->add('commentaireDestinationValidation', CKEditorType::class, [
                'required' => true,
                'label' => null,
            ])

            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer le compte-rendu'
            ]);

        if ($options["mode"] == StructureVoter::MODIFIER_APRES_VALIDATION) {
            $builder->add('autonomieStructure', ChoiceType::class, [
                'label' => "Autonomie de la structure",
                'help' => "Au regard des statuts de notre association, quel est le niveau d'autonomie de la SLA ?",
                'choices'  => [
                    "Autonome" => Structure::STATUT_STRUCTURE_AUTONOME,
                    "Rattachée" => Structure::STATUT_STRUCTURE_RATTACHEE,
                ],
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'expanded' => true,
                'multiple' => false,
            ]);
        }

        //On affiche "n'a pas eu lieu" uniquement si la personne à le droit de validation sur la structure
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var CompteRenduInstance $compteRenduInstance */
            $compteRenduInstance = $event->getData();
            $form = $event->getForm();
            if ($this->authorizationChecker->isGranted(StructureVoter::VALIDER, $compteRenduInstance->getStructure())) {

                //N'a pas eu lieu
                $form->add('naPasEuLieu', CheckboxType::class, [
                    "label" => "Aucune APL n'a eu lieu cette année pour cette structure",
                    'required' => false
                ]);
            }
        });

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var CompteRenduInstance $compteRenduInstance */
            $compteRenduInstance = $event->getData();
            $form = $event->getForm();
            if ($compteRenduInstance) {
                $form->add('representantEchelonSuperieurAbsent', CheckboxType::class, [
                    'mapped' => false,
                    'label' => "Absent·e(s)",
                    'required' => false,
                    'data' => ($compteRenduInstance->getRepresentantsEchelonSuperieur()->count() == 0) && ($compteRenduInstance->getDateInstance() != null),
                    'constraints' => [
                        new Callback([
                            'callback' => function ($value, ExecutionContextInterface $context) use ($compteRenduInstance) {
                                if (
                                    !$value
                                    && !$compteRenduInstance->isNaPasEuLieu()
                                    && ($compteRenduInstance->getRepresentantsEchelonSuperieur()->count() == 0)
                                ) {
                                    $context->buildViolation("Une personne au minimum doit être identifiée, ou la case doit être cochée.")
                                        ->atPath('representantEchelonSuperieurAbsent')
                                        ->addViolation();
                                };
                            }
                        ])
                    ],
                    'error_bubbling' => false
                ]);
            }
        });
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var CompteRenduInstance $compteRenduInstance */
            $compteRenduInstance = $event->getData();
            $form = $event->getForm();
            if ($compteRenduInstance) {
                $form->add('plusieursJourInstance', CheckboxType::class, [
                    'mapped' => false,
                    'label' => "A eu lieu sur plusieurs jours",
                    'required' => false,
                    //Par défaut décoché pour les APL
                    'data' => ($compteRenduInstance->getDateFinInstance() != null) && ($compteRenduInstance->getDateInstance() != null)
                ]);
            }
        });
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var CompteRenduInstance $compteRenduInstance */
            $compteRenduInstance = $event->getData();
            $form = $event->getForm();
            if ($compteRenduInstance) {
                $form->add('nombreVotesChangeant', CheckboxType::class, [
                    'mapped' => false,
                    'label' => "Ce nombre change selon les votes",
                    'required' => false,
                    'data' => ($compteRenduInstance->getNombreVotantsTotal() == null) && ($compteRenduInstance->getDateInstance() != null)
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompteRenduInstance::class,
            'mode' => null
        ]);
    }
}
