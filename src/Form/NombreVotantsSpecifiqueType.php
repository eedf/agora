<?php

namespace App\Form;

use App\Entity\NombreVotantsSpecifique;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NombreVotantsSpecifiqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombreVotants', IntegerType::class, [
                'attr' => [
                    'min' => 0,
                ],
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'required' => true
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $nombreVotantsSpecifique = $event->getData();
            $form = $event->getForm();
            /** @var NombreVotantsSpecifique $nombreVotantsSpecifique */
            if ($nombreVotantsSpecifique) {
                $form->add('nombreVotants', IntegerType::class, [
                    'label' => $nombreVotantsSpecifique->getCategorie(),
                    'attr' => [
                        'min' => 0,
                        'data-categorie' => str_replace(["'", " "], "", $nombreVotantsSpecifique->getCategorie())
                    ],
                    'label_attr' => [
                        'class' => 'fw-bold text-primary text-center',
                    ],
                    'required' => true

                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NombreVotantsSpecifique::class,
        ]);
    }
}
