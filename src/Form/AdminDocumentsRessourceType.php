<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminDocumentsRessourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $documents = $options["documents"];

        $builder->add('documents', CollectionType::class, [
            'entry_type' => DocumentRessourceType::class,
            'prototype' => true,
            'allow_add' => false,
            'allow_delete' => false,
            'data' => $documents,
            'mapped' => false,
            'entry_options' => ['label' => false],
        ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            "documents" => null
        ]);
    }
}
