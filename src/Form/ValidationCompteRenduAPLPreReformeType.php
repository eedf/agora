<?php

namespace App\Form;

use App\Entity\CompteRenduInstance;
use App\Entity\ElectionNominative;
use App\Entity\Structure;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/** @deprecated Modifications des statuts AG 2024 */
class ValidationCompteRenduAPLPreReformeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('autonomieStructure', ChoiceType::class, [
                'label' => "Autonomie de la structure",
                'choices'  => [
                    "Autonome" => Structure::STATUT_STRUCTURE_AUTONOME,
                    "Rattachée" => Structure::STATUT_STRUCTURE_RATTACHEE,
                ],
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'expanded' => true,
                'multiple' => false,
                "help" => "<p>Pour être qualifiée d’autonome, une structure locale d’activité doit être capable :
                    <ul>
                        <li>D’organiser et de faire valider son assemblée plénière locale par l’échelon dont elle dépend ;</li>
                        <li>D’avoir un fonctionnement démocratique et conforme aux règles établies par les EEDF ;</li>
                        <li>D’avoir une équipe de gestion et d’animation d’au moins 3 personnes, dont un·e responsable et un·e trésorier·e ;</li>
                    </ul>
                    Au regard de cela, quel est le niveau d’autonomie de la SLA ?
                </p>",
                "help_html" => true
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Finaliser la validation du compte-rendu'
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var  CompteRenduInstance $compteRendu*/
            $compteRendu = $event->getData();
            $form = $event->getForm();
            /** @var  ElectionNominative $electionResla*/
            $electionResla = $compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_RESLA)->first();
            if (!$electionResla->isDejaElu() && $electionResla->getPersonne()) {
                $form->add('validationResponsable', ChoiceType::class, [
                    'label' => "Responsable de la Structure Locale d'activité",
                    'help' => "Voulez-vous nommer " . $electionResla->getPersonne()->getPrenom() . ' ' . $electionResla->getPersonne()->getNom() . " au poste de Responsable de la structure " . $compteRendu->getStructure()->getNom() . " ?",
                    'choices'  => [
                        'Nommer' => true,
                        'Ne pas nommer' => false,
                    ],
                    'label_attr' => [
                        'class' => 'fw-bold text-primary text-center',
                    ],
                    'expanded' => true,
                    'multiple' => false,
                    'mapped' => false
                ]);
            }
            /** @var  ElectionNominative $electionTsla*/
            $electionTsla = $compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TSLA)->first();
            if (!$electionTsla->isDejaElu() && $electionTsla->getPersonne()) {
                $form->add('validationTresorier', ChoiceType::class, [
                    'label' => "Trésorier·e de la Structure Locale d'activité",
                    'help' => "Voulez-vous nommer " . $electionTsla->getPersonne()->getPrenom() . ' ' . $electionTsla->getPersonne()->getNom() . " au poste de Trésorier·e de la structure " . $compteRendu->getStructure()->getNom() . " ?",
                    'choices'  => [
                        'Nommer' => true,
                        'Ne pas nommer' => false,
                    ],
                    'label_attr' => [
                        'class' => 'fw-bold text-primary text-center',
                    ],
                    'expanded' => true,
                    'multiple' => false,
                    'mapped' => false
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompteRenduInstance::class,
        ]);
    }
}
