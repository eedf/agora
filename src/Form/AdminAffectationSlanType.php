<?php

namespace App\Form;

use App\Entity\Structure;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminAffectationSlanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $structuresSlan = $options["structuresSlan"];

        $builder->add('slans', CollectionType::class, [
            'entry_type' => AffectationSlanType::class,
            'prototype' => true,
            'allow_add' => false,
            'allow_delete' => false,
            'data' => $structuresSlan,
            'mapped' => false,
            'entry_options' => ['label' => false],
        ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            "structuresSlan" => null
        ]);
    }
}
