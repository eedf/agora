<?php

namespace App\Form;

use App\Entity\Vote;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombreVoixFavorables', IntegerType::class, [
                'label' => "Pour",
                'attr' => [
                    'min' => 0,
                    'style' => 'width:100%',
                    'data-categorie' => $options['categorie'] ? $options['categorie'] : ""
                ],
                'label_attr' => [
                    'class' => 'fw-normal text-reset'
                ],
                'required' => false
            ])
            ->add('nombreVoixDefavorables', IntegerType::class, [
                'label' => "Contre",
                'attr' => [
                    'min' => 0,
                    'style' => 'width:100%'
                ],
                'label_attr' => [
                    'class' => 'fw-normal text-reset'
                ],
                'required' => false
            ])
            ->add('nombreVoixBlancs', IntegerType::class, [
                'label' => "Blancs",
                'attr' => [
                    'min' => 0,
                    'style' => 'width:100%'
                ],
                'label_attr' => [
                    'class' => 'fw-normal text-reset'
                ],
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Vote::class,
            'categorie' => null
        ]);
    }
}
