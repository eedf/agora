<?php

namespace App\Form;

use App\Entity\CompteRenduInstance;
use App\Entity\ElectionEquipe;
use App\Entity\NombreVotantsSpecifique;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ElectionEquipeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $typeInstance = $options["typeInstance"];
        $builder
            ->add('roles', CollectionType::class, [
                'entry_type' => RoleEquipeElueType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'by_reference' => false,
            ])
            ->add('vote', VoteType::class, [
                'label' => false,
                'categorie' => str_replace(["'", " "], "", $typeInstance == CompteRenduInstance::TYPE_CONGRES ? NombreVotantsSpecifique::CATEGORIE_ELECTION_ER : NombreVotantsSpecifique::CATEGORIE_ELECTION_EGA)
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ElectionEquipe::class,
            'typeInstance' => null
        ]);
    }
}
