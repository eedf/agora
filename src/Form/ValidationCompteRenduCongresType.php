<?php

namespace App\Form;

use App\Entity\CompteRenduInstance;
use App\Entity\ElectionNominative;
use App\Entity\Structure;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ValidationCompteRenduCongresType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('autonomieStructure', ChoiceType::class, [
                'label' => "Autonomie de la structure",
                'choices'  => [
                    "Autonome" => Structure::STATUT_STRUCTURE_AUTONOME,
                    "Rattachée" => Structure::STATUT_STRUCTURE_RATTACHEE,
                ],
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'expanded' => true,
                'multiple' => false,
                "help" => "<p>Pour être qualifiée d’autonome, une région doit être capable :
                    <ul>
                        <li>D’organiser et de faire valider son congres par l’échelon national ;</li>
                        <li>D’avoir un fonctionnement démocratique et conforme aux règles établies par les EEDF ;</li>
                        <li>D’avoir une équipe régionale d’au moins 3 personnes, dont au moins deux assument les quatre missions institutionnelles de l'échelon régional ;</li>
                    </ul>
                    Au regard de cela, quel est le niveau d’autonomie de la région ?
                </p>",
                "help_html" => true
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Finaliser la validation du compte-rendu'
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var  CompteRenduInstance $compteRendu*/
            $compteRendu = $event->getData();
            $form = $event->getForm();
            /** @var  ElectionNominative $electionRR*/
            $electionRR = $compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_ORGANISATION_REGION)->first();
            if (!$electionRR->isDejaElu() && $electionRR->getPersonne()) {
                $form->add('validationMissionOrganisation', ChoiceType::class, [
                    'label' => "Mission d'organisation",
                    'help' => "Voulez-vous nommer " . $electionRR->getPersonne()->getPrenom() . ' ' . $electionRR->getPersonne()->getNom() . " sur la mission d'organisation de la structure " . $compteRendu->getStructure()->getNom() . " ?",
                    'choices'  => [
                        'Nommer' => true,
                        'Ne pas nommer' => false,
                    ],
                    'label_attr' => [
                        'class' => 'fw-bold text-primary text-center',
                    ],
                    'expanded' => true,
                    'multiple' => false,
                    'mapped' => false
                ]);
            }
            /** @var  ElectionNominative $electionTresorerie*/
            $electionTresorerie = $compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TRESORERIE_REGION)->first();
            if (!$electionTresorerie->isDejaElu() && $electionTresorerie->getPersonne()) {
                $form->add('validationMissionTresorerie', ChoiceType::class, [
                    'label' => "Mission de trésorerie",
                    'help' => "Voulez-vous nommer " . $electionTresorerie->getPersonne()->getPrenom() . ' ' . $electionTresorerie->getPersonne()->getNom() . " sur la mission de trésorerie de la structure " . $compteRendu->getStructure()->getNom() . " ?",
                    'choices'  => [
                        'Nommer' => true,
                        'Ne pas nommer' => false,
                    ],
                    'label_attr' => [
                        'class' => 'fw-bold text-primary text-center',
                    ],
                    'expanded' => true,
                    'multiple' => false,
                    'mapped' => false
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompteRenduInstance::class,
        ]);
    }
}
