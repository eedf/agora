<?php

namespace App\Form;

use App\Entity\Personne;
use App\Repository\PersonneRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Autocomplete\Form\AsEntityAutocompleteField;
use Symfony\UX\Autocomplete\Form\ParentEntityAutocompleteType;

#[AsEntityAutocompleteField]
class PersonneAutocompleteField extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => Personne::class,
            'placeholder' => 'Sélectionner...',
            'choice_label' => function (Personne $personne): string {
                return $personne->getAffichagePourRechercheAuto();
            },
            'filter_query' => function (QueryBuilder $queryBuilder, string $query, EntityRepository $repository) {
                if (!$query) {
                    return;
                }
                $query = trim($query);
                $pos_space = strpos($query, " ");


                $queryBuilder->leftJoin("personne.adherent", "adh");
                $queryBuilder->leftJoin("personne.employe", "empl");

                //Cas de plusieurs mots: nom prenom ou prenom nom
                if ($pos_space) {
                    $mot1 = substr($query, 0, $pos_space);
                    $mot2 = substr($query, $pos_space + 1);
                    $queryBuilder
                        ->orWhere('personne.nom LIKE :mot1 AND personne.prenom LIKE :mot2')
                        ->orWhere('personne.nom LIKE :mot2 AND personne.prenom LIKE :mot1')
                        ->setParameter('mot1', '%' . $mot1 . '%')
                        ->setParameter('mot2', '%' . $mot2 . '%');
                    //Cas d'un numéro d'adhérent ou de salarié commençant par 0
                } else if (str_starts_with($query, "0")) {
                    // On supprime les 0 en début de chaine
                    while (str_starts_with($query, "0")) {
                        $query = substr($query, 1);
                    }
                    $queryBuilder->orWhere('adh.numeroAdherent LIKE :str')
                        ->setParameter('str', $query . '%');
                    $queryBuilder->orWhere('empl.numeroSalarie LIKE :str')
                        ->setParameter('str', $query . '%');
                } else {
                    $queryBuilder
                        ->orWhere('personne.nom LIKE :str')
                        ->orWhere('personne.prenom LIKE :str')
                        ->orWhere('adh.numeroAdherent LIKE :str')
                        ->orWhere('empl.numeroSalarie LIKE :str')
                        ->setParameter('str', '%' . $query . '%');
                }

                $queryBuilder
                    ->andWhere('(empl.id IS NOT NULL) OR (adh.id IS NOT NULL)'); // On filtre uniquement parmi les salariés ou adhérents

            },
            'query_builder' => function (PersonneRepository $personneRepository) {
                return $personneRepository->createQueryBuilder('personne');
            },
            'no_results_found_text'  => "Aucun résultat"

            //'security' => 'ROLE_SOMETHING',
        ]);
    }

    public function getParent(): string
    {
        return ParentEntityAutocompleteType::class;
    }
}
