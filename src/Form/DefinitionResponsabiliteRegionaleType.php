<?php

namespace App\Form;

use App\Entity\Droit;
use App\Entity\Structure;
use App\Repository\StructureRepository;
use App\Service\DroitsHelper;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DefinitionResponsabiliteRegionaleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('structure', EntityType::class, [
                'label' => "Nouvelle structure",
                'choice_label' => 'nom',
                'class' => Structure::class,
                'query_builder' => function (EntityRepository $er) {
                    /** @var StructureRepository $er */
                    return $er->createQueryBuilder('s')
                        ->andWhere('s.responsabiliteRegionale = false')
                        ->andWhere('s.echelon = :echelonLocal')
                        ->andWhere('s.statut in (:statutsActifs)')
                        ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])
                        ->setParameter("echelonLocal", Structure::ECHELON_STRUCTURE_LOCAL)
                        ->orderBy('s.nom', 'ASC');
                },
                'group_by' => function ($choice, $nom, $value) {
                    /** @var Structure $choice*/
                    if ($choice->estUneSLA()) {
                        return ("SLA");
                    } else if ($choice->estUneSLAN()) {
                        return ("SLAN");
                    } else if ($choice->estUneRegion()) {
                        return ("Région");
                    } else {
                        return ("Autre");
                    }
                },
                'expanded' => false,
                'multiple' => false,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center col',
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Ajouter'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
