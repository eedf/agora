<?php

namespace App\Form;

use App\Entity\Droit;
use App\Entity\Structure;
use App\Service\DroitsHelper;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class DelegationDroitNonAdminType extends AbstractType
{
    private $token;
    private $droitsHelper;

    public function __construct(TokenStorageInterface $token, DroitsHelper $droitsHelper)
    {
        $this->token = $token;
        $this->droitsHelper = $droitsHelper;
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $structuresDisponibles = $options['structuresDisponibles'];
        $builder
            ->add('duree', DateIntervalType::class, [
                'input'        => 'string',
                'with_years'  => true,
                'with_months' => false,
                'with_days'   => false,
                'with_hours'  => false,
                'years' => [
                    "1 an" => 1,
                    "2 ans" => 2
                ],
                'labels' => ['years' => 'Durée de délégation'],
                'mapped' => false,
                'row_attr' => [
                    'class' => 'fw-bold text-primary col',
                ],
                'label' => false,

            ])
            ->add('structure', EntityType::class, [
                'label' => "Structure concernée",
                'choice_label' => 'nom',
                'class' => Structure::class,
                'choices' => $structuresDisponibles,
                'expanded' => false,
                'multiple' => false,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center col',
                ],
                'placeholder' => 'Sélectionner la structure',
            ])
            ->add('personne', AdherentOuSalarieAutocompleteField::class, [
                'label' => "Personne à qui déléguer les droits",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Ajouter'
            ]);



        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event): void {
                $form = $event->getForm();

                // this would be your entity
                /** @var Droit $data */
                $data = $event->getData();


                $structure = $data->getStructure();
                $this->formModifier($event->getForm(), $structure);
            }
        );

        $builder->get('structure')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event): void {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $structure = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback function!
                $this->formModifier($event->getForm()->getParent(), $structure);
            }
        );

        // by default, action does not appear in the <form> tag
        // you can set this value by passing the controller route
        $builder->setAction($options['action']);
    }


    private function formModifier(FormInterface $form, Structure $structure = null): void
    {
        $droitsAffectables = null === $structure ? [] : $this->droitsHelper->getDroitsAffectablesPourStructure($this->token->getToken()->getUser(), $structure);
        $form->add('droit', ChoiceType::class, [
            'expanded' => false,
            'multiple' => false,
            'placeholder' => null === $structure ? 'Sélectionnez la structure' : 'Sélectionnez le droit',
            'label' => 'Sélectionnez le droit à attribuer',
            'label_attr' => [
                'class' => 'fw-bold text-primary text-center col',
            ],
            'choices' => $droitsAffectables,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Droit::class,
            'structuresDisponibles' => null
        ]);
    }
}
