<?php

namespace App\Form;

use App\Entity\Droit;
use App\Entity\Structure;
use App\Repository\StructureRepository;
use App\Service\DroitsHelper;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DroitManuelStructureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('duree', DateIntervalType::class, [
                'input'        => 'string',
                'with_years'  => true,
                'with_months' => false,
                'with_days'   => false,
                'with_hours'  => false,
                'years' => [
                    "1 an" => 1,
                    "2 ans" => 2
                ],
                'labels' => ['years' => 'Durée de délégation'],
                'mapped' => false,
                'row_attr' => [
                    'class' => 'fw-bold text-primary col',
                ],
                'label' => false,
                'widget' => "choice"

            ])
            ->add('droit', ChoiceType::class, [
                'expanded' => false,
                'multiple' => false,
                'label' => 'Sélectionnez le droit à attribuer',
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center col',
                ],
                'choices' => array_flip(DroitsHelper::ARRAY_DROITS)
            ])
            ->add('personne', AdherentOuSalarieAutocompleteField::class, [
                'label' => "Personne à nommer",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Ajouter'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Droit::class,
        ]);
    }
}
