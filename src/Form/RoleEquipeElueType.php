<?php

namespace App\Form;

use App\Entity\RoleEquipeElue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoleEquipeElueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fonctionSpecifique', TextType::class, [
                'label' => false,
                "help" => "facultatif",
                'help_attr' => ['class' => "d-inline"],
                'required' => false
            ])
            ->add('personne', AdherentAutocompleteField::class, [
                'label' => false,

            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RoleEquipeElue::class,
        ]);
    }
}
