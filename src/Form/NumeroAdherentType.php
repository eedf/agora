<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThan;

/**
 * Formulaire de saisie du numéro d'adhérent·e
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class NumeroAdherentType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'label' => 'Saisissez un numéro d\'adhérent·e',
            'help' => 'le numéro d\'adhérent.e est composé de 6 chiffres au maximum',
            'attr' => [
                'min' => 1,
                'max' => 999999
            ],
            'constraints' => [
                new GreaterThan([
                    'message' => 'Le numéro d\'adhérent·e doit être compris entre 000000 et 999999',
                    'value' => 0
                ]),
                new LessThan([
                    'message' => 'Le numéro d\'adhérent·e doit être compris entre 000000 et 999999',
                    'value' => 1000000
                ]),
            ]
        ]);
    }

    public function getParent(): string
    {
        return NumberType::class;
    }
}
