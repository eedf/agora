<?php

namespace App\Form;

use App\Entity\Personne;
use App\Entity\Structure;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SelectionStructurePourInformationStructureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Structure $structureRegion */
        $structureRegion = $options['structureRegion'];
        $builder
            ->add('structure', EntityType::class, [
                'class' => Structure::class,
                'mapped' => false,
                'expanded' => false,
                'multiple' => false,
                'label' => null,
                'attr' => [
                    'class' => 'border-secondary',
                ],
                'choices' => array_merge([$structureRegion], $structureRegion->getStructuresFilles()->toArray(), $structureRegion->getStructureSlanAffiliees()->toArray())

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Continuer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
            'personne' => null
        ]);
    }
}
