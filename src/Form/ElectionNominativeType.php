<?php

namespace App\Form;

use App\Entity\ElectionNominative;
use App\Entity\SaisonDemocratique;
use App\Security\Voter\StructureVoter;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ElectionNominativeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pasDElection', CheckboxType::class, [
                'label' => "La mission est vacante",
                'required' => false
            ])

            ->add('personne', AdherentAutocompleteField::class, [
                'label' => false,
                'required' => false

            ])
            ->add('vote', VoteNominatifType::class, [
                'label' => false,
            ]);

        if (($options["mode"] == StructureVoter::MODIFIER_APRES_VALIDATION)) {
            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                /** @var  ElectionNominative $electionNominative*/
                $electionNominative = $event->getData();
                $form = $event->getForm();
                if ($electionNominative && in_array($electionNominative->getPoste(), ElectionNominative::LISTE_POSTES_ELECTIFS_PLUS_18_ANS)) {
                    $form->add('status', ChoiceType::class, [
                        'label' => false,
                        'help' => "Voulez-vous nommer cette personne ?",
                        'choices'  => [
                            'Nommer' => ElectionNominative::STATUS_VALIDE,
                            'Ne pas nommer' =>  ElectionNominative::STATUS_REFUSE,
                        ],
                        'label_attr' => [
                            'class' => 'fw-bold text-primary text-center radio-inline',
                        ],
                        'expanded' => true,
                        'multiple' => false,
                    ]);
                }
            });
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var  ElectionNominative $electionNominative*/
            $electionNominative = $event->getData();
            $form = $event->getForm();
            if ($electionNominative) {
                $form->add('vote', VoteNominatifType::class, [
                    'label' => $electionNominative->getPoste(),
                    'label_attr' => [
                        'class' => 'fw-bold text-primary text-center',
                    ]
                ]);
            }

            //Gestion de la case "Déjà élu·e l'an dernier"
            if (in_array($electionNominative->getPoste(), ElectionNominative::LISTE_POSTES_ELECTIFS_PLUSIEURS_ANNEES)) {
                $saisonPrecedente = $electionNominative->getCompteRendu()->getSaison()->getSaisonPrecedente();

                //Si la Saison courante est la première depuis le changement des status de l'AG 2024, on n'affiche pas les cases déjà élu
                if (
                    $saisonPrecedente
                    && ($saisonPrecedente->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024))
                    && ($saisonPrecedente->getDateFin() > new DateTime(SaisonDemocratique::DATE_AG_2024))
                ) {
                } else {
                    $label = "Déjà élu·e l'an dernier";

                    if ($saisonPrecedente && $electionNominative->getCompteRendu()->getStructure()->getCompteRenduInstancePourSaison($saisonPrecedente)) {

                        /** @var ElectionNominative $electionNominativeAnneePrecedente */

                        $electionNominativeAnneePrecedente = $electionNominative->getCompteRendu()->getStructure()->getCompteRenduInstancePourSaison($saisonPrecedente)->getElectionsNominativesPourPoste($electionNominative->getPoste())->first();
                        if (
                            $electionNominativeAnneePrecedente
                            && ($electionNominativeAnneePrecedente->getStatus() == ElectionNominative::STATUS_VALIDE)
                            && $electionNominativeAnneePrecedente->getPersonne()
                        ) {
                            $label = "Déjà élu·e en " . $saisonPrecedente->getNom() . " (" . $electionNominativeAnneePrecedente->getPersonne()->getPrenom() . " " . $electionNominativeAnneePrecedente->getPersonne()->getNom() . ")";
                        } else {
                            $label = "Déjà élu·e en " . $saisonPrecedente->getNom();
                        }
                    }
                    $form->add('dejaElu', CheckboxType::class, [
                        'label' => $label,
                        'required' => false
                    ]);
                }
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ElectionNominative::class,
            "mode" => null
        ]);
    }
}
