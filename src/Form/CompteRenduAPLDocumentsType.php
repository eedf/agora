<?php

namespace App\Form;

use App\Entity\CompteRenduInstance;
use App\Entity\Structure;
use App\Security\Voter\StructureVoter;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompteRenduAPLDocumentsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            //Documents
            ->add('rapportActivites', DocumentFileType::class, [
                'required' => false,
                'label' => "Rapport d'activités",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('rapportFinancier', DocumentFileType::class, [
                'required' => false,
                'label' => "Rapport financier",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('calendrierPrevisionnel', DocumentFileType::class, [
                'required' => false,
                'label' => "Calendrier",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('planAction', DocumentFileType::class, [
                'required' => false,
                'label' => "Plan d'action local",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('budgetPrevisionnel', DocumentFileType::class, [
                'required' => false,
                'label' => "Budget prévisionnel",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('projetPedagogique', DocumentFileType::class, [
                'required' => false,
                'label' => "Projet pédagogique",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('rapportMoral', DocumentFileType::class, [
                'required' => false,
                'label' => "Rapport moral",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('listeEmargement', DocumentFileType::class, [
                'required' => false,
                'label' => "Liste d'émargement",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('fonctionnementStructure', DocumentFileType::class, [
                'required' => false,
                'label' => "Règles de fonctionnement",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
            ])
            ->add('autresDocuments', CollectionType::class, [
                'entry_type' => AutreDocumentType::class,
                'entry_options' => [
                    'label' => false,
                    'error_bubbling' => false
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'help' => null,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'error_bubbling' => false,
                'by_reference' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer les documents'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompteRenduInstance::class,
            'mode' => null
        ]);
    }
}
