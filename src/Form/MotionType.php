<?php

namespace App\Form;

use App\Entity\Motion;
use App\Entity\NombreVotantsSpecifique;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MotionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'required' => true,
                'label' => "Titre de la motion",
                'label_attr' => [
                    'class' => 'fw-bold text-primary',
                ],
                "attr" => ["class" => "w-100"]
            ])
            ->add('contenu', CKEditorType::class, [
                'required' => true,
                'label' => "Contenu de la motion",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'attr' => ['class' => 'ckeditor']
            ])
            ->add('vote', VoteType::class, [
                'label' => false,
                'categorie' => NombreVotantsSpecifique::CATEGORIE_MOTIONS
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Motion::class,
        ]);
    }
}
