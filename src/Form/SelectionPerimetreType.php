<?php

namespace App\Form;

use App\Entity\Personne;
use App\Entity\Structure;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SelectionPerimetreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Personne $personne */
        $personne = $options['personne'];
        $builder
            ->add('perimetre', ChoiceType::class, [
                'mapped' => false,
                'expanded' => false,
                'multiple' => false,
                'label' => 'Sélectionnez le périmètre',
                'choices' => $personne->getPerimetresDisponibles(),
                "attr" => [
                    "class" => "text-center border-secondary",
                ],

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Continuer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => null,
            'personne' => null
        ]);
    }
}
