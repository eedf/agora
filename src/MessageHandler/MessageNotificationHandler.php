<?php

namespace App\MessageHandler;

use App\Message\MessageNotification;
use App\Service\NotificationHelper;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class MessageNotificationHandler
{

    public function __construct(
        private NotificationHelper $notificationHelper
    ) {
    }

    public function __invoke(MessageNotification $message)
    {
        $this->notificationHelper->envoiNotifications($message);
    }
}
