<?php

namespace App\MessageHandler;

use App\Message\MessageSynchroTempsReel;
use App\Service\JeitoSyncronisationHelper;
use Exception;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class MessageSynchroTempsReelHandler
{
    public function __construct(
        private JeitoSyncronisationHelper $jeitoSyncronisationHelper,
    ) {}
    public function __invoke(MessageSynchroTempsReel $message, string $routingKey)
    {
        try {
            switch ($routingKey) {
                case MessageSynchroTempsReel::ROUTING_KEY_ADHERENT:
                    switch ($message->getOperation()) {
                        case MessageSynchroTempsReel::OPERATION_CREATE_UPDATE:
                            $this->jeitoSyncronisationHelper->createOrUpdateAdherentUnitaire($message->getAttributes());
                            break;
                        case MessageSynchroTempsReel::OPERATION_DELETE:
                            $this->jeitoSyncronisationHelper->suppressionAdherentUnitaire($message->getUuid());
                            break;
                    }
                    break;
                case MessageSynchroTempsReel::ROUTING_KEY_ADHESION:
                    switch ($message->getOperation()) {
                        case MessageSynchroTempsReel::OPERATION_CREATE_UPDATE:
                            $this->jeitoSyncronisationHelper->createOrUpdateAdhesionUnitaire($message->getAttributes());
                            break;
                        case MessageSynchroTempsReel::OPERATION_DELETE:
                            $this->jeitoSyncronisationHelper->suppressionAdhesionUnitaire($message->getUuid());
                            break;
                    }
                    break;
                case MessageSynchroTempsReel::ROUTING_KEY_STRUCTURE:
                    switch ($message->getOperation()) {
                        case MessageSynchroTempsReel::OPERATION_CREATE_UPDATE:
                            $this->jeitoSyncronisationHelper->createOrUpdateStructureUnitaire($message->getAttributes());
                            break;
                        case MessageSynchroTempsReel::OPERATION_DELETE:
                            $this->jeitoSyncronisationHelper->suppressionStructureUnitaire($message->getUuid());
                            break;
                    }
                    break;
                case MessageSynchroTempsReel::ROUTING_KEY_EQUIPE:
                    switch ($message->getOperation()) {
                        case MessageSynchroTempsReel::OPERATION_CREATE_UPDATE:
                            $this->jeitoSyncronisationHelper->createOrUpdateEquipeUnitaire($message->getAttributes());
                            break;
                        case MessageSynchroTempsReel::OPERATION_DELETE:
                            $this->jeitoSyncronisationHelper->suppressionEquipeUnitaire($message->getUuid());
                            break;
                    }
                    break;
                case MessageSynchroTempsReel::ROUTING_KEY_PERSONNE:
                    switch ($message->getOperation()) {
                        case MessageSynchroTempsReel::OPERATION_CREATE_UPDATE:
                            $this->jeitoSyncronisationHelper->createOrUpdatePersonneUnitaire($message->getAttributes());
                            break;
                        case MessageSynchroTempsReel::OPERATION_DELETE:
                            $this->jeitoSyncronisationHelper->suppressionPersonneUnitaire($message->getUuid());
                            break;
                    }
                    break;
                case MessageSynchroTempsReel::ROUTING_KEY_SALARIE:
                    switch ($message->getOperation()) {
                        case MessageSynchroTempsReel::OPERATION_CREATE_UPDATE:
                            $this->jeitoSyncronisationHelper->createOrUpdateEmployeUnitaire($message->getAttributes());
                            break;
                        case MessageSynchroTempsReel::OPERATION_DELETE:
                            $this->jeitoSyncronisationHelper->suppressionEmployeUnitaire($message->getUuid());
                            break;
                    }
                    break;
                case MessageSynchroTempsReel::ROUTING_KEY_CONTRAT_TRAVAIL:
                    switch ($message->getOperation()) {
                        case MessageSynchroTempsReel::OPERATION_CREATE_UPDATE:
                            $this->jeitoSyncronisationHelper->createOrUpdateContratTravailUnitaire($message->getAttributes());
                            break;
                        case MessageSynchroTempsReel::OPERATION_DELETE:
                            $this->jeitoSyncronisationHelper->suppressionContratTravailUnitaire($message->getUuid());
                            break;
                    }
                    break;
                case MessageSynchroTempsReel::ROUTING_KEY_FONCTION:
                    switch ($message->getOperation()) {
                        case MessageSynchroTempsReel::OPERATION_CREATE_UPDATE:
                            $this->jeitoSyncronisationHelper->createOrUpdateFonctionUnitaire($message->getAttributes());
                            break;
                        case MessageSynchroTempsReel::OPERATION_DELETE:
                            $this->jeitoSyncronisationHelper->suppressionFonctionUnitaire($message->getUuid());
                            break;
                    }
                    break;
            }
        } catch (Exception $e) {
            $this->jeitoSyncronisationHelper->envoiMailErreurSupport("[" . $routingKey . "][" . $message->getOperation() . "]" . $e->getMessage(), $message);
        }
        return 0;
    }
}
