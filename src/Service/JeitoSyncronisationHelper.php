<?php

namespace App\Service;

use App\Entity\Adherent;
use App\Entity\Adhesion;
use App\Entity\AdressePostale;
use App\Entity\CompteRenduInstance;
use App\Entity\ContratTravail;
use App\Entity\Coordonnees;
use App\Entity\Droit;
use App\Entity\Employe;
use App\Entity\Equipe;
use App\Entity\Fonction;
use App\Entity\Personne;
use App\Entity\SaisonDemocratique;
use App\Entity\Structure;
use App\Message\MessageSynchroTempsReel;
use App\Repository\AdherentRepository;
use App\Repository\AdhesionRepository;
use App\Repository\ContratTravailRepository;
use App\Repository\EmployeRepository;
use App\Repository\EquipeRepository;
use App\Repository\FonctionRepository;
use App\Repository\PersonneRepository;
use App\Repository\SaisonDemocratiqueRepository;
use App\Repository\StructureRepository;
use App\Security\Voter\StructureVoter;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use libphonenumber\PhoneNumber;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Workflow\WorkflowInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Helper pour la gestion des droits dans Agora
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class JeitoSyncronisationHelper
{

    private $saisonActive;
    public function __construct(
        private HttpClientInterface $client,
        private String $jeitoApiToken,
        private String $jeitoApiUrl,
        private StructureRepository $structureRepository,
        private EquipeRepository $equipeRepository,
        private PersonneRepository $personneRepository,
        private AdherentRepository $adherentRepository,
        private EmployeRepository $employeRepository,
        private ContratTravailRepository $contratTravailRepository,
        private FonctionRepository $fonctionRepository,
        private AdhesionRepository $adhesionRepository,
        private SaisonDemocratiqueRepository $saisonDemocratiqueRepository,
        private EntityManagerInterface $entityManager,
        private ValidatorInterface $validatorInterface,
        private NotificationHelper $notificationHelper,
        private DroitsHelper $droitsHelper,
        private MailerInterface $mailer,
        private String $adresseMailSupport,
    ) {
        $this->saisonActive = $this->saisonDemocratiqueRepository->findOneBy(['actif' => true]);
    }


    public function createOrUpdateStructureUnitaire(array $donneesStructure)
    {
        $dateCourante = new DateTime();

        $uuid = $donneesStructure['uuid'];
        $structure = $this->structureRepository->findOneBy(['uuid' => $uuid]);

        $parent_uuid = $donneesStructure['parent_uuid'];
        $structureParent = $this->structureRepository->findOneBy(['uuid' => $parent_uuid]);

        $this->createOrUpdateStructure($structure, $structureParent, $donneesStructure, $dateCourante);
        $this->entityManager->flush();
    }


    public function createOrUpdateStructure(?Structure $structure, ?Structure $structureParent, array $donneesStructure, DateTime $dateCourante): Structure
    {
        $structureCree = false;

        $nom = $donneesStructure['name'];
        $type = $donneesStructure['type'];
        if (!$structure) {
            $uuid = $donneesStructure['uuid'];
            //On crée une Structure
            $structure = new Structure();
            $structure->setUuid(Uuid::fromString($uuid));
            $structureCree = true;
        }

        /** @var Structure $structure */
        $structure->setStatut($donneesStructure['status']);
        //On supprime le compte rendu de la saison active si le statut de la structure est fermé et s'il existe un compte rendu
        if (($structure->getStatut() == Structure::STATUT_STRUCTURE_FERMEE) && ($this->saisonActive !== null)) {
            $compteRenduInstanceSaisonActive = $structure->getCompteRenduInstancePourSaison($this->saisonActive);
            if ($compteRenduInstanceSaisonActive) {
                $structure->removeCompteRendusInstance($compteRenduInstanceSaisonActive);
            }
        }
        $structure->setNom($nom);
        $structure->setType($type);
        $structure->setEchelon(Structure::MAP_ECHELON_BY_TYPE_STRUCTURE[$type]);
        $structure->setDateModification($dateCourante);

        //Ajout de la structure parent

        if ($structureParent) {
            $structure->setStructureParent($structureParent);
        } else {
            $parent_uuid = $donneesStructure['parent_uuid'];
            if ($parent_uuid) { // On envoie une erreur uniquement si le parentId est défini
                throw new Exception("La structure parent \"" . $parent_uuid . "\" de la structure " . $nom . " (" . $uuid . ") n'existe pas, la structure sera ajoutée sans parent");
            }
            $structure->setStructureParent(null);
        }
        //S'il s'agit d'une structure nouvellement créee, on crée un Compte rendu 
        if ($structureCree && ($this->saisonActive !== null)) {
            //On ne crée le compte rendu que si le statut n'est pas fermé
            if (
                $structure->getStatut() != Structure::STATUT_STRUCTURE_FERMEE
                && ($structure->estUneSLA() || $structure->estUneSLAN() || $structure->estUneSLAN())
            ) {

                if ($structure->estUneSLA()) {
                    $compteRenduInstance = new CompteRenduInstance(CompteRenduInstance::TYPE_APL_SLA);
                } else if ($structure->estUneSLAN()) {
                    $compteRenduInstance = new CompteRenduInstance(CompteRenduInstance::TYPE_APL_SLAN);
                } else if ($structure->estUneRegion()) {
                    $compteRenduInstance = new CompteRenduInstance(CompteRenduInstance::TYPE_CONGRES);
                }
                $compteRenduInstance->setSaison($this->saisonActive);
                $structure->addCompteRendusInstance($compteRenduInstance);
                $this->entityManager->persist($this->saisonActive);
            }
        }
        $this->entityManager->persist($structure);
        return $structure;
    }

    public function createOrUpdateEquipeUnitaire(?array $dataEquipe)
    {

        $dateCourante = new DateTime();
        $uuid = $dataEquipe['uuid'];
        $equipe = $this->equipeRepository->findOneBy(['uuid' => $uuid]);

        $structure_uuid = $dataEquipe['structure_uuid'];
        $structureParent = $this->structureRepository->findOneBy(['uuid' => $structure_uuid]);

        $this->createOrUpdateEquipe($equipe, $structureParent, $dataEquipe, $dateCourante);
        $this->entityManager->flush();
    }
    public function createOrUpdateEquipe(?Equipe $equipe, ?Structure $structureParent, array $dataEquipe, DateTime $dateCourante)
    {
        //Cas des types d'équipe personnalisés
        $nom = $dataEquipe['name'];
        $uuid = $dataEquipe['uuid'];
        $type = $dataEquipe['type'];

        if (!$equipe) {
            //On crée une Equipe
            $equipe = new Equipe();
            $equipe->setUuid(Uuid::fromString($uuid));
        }
        $creationDateAsString = $dataEquipe['creation_date'];
        if (null !== $creationDateAsString) {
            $equipe->setDateCreation(DateTime::createFromFormat("U", strtotime($creationDateAsString)));
        }
        $desactivationDateAsString = $dataEquipe['deactivation_date'];
        if (null !== $desactivationDateAsString) {
            $equipe->setDateDesactivation(DateTime::createFromFormat("U", strtotime($desactivationDateAsString)));
        }
        $equipe->setNom($nom);
        $equipe->setType($type);
        $equipe->setDateModification($dateCourante);

        if ($structureParent !== null) {
            $equipe->setStructure($structureParent);
            $this->entityManager->persist($equipe);
        } else {

            $structure_uuid = $dataEquipe['structure_uuid'];
            throw new Exception("La structure parent \"" . $structure_uuid . "\" de l'équipe " . $nom . " (" . $uuid . ") n'existe pas, l'équipe ne sera pas ajoutée ni mise à jour");
        }
    }

    public function createOrUpdatePersonneUnitaire(array $dataPersonne)
    {
        $contrainteEmailValid = new Assert\Email([
            'message' => 'L\'email "{{ value }}" n\'est pas valide.'
        ]);

        $dateCourante = new DateTime();
        $doublons = array();
        $uuid = $dataPersonne['uuid'];
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuid]);
        $uuidResponsableLegal1 = $dataPersonne['legal_guardian1_uuid'];
        $responsableLegal1 = $this->personneRepository->findOneBy(['uuid' => $uuidResponsableLegal1]);
        $uuidResponsableLegal2 = $dataPersonne['legal_guardian2_uuid'];
        $responsableLegal2 = $this->personneRepository->findOneBy(['uuid' => $uuidResponsableLegal2]);
        foreach ($dataPersonne['merged_with'] as $uuidDoublon) {
            $doublon = $this->personneRepository->findOneBy(['uuid' => $uuidDoublon]);
            if ($doublon) {
                $doublons[] = $doublon;
            }
        }


        $this->createOrUpdatePersonne($personne, $responsableLegal1, $responsableLegal2, $doublons, $dataPersonne, $dateCourante, $contrainteEmailValid);
        $this->entityManager->flush();
    }


    public function createOrUpdatePersonne(?Personne $personne, ?Personne $responsableLegal1, ?Personne $responsableLegal2, array $doublonsPersonne, array $dataPersonne, DateTime $dateCourante, Constraint $contrainteEmailValid): Personne
    {
        $uuid = $dataPersonne['uuid'];
        $uuidResponsableLegal1 = $dataPersonne['legal_guardian1_uuid'];
        $uuidResponsableLegal2 = $dataPersonne['legal_guardian2_uuid'];

        $genre = $dataPersonne['gender'];
        $prenom = $dataPersonne['first_name'];
        $nom = $dataPersonne['last_name'];
        $dateNaissanceString = $dataPersonne['birthdate'];
        $dateNaissance = null;
        if ($dateNaissanceString !== null) {
            $dateNaissance = DateTime::createFromFormat("Y-m-d|", $dateNaissanceString);
        }

        //Coordonnées
        $address1 = $dataPersonne['address1'];
        $address2 = $dataPersonne['address2'];
        $address3 = $dataPersonne['address3'];
        $codePostal = $dataPersonne['postal_code'];
        $ville = $dataPersonne['city'];
        $pays = $dataPersonne['country'];

        $email = $dataPersonne['email'];

        $droitImage = $dataPersonne['image_rights'];
        if ($droitImage == null) {
            $droitImage = false;
        }

        /** @var Personne $personne */
        if (!$personne) {
            //On crée une Personne
            $personne = new Personne();
        }

        foreach ($doublonsPersonne as $doublon) {
            $personne->fusionDoublon($doublon);
            $this->entityManager->persist($doublon);
        }

        $personne->setUuid(Uuid::fromString($uuid));
        $personne->setDateModification($dateCourante);
        $personne->setNom($nom);
        $personne->setPrenom($prenom);
        $personne->setDateNaissance($dateNaissance);
        $personne->setGenre($genre);
        $personne->setDroitImage($droitImage);
        $personne->setAdresseMail($email);

        $coordonnees = $personne->getCoordonnees();
        if (!$coordonnees) {
            $coordonnees = new Coordonnees();
            $personne->setCoordonnees($coordonnees);
        }
        $coordonnees->setNom($nom);
        $coordonnees->setPrenom($prenom);
        $coordonnees->setAdresseMail($email);
        if ($dataPersonne['home_phone'] !== null && $dataPersonne['home_phone'] !== "") {
            $telDomicile = new PhoneNumber();
            $telDomicile->setRawInput($dataPersonne['home_phone']);
        } else {
            $telDomicile = null;
        }
        $coordonnees->setTelDomicile($telDomicile);
        if ($dataPersonne['mobile_phone'] !== null && $dataPersonne['mobile_phone'] !== "") {

            $telMobile = new PhoneNumber();
            $telMobile->setRawInput($dataPersonne['mobile_phone']);
        } else {
            $telMobile = null;
        }
        $coordonnees->setTelMobile($telMobile);

        //Adresse postale
        $adressePostale = $coordonnees->getAdressePostale();
        if (!$adressePostale) {
            $adressePostale = new AdressePostale();
            $coordonnees->setAdressePostale($adressePostale);
        }
        $adressePostale->setAdresse($address1);
        $adressePostale->setComplementAdresse($address2 . " " . $address3);
        $adressePostale->setCodePostal($codePostal);
        $adressePostale->setVille($ville);
        $adressePostale->setPays($pays);

        //Responsable Légal N°1
        if (!$uuidResponsableLegal1) {
            $personne->setResponsableLegal1(null);
        } else {
            if ($responsableLegal1) {
                $personne->setResponsableLegal1($responsableLegal1);
            } else {
                throw new Exception("Le responsable légal n° 1 \"" . $uuidResponsableLegal1 . "\" de la personne " . $prenom . " " . $nom . " (" . $uuid . ") n'existe pas, la personne n'est pas ajoutée ni mise à jour");
            }
        }
        //Responsable Légal N°2
        if (!$uuidResponsableLegal2) {
            $personne->setResponsableLegal2(null);
        } else {
            /** @var Personne $responsableLegal2 */
            if ($responsableLegal2) {
                $personne->setResponsableLegal2($responsableLegal2);
            } else {
                throw new Exception("Le responsable légal n° 2 \"" . $uuidResponsableLegal2 . "\" de la personne " . $prenom . " " . $nom . " (" . $uuid . ") n'existe pas, la personne n'est pas ajoutée ni mise à jour");
            }
        }
        //Validation de la personne

        //Cas spécifique d'une adresse mail mal renseignée (mauvais format)
        $errors_email = $this->validatorInterface->validate($personne->getAdresseMail(), $contrainteEmailValid);
        if (0 !== count($errors_email)) {
            //Si l'adresse mail est dans un format incorrect, on positionne l'adresse mail à null
            $personne->setAdresseMail("");
            foreach ($errors_email as $violation) {

                throw new Exception("Attention: L'adresse e-mail à été positionnée à vide pour la personne n°" . $personne->getUuid() . ': CHAMP='
                    . $violation->getPropertyPath() . " (VALEUR INITIALE=\"" . $violation->getInvalidValue() . "\"): " . $violation->getMessage());
            }
        }

        $errors = $this->validatorInterface->validate($personne);

        if (0 !== count($errors)) {
            // there are errors, now you can show them
            foreach ($errors as $violation) {

                throw new Exception("Erreur pour la personne n°" . $personne->getUuid() . ': CHAMP='
                    . $violation->getPropertyPath() . " (VALEUR=\"" . $violation->getInvalidValue() . "\"): " . $violation->getMessage());
            }
        } else {

            $this->entityManager->persist($personne);
        }
        return $personne;
    }
    public function createOrUpdateAdherentUnitaire(array $dataAdherent)
    {
        $dateCourante = new DateTime();

        $uuid = $dataAdherent['uuid'];
        $adherent = $this->adherentRepository->findOneBy(['uuid' => $uuid]);
        $uuidPersonne = $dataAdherent['person_uuid'];
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuidPersonne]);

        $this->createOrUpdateAdherent($adherent, $personne, $dataAdherent, $dateCourante);
        $this->entityManager->flush();
    }

    public function createOrUpdateAdherent(?Adherent $adherent, ?Personne $personne, array $dataAdherent, DateTime $dateCourante): Adherent
    {
        $uuid = $dataAdherent['uuid'];
        $numeroAdherent = $dataAdherent['id'];
        $status = $dataAdherent['status'];
        $uuidPersonne = $dataAdherent['person_uuid'];
        /** @var Adherent $adherent */
        if (!$adherent) {
            //On crée un adhérent
            $adherent = new Adherent();
        }
        $adherent->setUuid(Uuid::fromString($uuid));
        /** @var Personne $personne */
        if ($personne) {
            $adherent->setPersonne($personne);
            $adherent->setDateModification($dateCourante);
            $adherent->setStatut($status);
            $adherent->setNumeroAdherent($numeroAdherent);
            $this->entityManager->persist($adherent);
        } else {
            throw new Exception("La personne d'uuid \"" . $uuidPersonne . "\" de l'adhérent·e de numéro " . $numeroAdherent . " n'existe pas, l'adhérent·e n'est pas inséré");
        }
        return $adherent;
    }

    public function createOrUpdateAdhesionUnitaire(array $dataAdhesion)
    {
        $dateCourante = new DateTime();

        $uuid = $dataAdhesion['uuid'];
        $adhesion = $this->adhesionRepository->findOneBy(['uuid' => $uuid]);
        $uuidAdherent = $dataAdhesion['adherent_uuid'];
        $adherent = $this->adherentRepository->findOneBy(['uuid' => $uuidAdherent]);
        $uuidStructure = $dataAdhesion['structure_uuid'];
        $structure = $this->structureRepository->findOneBy(['uuid' => $uuidStructure]);

        if ($adhesion) {
            $adhesion = $this->supprimerDroitPourAdhesion($adhesion);
        }

        $this->createOrUpdateAdhesion($adhesion, $adherent, $structure, $dataAdhesion, $dateCourante);
        $this->entityManager->flush();
    }

    public function createOrUpdateAdhesion(?Adhesion $adhesion, ?Adherent $adherent, ?Structure $structure, array $dataAdhesion, DateTime $dateCourante): Adhesion
    {

        $uuid = $dataAdhesion['uuid'];
        $dateDebutAsString = $dataAdhesion['begin'];
        $dateFin = $dataAdhesion['end'];
        $canceled = $dataAdhesion['canceled'];
        $uuidStructure = $dataAdhesion['structure_uuid'];
        $uuidAdherent = $dataAdhesion['adherent_uuid'];
        if (!$adhesion) {
            //On crée une adhésion
            $adhesion = new Adhesion();
        }
        if ($adherent) {
            if ($structure) {
                $adhesion->setStructure($structure);
                $adhesion->setUuid(Uuid::fromString($uuid));
                $adhesion->setAdherent($adherent);
                $adhesion->setDateModification($dateCourante);
                $dateDebut = DateTime::createFromFormat("Y-m-d|", $dateDebutAsString);
                if ($dateDebut) {
                    $adhesion->setDateDebut($dateDebut);
                    $adhesion->setDateFin(DateTime::createFromFormat("Y-m-d|", $dateFin));
                    $adhesion->setAnnule($canceled);
                    if ($adhesion->estActive()) {
                        $structuresSLAN = $this->structureRepository->findStructuresSLAN();
                        $droitsACreer = $this->creationDroitsPourAdhesion($adhesion, $structuresSLAN);
                        foreach ($droitsACreer as $droitACreer) {
                            $this->entityManager->persist($droitACreer);
                        }
                        unset($droitsACreer);
                        $droitsACreer = null;
                    }
                    $this->entityManager->persist($adhesion);
                } else {
                    throw new Exception("Pour l'adhésion d'id Jéito\"" . $uuid . "\" la date de début est incorrecte, l'adhésion n'est pas insérée");
                }
            } else {
                throw new Exception("La structure d'id Jéito\"" . $uuidStructure . "\" pour l'adhésion d'id Jéito " . $uuid . " n'existe pas, l'adhésion n'est pas insérée");
            }
        } else {
            throw new Exception("L'adhérent·e avec le numéro \"" . $uuidAdherent . "\" pour l'adhésion d'id Jéito " . $uuid . " n'existe pas, l'adhésion n'est pas insérée");
        }
        return $adhesion;
    }

    public function creationDroitsPourAdhesion(Adhesion $adhesion, array $structuresSLAN): array
    {
        $result = array();
        $structure = $adhesion->getStructure();
        $adherent = $adhesion->getAdherent();
        $personne = $adherent->getPersonne();
        $age = $personne->getAge();


        //On donne dans tous les cas le droit de consultation du CR de la structure dans laquelle la personne est adhérente
        $result[] = $this->createDroit($structure, $personne, StructureVoter::CONSULTER, $adhesion);


        if ($age < 16) { //Pour les personnes de moins 16 ans, on donne les mêmes droits aux parents
            $responsableLegal1 = $personne->getResponsableLegal1();
            $responsableLegal2 = $personne->getResponsableLegal2();
            if ($responsableLegal1) {
                $result[] = $this->createDroit($structure, $responsableLegal1, StructureVoter::CONSULTER, $adhesion);
            }

            if ($responsableLegal2) {
                $result[] = $this->createDroit($structure, $responsableLegal2, StructureVoter::CONSULTER, $adhesion);
            }
        }

        //Cas d'une SLA
        if ($structure->estUneSLA()) {
            $region = $structure->getStructureParent();
            //On rajoute les droits de consultation du CR de la région
            $result[] = $this->createDroit($region, $personne, StructureVoter::CONSULTER, $adhesion);
            //Pour les personnes de 16 ans et plus
            if ($age >= 16) {
                //On rajoute les droits de consultation de CR pour toutes les SLAR de la région 
                foreach ($region->getStructuresFillesDeResponsabiliteRegionale() as $structureFilleDeResponsabiliteRegionale) {
                    $result[] = $this->createDroit($structureFilleDeResponsabiliteRegionale, $personne, StructureVoter::CONSULTER, $adhesion);
                }
                //On rajoute les droits de consultation de CR pour toutes les SLAN 
                foreach ($structuresSLAN as $slan) {
                    $result[] = $this->createDroit($slan, $personne, StructureVoter::CONSULTER, $adhesion);
                }
            } else { //Pour les personnes de moins 16 ans, on donne les mêmes droits aux parents et pas de droit sur les SLAN et SLAR
                $responsableLegal1 = $personne->getResponsableLegal1();
                $responsableLegal2 = $personne->getResponsableLegal2();
                if ($responsableLegal1) {
                    $result[] = $this->createDroit($structure->getStructureParent(), $responsableLegal1, StructureVoter::CONSULTER, $adhesion);
                }

                if ($responsableLegal2) {
                    $result[] = $this->createDroit($structure->getStructureParent(), $responsableLegal2, StructureVoter::CONSULTER, $adhesion);
                }
            }
        } else {
            if ($age >= 16) { //Pour les adultes, y compris pour les mineurs de plus de 16 ans
                if ($structure->estUneRegion()) {
                    //On rajoute les droits de consultation de CR pour toutes les SLAR de la région 
                    foreach ($structure->getStructuresFillesDeResponsabiliteRegionale() as $structureFilleDeResponsabiliteRegionale) {
                        $result[] = $this->createDroit($structureFilleDeResponsabiliteRegionale, $personne, StructureVoter::CONSULTER, $adhesion);
                    }
                }
                foreach ($structuresSLAN as $slan) { //On rajoute les droits de consultation de CR pour toutes les SLAN
                    $result[] = $this->createDroit($slan, $personne, StructureVoter::CONSULTER, $adhesion);
                }
            }
        }




        //libération mémoire

        unset($responsableLegal1);
        $responsableLegal1 = null;

        unset($responsableLegal2);
        $responsableLegal2 = null;


        unset($personne);
        $personne = null;

        unset($adherent);
        $adherent = null;

        unset($adhesion);
        $adhesion = null;
        unset($structure);
        $structure = null;
        return $result;
    }

    public function createOrUpdateEmployeUnitaire(array $dataEmploye)
    {
        $dateCourante = new DateTime();

        $uuid = $dataEmploye['uuid'];
        $employe = $this->employeRepository->findOneBy(['uuid' => $uuid]);
        $uuidPersonne = $dataEmploye['person_uuid'];
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuidPersonne]);


        $this->createOrUpdateEmploye($employe, $personne, $dataEmploye, $dateCourante);

        $this->entityManager->flush();
    }

    public function createOrUpdateEmploye(?Employe $employe, ?Personne $personne, array $dataEmploye, DateTime $dateCourante): Employe
    {

        $uuid = $dataEmploye['uuid'];
        $numeroSalarie = $dataEmploye['id'];
        $uuidPersonne = $dataEmploye['person_uuid'];


        if (!$employe) {
            //On crée un employe
            $employe = new Employe();
        }
        /** @var Personne $personne */
        if ($personne) {
            $employe->setUuid(Uuid::fromString($uuid));
            $employe->setNumeroSalarie($numeroSalarie);
            $employe->setPersonne($personne);
            $employe->setDateModification($dateCourante);
            $employe->setEmail($dataEmploye['email']);
            if ($dataEmploye['phone'] && $dataEmploye['phone'] !== "") {
                $telEmploye = new PhoneNumber();
                $telEmploye->setRawInput($dataEmploye['phone']);
            } else {
                $telEmploye = null;
            }

            $employe->setTelephone($telEmploye);
            $this->entityManager->persist($employe);
        } else {
            throw new Exception("La personne \"" . $uuidPersonne . "\" de l'employé·e " . $numeroSalarie . " n'existe pas, l'adhésion n'est pas insérée");
        }
        return $employe;
    }


    public function createOrUpdateContratTravailUnitaire(array $dataContratTravail)
    {
        $dateCourante = new DateTime();

        $uuid = $dataContratTravail['uuid'];
        $contratTravail = $this->contratTravailRepository->findOneBy(['uuid' => $uuid]);
        $uuidEmploye = $dataContratTravail['employee_uuid'];
        $employe = $this->employeRepository->findOneBy(['uuid' => $uuidEmploye]);

        $this->createOrUpdateContratTravail($contratTravail, $employe, $dataContratTravail, $dateCourante);
        $this->entityManager->flush();
    }

    public function createOrUpdateContratTravail(?ContratTravail $contratTravail, ?Employe $employe, array $dataContratTravail, DateTime $dateCourante): ContratTravail
    {
        $uuid = $dataContratTravail['uuid'];
        $dateDebut = $dataContratTravail['begin'];
        $dateFinString = $dataContratTravail['end'];
        $nom = $dataContratTravail['name'];
        $type = $dataContratTravail['type'];
        $uuidEmploye = $dataContratTravail['employee_uuid'];
        /** @var ContratTravail $contratTravail */
        if (!$contratTravail) {
            //On crée un contrat de travail
            $contratTravail = new ContratTravail();
        }
        if ($employe) {
            $contratTravail->setUuid(Uuid::fromString($uuid));
            $contratTravail->setEmploye($employe);
            $contratTravail->setDateModification($dateCourante);
            $contratTravail->setDateDebut(DateTime::createFromFormat("Y-m-d|", $dateDebut));
            $dateFin = null;
            if ($dateFinString !== null) {
                $dateFin = DateTime::createFromFormat("Y-m-d|", $dateFinString);
            }

            $contratTravail->setDateFin($dateFin);
            $contratTravail->setNom($nom);
            $contratTravail->setType($type);
            $this->entityManager->persist($contratTravail);
        } else {
            throw new Exception("L'employé·e \"" . $uuidEmploye . "\" du contrat de travail " . $uuid . " n'existe pas, le contrat de travail n'est pas inséré");
        }

        return $contratTravail;
    }
    public function createOrUpdateFonctionUnitaire(array $dataFonction)
    {
        $dateCourante = new DateTime();

        $uuid = $dataFonction['uuid'];
        $fonction = $this->fonctionRepository->findOneBy(['uuid' => $uuid]);

        $uuidPersonne = $dataFonction['person_uuid'];
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuidPersonne]);

        $uuidEquipe = $dataFonction['team_uuid'];
        $equipe = $this->equipeRepository->findOneBy(['uuid' => $uuidEquipe]);

        if ($fonction) {
            $fonction = $this->supprimerDroitPourFonction($fonction);
        }

        $this->createOrUpdateFonction($fonction, $personne, $equipe, $dataFonction, $dateCourante);
        $this->entityManager->flush();
    }

    public function createOrUpdateFonction(?Fonction $fonction, ?Personne $personne, ?Equipe $equipe, array $dataFonction, DateTime $dateCourante): Fonction
    {
        $uuid = $dataFonction['uuid'];
        $dateDebut = $dataFonction['begin'];
        $dateFin = $dataFonction['end'];
        $nom = $dataFonction['name'];
        $uuidEquipe = $dataFonction['team_uuid'];
        $uuidPersonne = $dataFonction['person_uuid'];
        $type = $dataFonction['type'];

        if ($personne) {
            if ($equipe) {
                $nouvelleFonction = false;
                if (!$fonction) {
                    //On crée une fonction
                    $fonction = new Fonction();
                    $nouvelleFonction = true;
                }
                /** @var Fonction $fonction */
                $fonction->setEquipe($equipe);
                $fonction->setUuid(Uuid::fromString($uuid));
                $fonction->setPersonne($personne);
                $fonction->setDateModification($dateCourante);
                $fonction->setDateDebut(DateTime::createFromFormat("Y-m-d|", $dateDebut));
                if ($dateFin !== null) {
                    $fonction->setDateFin(DateTime::createFromFormat("Y-m-d|", $dateFin));
                } else {
                    $fonction->setDateFin(null);
                }
                $fonction->setNom($nom);
                $fonction->setType($type);
                $fonction->setStatut(Fonction::MAP_CATEGORY_BY_TYPE_FONCTION[$type]);

                $fonction = $this->creationMAJDroitPourFonction($fonction, $nouvelleFonction);

                $this->entityManager->persist($fonction);
            } else {
                throw new Exception("L'équipe \"" . $uuidEquipe . "\" pour la fonction " . $uuid . " n'existe pas, la fonction n'est pas insérée");
            }
        } else {
            throw new Exception("La personne \"" . $uuidPersonne . "\" pour la fonction " . $uuid . " n'existe pas, la fonction n'est pas insérée");
        }


        return $fonction;
    }

    public function suppressionFonctionUnitaire(String $uuid)
    {
        $fonction = $this->fonctionRepository->findOneBy(['uuid' => $uuid]);
        if ($fonction) {
            $this->suppressionFonction($fonction);
            $this->entityManager->flush();
        } else {
            throw new Exception("La fonction \"" . $uuid . "\" n'existe pas, la fonction n'est pas supprimée");
        }
    }
    public function suppressionFonction(Fonction $fonction)
    {
        $this->supprimerDroitPourFonction($fonction);
        $this->entityManager->remove($fonction);
    }

    public function suppressionAdhesionUnitaire(String $uuid)
    {
        $adhesion = $this->adhesionRepository->findOneBy(['uuid' => $uuid]);
        if ($adhesion) {
            $this->suppressionAdhesion($adhesion);
            $this->entityManager->flush();
        } else {
            throw new Exception("L'adhésion \"" . $uuid . "\" n'existe pas, elle n'est pas supprimée");
        }
    }
    public function suppressionAdhesion(Adhesion $adhesion)
    {
        $this->entityManager->remove($adhesion);
    }

    public function supprimerDroitPourAdhesion(Adhesion $adhesion): Adhesion
    {

        foreach ($adhesion->getDroitsAssocies() as $droitAssocie) {
            $adhesion->removeDroitAssocie($droitAssocie);
            //libération mémoire
            $droitAssocie = null;
            unset($droitAssocie);
        }
        $this->entityManager->persist($adhesion);
        return $adhesion;
    }

    public function suppressionAdherentUnitaire(String $uuid)
    {
        $adherent = $this->adherentRepository->findOneBy(['uuid' => $uuid]);
        if ($adherent) {
            $this->suppressionAdherent($adherent);
            $this->entityManager->flush();
        } else {
            throw new Exception("L'adhérent·e \"" . $uuid . "\" n'existe pas, elle n'est pas supprimée");
        }
    }
    public function suppressionAdherent(Adherent $adherent)
    {
        $this->entityManager->remove($adherent);
    }

    public function suppressionStructureUnitaire(String $uuid)
    {
        $structure = $this->structureRepository->findOneBy(['uuid' => $uuid]);
        if ($structure) {
            $this->suppressionStructure($structure);
            $this->entityManager->flush();
        } else {
            throw new Exception("La structure \"" . $uuid . "\" n'existe pas, elle n'est pas supprimée");
        }
    }
    public function suppressionStructure(Structure $structure)
    {
        $this->entityManager->remove($structure);
    }

    public function suppressionEquipeUnitaire(String $uuid)
    {
        $equipe = $this->equipeRepository->findOneBy(['uuid' => $uuid]);
        if ($equipe) {
            $this->suppressionEquipe($equipe);
            $this->entityManager->flush();
        } else {
            throw new Exception("L'équipe \"" . $uuid . "\" n'existe pas, elle n'est pas supprimée");
        }
    }
    public function suppressionEquipe(Equipe $equipe)
    {
        $this->entityManager->remove($equipe);
    }

    public function suppressionEmployeUnitaire(String $uuid)
    {
        $employe = $this->employeRepository->findOneBy(['uuid' => $uuid]);
        if ($employe) {
            $this->suppressionEmploye($employe);
            $this->entityManager->flush();
        } else {
            throw new Exception("L'employé " . $uuid . " n'existe pas, l'employé n'est pas supprimé");
        }
    }
    public function suppressionEmploye(Employe $employe)
    {
        $this->entityManager->remove($employe);
    }

    public function suppressionContratTravailUnitaire(String $uuid)
    {
        $contratTravail = $this->contratTravailRepository->findOneBy(['uuid' => $uuid]);
        if ($contratTravail) {
            $this->suppressionContratTravail($contratTravail);
            $this->entityManager->flush();
        } else {
            throw new Exception("Le contrat de travail \"" . $uuid . "\" n'existe pas, le contrat de travail n'est pas supprimé");
        }
    }
    public function suppressionContratTravail(ContratTravail $contratTravail)
    {
        $this->entityManager->remove($contratTravail);
    }

    public function suppressionPersonneUnitaire(String $uuid)
    {
        $personne = $this->personneRepository->findOneBy(['uuid' => $uuid]);
        if ($personne) {
            $this->suppressionPersonne($personne);
            $this->entityManager->flush();
        } else {
            throw new Exception("La personne \"" . $uuid . "\" n'existe pas, la personne n'est pas supprimée");
        }
    }
    public function suppressionPersonne(Personne $personne)
    {
        foreach ($personne->getPersonnesDontEstResponsableLegal1() as $personnesDontEstResponsableLegal1) {
            $personnesDontEstResponsableLegal1->setResponsableLegal1(null);
            $this->entityManager->persist($personnesDontEstResponsableLegal1);
        }
        foreach ($personne->getPersonnesDontEstResponsableLegal2() as $personnesDontEstResponsableLegal2) {
            $personnesDontEstResponsableLegal2->setResponsableLegal2(null);
            $this->entityManager->persist($personnesDontEstResponsableLegal2);
        }
        $this->entityManager->remove($personne);
    }


    private function createDroit(Structure $structure, Personne $personne, String $droitADonner, Adhesion $adhesion = null, Fonction $fonction = null): Droit
    {
        $droit = new Droit();
        $droit->setPersonne($personne);
        $droit->setDroit($droitADonner);
        $droit->setStructure($structure);
        $droit->setType(Droit::TYPE_DROIT_AUTO);
        $droit->setAdhesion($adhesion);
        $droit->setFonction($fonction);


        return $droit;
    }



    public function supprimerDroitPourFonction(Fonction $fonction): Fonction
    {
        foreach ($fonction->getDroitsAssocies() as $droit) {
            //Si la fonction n'est plus active, on envoie une notification pour chacun des droits supprimés
            if (!$fonction->estActive()) {
                $this->notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_006, [
                    NotificationHelper::PERIMETRE_FONCTION => $fonction,
                    NotificationHelper::PERIMETRE_DROIT => $droit
                ]);
            }
            foreach ($fonction->getDroitsAssocies() as $droitAssocie) {
                $this->entityManager->remove($droitAssocie);
                //libération mémoire
                $droitAssocie = null;
                unset($droitAssocie);
            }
        }
        return $fonction;
    }

    public function creationMAJDroitPourFonction(Fonction $fonction, bool $nouvelleFonction): Fonction
    {
        $comiteDirecteur = false;
        /** @deprecated Historisation - Réforme AG2024 des statuts */
        $responsableRegional = false;
        $organisationRegionale = false;
        $equipeSupportDeveloppementRegionale = false;
        $equipeSupportDeveloppementNationale = false;
        $equipeSupportDeveloppementStructureCourante = false;
        $equipeRegionale = false;
        $equipeNationale = false;
        $equipeGestionStructureCourante = false;
        $droitsAAjouter = array();





        if ($fonction->estActive()) {
            $structure = $fonction->getEquipe()->getStructure();
            $personne = $fonction->getPersonne();
            $age = $personne->getAge();

            if ($structure->estNational()) {
                //Comité directeur
                if ($fonction->getEquipe()->getType() == Equipe::TYPE_EQUIPE_EQUIPE_GESTION) {
                    $comiteDirecteur = true;
                } else if ($fonction->getEquipe()->getType() == Equipe::TYPE_EQUIPE_EQUIPE_NATIONALE) {
                    $equipeNationale = true;
                } else if ($fonction->getEquipe()->getType() == Equipe::TYPE_EQUIPE_SUPPORT_DEVELOPPEMENT) {
                    $equipeSupportDeveloppementNationale = true;
                }
            } else if ($structure->estUneRegion()) {
                if (($fonction->getEquipe()->getType() == Equipe::TYPE_EQUIPE_EQUIPE_GESTION)) {
                    //Responsable régional
                    /** @deprecated Historisation - Réforme AG2024 des statuts */
                    if ($fonction->getType() == Fonction::TYPE_FONCTION_RESPONSABLE) {
                        $responsableRegional = true;
                        //Organisation régionale
                    } else if ($fonction->getType() == Fonction::TYPE_MISSION_ORGANISATION) {
                        $organisationRegionale = true;
                        //Membre Equipe régionale
                    } else {
                        $equipeRegionale = true;
                    }

                    //Equipe Support et Developpement Régionale
                } else if ($fonction->getEquipe()->getType() == Equipe::TYPE_EQUIPE_SUPPORT_DEVELOPPEMENT) {
                    $equipeSupportDeveloppementRegionale = true;
                }
            }

            //Equipe Support et Developpement de la structure courante
            if ($fonction->getEquipe()->getType() == Equipe::TYPE_EQUIPE_SUPPORT_DEVELOPPEMENT) {
                $equipeSupportDeveloppementStructureCourante = true;
            }
            //Equipe de gestion de la structure courante
            if ($fonction->getEquipe()->getType() == Equipe::TYPE_EQUIPE_EQUIPE_GESTION) {
                $equipeGestionStructureCourante = true;
            }
            if ($comiteDirecteur) {
                $structuresSLAN = $this->structureRepository->findStructuresSLAN();
                foreach ($structuresSLAN as $structureSLAN) {
                    $droitsAAjouter[] = $this->createDroit($structureSLAN, $personne, StructureVoter::VALIDER);
                }
                $structuresRegion = $this->structureRepository->findStructuresRegion();
                foreach ($structuresRegion as $structureRegion) {
                    $droitsAAjouter[] = $this->createDroit($structureRegion, $personne, StructureVoter::VALIDER);
                }
                $structuresSLA = $this->structureRepository->findStructuresSLA();
                foreach ($structuresSLA as $structureSLA) {
                    $droitsAAjouter[] = $this->createDroit($structureSLA, $personne, StructureVoter::CONSULTER_ARCHIVE);
                }
            }
            $structuresSLARegion = $this->structureRepository->findStructuresFillesSLAActives($structure);
            if ($responsableRegional || $organisationRegionale || $equipeSupportDeveloppementRegionale) {
                foreach ($structuresSLARegion as $structureSLARegion) {
                    $droitsAAjouter[] = $this->createDroit($structureSLARegion, $personne, StructureVoter::VALIDER);
                }
            }
            if ($equipeRegionale) {
                foreach ($structuresSLARegion as $structureSLARegion) {
                    $droitsAAjouter[] = $this->createDroit($structureSLARegion, $personne, StructureVoter::SAISIR);
                }
            }
            if ($equipeRegionale || $organisationRegionale || $responsableRegional) {
                $structureSLANAffiliees = $structure->getStructureSlanAffiliees();
                foreach ($structureSLANAffiliees as $structureSLANAffiliee) {
                    $droitsAAjouter[] = $this->createDroit($structureSLANAffiliee, $personne, StructureVoter::CONSULTER_ARCHIVE);
                }
            }
            if ($equipeNationale || $equipeSupportDeveloppementNationale) {
                $toutesLesStructuresActives = $this->structureRepository->findAllStructuresActives();
                foreach ($toutesLesStructuresActives as $structureActive) {
                    $droitsAAjouter[] = $this->createDroit($structureActive, $personne, StructureVoter::VALIDER);
                }
            }
            if ($equipeSupportDeveloppementStructureCourante) {
                $droitsAAjouter[] = $this->createDroit($structure, $personne, StructureVoter::VALIDER);
            }
            if ($equipeGestionStructureCourante) {
                $droitsAAjouter[] = $this->createDroit($structure, $personne, StructureVoter::SAISIR);
            }


            //Dans tous les cas sauf celui d'une SLAN: 
            //On ajoute le droit de consultation pour la structure de la fonction, et pour les parents aussi


            //On donne dans tous les cas le droit de consultation du CR de la structure dans laquelle la personne a la fonction
            $result[] = $this->createDroit($structure, $personne, StructureVoter::CONSULTER);


            if ($age < 16) { //Pour les personnes de moins 16 ans, on donne les mêmes droits aux parents
                $responsableLegal1 = $personne->getResponsableLegal1();
                $responsableLegal2 = $personne->getResponsableLegal2();
                if ($responsableLegal1) {
                    $result[] = $this->createDroit($structure, $responsableLegal1, StructureVoter::CONSULTER);
                }

                if ($responsableLegal2) {
                    $result[] = $this->createDroit($structure, $responsableLegal2, StructureVoter::CONSULTER);
                }
            }

            //Cas d'une SLA
            if ($structure->estUneSLA()) {
                $region = $structure->getStructureParent();
                //On rajoute les droits de consultation du CR de la région
                $result[] = $this->createDroit($region, $personne, StructureVoter::CONSULTER);
                //Pour les personnes de 16 ans et plus
                if ($age >= 16) {
                    //On rajoute les droits de consultation de CR pour toutes les SLAR de la région 
                    foreach ($region->getStructuresFillesDeResponsabiliteRegionale() as $structureFilleDeResponsabiliteRegionale) {
                        $result[] = $this->createDroit($structureFilleDeResponsabiliteRegionale, $personne, StructureVoter::CONSULTER);
                    }
                } else { //Pour les personnes de moins 16 ans, on donne les mêmes droits aux parents et pas de droit sur les SLAN et SLAR
                    $responsableLegal1 = $personne->getResponsableLegal1();
                    $responsableLegal2 = $personne->getResponsableLegal2();
                    if ($responsableLegal1) {
                        $result[] = $this->createDroit($structure->getStructureParent(), $responsableLegal1, StructureVoter::CONSULTER);
                    }

                    if ($responsableLegal2) {
                        $result[] = $this->createDroit($structure->getStructureParent(), $responsableLegal2, StructureVoter::CONSULTER);
                    }
                }
            } else {
                if ($structure->estUneRegion()) {
                    //Pour les personnes de 16 ans et plus
                    if ($age >= 16) {
                        //On rajoute les droits de consultation de CR pour toutes les SLAR de la région 
                        foreach ($structure->getStructuresFillesDeResponsabiliteRegionale() as $structureFilleDeResponsabiliteRegionale) {
                            $result[] = $this->createDroit($structureFilleDeResponsabiliteRegionale, $personne, StructureVoter::CONSULTER);
                        }
                    }
                }
            }


            if (!$structure->estUneSLAN()) {
                $droitsAAjouter[] = $this->createDroit($structure, $personne, StructureVoter::CONSULTER);
            }
            if ($structure->estUneSLA()) {
                $droitsAAjouter[] = $this->createDroit($structure->getStructureParent(), $personne, StructureVoter::CONSULTER);
            }
            $responsableLegal1 = $personne->getResponsableLegal1();
            $responsableLegal2 = $personne->getResponsableLegal2();
            if ($responsableLegal1) {
                if (!$structure->estUneSLAN()) {
                    $droitsAAjouter[] = $this->createDroit($structure, $responsableLegal1, StructureVoter::CONSULTER);
                }
                if ($structure->estUneSLA()) {
                    $droitsAAjouter[] = $this->createDroit($structure->getStructureParent(), $responsableLegal1, StructureVoter::CONSULTER);
                }
            }

            if ($responsableLegal2) {
                if (!$structure->estUneSLAN()) {
                    $droitsAAjouter[] = $this->createDroit($structure, $responsableLegal2, StructureVoter::CONSULTER);
                }
                if ($structure->estUneSLA()) {
                    $droitsAAjouter[] = $this->createDroit($structure->getStructureParent(), $responsableLegal2, StructureVoter::CONSULTER);
                }
            }
            $droitPlusHautQueConsulter = false;
            foreach ($droitsAAjouter as $droitAAjouter) {
                if ($this->droitsHelper->estPlusFortOuEquivalent(
                    $droitAAjouter->getDroit(),
                    StructureVoter::CONSULTER_ARCHIVE
                )) {
                    $droitPlusHautQueConsulter = true;
                }
                $fonction->addDroitAssocie($droitAAjouter);
            }

            $droitsAAjouter = null;
            unset($droitsAAjouter);

            // On envoie une notification uniquement dans le cas ou la Fonction est nouvelle et qu'un de ses droit affecctés est plus haut que CONSULTER
            if ($nouvelleFonction && $droitPlusHautQueConsulter) {
                $this->notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_004, [
                    NotificationHelper::PERIMETRE_FONCTION => $fonction
                ]);
            }
        }
        return $fonction;
    }

    function envoiMailErreurSupport(String $erreur, MessageSynchroTempsReel $message)
    {
        //dump($erreur);
        //envoi du mail à l'adresse support:
        if ($this->adresseMailSupport) {
            $message = (new TemplatedEmail())
                // On attribue le destinataire
                ->to($this->adresseMailSupport)
                ->subject("[Support Agora] Erreur lors de la synchronisation temps réel depuis Jéito vers Agora via le broker")
                ->htmlTemplate('mails_monitoring_support/erreur_synchro_temps_reel_email.html.twig')
                ->context([
                    'erreur' => $erreur,
                    'message' => $message
                ]);

            $this->mailer->send($message);
        }
    }
}
