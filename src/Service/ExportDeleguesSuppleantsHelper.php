<?php

namespace App\Service;

use App\Entity\CompteRenduInstance;
use App\Entity\ElectionNominative;
use App\Entity\Fonction;
use App\Entity\Structure;
use App\Repository\SaisonDemocratiqueRepository;
use App\Repository\StructureRepository;
use Doctrine\Common\Collections\Collection;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class ExportDeleguesSuppleantsHelper
{

    protected $format_date_xls = 'dd/mm/yyyy h:mm';
    protected $styleEnteteArray = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR,
            ],
        ],
        'fill' => [
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => [
                'argb' => 'AEC7D2',
            ],
        ],
    ];

    protected $styleContenuArray = [
        'borders' => [
            'vertical' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR,
            ],
            'outline' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR,
            ],
        ],
    ];

    public function __construct(private SaisonDemocratiqueRepository $saisonDemocratiqueRepository, private StructureRepository $structureRepository) {}


    public function exportDeleguesSuppleants(String $fileName, ?Structure $region = null)
    {

        $saisonCourante = $this->saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);

        $sheetActive = $spreadsheet->getActiveSheet();

        $sheetActive->setTitle("Délégués et Suppléant·e·s");

        $n = 1;
        $COL_NOM = $n++;
        $COL_PRENOM = $n++;
        $COL_NUMERO_ADHERENT = $n++;
        $COL_ADRESSE_MAIL = $n++;
        $COL_FONCTION = $n++;
        $COL_STRUCTURE = $n++;
        if (!$region) {
            $COL_DELEGATION = $n++;
        }

        $rowHeader1 = 1;
        //Ligne d'en tete
        $sheetActive->setCellValue([$COL_NOM, $rowHeader1], "Nom");
        $sheetActive->setCellValue([$COL_PRENOM, $rowHeader1], "Prénom");
        $sheetActive->setCellValue([$COL_NUMERO_ADHERENT, $rowHeader1], "Numéro d'adhérent·e");
        $sheetActive->setCellValue([$COL_ADRESSE_MAIL, $rowHeader1], "Adresse mail");
        $sheetActive->setCellValue([$COL_FONCTION, $rowHeader1], "Fonction");
        $sheetActive->setCellValue([$COL_STRUCTURE, $rowHeader1], "Structure d'élection");
        if (!$region) {
            $sheetActive->setCellValue([$COL_DELEGATION, $rowHeader1], "Délégation");
        }

        //Application du style d'en-têtes
        if (!$region) {
            $sheetActive->getStyle([$COL_NOM, $rowHeader1, $COL_DELEGATION, $rowHeader1])->applyFromArray($this->styleEnteteArray);
        } else {
            $sheetActive->getStyle([$COL_NOM, $rowHeader1, $COL_STRUCTURE, $rowHeader1])->applyFromArray($this->styleEnteteArray);
        }

        //Largeur des colonnes
        $sheetActive->getColumnDimensionByColumn($COL_NOM)->setAutoSize(true);
        $sheetActive->getColumnDimensionByColumn($COL_PRENOM)->setAutoSize(true);
        $sheetActive->getColumnDimensionByColumn($COL_NUMERO_ADHERENT)->setAutoSize(true);
        $sheetActive->getColumnDimensionByColumn($COL_ADRESSE_MAIL)->setAutoSize(true);
        $sheetActive->getColumnDimensionByColumn($COL_FONCTION)->setAutoSize(true);
        $sheetActive->getColumnDimensionByColumn($COL_STRUCTURE)->setAutoSize(true);
        if (!$region) {
            $sheetActive->getColumnDimensionByColumn($COL_DELEGATION)->setAutoSize(true);
        }


        $listeElectionsNominatives = array();
        $arrayData = array();
        if ($region) {
            $listeStructuresDelegationRegionale[] = $region;
            foreach ($region->getStructuresFilles() as $sla) {
                $listeStructuresDelegationRegionale[] = $sla;
            }
            foreach ($region->getStructureSlanAffiliees() as $slan) {
                $listeStructuresDelegationRegionale[] = $slan;
            }

            foreach ($listeStructuresDelegationRegionale as $structure) {
                $compterenduAnneeCourante = $structure->getCompteRenduInstancePourSaison($saisonCourante);
                if ($compterenduAnneeCourante) {
                    foreach ($compterenduAnneeCourante->getElectionsNominatives() as $electionNominative) {
                        if ((
                            $electionNominative->getPoste() == ElectionNominative::POSTE_SUPPLEANT_AG
                            || $electionNominative->getPoste() == ElectionNominative::POSTE_DELEGUE_AG
                            || $electionNominative->getPoste() == ElectionNominative::POSTE_REPRESENTATION_REGION
                            || $electionNominative->getPoste() == ElectionNominative::POSTE_REPRESENTATION_SLA
                        )) {
                            $listeElectionsNominatives[] = $electionNominative;
                        }
                    }
                }
            }
        } else {
            //On rajoute les membres du CD
            $structureNationale = $this->structureRepository->findOneBy(["echelon" => Structure::ECHELON_STRUCTURE_NATIONAL]);
            $comiteDirecteur = $structureNationale->getEquipeGestion();
            foreach ($comiteDirecteur->getFonctionsActives(Fonction::TYPE_FONCTION_MEMBRE) as $membreComiteDirecteur) {
                $arrayLine[$COL_NOM] = $membreComiteDirecteur->getPersonne()->getNom();
                $arrayLine[$COL_PRENOM] =  $membreComiteDirecteur->getPersonne()->getPrenom();
                $arrayLine[$COL_NUMERO_ADHERENT] = $membreComiteDirecteur->getPersonne()->getNumeroAdherentOuMatricule();
                $arrayLine[$COL_ADRESSE_MAIL] = $membreComiteDirecteur->getPersonne()->getAdresseMail();
                $arrayLine[$COL_FONCTION] = "Membre de droit";
                $arrayLine[$COL_STRUCTURE] = "Comité directeur";
                $arrayLine[$COL_DELEGATION] = "Nation";

                $arrayData[] = $arrayLine;
            }

            $compteRendus = $saisonCourante->getComptesRendusInstances();
            /** @var CompteRenduInstance $compteRendu */
            foreach ($compteRendus as $compteRendu) {

                /** @var ElectionNominative $election */
                foreach ($compteRendu->getElectionsNominatives() as $election) {
                    if (in_array($election->getPoste(), [ElectionNominative::POSTE_REPRESENTATION_REGION, ElectionNominative::POSTE_REPRESENTATION_SLA, ElectionNominative::POSTE_DELEGUE_AG, ElectionNominative::POSTE_SUPPLEANT_AG])) {
                        $listeElectionsNominatives[] = $election;
                    }
                }
            }
        }
        /** @var ElectionNominative $posteAAjouter */
        foreach ($listeElectionsNominatives as $posteAAjouter) {
            $arrayLine = array();
            $arrayLine[$COL_NOM] = "";
            $arrayLine[$COL_PRENOM] = "";
            $arrayLine[$COL_NUMERO_ADHERENT] = "";
            $arrayLine[$COL_ADRESSE_MAIL] = "";
            if ($posteAAjouter->getPersonne() && $posteAAjouter->getStatus() == ElectionNominative::STATUS_VALIDE) {

                $arrayLine[$COL_NOM] = $posteAAjouter->getPersonne()->getNom();
                $arrayLine[$COL_PRENOM] =  $posteAAjouter->getPersonne()->getPrenom();
                $arrayLine[$COL_NUMERO_ADHERENT] = $posteAAjouter->getPersonne()->getNumeroAdherentOuMatricule();
                $arrayLine[$COL_ADRESSE_MAIL] = $posteAAjouter->getPersonne()->getAdresseMail();
                $arrayLine[$COL_FONCTION] = $posteAAjouter->getPoste();

                $arrayLine[$COL_STRUCTURE] = $posteAAjouter->getCompteRendu()->getStructure()->getNom();
                if (!$region) {
                    if ($posteAAjouter->getCompteRendu()->getStructure()->estUneRegion()) {
                        $arrayLine[$COL_DELEGATION] = $posteAAjouter->getCompteRendu()->getStructure()->getNom();
                    } else if ($posteAAjouter->getCompteRendu()->getStructure()->estUneSLAN()) {
                        $arrayLine[$COL_DELEGATION] = $posteAAjouter->getCompteRendu()->getStructure()->getStructureRegionaleAffiliee()->getNom();
                    } else {
                        $arrayLine[$COL_DELEGATION] = $posteAAjouter->getCompteRendu()->getStructure()->getStructureParent()->getNom();
                    }
                }
                $arrayData[] = $arrayLine;
            }
        }

        $sheetActive->fromArray(
            $arrayData,  // The data to set
            NULL,        // Array values with this value will not be set
            'A2'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
        );

        //Application du style de contenu
        if (!$region) {
            $sheetActive->getStyle([$COL_NOM, 2, $COL_DELEGATION, count($arrayData) + 1])->applyFromArray($this->styleContenuArray);
        } else {
            $sheetActive->getStyle([$COL_NOM, 2, $COL_STRUCTURE, count($arrayData) + 1])->applyFromArray($this->styleContenuArray);
        }
        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system

        $fileName = $this->filter_filename($fileName);
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $temp_file;
    }







    function filter_filename($filename)
    {
        // sanitize filename
        $filename = preg_replace(
            '~
            [<>:"/\\|?*]|            # file system reserved https://en.wikipedia.org/wiki/Filename#Reserved_characters_and_words
            [\x00-\x1F]|             # control characters http://msdn.microsoft.com/en-us/library/windows/desktop/aa365247%28v=vs.85%29.aspx
            [\x7F\xA0\xAD]|          # non-printing characters DEL, NO-BREAK SPACE, SOFT HYPHEN
            [#\[\]@!$&\'()+,;=]|     # URI reserved https://tools.ietf.org/html/rfc3986#section-2.2
            [{}^\~`]                 # URL unsafe characters https://www.ietf.org/rfc/rfc1738.txt
            ~x',
            '-',
            $filename
        );
        // avoids ".", ".." or ".hiddenFiles"
        $filename = ltrim($filename, '.-');
        // maximize filename length to 255 bytes http://serverfault.com/a/9548/44086
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $filename = mb_strcut(pathinfo($filename, PATHINFO_FILENAME), 0, 255 - ($ext ? strlen($ext) + 1 : 0), mb_detect_encoding($filename)) . ($ext ? '.' . $ext : '');
        return $filename;
    }
}
