<?php

namespace App\Service;

use App\Entity\AttributionFonctionElective;
use App\Entity\CompteRenduInstance;
use App\Entity\Fonction;
use App\Entity\Personne;
use App\Repository\AttributionFonctionElectiveRepository;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Exception;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\HttpOptions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Uid\Factory\UuidFactory;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV4;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class EcritureFonctionJeitoHelper
{
    private $namespace_uuid;

    public function __construct(private UuidFactory $uuidFactory, private HttpClientInterface $client, private String $jeitoApiToken, private String $jeitoApiUrl, private AttributionFonctionElectiveRepository $attributionFonctionElectiveRepository)
    {
        $this->namespace_uuid = Uuid::v5(Uuid::fromString(Uuid::NAMESPACE_DNS), "agora.eedf.fr");
    }

    public function ecritureCompteRendu(CompteRenduInstance $compteRenduInstance)
    {

        $arrayOperation = [
            Request::METHOD_POST => [],
            Request::METHOD_PATCH => [],
            Request::METHOD_DELETE => []
        ];
        if ($compteRenduInstance->getAttributionsFonctionsElectives()->count() > 0) {
            $functionTypesByCategorieByNom = $this->chargerTypesFonctionDepuisJeito();

            //Ecriture des missions électives:
            foreach ($compteRenduInstance->getAttributionsFonctionsElectives() as $attributionFonctionElective) {
                $arrayOperation[$attributionFonctionElective->getOperation()][] = $this->ecritureFonction($attributionFonctionElective, $functionTypesByCategorieByNom);
            }
        }


        if ($arrayOperation[Request::METHOD_POST]) {
            $response = $this->postFonctions($arrayOperation[Request::METHOD_POST]);
            if ((Response::HTTP_OK !== $response->getStatusCode()) && (Response::HTTP_CREATED !== $response->getStatusCode())) {
                throw new Exception('Ecriture des nouvelles missions électives (POST), retour:' . $response->getStatusCode());
            }
        }
        if ($arrayOperation[Request::METHOD_PATCH]) {
            foreach ($arrayOperation[Request::METHOD_PATCH] as $parametrePatch) {

                $response = $this->patchFonction($parametrePatch['uuid'], $parametrePatch['parametre']);
                if ((Response::HTTP_OK !== $response->getStatusCode()) && (Response::HTTP_CREATED !== $response->getStatusCode())) {
                    throw new Exception("Modification d'une fonction élective existante (PATCH), UUID:" . $parametrePatch['uuid']->toRfc4122() . ", retour:" . $response->getStatusCode());
                }
            }
        }
        if ($arrayOperation[Request::METHOD_DELETE]) {
            foreach ($arrayOperation[Request::METHOD_DELETE] as $parametreDelete) {

                /** @var Uuid $uuid */
                $uuid = $parametreDelete['uuid'];
                $response = $this->client->request(Request::METHOD_DELETE, $this->jeitoApiUrl . "function" . "/" . $uuid->toRfc4122() . "/", [
                    'headers' => [
                        'Authorization' => 'token ' . $this->jeitoApiToken
                    ]
                ]);
                if ((Response::HTTP_NO_CONTENT !== $response->getStatusCode()) && (Response::HTTP_NOT_FOUND !== $response->getStatusCode())) {
                    throw new Exception("Suppression de missions électives (DELETE), UUID:" . $uuid->toRfc4122() . ", retour:" . $response->getStatusCode());
                } else {
                    //Si çà a marché, on supprime l'attribution du compte rendu
                    $this->attributionFonctionElectiveRepository->deleteWhereId($parametreDelete['id_attribution']);
                }
            }
        }
        //Ecriture de l'autonomie de la structure:
        if ($compteRenduInstance->getAutonomieStructure() !== null) {
            $response = $this->client->request(Request::METHOD_PATCH, $this->jeitoApiUrl . "structure" . "/" . $compteRenduInstance->getStructure()->getUuid()->toRfc4122() . "/", [
                'headers' => [
                    'Authorization' => 'token ' . $this->jeitoApiToken,
                    'Content-Type: application/json',
                    'accept: application/json'
                ],
                'body' => ['status' => $compteRenduInstance->getAutonomieStructure()],
            ]);
            if ((Response::HTTP_OK !== $response->getStatusCode()) && (Response::HTTP_CREATED !== $response->getStatusCode())) {
                throw new Exception("Modification de l'autonomie de la structure (PATCH) UUID:" . $compteRenduInstance->getStructure()->getUuid()->toRfc4122() . ", retour:" . $response->getStatusCode());
            }
        }
    }
    public function patchFonction(Uuid $uuid, array $parametre): ResponseInterface
    {
        /** @var Uuid $uuid */
        $response = $this->client->request(Request::METHOD_PATCH, $this->jeitoApiUrl . "function" . "/" . $uuid->toRfc4122() . "/", [
            'headers' => [
                'Authorization' => 'token ' . $this->jeitoApiToken,
                'Content-Type: application/json',
                'accept: application/json'
            ],
            'body' => json_encode($parametre, JSON_THROW_ON_ERROR)
        ]);
        return $response;
    }

    public function postFonctions(array $parametres): ResponseInterface
    {
        $requestJson = json_encode($parametres, JSON_THROW_ON_ERROR);
        $response = $this->client->request(Request::METHOD_POST, $this->jeitoApiUrl . "function" . "/", [
            'headers' => [
                'Content-Type: application/json',
                'accept: application/json',
                'Authorization' => 'Token ' . $this->jeitoApiToken
            ],
            'body' => $requestJson,
        ]);
        return $response;
    }


    public function raccourcirFonction(AttributionFonctionElective $attributionFonctionElective)
    {
        //On génére l'UUID de la fonction par rapport à l'attribution de fonction élective
        $uuid = Uuid::v5($this->namespace_uuid, "function/" . $attributionFonctionElective->getId());
        $parametre = ["end" => $attributionFonctionElective->getDateFin()->format("Y-m-d")];

        $response = $this->patchFonction($uuid,  $parametre);
        if ((Response::HTTP_OK !== $response->getStatusCode()) && (Response::HTTP_CREATED !== $response->getStatusCode())) {
            throw new Exception("Changement d'une fonction élective par l'administration (PATCH), retour:" . $response->getStatusCode());
        }
    }
    public function ecritureFonction(AttributionFonctionElective $attributionFonctionElective, array $functionTypesByCategorieByNom): array
    {
        $operationParameter = [];

        switch ($attributionFonctionElective->getOperation()) {
            case Request::METHOD_POST:
                $operationParameter = [
                    "uuid" => Uuid::v5($this->namespace_uuid, "function/" . $attributionFonctionElective->getId()),
                    "name" => $attributionFonctionElective->getNom(),
                    "team_uuid" => $attributionFonctionElective->getEquipe()->getUuid(),
                    "person_uuid" => $attributionFonctionElective->getPersonne()->getUuid(),
                    "type_uuid" =>  $functionTypesByCategorieByNom[$attributionFonctionElective->getStatus()][$attributionFonctionElective->getType()],
                    "begin" => $attributionFonctionElective->getDateDebut()->format("Y-m-d"),
                    "end" => $attributionFonctionElective->getDateFin()->format("Y-m-d"),

                ];
                break;
            case Request::METHOD_PATCH:
                $operationParameter = [
                    "uuid" => $attributionFonctionElective->getFonction()->getUuid(),
                    "parametre" => ["end" => $attributionFonctionElective->getDateFin()->format("Y-m-d")],

                ];
                break;
            case Request::METHOD_DELETE:
                $operationParameter = [
                    "uuid" => Uuid::v5($this->namespace_uuid, "function/" . $attributionFonctionElective->getId()),
                    "id_attribution" => $attributionFonctionElective->getId()

                ];
                break;
        }
        return $operationParameter;

        //Serialize Fonction
    }

    public function chargerTypesFonctionDepuisJeito(): array
    {
        $response = $this->client->request(Request::METHOD_GET, $this->jeitoApiUrl . "function_type", [
            'headers' => [
                'Authorization' => 'token ' . $this->jeitoApiToken
            ]
        ]);

        $content = $response->getContent();
        $arrayFunctionTypes = json_decode($content, true);

        $result = array();
        foreach ($arrayFunctionTypes['results'] as  $functionType) {
            $uuid = $functionType["uuid"];
            $name = $functionType["name"];
            $category = $functionType["category"];
            if (!array_key_exists($category, $result)) {
                $result[$category] = array();
            }
            $result[$category][$name] = $uuid;
        }
        return $result;
    }
}
