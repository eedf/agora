<?php

namespace App\Service;

use App\Entity\Adhesion;
use App\Entity\Droit;
use App\Entity\Equipe;
use App\Entity\Fonction;
use App\Entity\Personne;
use App\Entity\Structure;
use App\Repository\PersonneRepository;
use App\Repository\StructureRepository;
use App\Security\Voter\StructureVoter;
use DateTime;
use Doctrine\Common\Collections\Collection;

/**
 * Helper pour la gestion des droits dans Agora
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class DroitsHelper
{
    private StructureRepository $structureRepository;
    private PersonneRepository $personneRepository;

    const ARRAY_DROITS = [
        StructureVoter::CONSULTER => "Consultation de Compte-rendu",
        StructureVoter::CONSULTER_ARCHIVE => "Consultation d'archives de Compte-rendu",
        StructureVoter::SAISIR => "Saisie de Compte-rendu",
        StructureVoter::VALIDER => "Validation de Compte-rendu",
        StructureVoter::MODIFIER_APRES_VALIDATION => "Modification après validation de Compte-rendu",
    ];

    const ARRAY_HIERARCHY_DROITS = [
        StructureVoter::CONSULTER => 1,
        StructureVoter::CONSULTER_ARCHIVE => 2,
        StructureVoter::SAISIR => 3,
        StructureVoter::VALIDER => 4,
        StructureVoter::MODIFIER_APRES_VALIDATION => 5,
    ];

    public function __construct(StructureRepository $structureRepository, PersonneRepository $personneRepository)
    {
        $this->structureRepository = $structureRepository;
        $this->personneRepository = $personneRepository;
    }

    public function getDroitsPersonne(Personne $personne): array
    {
        $droitsPersonne = array();
        //Role administateur
        if ($personne->hasRole(Personne::ROLE_ADMIN)) {
            $droitsPersonne["Administration"] = "Administration";
        } else {
            foreach (array_values($this->getListeDroitsLePlusFortPourCoupleStructurePersonne($personne->getDroits()->toArray())) as $droit) {
                $libelle = self::ARRAY_DROITS[$droit->getDroit()];
                if (!array_key_exists($libelle, $droitsPersonne)) {
                    $droitsPersonne[$libelle] = [];
                }
                $droitsPersonne[$libelle][] = $droit->getStructure()->getNom();
            }
        }

        return $droitsPersonne;
    }

    public function getDroitsPlusFaibles(String $droit): array
    {
        $result = [];
        foreach (array_keys(self::ARRAY_DROITS) as $droitATester) {
            if ($this->estPlusFortOuEquivalent($droit, $droitATester)) {
                $result[] = $droitATester;
            }
        }
        return $result;
    }

    public function estPlusFortOuEquivalent(String $droit, String $droitAComparer): bool
    {
        return (self::ARRAY_HIERARCHY_DROITS[$droit] >= self::ARRAY_HIERARCHY_DROITS[$droitAComparer]);
    }

    public function getListeDroitsLePlusFortPourCoupleStructurePersonne(array $droits): array
    {
        $listeDesDroitsParStructure = array();
        foreach ($droits as $droit) {
            if ($droit->estActif()) {
                $idStructurePersonne = $droit->getStructure()->getId() . "_" . $droit->getPersonne()->getId();
                if (in_array($idStructurePersonne, array_keys($listeDesDroitsParStructure))) {
                    //On compare les priorités du droit déjà stocké et celui du nouveau, et on garde le plus fort
                    if ($this->estPlusFortOuEquivalent($droit->getDroit(), $listeDesDroitsParStructure[$idStructurePersonne]->getDroit())) {
                        $listeDesDroitsParStructure[$idStructurePersonne] = $droit;
                    }
                } else {
                    $listeDesDroitsParStructure[$idStructurePersonne] = $droit;
                }
            }
        }
        usort($listeDesDroitsParStructure, function (Droit $a, Droit $b) { {
                $hierarchieDroit = DroitsHelper::ARRAY_HIERARCHY_DROITS[$b->getDroit()] - DroitsHelper::ARRAY_HIERARCHY_DROITS[$a->getDroit()];
                if ($hierarchieDroit == 0) {
                    $ordrePersonne = strcmp($a->getPersonne()->getNom() . $a->getPersonne()->getPrenom(), $b->getPersonne()->getNom() . $b->getPersonne()->getPrenom());
                    if ($ordrePersonne == 0) {
                        return strcmp($a->getStructure()->getNom(), $b->getStructure()->getNom());
                    }
                    return $ordrePersonne;
                }
                return $hierarchieDroit;
            }
        });


        return $listeDesDroitsParStructure;
    }
    public function getDroitsAffectablesPourStructure(Personne $user, Structure $structure): array
    {
        $personne = $this->personneRepository->findOneBy(["id" => $user]);
        $droitsPourStructure = $personne->getDroitsPourStructure($structure);
        /** @var Droit $droitLePlusFort */
        $droitLePluFort = $this->getListeDroitsLePlusFortPourCoupleStructurePersonne($droitsPourStructure->toArray());

        //on prend le premier élement de la liste grace à reset()
        $droitsAffectables = $this->getDroitsPlusFaibles(reset($droitLePluFort)->getDroit());
        $result = array();
        foreach ($droitsAffectables as $droit) {
            $result[$droit] = self::ARRAY_DROITS[$droit];
        }
        return array_flip($result);
    }

    public function getStructuresDroitsPersonne(Personne $personne): array
    {
        $listeStructureAvecDroitsPersonne = array();

        foreach ($personne->getDroits() as $droitsPersonne) {
            //A minima la personne doit avoir le droit de consulter les archives pour déléguer les droits de consultation
            if ($droitsPersonne->estActif()) {
                if ($droitsPersonne !== StructureVoter::CONSULTER) {
                    $listeStructureAvecDroitsPersonne[] = $droitsPersonne->getStructure();
                }
            }
        }

        //dump($listeStructureAvecDroitsPersonne);
        return  $listeStructureAvecDroitsPersonne;
    }
}
