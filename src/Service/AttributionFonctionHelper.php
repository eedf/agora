<?php

namespace App\Service;

use App\Entity\Adhesion;
use App\Entity\AttributionFonctionElective;
use App\Entity\CompteRenduInstance;
use App\Entity\Droit;
use App\Entity\ElectionNominative;
use App\Entity\Equipe;
use App\Entity\Fonction;
use App\Entity\Personne;
use App\Entity\SaisonDemocratique;
use App\Entity\Structure;
use App\Repository\PersonneRepository;
use App\Repository\StructureRepository;
use App\Security\Voter\StructureVoter;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Helper pour l'attribution des missions électives d'une personne dans AGORA
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class AttributionFonctionHelper
{
    // par rapport au 1er septembre
    const DATE_FIN_MANDAT_ORGANISATION_REGION = "last day of march next year +2 years";
    const DATE_FIN_MANDAT_TRESORERIE_REGION = "last day of march next year +2 years";
    const DATE_FIN_MANDAT_REPRESENTATION_REGION = "last day of march next year +2 years";
    const DATE_FIN_MANDAT_COORDINATION_REGION = "last day of march next year +2 years";
    const DATE_FIN_MANDAT_ORGANISATION_SLA = "last day of december next year +1 years";
    const DATE_FIN_MANDAT_TRESORERIE_SLA = "last day of december next year +1 years";
    const DATE_FIN_MANDAT_REPRESENTATION_SLA = "last day of december next year +1 years";
    const DATE_FIN_MANDAT_COORDINATION_SLA = "last day of december next year +1 years";


    /** @deprecated (depuis AG 2024 utile pour historisation) */
    const DATE_FIN_MANDAT_RR = "last day of march next year +2 years";
    /** @deprecated (depuis AG 2024 utile pour historisation) */
    const DATE_FIN_MANDAT_TR = "last day of march next year +2 years";
    /** @deprecated (depuis AG 2024 utile pour historisation) */
    const DATE_FIN_MANDAT_RSLA = "last day of december next year +1 years";
    /** @deprecated (depuis AG 2024 utile pour historisation) */
    const DATE_FIN_MANDAT_TSLA = "last day of december next year +1 years";


    const DATE_FIN_MANDAT_ER = "last day of march next year +1 years";
    const DATE_FIN_MANDAT_EGA = "last day of december next year";

    const ARRAY_MAP_NOM_FONCTION_ELECTIVE_PAR_TYPE_STRUCTURE_PAR_TYPE_FONCTION = [

        Structure::TYPE_STRUCTURE_GROUPE_LOCAL =>
        [
            Fonction::TYPE_FONCTION_MEMBRE => "Membre de l'équipe de gestion et d'animation",
            Fonction::TYPE_FONCTION_DELEGUE_AG => "Délégué·e à l'AG",
            Fonction::TYPE_FONCTION_RESPONSABLE => "Responsable de groupe",
            Fonction::TYPE_FONCTION_TRESORIER => "Trésorier·e de groupe",
            Fonction::TYPE_MISSION_ORGANISATION => "Mission d'organisation",
            Fonction::TYPE_MISSION_COORDINATION => "Mission de coordination",
            Fonction::TYPE_MISSION_REPRESENTATION => "Mission de représentation",
            Fonction::TYPE_MISSION_TRESORERIE => "Mission de trésorerie",
        ],
        Structure::TYPE_STRUCTURE_REGION => [
            Fonction::TYPE_FONCTION_MEMBRE => "Membre de l'équipe régionale",
            Fonction::TYPE_FONCTION_DELEGUE_AG => "Délégué·e à l'AG",
            Fonction::TYPE_FONCTION_RESPONSABLE => "Responsable régional·e",
            Fonction::TYPE_FONCTION_TRESORIER => "Trésorier·e régional·e",
            Fonction::TYPE_MISSION_ORGANISATION => "Mission d'organisation",
            Fonction::TYPE_MISSION_COORDINATION => "Mission de coordination",
            Fonction::TYPE_MISSION_REPRESENTATION => "Mission de représentation",
            Fonction::TYPE_MISSION_TRESORERIE => "Mission de trésorerie",
        ],
        Structure::TYPE_STRUCTURE_CENTRE => [
            Fonction::TYPE_FONCTION_MEMBRE => "Membre de l'équipe de gestion et d'animation",
            Fonction::TYPE_FONCTION_DELEGUE_AG => "Délégué·e à l'AG",
            Fonction::TYPE_FONCTION_RESPONSABLE => "Responsable de centre",
            Fonction::TYPE_FONCTION_TRESORIER => "Trésorier·e de centre",
            Fonction::TYPE_MISSION_ORGANISATION => "Mission d'organisation",
            Fonction::TYPE_MISSION_COORDINATION => "Mission de coordination",
            Fonction::TYPE_MISSION_REPRESENTATION => "Mission de représentation",
            Fonction::TYPE_MISSION_TRESORERIE => "Mission de trésorerie",
        ],
        Structure::TYPE_STRUCTURE_SERVICE_VACANCES => [
            Fonction::TYPE_FONCTION_MEMBRE => "Membre de l'équipe de gestion et d'animation",
            Fonction::TYPE_FONCTION_DELEGUE_AG => "Délégué·e à l'AG",
            Fonction::TYPE_FONCTION_RESPONSABLE => "Responsable de service vacances",
            Fonction::TYPE_FONCTION_TRESORIER => "Trésorier·e de service vacances",
            Fonction::TYPE_MISSION_ORGANISATION => "Mission d'organisation",
            Fonction::TYPE_MISSION_COORDINATION => "Mission de coordination",
            Fonction::TYPE_MISSION_REPRESENTATION => "Mission de représentation",
            Fonction::TYPE_MISSION_TRESORERIE => "Mission de trésorerie",
        ],
        Structure::TYPE_STRUCTURE_AUTRES => [
            Fonction::TYPE_FONCTION_MEMBRE => "Membre de l'équipe de gestion et d'animation",
            Fonction::TYPE_FONCTION_DELEGUE_AG => "Délégué·e à l'AG",
            Fonction::TYPE_FONCTION_RESPONSABLE => "Responsable de SLA",
            Fonction::TYPE_FONCTION_TRESORIER => "Trésorier·e de SLA",
            Fonction::TYPE_MISSION_ORGANISATION => "Mission d'organisation",
            Fonction::TYPE_MISSION_COORDINATION => "Mission de coordination",
            Fonction::TYPE_MISSION_REPRESENTATION => "Mission de représentation",
            Fonction::TYPE_MISSION_TRESORERIE => "Mission de trésorerie",
        ]
    ];


    public function __construct(private NotificationHelper $notificationHelper) {}



    public function genererAttributionFonctionsElectives(CompteRenduInstance $compteRenduInstance): CompteRenduInstance
    {

        $attributionsACreerPatch = array();
        $attributionsACreerPost = array();
        $dateDebutSaison = $compteRenduInstance->getSaison()->getDateDebut();
        //Si l'instance n'a pas eu lieu, on prend comme date  de fin des fonctions en cours la date de fin de mandat de l'équipe de gestion (EGA ou ER)
        if (!$compteRenduInstance->isNaPasEuLieu()) {
            $dateFinFonctionEventuelle = $compteRenduInstance->getDateInstance();
        } else {
            if ($compteRenduInstance->getType() == CompteRenduInstance::TYPE_CONGRES) {
                $dateFinFonctionEventuelle = DateTime::createFromInterface($dateDebutSaison)->modify(self::DATE_FIN_MANDAT_ER);
            } else {
                $dateFinFonctionEventuelle = DateTime::createFromInterface($dateDebutSaison)->modify(self::DATE_FIN_MANDAT_EGA);
            }
        }



        if ($compteRenduInstance->getElectionEquipe()) {
            //On ajoute les atttributions de mission aux personnes de l'équipe de gestion.
            foreach ($compteRenduInstance->getElectionEquipe()->getRoles() as $roleElectionEquipe) {
                $attribution = new AttributionFonctionElective();
                $attribution->setOperation(Request::METHOD_POST);
                $attribution->setPersonne($roleElectionEquipe->getPersonne());
                $attribution->setEquipe($compteRenduInstance->getStructure()->getEquipeGestion());
                if ($roleElectionEquipe->getFonctionSpecifique()) {
                    $nomFonction = $roleElectionEquipe->getFonctionSpecifique();
                } else {
                    $nomFonction = self::ARRAY_MAP_NOM_FONCTION_ELECTIVE_PAR_TYPE_STRUCTURE_PAR_TYPE_FONCTION[$compteRenduInstance->getStructure()->getType()][Fonction::TYPE_FONCTION_MEMBRE];
                }
                $attribution->setNom($nomFonction);
                $attribution->setStatus(Fonction::CATEGORY_FONCTION_BENEVOLE);
                $attribution->setType(Fonction::TYPE_FONCTION_MEMBRE);
                $attribution->setDateDebut($compteRenduInstance->getDateInstance());
                if ($compteRenduInstance->getType() == CompteRenduInstance::TYPE_CONGRES) {
                    $attribution->setDateFin(DateTime::createFromInterface($dateDebutSaison)->modify(self::DATE_FIN_MANDAT_ER));
                } else {
                    $attribution->setDateFin(DateTime::createFromInterface($dateDebutSaison)->modify(self::DATE_FIN_MANDAT_EGA));
                }

                $attributionsACreerPost[] = $attribution;
            }
        }

        //On réduit les fonctions de l'équipe de gestion actuelle à la date de l'instance
        foreach ($compteRenduInstance->getStructure()->getEquipeGestion()->getFonctionsActives(Fonction::TYPE_FONCTION_MEMBRE, $dateFinFonctionEventuelle) as $fonctionsActiveEquipe) {
            if (!($fonctionsActiveEquipe->getDateDebut() == $dateFinFonctionEventuelle)) {
                $attribution = new AttributionFonctionElective();
                $attribution->setOperation(Request::METHOD_PATCH);
                $attribution->setFonction($fonctionsActiveEquipe);
                $attribution->setDateFin($dateFinFonctionEventuelle);
                $attributionsACreerPatch[] = $attribution;
            }
        }
        //On ajoute les attributions de mission aux membres élus nominativement;
        foreach ($compteRenduInstance->getElectionsNominatives() as $electionNominative) {
            $typeFonction = null;
            $dateFinMandat = null;
            switch ($electionNominative->getPoste()) {
                case ElectionNominative::POSTE_DELEGUE_AG:
                case ElectionNominative::POSTE_SUPPLEANT_AG:
                    //On ne fait rien (on stocke cette information uniquement dans AGORA)
                    break;
                //Organisation
                case ElectionNominative::POSTE_ORGANISATION_SLA:
                    $dateFinMandat = self::DATE_FIN_MANDAT_ORGANISATION_SLA;
                    $typeFonction = Fonction::TYPE_MISSION_ORGANISATION;
                    break;
                case ElectionNominative::POSTE_ORGANISATION_REGION:
                    $dateFinMandat = self::DATE_FIN_MANDAT_ORGANISATION_REGION;
                    $typeFonction = Fonction::TYPE_MISSION_ORGANISATION;
                    break;
                //Trésorerie
                case ElectionNominative::POSTE_TRESORERIE_SLA:
                    $dateFinMandat = self::DATE_FIN_MANDAT_TRESORERIE_SLA;
                    $typeFonction = Fonction::TYPE_MISSION_TRESORERIE;
                    break;
                case ElectionNominative::POSTE_TRESORERIE_REGION:
                    $dateFinMandat = self::DATE_FIN_MANDAT_TRESORERIE_REGION;
                    $typeFonction = Fonction::TYPE_MISSION_TRESORERIE;
                    break;
                //Coordination
                case ElectionNominative::POSTE_COORDINATION_SLA:
                    $dateFinMandat = self::DATE_FIN_MANDAT_COORDINATION_SLA;
                    $typeFonction = Fonction::TYPE_MISSION_COORDINATION;
                    break;
                case ElectionNominative::POSTE_COORDINATION_REGION:
                    $dateFinMandat = self::DATE_FIN_MANDAT_COORDINATION_REGION;
                    $typeFonction = Fonction::TYPE_MISSION_COORDINATION;
                    break;
                //Representation
                case ElectionNominative::POSTE_REPRESENTATION_SLA:
                    $dateFinMandat = self::DATE_FIN_MANDAT_REPRESENTATION_SLA;
                    $typeFonction = Fonction::TYPE_MISSION_REPRESENTATION;
                    break;
                case ElectionNominative::POSTE_REPRESENTATION_REGION:
                    $dateFinMandat = self::DATE_FIN_MANDAT_REPRESENTATION_REGION;
                    $typeFonction = Fonction::TYPE_MISSION_REPRESENTATION;
                    break;

                case ElectionNominative::POSTE_RESLA:
                    $dateFinMandat = self::DATE_FIN_MANDAT_RSLA;
                    $typeFonction = Fonction::TYPE_FONCTION_RESPONSABLE;
                    break;
                case ElectionNominative::POSTE_RR:
                    $dateFinMandat = self::DATE_FIN_MANDAT_RR;
                    $typeFonction = Fonction::TYPE_FONCTION_RESPONSABLE;
                    break;
                case ElectionNominative::POSTE_TR:
                    $dateFinMandat = self::DATE_FIN_MANDAT_TR;
                    $typeFonction = Fonction::TYPE_FONCTION_TRESORIER;
                    break;
                case ElectionNominative::POSTE_TSLA:
                    $dateFinMandat = self::DATE_FIN_MANDAT_TSLA;
                    $typeFonction = Fonction::TYPE_FONCTION_TRESORIER;
                    break;
            }

            if ($dateFinMandat && $typeFonction) {
                //Si la personne est déjà élue, on ne fait rien
                if (!$electionNominative->isDejaElu()) {
                    if (($electionNominative->getPersonne()) &&
                        ($electionNominative->getStatus() == ElectionNominative::STATUS_VALIDE)
                    ) {

                        $attribution = new AttributionFonctionElective();
                        $attribution->setOperation(Request::METHOD_POST);
                        $attribution->setPersonne($electionNominative->getPersonne());
                        $attribution->setNom(self::ARRAY_MAP_NOM_FONCTION_ELECTIVE_PAR_TYPE_STRUCTURE_PAR_TYPE_FONCTION[$compteRenduInstance->getStructure()->getType()][$typeFonction]);
                        $attribution->setEquipe($compteRenduInstance->getStructure()->getEquipeGestion());
                        $attribution->setStatus(Fonction::CATEGORY_FONCTION_BENEVOLE);
                        $attribution->setType($typeFonction);
                        $attribution->setDateDebut($dateFinFonctionEventuelle);
                        $attribution->setDateFin(DateTime::createFromInterface($dateDebutSaison)->modify($dateFinMandat));
                        $attributionsACreerPost[] = $attribution;
                    }
                    //Dans tous les cas, on enleve la mission aux personnes ayant ce droit (sauf la personne qui doit réellement avoir ce droit)
                    foreach ($compteRenduInstance->getStructure()->getEquipeGestion()->getFonctionsActives($typeFonction, $dateFinFonctionEventuelle) as $fonctionsActive) {
                        if (!($fonctionsActive->getDateDebut() == $dateFinFonctionEventuelle)) {
                            $attribution = new AttributionFonctionElective();
                            $attribution->setOperation(Request::METHOD_PATCH);
                            $attribution->setPersonne($fonctionsActive->getPersonne());
                            $attribution->setFonction($fonctionsActive);
                            $attribution->setDateFin($dateFinFonctionEventuelle);
                            $attributionsACreerPatch[] = $attribution;
                            if ($electionNominative->isPasDElection()) { // S'il n'ya pas eu d'élection pour cette mission, on envoie un message
                                $this->notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_008, [
                                    NotificationHelper::PERIMETRE_ATTRIBUTION_FONCTION_ELECTIVE => $attribution
                                ]);
                            }
                        }
                    }
                }
            }
        }

        //Si la date de la saison démocratique est apres la date de modification des status (AG 2024), on termine les Missions de RR,RESLA,TR et TSLA:
        if ($compteRenduInstance->getSaison()->getDateDebut() >= new DateTime(SaisonDemocratique::DATE_AG_2024)) {
            foreach ($compteRenduInstance->getStructure()->getEquipeGestion()->getFonctionsActives(Fonction::TYPE_FONCTION_TRESORIER, $dateFinFonctionEventuelle) as $fonctionsActive) {
                if (!($fonctionsActive->getDateDebut() == $dateFinFonctionEventuelle)) {
                    $attribution = new AttributionFonctionElective();
                    $attribution->setOperation(Request::METHOD_PATCH);
                    $attribution->setFonction($fonctionsActive);
                    $attribution->setDateFin($dateFinFonctionEventuelle);
                    $attributionsACreerPatch[] = $attribution;
                }
            }
            foreach ($compteRenduInstance->getStructure()->getEquipeGestion()->getFonctionsActives(Fonction::TYPE_FONCTION_RESPONSABLE, $dateFinFonctionEventuelle) as $fonctionsActive) {
                if (!($fonctionsActive->getDateDebut() == $dateFinFonctionEventuelle)) {
                    $attribution = new AttributionFonctionElective();
                    $attribution->setOperation(Request::METHOD_PATCH);
                    $attribution->setFonction($fonctionsActive);
                    $attribution->setDateFin($dateFinFonctionEventuelle);
                    $attributionsACreerPatch[] = $attribution;
                }
            }
        }

        /** @var AttributionFonctionElective $attributionExitantePatch*/
        foreach ($compteRenduInstance->getAttributionsFonctionsElectivesPourOperation(Request::METHOD_PATCH) as $attributionExitantePatch) {
            $attributionTrouvee = false;
            /** @var AttributionFonctionElective $attributionCree*/
            foreach ($attributionsACreerPatch as $index => $attributionACreerPatch) {
                //S'il s'agit de la meme mission
                if ($attributionExitantePatch->getFonction() == $attributionACreerPatch->getFonction()) {
                    $attributionTrouvee = true;
                    //Si la date de fin est la même on ne change rien, Sinon, il faut la modifier
                    $attributionExitantePatch->setDateFin($attributionACreerPatch->getDateFin());
                    //On enleve l'attribution des attributions à créer
                    unset($attributionsACreerPatch[$index]);
                }
            }
            if (!$attributionTrouvee) {
                $compteRenduInstance->removeAttributionsFonctionsElective($attributionExitantePatch);
            }
        }
        //Pour toutes les attributions restantes à créer, on les crée
        foreach ($attributionsACreerPatch as $attributionACreerPatch) {
            $compteRenduInstance->addAttributionsFonctionsElective($attributionACreerPatch);
        }
        //POST
        /** @var AttributionFonctionElective $attributionExistantePost*/
        foreach ($compteRenduInstance->getAttributionsFonctionsElectivesPourOperation(Request::METHOD_POST) as $attributionExistantePost) {
            /** @var AttributionFonctionElective $attributionACreerPost*/
            $attributionTrouvee = false;
            foreach ($attributionsACreerPost as $index => $attributionACreerPost) {
                //S'il s'agit de la meme mission
                if (($attributionExistantePost->getPersonne() == $attributionACreerPost->getPersonne())
                    && ($attributionExistantePost->getStatus() == $attributionACreerPost->getStatus())
                    && ($attributionExistantePost->getType() == $attributionACreerPost->getType())
                ) {
                    $attributionTrouvee = true;
                    $attributionExistantePost->setNom($attributionACreerPost->getNom());
                    $attributionExistantePost->setDateFin($attributionACreerPost->getDateFin());
                    $attributionExistantePost->setDateDebut($attributionACreerPost->getDateDebut());
                    //On enleve l'attribution des attributions à créer
                    unset($attributionsACreerPost[$index]);
                }
            }
            if (!$attributionTrouvee) { // Si l'attribution n'est pas trouvée, il faut indiquer à Jéito de la supprimer
                $compteRenduInstance->removeAttributionsFonctionsElective($attributionExistantePost);
                $attributionExistantePost->setOperation(Request::METHOD_DELETE);
                $compteRenduInstance->addAttributionsFonctionsElective($attributionExistantePost);
            }
        }
        //Pour toutes les attributions restantes à créer, on les supprime

        foreach ($attributionsACreerPost as $attributionACreerPost) {
            $compteRenduInstance->addAttributionsFonctionsElective($attributionACreerPost);
        }
        return $compteRenduInstance;
    }
}
