<?php

namespace App\Service;

use App\Entity\Adhesion;
use App\Entity\Droit;
use App\Entity\Equipe;
use App\Entity\Fonction;
use App\Entity\Personne;
use App\Entity\Structure;
use App\Repository\PersonneRepository;
use App\Repository\StructureRepository;
use App\Security\Voter\StructureVoter;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Request;

/**
 * Helper pour la gestion des droits dans Agora
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class RedirectionHelper
{
    public function retourHomePagePerimetre(Request $request): string
    {
        $typePerimetre = $request->getSession()->get('type_perimetre');
        switch ($typePerimetre) {
            case Personne::TYPE_PERIMETRE_ADMIN:
                $redirectRoute = 'home_admin';
                break;
            case Personne::TYPE_PERIMETRE_NATIONAL:
                $redirectRoute = 'home_national';
                break;
            case Personne::TYPE_PERIMETRE_SLAN:
                $redirectRoute = 'home_slan';
                break;
            case Personne::TYPE_PERIMETRE_REGION:
                $redirectRoute = 'home_region';
                break;
            case Personne::TYPE_PERIMETRE_SLA:
                $redirectRoute = 'home_sla';
                break;
            default:
                $redirectRoute = 'selection_perimetre';
                break;
        }
        return $redirectRoute;
    }
}
