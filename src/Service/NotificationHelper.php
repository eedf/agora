<?php

namespace App\Service;

use App\Entity\AttributionFonctionElective;
use App\Entity\CompteRenduInstance;
use App\Entity\Droit;
use App\Entity\Fonction;
use App\Entity\Notification;
use App\Entity\Personne;
use App\Message\MessageNotification;
use App\Repository\PersonneRepository;
use App\Security\Voter\StructureVoter;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Messenger\MessageBusInterface;
use Twig\Environment;

/**
 * Helper pour la gestion des notifications
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class NotificationHelper
{
    const DESTINATAIRE_CONSULTATION_CR = StructureVoter::CONSULTER;
    const DESTINATAIRE_SAISIE_CR = StructureVoter::SAISIR;
    const DESTINATAIRE_VALIDATION_CR = StructureVoter::VALIDER;
    const DESTINATAIRE_BENEFICIAIRE_DROIT = "beneficiaire droit";
    const DESTINATAIRE_PERSONNE_CONCERNEE = "personne concernée";
    const DESTINATAIRE_BENEFICIAIRE_FONCTION = "beneficiare fonction";
    const DESTINATAIRE_BENEFICIAIRE_ATTRIBUTION_FONCTION_ELECTIVE = "beneficiare attributionFonctionElective";

    const PERIMETRE_PERSONNE = "personne";
    const PERIMETRE_COMPTERENDU = "compteRendu";
    const PERIMETRE_DROIT = "droit";
    const PERIMETRE_FONCTION = "fonction";
    const PERIMETRE_ATTRIBUTION_FONCTION_ELECTIVE = "attributionFonctionElective";


    const NOTIFICATION_001 = "Validation de compte-rendu";
    const NOTIFICATION_002 = "Invalidation d'un compte-rendu";
    const NOTIFICATION_003 = "Envoi d'un compte-rendu";
    //Ajout Droit Auto
    const NOTIFICATION_004 = "Acquisition de droits automatique";
    // perte de droit Manuel
    const NOTIFICATION_005 = "Suppression d'un droit manuel";
    //Perte de fonction
    const NOTIFICATION_006 = "Perte d'un droit automatique";
    //Ajout Droit Manuel
    const NOTIFICATION_007 = "Acquisition d'un droit manuel";
    const NOTIFICATION_008 = "Fin de fonction anticipée";
    const NOTIFICATION_009 = "Expiration d'un droit manuel";

    const LISTE_NOTIFICATIONS = [
        self::NOTIFICATION_001 => [
            'template' => "notif_001",
            'templateLien' => "lien_notif_001",
            "destinataires" => [self::DESTINATAIRE_CONSULTATION_CR],
            'contexte' => [
                self::PERIMETRE_COMPTERENDU => CompteRenduInstance::class
            ]
        ],
        self::NOTIFICATION_002 => [
            'template' => "notif_002",
            'templateLien' => "lien_notif_002",
            "destinataires" => [self::DESTINATAIRE_SAISIE_CR],
            'contexte' => [
                self::PERIMETRE_COMPTERENDU => CompteRenduInstance::class
            ]
        ],
        self::NOTIFICATION_003 => [
            'template' => "notif_003",
            'templateLien' => "lien_notif_003",
            "destinataires" => [self::DESTINATAIRE_VALIDATION_CR],
            'contexte' => [
                self::PERIMETRE_COMPTERENDU => CompteRenduInstance::class
            ]
        ],
        self::NOTIFICATION_004 => [
            'template' => "notif_004",
            "destinataires" => [self::DESTINATAIRE_BENEFICIAIRE_FONCTION],
            'contexte' => [
                self::PERIMETRE_FONCTION => Fonction::class
            ]

        ],
        self::NOTIFICATION_005 => [
            'template' => "notif_005",
            "destinataires" => [self::DESTINATAIRE_BENEFICIAIRE_DROIT],
            'contexte' => [
                self::PERIMETRE_DROIT => Droit::class
            ]

        ],
        self::NOTIFICATION_006 => [
            'template' => "notif_006",
            "destinataires" => [self::DESTINATAIRE_BENEFICIAIRE_FONCTION],
            'contexte' => [
                self::PERIMETRE_FONCTION => Fonction::class,
                self::PERIMETRE_DROIT => Droit::class,
            ]

        ],
        self::NOTIFICATION_007 => [
            'template' => "notif_007",
            "destinataires" => [self::DESTINATAIRE_BENEFICIAIRE_DROIT],
            'contexte' => [
                self::PERIMETRE_DROIT => Droit::class
            ]

        ],
        self::NOTIFICATION_008 => [
            'template' => "notif_008",
            "destinataires" => [self::DESTINATAIRE_BENEFICIAIRE_ATTRIBUTION_FONCTION_ELECTIVE],
            'contexte' => [
                self::PERIMETRE_ATTRIBUTION_FONCTION_ELECTIVE => AttributionFonctionElective::class
            ]

        ],
        self::NOTIFICATION_009 => [
            'template' => "notif_009",
            "destinataires" => [self::DESTINATAIRE_BENEFICIAIRE_DROIT],
            'contexte' => [
                self::PERIMETRE_DROIT => Droit::class
            ]

        ],
    ];

    private DroitsHelper $droitsHelper;
    private EntityManagerInterface $manager;
    private $twig;
    private MessageBusInterface $busNotification;
    private PersonneRepository $personneRepository;

    public function __construct(EntityManagerInterface $manager, Environment $twig, PersonneRepository $personneRepository, DroitsHelper $droitsHelper, MessageBusInterface $busNotification)
    {
        $this->manager = $manager;
        $this->droitsHelper = $droitsHelper;
        $this->twig = $twig;
        $this->twig->addExtension(new \Twig\Extension\StringLoaderExtension());
        $this->busNotification = $busNotification;
        $this->personneRepository = $personneRepository;
    }

    public function gestionNotification($nom_modele_notif, array $contexte, array $destinataires = null)
    {

        //On vérifie si la notification existe
        if (!array_key_exists($nom_modele_notif, self::LISTE_NOTIFICATIONS)) {
            throw new Exception("Le modèle de notification " . $nom_modele_notif . " n'existe pas");
        }

        //On vérifie si le contexte existe
        $modele_notif = self::LISTE_NOTIFICATIONS[$nom_modele_notif];
        foreach ($modele_notif["contexte"] as $cle_contexte_attendu => $classe_contexte_attendu) {
            if (!array_key_exists($cle_contexte_attendu, $contexte)) {
                throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . $cle_contexte_attendu . " n'est pas présent dans le contexte");
            }
            if (!get_class($contexte[$cle_contexte_attendu]) == ($classe_contexte_attendu)) {
                throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . $cle_contexte_attendu . " n'est pas de la classe " . $classe_contexte_attendu);
            }
        }

        $templateTitre = $this->twig->createTemplate($nom_modele_notif);
        $pathTemplateContenu = 'notification/messages/' . $modele_notif["template"] . '.html.twig';
        $contenu = $this->twig->render($pathTemplateContenu, $contexte);

        if (array_key_exists("templateLien", $modele_notif)) {
            $pathTemplateLien = 'notification/messages/' . $modele_notif["templateLien"] . '.html.twig';
            $lien = $this->twig->render($pathTemplateLien, $contexte);
        } else {
            $lien = null;
        }
        $resultDestinataires = $this->determinerDestinataires($nom_modele_notif,  $contexte,  $destinataires);
        $titre = $this->twig->render($templateTitre);
        $this->busNotification->dispatch(new MessageNotification($titre, $lien,  $contenu, $resultDestinataires));
    }

    public function determinerDestinataires(String $nom_modele_notif, array $contexte, array $destinatairesPersonnes = null): array
    {
        $destinataires = array();
        //On récupere les destinataires désignés
        if ($destinatairesPersonnes) {
            foreach ($destinatairesPersonnes as $destinatairePersonne) {
                $destinataires[] = $destinatairePersonne->getId();
            }
        }
        $modele_notif = self::LISTE_NOTIFICATIONS[$nom_modele_notif];
        if (array_key_exists('destinataires', $modele_notif)) {
            foreach ($modele_notif['destinataires'] as $role) {
                switch ($role) {
                    case self::DESTINATAIRE_PERSONNE_CONCERNEE:

                        if (!array_key_exists(self::PERIMETRE_PERSONNE, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . self::PERIMETRE_PERSONNE . " n'est pas présent dans le contexte");
                        }
                        /**  @var Personne $personne */
                        $personne = $contexte[self::PERIMETRE_PERSONNE];
                        $destinataires[] = $personne->getId();
                        break;
                    case self::DESTINATAIRE_BENEFICIAIRE_DROIT:

                        if (!array_key_exists(self::PERIMETRE_DROIT, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . self::PERIMETRE_DROIT . " n'est pas présent dans le contexte");
                        }
                        /**  @var Droit $droit */
                        $droit = $contexte[self::PERIMETRE_DROIT];


                        $destinataires[] = $droit->getPersonne()->getId();
                        break;
                    case self::DESTINATAIRE_BENEFICIAIRE_FONCTION:

                        if (!array_key_exists(self::PERIMETRE_FONCTION, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . self::PERIMETRE_FONCTION . " n'est pas présent dans le contexte");
                        }
                        /**  @var Fonction $fonction */
                        $fonction = $contexte[self::PERIMETRE_FONCTION];
                        $destinataires[] = $fonction->getPersonne()->getId();
                        break;
                    case self::DESTINATAIRE_BENEFICIAIRE_ATTRIBUTION_FONCTION_ELECTIVE:

                        if (!array_key_exists(self::PERIMETRE_ATTRIBUTION_FONCTION_ELECTIVE, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . self::PERIMETRE_ATTRIBUTION_FONCTION_ELECTIVE . " n'est pas présent dans le contexte");
                        }
                        /**  @var AttributionFonctionElective $attributionFonctionElective */
                        $attributionFonctionElective = $contexte[self::PERIMETRE_ATTRIBUTION_FONCTION_ELECTIVE];
                        $destinataires[] = $attributionFonctionElective->getPersonne()->getId();
                        break;
                    case self::DESTINATAIRE_CONSULTATION_CR:

                        if (!array_key_exists(self::PERIMETRE_COMPTERENDU, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . self::PERIMETRE_COMPTERENDU . " n'est pas présent dans le contexte");
                        }
                        /**  @var CompteRenduInstance $compteRendu */
                        $compteRendu = $contexte[self::PERIMETRE_COMPTERENDU];
                        $droitStructure = $compteRendu->getStructure()->getDroitsSuperieursOuEquivalents(StructureVoter::CONSULTER);
                        /** @var Droit $droit */
                        foreach ($droitStructure as $droit) {
                            $destinataires[] = $droit->getPersonne()->getId();
                        }
                        break;

                        break;
                    case self::DESTINATAIRE_SAISIE_CR:
                        if (!array_key_exists(self::PERIMETRE_COMPTERENDU, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . self::PERIMETRE_COMPTERENDU . " n'est pas présent dans le contexte");
                        }
                        /**  @var CompteRenduInstance $compteRendu */
                        $compteRendu = $contexte[self::PERIMETRE_COMPTERENDU];
                        $droitStructure = $compteRendu->getStructure()->getDroitsSuperieursOuEquivalents(StructureVoter::SAISIR);
                        /** @var Droit $droit */
                        foreach ($droitStructure as $droit) {
                            $destinataires[] = $droit->getPersonne()->getId();
                        }
                        break;
                    case self::DESTINATAIRE_VALIDATION_CR:
                        if (!array_key_exists(self::PERIMETRE_COMPTERENDU, $contexte)) {
                            throw new Exception("Erreur lors de l'envoi de la notification " . $nom_modele_notif . ": l'élément " . self::PERIMETRE_COMPTERENDU . " n'est pas présent dans le contexte");
                        }
                        /**  @var CompteRenduInstance $compteRendu */
                        $compteRendu = $contexte[self::PERIMETRE_COMPTERENDU];
                        $droitStructure = $compteRendu->getStructure()->getDroitsSuperieursOuEquivalents(StructureVoter::VALIDER);
                        /** @var Droit $droit */
                        foreach ($droitStructure as $droit) {
                            $destinataires[] = $droit->getPersonne()->getId();
                        }
                        break;
                }
            }
        }
        return (array_values(array_unique($destinataires)));
    }


    public function envoiNotifications(MessageNotification $message)
    {

        $destinataires = $this->personneRepository->findWhereIdIn($message->getDestinataires());
        /**  @var Personne $destinataire */
        foreach ($destinataires as $destinataire) {
            //On envoie les notifications uniquement aux personnes adhérentes
            if (($destinataire->estAdherentValide())
                || ($destinataire->estSalarieValide())
            ) {

                $this->envoiNotification($destinataire, $message->getContenu(), $message->getLien(), $message->getTitre());
            }
        }
    }

    public function envoiNotification(Personne $personne, String $contenu, ?String $lien, string $titre)
    {
        $notification = new Notification();

        $notification->setPersonne($personne);
        $notification->setContenu($contenu);

        $notification->setLien($lien);
        $notification->setTitre($titre);

        $notification->setLu(false);

        $currentDate = new DateTime();
        $notification->setDate($currentDate);
        $this->manager->persist($notification);
        $this->manager->flush();
    }
}
