<?php

namespace App\Service;

use App\Entity\Adhesion;
use App\Entity\CompteRenduInstance;
use App\Entity\Droit;
use App\Entity\Equipe;
use App\Entity\Fonction;
use App\Entity\Personne;
use App\Entity\SaisonDemocratique;
use App\Entity\Structure;
use App\Repository\PersonneRepository;
use App\Repository\StructureRepository;
use App\Security\Voter\StructureVoter;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * Helper pour la gestion des droits dans Agora
 * @author Antoine Faysse <antoine.faysse@gwenan.net>
 */
class BoutonsAccesRapideHomePageHelper
{
    public function __construct(private UrlGeneratorInterface $router, private AuthorizationCheckerInterface $authorizationChecker, private WorkflowInterface $compteRenduInstanceWorkflow)
    {
    }
    public function genererListeBoutonsAccesRapide(?Structure $structureActive, SaisonDemocratique $saisonCourante, String $perimetre): array
    {
        $boutonsAccesRapide = array();
        switch ($perimetre) {
            case Personne::TYPE_PERIMETRE_ADMIN:
                $boutonsAccesRapide =
                    [
                        [
                            'path' => $this->router->generate('gestion_structures_sla'),
                            'texte' => "<b>Voir</b><br/> les compte-rendus des APL"
                        ],
                        [
                            'path' => $this->router->generate('gestion_structures_regions'),
                            'texte' => "<b>Voir</b><br/> les compte-rendus des congrès"
                        ], [
                            'path' => $this->router->generate('gestion_structures_slan'),
                            'texte' => "<b>Voir</b><br/> les compte-rendus des APL des structures nationales"
                        ], [
                            'path' => $this->router->generate('administration_delegues_suppleants_ag_export'),
                            'texte' => "<b>Télécharger</b><br/> la liste des délégué·es et suppléant·es à l'AG"
                        ]
                    ];
                break;
            case Personne::TYPE_PERIMETRE_NATIONAL:
                $boutonsAccesRapide =
                    [
                        [
                            'path' => $this->router->generate('gestion_structures_sla'),
                            'texte' => "<b>Voir</b><br/> les compte-rendus des APL"
                        ],
                        [
                            'path' => $this->router->generate('gestion_structures_regions'),
                            'texte' => "<b>Voir</b><br/> les compte-rendus des congrès"
                        ], [
                            'path' => $this->router->generate('gestion_structures_slan'),
                            'texte' => "<b>Voir</b><br/> les compte-rendus des APL des structures nationales"
                        ]
                    ];
                break;
            case Personne::TYPE_PERIMETRE_SLAN:
                $compteRenduAPLAnneeCourante = $structureActive->getCompteRenduInstancePourSaison($saisonCourante);
                $compteRenduCongresAnneeCourante = null;
                $compteRenduCongresAnneePrecedente = null;
                if ($structureActive->getStructureRegionaleAffiliee()) {
                    $compteRenduCongresAnneeCourante = $structureActive->getStructureRegionaleAffiliee()->getCompteRenduInstancePourSaison($saisonCourante);
                }
                $saisonPrecedente = $saisonCourante->getSaisonPrecedente();
                $compteRenduAPLAnneePrecedente = null;

                if ($saisonPrecedente) {
                    if ($structureActive->getStructureRegionaleAffiliee()) {
                        $compteRenduCongresAnneePrecedente = $structureActive->getStructureRegionaleAffiliee()->getCompteRenduInstancePourSaison($saisonPrecedente);
                    }
                    $compteRenduAPLAnneePrecedente = $structureActive->getCompteRenduInstancePourSaison($saisonPrecedente);
                }

                $boutonsAccesRapideAPL = $this->boutonPourCRUnique($structureActive, $saisonPrecedente, $compteRenduAPLAnneeCourante, $compteRenduAPLAnneePrecedente);
                $boutonsAccesRapideCongres = [];
                if ($structureActive->getStructureRegionaleAffiliee()) {
                    $boutonsAccesRapideCongres = $this->boutonPourCRUnique($structureActive->getStructureRegionaleAffiliee(), $saisonPrecedente, $compteRenduCongresAnneeCourante, $compteRenduCongresAnneePrecedente);
                }
                $boutonsAccesRapide = array_merge($boutonsAccesRapide, $boutonsAccesRapideAPL, $boutonsAccesRapideCongres);

                break;
            case Personne::TYPE_PERIMETRE_REGION:
                $compteRenduCongresAnneeCourante = $structureActive->getCompteRenduInstancePourSaison($saisonCourante);
                $saisonPrecedente = $saisonCourante->getSaisonPrecedente();
                $compteRenduCongresAnneePrecedente = null;
                if ($saisonPrecedente) {
                    $compteRenduCongresAnneePrecedente = $structureActive->getCompteRenduInstancePourSaison($saisonPrecedente);
                }
                $boutonsAccesRapide = $this->boutonPourCRUnique($structureActive, $saisonPrecedente, $compteRenduCongresAnneeCourante, $compteRenduCongresAnneePrecedente);
                $boutonsAccesRapide[] = [
                    'path' => $this->router->generate('gestion_structures_sla'),
                    'texte' => "<b>Voir</b><br/> les comptes-rendus des APL de la région"
                ];
                break;
            case Personne::TYPE_PERIMETRE_SLA:
                $compteRenduAPLAnneeCourante = $structureActive->getCompteRenduInstancePourSaison($saisonCourante);
                $compteRenduCongresAnneeCourante = $structureActive->getStructureParent()->getCompteRenduInstancePourSaison($saisonCourante);

                $saisonPrecedente = $saisonCourante->getSaisonPrecedente();
                $compteRenduAPLAnneePrecedente = null;
                $compteRenduCongresAnneePrecedente = null;
                if ($saisonPrecedente) {
                    $compteRenduAPLAnneePrecedente = $structureActive->getCompteRenduInstancePourSaison($saisonPrecedente);
                    $compteRenduCongresAnneePrecedente = $structureActive->getStructureParent()->getCompteRenduInstancePourSaison($saisonPrecedente);
                }

                $boutonsAccesRapideAPL = $this->boutonPourCRUnique($structureActive, $saisonPrecedente, $compteRenduAPLAnneeCourante, $compteRenduAPLAnneePrecedente);
                $boutonsAccesRapideCongres = $this->boutonPourCRUnique($structureActive->getStructureParent(), $saisonPrecedente, $compteRenduCongresAnneeCourante, $compteRenduCongresAnneePrecedente);

                $boutonsAccesRapide = array_merge($boutonsAccesRapide, $boutonsAccesRapideAPL, $boutonsAccesRapideCongres);

                break;
        }
        return $boutonsAccesRapide;
    }

    public function boutonPourCRUnique(Structure $structure, ?SaisonDemocratique $saisonPrecedente, CompteRenduInstance $compteRenduAnneeCourante, ?CompteRenduInstance $compteRenduAnneePrecedente): array
    {


        switch ($compteRenduAnneeCourante->getType()) {
            case CompteRenduInstance::TYPE_CONGRES:
                $texteInstance = "du Congrès Régional";
                break;
            case CompteRenduInstance::TYPE_APL_SLA:
            case CompteRenduInstance::TYPE_APL_SLAN:
                $texteInstance = "de l'APL";
                break;
        }
        $boutonsAccesRapide = array();
        $etatCompteRendu = $this->compteRenduInstanceWorkflow->getMarking($compteRenduAnneeCourante);
        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI)) {
            if ($this->authorizationChecker->isGranted(StructureVoter::SAISIR, $structure)) {
                $boutonsAccesRapide[] = [
                    'path' => $this->router->generate('compte_rendu_action', ['action' => 'saisie', 'compteRendu' => $compteRenduAnneeCourante->getId()]),
                    'texte' => "<b>Saisir</b><br/> le compte-rendu " . $texteInstance
                ];
            } else if ($this->authorizationChecker->isGranted(StructureVoter::CONSULTER, $structure)) {
                if ($saisonPrecedente) {
                    $boutonsAccesRapide[] = [
                        'path' => $this->router->generate('compte_rendu_action', ['action' => 'consultation', 'compteRendu' => $compteRenduAnneePrecedente->getId()]),
                        'texte' => "<b>Voir</b><br/> le compte-rendu " . $texteInstance . " " . $saisonPrecedente->getNom()
                    ];
                }
            }
        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_ATTENTE_VALIDATION)) {
            if ($this->authorizationChecker->isGranted(StructureVoter::VALIDER, $structure)) {
                $boutonsAccesRapide[] = [
                    'path' => $this->router->generate('compte_rendu_action', ['action' => 'consultation', 'compteRendu' => $compteRenduAnneeCourante->getId()]),
                    'texte' => "<b>Vérifier</b><br/> le compte-rendu " . $texteInstance
                ];
            } else   if ($this->authorizationChecker->isGranted(StructureVoter::SAISIR, $structure)) {
                $boutonsAccesRapide[] = [
                    'path' => $this->router->generate('compte_rendu_action', ['action' => 'saisie', 'compteRendu' => $compteRenduAnneeCourante->getId()]),
                    'texte' => "<b>Modifier</b><br/> le compte-rendu " . $texteInstance
                ];
            } else   if ($this->authorizationChecker->isGranted(StructureVoter::CONSULTER, $structure)) {
                if ($saisonPrecedente) {
                    $boutonsAccesRapide[] = [
                        'path' => $this->router->generate('compte_rendu_action', ['action' => 'consultation', 'compteRendu' => $compteRenduAnneePrecedente->getId()]),
                        'texte' => "<b>Voir</b><br/> le compte-rendu " . $texteInstance . " " . $saisonPrecedente->getNom()
                    ];
                }
            }
        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_VALIDE)) {
            if ($this->authorizationChecker->isGranted(StructureVoter::SAISIR, $structure)) {

                $boutonsAccesRapide[] = [
                    'path' => $this->router->generate('compte_rendu_action', ['action' => 'saisie', 'compteRendu' => $compteRenduAnneeCourante->getId()]),
                    'texte' => "<b>Corriger</b><br/> le compte-rendu " . $texteInstance
                ];
            } else         if ($this->authorizationChecker->isGranted(StructureVoter::CONSULTER, $structure)) {
                if ($saisonPrecedente) {
                    $boutonsAccesRapide[] = [
                        'path' => $this->router->generate('compte_rendu_action', ['action' => 'consultation', 'compteRendu' => $compteRenduAnneePrecedente->getId()]),
                        'texte' => "<b>Voir</b><br/> le compte-rendu " . $texteInstance . " " . $saisonPrecedente->getNom()
                    ];
                }
            }
        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_VALIDE)) {

            if ($this->authorizationChecker->isGranted(StructureVoter::CONSULTER, $structure)) {
                $boutonsAccesRapide[] = [
                    'path' => $this->router->generate('compte_rendu_action', ['action' => 'consultation', 'compteRendu' => $compteRenduAnneeCourante->getId()]),
                    'texte' => "<b>Voir</b><br/> le compte-rendu " . $texteInstance
                ];
            }
        }
        return $boutonsAccesRapide;
    }
}
