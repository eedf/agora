<?php

namespace App\Controller;

use App\Entity\AttributionFonctionElective;
use App\Entity\CompteRenduInstance;
use App\Entity\ElectionNominative;
use App\Entity\Fonction;
use App\Entity\Personne;
use App\Entity\SaisonDemocratique;
use App\Entity\Structure;
use App\Entity\VoteInstance;
use App\Form\CompteRenduAPLDocumentsType;
use App\Form\CompteRenduAPLPreReformeType;
use App\Form\CompteRenduAPLType;
use App\Form\CompteRenduCongresDocumentsType;
use App\Form\CompteRenduCongresPreReformeType;
use App\Form\CompteRenduCongresType;
use App\Form\InvalidationCompteRenduType;
use App\Form\ValidationCompteRenduAPLPreReformeType;
use App\Form\ValidationCompteRenduAPLType;
use App\Form\ValidationCompteRenduCongresPreReformeType;
use App\Form\ValidationCompteRenduCongresType;
use App\Repository\PersonneRepository;
use App\Repository\SaisonDemocratiqueRepository;
use App\Security\Voter\StructureVoter;
use App\Service\AttributionFonctionHelper;
use App\Service\EcritureFonctionJeitoHelper;
use App\Service\NotificationHelper;
use App\Service\RedirectionHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\WorkflowInterface;

class CompteRenduController extends AbstractController
{
    #[Route('/compte/rendu', name: 'app_compte_rendu')]
    public function index(): Response
    {
        return $this->render('compte_rendu/index.html.twig', [
            'controller_name' => 'CompteRenduController',
        ]);
    }

    #[Route('/compte/rendu/indisponible', name: 'compte_rendu_indisponible')]
    public function compteRenduInsdiponible(): Response
    {
        return $this->render('compte_rendu/indispo.html.twig', []);
    }

    #[Route('/compte/rendu/{action}/{compteRendu}', name: 'compte_rendu_action')]
    public function compteRenduAction(String $action, CompteRenduInstance $compteRendu): Response
    {
        switch ($compteRendu->getType()) {
            case CompteRenduInstance::TYPE_APL_SLA:
                $type_cr = "apl_sla";
                break;
            case CompteRenduInstance::TYPE_APL_SLAN:
                $type_cr = "apl_sla";
                break;
            case CompteRenduInstance::TYPE_CONGRES:
                $type_cr = "congres";
                break;
        }
        $route = $action . "_cr_" . $type_cr;

        return $this->redirectToRoute($route, ['compteRendu' => $compteRendu->getId()]);
    }

    #[Route('/compte/rendu/region/{compteRendu}/saisie', name: 'saisie_cr_congres')]
    public function saisieCompteRenduCongres(
        Request $request,
        MailerInterface $mailer,
        String $adresseMailSupport,
        RedirectionHelper $redirectionHelper,
        NotificationHelper $notificationHelper,
        AttributionFonctionHelper $attributionFonctionHelper,
        EntityManagerInterface $manager,
        EcritureFonctionJeitoHelper $ecritureFonctionJeitoHelper,
        WorkflowInterface $compteRenduInstanceWorkflow,
        CompteRenduInstance $compteRendu,
        PersonneRepository $personneRepository
    ): Response {
        $this->denyAccessUnlessGranted(StructureVoter::SAISIR, $compteRendu->getStructure());
        /** @var Personne $personne */
        $personne = $this->getUser();
        $personne = $personneRepository->findOneBy(["id" => $personne->getId()]);
        $dateCourante = new DateTime();
        $mode = "saisie";
        $etatCompteRendu = $compteRenduInstanceWorkflow->getMarking($compteRendu);
        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_VALIDE) || $etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_SAISON_PASSEE)) {
            $this->denyAccessUnlessGranted(StructureVoter::MODIFIER_APRES_VALIDATION, $compteRendu->getStructure());
            $mode = StructureVoter::MODIFIER_APRES_VALIDATION;
        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_PAS_D_INSTANCE)) {
            $this->denyAccessUnlessGranted(StructureVoter::VALIDER, $compteRendu->getStructure());
            $mode = StructureVoter::MODIFIER_APRES_VALIDATION;
        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI)) {
            $mode = CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI;
        }

        //Vérifier que la structure est une Région
        if (!$compteRendu->getStructure()->estUneRegion()) {

            $this->addFlash('danger', "Ce compte-rendu n'est pas celui d'une région, il ne peut pas être saisi.");
            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }
        //On caclule le nombre de délégués en fonction du nombre d'adhérents
        if ($compteRendu->getElectionsNominatives()->count() == 0) {
            $compteRendu->ajouterFonctionElectives();
        }

        if ($compteRendu->getNombreVotantsSpecifiques()->count() == 0) {
            $compteRendu->ajouterNombreVotantsSpecifiques();
        }
        if ($compteRendu->getSaison()->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
            $form_saisie_cr_congres = $this->createForm(CompteRenduCongresPreReformeType::class, $compteRendu, ["mode" => $mode]);
        } else {
            $form_saisie_cr_congres = $this->createForm(CompteRenduCongresType::class, $compteRendu, ["mode" => $mode]);
        }
        $form_saisie_cr_congres->handleRequest($request);
        if ($form_saisie_cr_congres->isSubmitted()) {
            if ($form_saisie_cr_congres->isValid()) {
                try {
                    $etatCompteRendu = $compteRenduInstanceWorkflow->getMarking($compteRendu);
                    $compteRendu->setDateModification($dateCourante);
                    $compteRendu->setDernierePersonneSoummetteur($personne);
                    if ($compteRendu->isNaPasEuLieu()) {
                        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_NON_SAISI_PAS_D_INSTANCE);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_ATTENTE_VALIDATION)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_ATTENTE_VALIDATION_PAS_D_INSTANCE);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_VALIDE)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_INVALIDE_PAS_D_INSTANCE);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_VALIDE)) {
                            $compteRendu = $attributionFonctionHelper->genererAttributionFonctionsElectives($compteRendu);
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_VALIDE_PAS_D_INSTANCE);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_SAISON_PASSEE)) {
                            //On ne change pas d'état, mais on supprime les attributions de fonction élective 
                            $compteRendu = $attributionFonctionHelper->genererAttributionFonctionsElectives($compteRendu);
                        }
                        //Si l'APL n'a pas eu lieu, la structure est rattachée
                        $compteRendu->setAutonomieStructure(Structure::STATUT_STRUCTURE_RATTACHEE);
                        $manager->persist($compteRendu);
                        $manager->flush();
                        try {
                            $ecritureFonctionJeitoHelper->ecritureCompteRendu($compteRendu);
                        } catch (Exception $e) {
                            $this->envoiMailErreurSupport($e, $compteRendu, $mailer, $adresseMailSupport);
                        }


                        $this->addFlash("success", "Le compte rendu de congrès a été enregistré");
                    } else {

                        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_SOUMISSION_VALIDATION);
                            $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_003, [
                                NotificationHelper::PERIMETRE_COMPTERENDU => $compteRendu
                            ]);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_ATTENTE_VALIDATION)) {
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_VALIDE)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_RESSOUMISSION);
                            $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_003, [
                                NotificationHelper::PERIMETRE_COMPTERENDU => $compteRendu
                            ]);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_PAS_D_INSTANCE)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_PAS_D_INSTANCE_ATTENTE_VALIDATION);
                            $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_003, [
                                NotificationHelper::PERIMETRE_COMPTERENDU => $compteRendu
                            ]);
                        } else if ($mode == StructureVoter::MODIFIER_APRES_VALIDATION) {
                            $compteRendu = $attributionFonctionHelper->genererAttributionFonctionsElectives($compteRendu);
                        }
                        $manager->persist($compteRendu);
                        $manager->flush();
                        if ($mode == StructureVoter::MODIFIER_APRES_VALIDATION) {

                            try {
                                $ecritureFonctionJeitoHelper->ecritureCompteRendu($compteRendu);
                            } catch (Exception $e) {
                                $this->envoiMailErreurSupport($e, $compteRendu, $mailer, $adresseMailSupport);
                            }
                            $this->addFlash("success", "Le compte rendu de congrès a été enregistré, les données ont été modifiées dans Jéito");
                        } else if ($mode == CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI) {
                            $this->addFlash("success", "Le compte rendu de congrès a été enregistré, vous pouvez y ajouter des documents");
                            return $this->redirectToRoute("saisie_documents_cr_congres", ['compteRendu' => $compteRendu->getId()]);
                        } else {
                            $this->addFlash("success", "Le compte rendu de congrès a été enregistré");
                        }
                    }
                    return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'enregistrement: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger', "Données incorrectes, veuillez corriger ");
            }
        }


        return $this->render('compte_rendu/congres/saisie.html.twig', [
            'form_saisie_cr_congres' => $form_saisie_cr_congres->createView(),
            'compteRendu' => $compteRendu
        ]);
    }

    #[Route('/compte/rendu/documents/congres/{compteRendu}/saisie', name: 'saisie_documents_cr_congres')]
    public function saisieDocumentsCompteRenduCongres(
        Request $request,
        RedirectionHelper $redirectionHelper,
        EntityManagerInterface $manager,
        CompteRenduInstance $compteRendu,
        WorkflowInterface $compteRenduInstanceWorkflow
    ): Response {
        $this->denyAccessUnlessGranted(StructureVoter::SAISIR, $compteRendu->getStructure());

        //Vérifier que la structure est une Région
        if (!$compteRendu->getStructure()->estUneRegion()) {

            $this->addFlash('danger', "Ce compte-rendu n'est pas celui d'une région, il ne peut pas être saisi.");
            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }
        $etatCompteRendu = $compteRenduInstanceWorkflow->getMarking($compteRendu);
        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_PAS_D_INSTANCE)) {
            return $this->redirectToRoute("consultation_cr_congres", ["compteRendu" => $compteRendu->getId()]);
        }
        $form_saisie_documents_cr_congres = $this->createForm(CompteRenduCongresDocumentsType::class, $compteRendu, []);

        $form_saisie_documents_cr_congres->handleRequest($request);
        if ($form_saisie_documents_cr_congres->isSubmitted()) {
            if ($form_saisie_documents_cr_congres->isValid()) {
                try {
                    $manager->persist($compteRendu);
                    $manager->flush();

                    $this->addFlash("success", "Les documents du compte rendu de congrès ont été enregistrés");
                    return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'enregistrement des documents: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger', "Données incorrectes, veuillez corriger ");
            }
        }

        return $this->render('compte_rendu/congres/saisie_documents.html.twig', [
            'form_saisie_documents_cr_congres' => $form_saisie_documents_cr_congres->createView(),
            'compteRendu' => $compteRendu
        ]);
    }

    #[Route('/compte/rendu/slan/{compteRendu}/consultation', name: 'consultation_cr_apl_slan')]
    #[Route('/compte/rendu/sla/{compteRendu}/consultation', name: 'consultation_cr_apl_sla')]
    public function consultationCompteRenduAPLSLA(CompteRenduInstance $compteRendu): Response
    {
        $this->denyAccessUnlessGranted(StructureVoter::CONSULTER, $compteRendu->getStructure());

        return $this->render('compte_rendu/apl/consultation.html.twig', [
            'compteRendu' => $compteRendu
        ]);
    }

    #[Route('/compte/rendu/region/{compteRendu}/consultation', name: 'consultation_cr_congres')]
    public function consultationCompteRenduCongres(CompteRenduInstance $compteRendu): Response
    {
        $this->denyAccessUnlessGranted(StructureVoter::CONSULTER, $compteRendu->getStructure());

        return $this->render('compte_rendu/congres/consultation.html.twig', [
            'compteRendu' => $compteRendu
        ]);
    }
    #[Route('/compte/rendu/slan/{compteRendu}/saisie', name: 'saisie_cr_apl_slan')]
    #[Route('/compte/rendu/sla/{compteRendu}/saisie', name: 'saisie_cr_apl_sla')]
    public function saisieCompteRenduAPL(
        Request $request,
        RedirectionHelper $redirectionHelper,
        MailerInterface $mailer,
        String $adresseMailSupport,
        NotificationHelper $notificationHelper,
        AttributionFonctionHelper $attributionFonctionHelper,
        EntityManagerInterface $manager,
        EcritureFonctionJeitoHelper $ecritureFonctionJeitoHelper,
        CompteRenduInstance $compteRendu,
        WorkflowInterface $compteRenduInstanceWorkflow,
        PersonneRepository $personneRepository
    ): Response {
        $this->denyAccessUnlessGranted(StructureVoter::SAISIR, $compteRendu->getStructure());
        /** @var Personne $personne */
        $personne = $this->getUser();
        $personne = $personneRepository->findOneBy(["id" => $personne->getId()]);

        $dateCourante = new DateTime();

        $mode = "saisie";
        //Un compte rendu déjà validé ou de l'an passé n'est accessible à la saisie que pour les personnes ayant le droit de modification apres validation
        $etatCompteRendu = $compteRenduInstanceWorkflow->getMarking($compteRendu);
        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_VALIDE) || $etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_SAISON_PASSEE)) {
            $this->denyAccessUnlessGranted(StructureVoter::MODIFIER_APRES_VALIDATION, $compteRendu->getStructure());
            $mode = StructureVoter::MODIFIER_APRES_VALIDATION;
        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_PAS_D_INSTANCE)) {
            $this->denyAccessUnlessGranted(StructureVoter::VALIDER, $compteRendu->getStructure());
            $mode = StructureVoter::MODIFIER_APRES_VALIDATION;
        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI)) {
            $mode = CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI;
        }
        //Vérifier que la structure est une SLA ou une SLAN
        if (!$compteRendu->getStructure()->estUneSLA() && !$compteRendu->getStructure()->estUneSLAN()) {
            if (!$compteRendu->getStructure()->estUneSLA() && !$compteRendu->getStructure()->estUneSLAN()) {
                $this->addFlash('danger', "Ce compte-rendu n'est pas celui d'une SLA ou SLAN, il ne peut pas être saisi.");
                return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
            }
        }
        //On caclule le nombre de délégués en fonction du nombre d'adhérents
        if ($compteRendu->getElectionsNominatives()->count() == 0) {
            $compteRendu->ajouterFonctionElectives();
        }
        if ($compteRendu->getNombreVotantsSpecifiques()->count() == 0) {
            $compteRendu->ajouterNombreVotantsSpecifiques();
        }
        if ($compteRendu->getSaison()->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
            $form_saisie_cr_apl = $this->createForm(CompteRenduAPLPreReformeType::class, $compteRendu, [
                "mode" => $mode
            ]);
        } else {
            $form_saisie_cr_apl = $this->createForm(CompteRenduAPLType::class, $compteRendu, [
                "mode" => $mode
            ]);
        }
        $form_saisie_cr_apl->handleRequest($request);
        if ($form_saisie_cr_apl->isSubmitted()) {
            if ($form_saisie_cr_apl->isValid()) {
                try {
                    $compteRendu->setDateModification($dateCourante);
                    $compteRendu->setDernierePersonneSoummetteur($personne);
                    if ($compteRendu->isNaPasEuLieu()) {
                        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_NON_SAISI_PAS_D_INSTANCE);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_ATTENTE_VALIDATION)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_ATTENTE_VALIDATION_PAS_D_INSTANCE);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_VALIDE)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_INVALIDE_PAS_D_INSTANCE);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_VALIDE)) {
                            $compteRendu = $attributionFonctionHelper->genererAttributionFonctionsElectives($compteRendu);
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_VALIDE_PAS_D_INSTANCE);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_SAISON_PASSEE)) {
                            //On ne change pas d'état, mais on supprime les attributions de fonction élective 
                            $compteRendu = $attributionFonctionHelper->genererAttributionFonctionsElectives($compteRendu);
                        }
                        //Si l'APL n'a pas eu lieu, la structure est rattachée
                        $compteRendu->setAutonomieStructure(Structure::STATUT_STRUCTURE_RATTACHEE);
                        $manager->persist($compteRendu);
                        $manager->flush();
                        try {
                            $ecritureFonctionJeitoHelper->ecritureCompteRendu($compteRendu);
                        } catch (Exception $e) {
                            $this->envoiMailErreurSupport($e, $compteRendu, $mailer, $adresseMailSupport);
                        }


                        $this->addFlash("success", "Le compte rendu d'APL a été enregistré");
                    } else {

                        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_SOUMISSION_VALIDATION);
                            $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_003, [
                                NotificationHelper::PERIMETRE_COMPTERENDU => $compteRendu
                            ]);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_PAS_D_INSTANCE)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_PAS_D_INSTANCE_ATTENTE_VALIDATION);
                            $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_003, [
                                NotificationHelper::PERIMETRE_COMPTERENDU => $compteRendu
                            ]);
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_ATTENTE_VALIDATION)) {
                        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_VALIDE)) {
                            $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_RESSOUMISSION);
                            $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_003, [
                                NotificationHelper::PERIMETRE_COMPTERENDU => $compteRendu
                            ]);
                        } else if ($mode == StructureVoter::MODIFIER_APRES_VALIDATION) {
                            $compteRendu = $attributionFonctionHelper->genererAttributionFonctionsElectives($compteRendu);
                        }
                        $manager->persist($compteRendu);
                        $manager->flush();


                        if ($mode == StructureVoter::MODIFIER_APRES_VALIDATION) {

                            try {
                                $ecritureFonctionJeitoHelper->ecritureCompteRendu($compteRendu);
                            } catch (Exception $e) {
                                $this->envoiMailErreurSupport($e, $compteRendu, $mailer, $adresseMailSupport);
                            }
                            $this->addFlash("success", "Le compte rendu d'APL a été enregistré, les données ont été modifiées dans Jéito");
                        } else if ($mode == CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI) {
                            $this->addFlash("success", "Le compte rendu d'APL a été enregistré, vous pouvez y ajouter des documents");
                            return $this->redirectToRoute("saisie_documents_cr_apl_sla", ['compteRendu' => $compteRendu->getId()]);
                        } else {
                            $this->addFlash("success", "Le compte rendu d'APL a été enregistré");
                        }
                    }

                    return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'enregistrement: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger', "Données incorrectes, veuillez corriger ");
            }
        }

        return $this->render('compte_rendu/apl/saisie.html.twig', [
            'form_saisie_cr_apl' => $form_saisie_cr_apl->createView(),
            'compteRendu' => $compteRendu
        ]);
    }

    #[Route('/compte/rendu/documents/slan/{compteRendu}/saisie', name: 'saisie_documents_cr_apl_slan')]
    #[Route('/compte/rendu/documents/sla/{compteRendu}/saisie', name: 'saisie_documents_cr_apl_sla')]
    public function saisieDocumentsCompteRenduAPL(
        Request $request,
        RedirectionHelper $redirectionHelper,
        EntityManagerInterface $manager,
        CompteRenduInstance $compteRendu,
        WorkflowInterface $compteRenduInstanceWorkflow
    ): Response {
        $this->denyAccessUnlessGranted(StructureVoter::SAISIR, $compteRendu->getStructure());

        //Vérifier que la structure est une SLA ou une SLAN
        if (!$compteRendu->getStructure()->estUneSLA() && !$compteRendu->getStructure()->estUneSLAN()) {
            if (!$compteRendu->getStructure()->estUneSLA() && !$compteRendu->getStructure()->estUneSLAN()) {
                $this->addFlash('danger', "Ce compte-rendu n'est pas celui d'une SLA ou SLAN, il ne peut pas être saisi.");
                return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
            }
        }
        $etatCompteRendu = $compteRenduInstanceWorkflow->getMarking($compteRendu);

        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_PAS_D_INSTANCE)) {
            return $this->redirectToRoute("consultation_cr_apl_sla", ["compteRendu" => $compteRendu->getId()]);
        }
        $form_saisie_documents_cr_apl = $this->createForm(CompteRenduAPLDocumentsType::class, $compteRendu, []);

        $form_saisie_documents_cr_apl->handleRequest($request);
        if ($form_saisie_documents_cr_apl->isSubmitted()) {
            if ($form_saisie_documents_cr_apl->isValid()) {
                try {
                    $manager->persist($compteRendu);
                    $manager->flush();

                    $this->addFlash("success", "Les documents du compte rendu d'APL ont été enregistrés");
                    return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'enregistrement des documents: " . $e->getMessage());
                }
            } else {
                $this->addFlash('danger', "Données incorrectes, veuillez corriger ");
            }
        }

        return $this->render('compte_rendu/apl/saisie_documents.html.twig', [
            'form_saisie_documents_cr_apl' => $form_saisie_documents_cr_apl->createView(),
            'compteRendu' => $compteRendu
        ]);
    }


    #[Route('/compte/rendu/region/{compteRendu}/validation', name: 'validation_cr_congres')]
    public function validationCompteRenduCongres(Request $request, MailerInterface $mailer, String $adresseMailSupport, RedirectionHelper $redirectionHelper, EcritureFonctionJeitoHelper $ecritureFonctionJeitoHelper, NotificationHelper $notificationHelper, EntityManagerInterface $manager, CompteRenduInstance $compteRendu, WorkflowInterface $compteRenduInstanceWorkflow, AttributionFonctionHelper $attributionFonctionHelper, PersonneRepository $personneRepository): Response
    {
        $this->denyAccessUnlessGranted(StructureVoter::VALIDER, $compteRendu->getStructure());
        if (!$compteRendu->getStructure()->estUneRegion()) {
            $this->addFlash('danger', "Ce compte-rendu n'est pas celui d'une région, il ne peut pas être validé");
            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }
        if (!($compteRenduInstanceWorkflow->can($compteRendu, CompteRenduInstance::TRANSITION_VALIDATION))) {
            $this->addFlash("danger", "Ce compte-rendu ne peut pas être validé");
            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }

        /** @var Personne $personne */
        $personne = $this->getUser();
        $personne = $personneRepository->findOneBy(["id" => $personne->getId()]);
        $dateCourante = new DateTime();


        //Vérifier que la structure est une SLA ou une SLAN
        if ($compteRendu->getSaison()->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
            $form_validation_cr_congres = $this->createForm(ValidationCompteRenduCongresPreReformeType::class, $compteRendu);
        } else {
            $form_validation_cr_congres = $this->createForm(ValidationCompteRenduCongresType::class, $compteRendu);
        }
        $form_validation_cr_congres->handleRequest($request);

        if ($form_validation_cr_congres->isSubmitted()) {
            if ($form_validation_cr_congres->isValid() && ($compteRenduInstanceWorkflow->can($compteRendu, CompteRenduInstance::TRANSITION_VALIDATION))) {
                try {

                    $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_VALIDATION);
                    if ($compteRendu->getSaison()->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
                        if ($form_validation_cr_congres->has('validationResponsable')) {
                            if ($form_validation_cr_congres->get('validationResponsable')->getData()) {
                                $statusResponsable = ElectionNominative::STATUS_VALIDE;
                            } else {
                                $statusResponsable = ElectionNominative::STATUS_REFUSE;
                            }
                            foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_RR) as $electionRR) {
                                $electionRR->setStatus($statusResponsable);
                            }
                        }



                        if ($form_validation_cr_congres->has('validationTresorier')) {
                            if ($form_validation_cr_congres->get('validationTresorier')->getData()) {
                                $statusTresorier = ElectionNominative::STATUS_VALIDE;
                            } else {
                                $statusTresorier = ElectionNominative::STATUS_REFUSE;
                            }
                            foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TR) as $electionTR) {
                                $electionTR->setStatus($statusTresorier);
                            }
                        }
                    } else {
                        if ($form_validation_cr_congres->has('validationMissionOrganisation')) {
                            if ($form_validation_cr_congres->get('validationMissionOrganisation')->getData()) {
                                $statusOrganisation = ElectionNominative::STATUS_VALIDE;
                            } else {
                                $statusOrganisation = ElectionNominative::STATUS_REFUSE;
                            }
                            foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_ORGANISATION_REGION) as $electionOrganisation) {
                                $electionOrganisation->setStatus($statusOrganisation);
                            }
                        }


                        if ($form_validation_cr_congres->has('validationMissionTresorerie')) {
                            if ($form_validation_cr_congres->get('validationMissionTresorerie')->getData()) {
                                $statusTresorerie = ElectionNominative::STATUS_VALIDE;
                            } else {
                                $statusTresorerie = ElectionNominative::STATUS_REFUSE;
                            }
                            foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TRESORERIE_REGION) as $electionTresorerie) {
                                $electionTresorerie->setStatus($statusTresorerie);
                            }
                        }
                    }


                    //Pour les autres postes, la validation est automatique
                    foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_DELEGUE_AG) as $electionDelegue) {
                        $electionDelegue->setStatus(ElectionNominative::STATUS_VALIDE);
                    }
                    foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_SUPPLEANT_AG) as $electionSuppleant) {
                        $electionSuppleant->setStatus(ElectionNominative::STATUS_VALIDE);
                    }
                    foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_REPRESENTATION_REGION) as $electionRepresentation) {
                        $electionRepresentation->setStatus(ElectionNominative::STATUS_VALIDE);
                    }
                    foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_COORDINATION_REGION) as $electionCoordinateur) {
                        $electionCoordinateur->setStatus(ElectionNominative::STATUS_VALIDE);
                    }

                    $compteRendu = $attributionFonctionHelper->genererAttributionFonctionsElectives($compteRendu);

                    $compteRendu->setDateValidation($dateCourante);
                    $compteRendu->setPersonneValideuse($personne);
                    $compteRendu->setCommentaireDestinationValidation(null);
                    $compteRendu->setCommentaireInvalidation(null);
                    $manager->persist($compteRendu);
                    $manager->flush();
                    try {
                        $ecritureFonctionJeitoHelper->ecritureCompteRendu($compteRendu);
                    } catch (Exception $e) {
                        $this->envoiMailErreurSupport($e, $compteRendu, $mailer, $adresseMailSupport);
                    }
                    $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_001, [
                        NotificationHelper::PERIMETRE_COMPTERENDU => $compteRendu
                    ]);
                    $this->addFlash("success", "Le compte rendu de congrès a été validé");
                    return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de la validation :" . $e->getMessage());
                }
            } else {
                $this->addFlash('danger', "Données incorrectes, veuillez corriger ");
            }
        }

        return $this->render('compte_rendu/congres/validation.html.twig', [
            'form_validation_cr_congres' => $form_validation_cr_congres->createView(),
            'compteRendu' => $compteRendu
        ]);
    }

    #[Route('/compte/rendu/slan/{compteRendu}/validation', name: 'validation_cr_apl_slan')]
    #[Route('/compte/rendu/sla/{compteRendu}/validation', name: 'validation_cr_apl_sla')]
    public function validationCompteRenduAPLSLA(Request $request, MailerInterface $mailer, String $adresseMailSupport, NotificationHelper $notificationHelper, EcritureFonctionJeitoHelper $ecritureFonctionJeitoHelper, RedirectionHelper $redirectionHelper, AttributionFonctionHelper $attributionFonctionHelper, EntityManagerInterface $manager, PersonneRepository $personneRepository, CompteRenduInstance $compteRendu, WorkflowInterface $compteRenduInstanceWorkflow): Response
    {
        $this->denyAccessUnlessGranted(StructureVoter::VALIDER, $compteRendu->getStructure());
        if (!($compteRenduInstanceWorkflow->can($compteRendu, CompteRenduInstance::TRANSITION_VALIDATION))) {
            $this->addFlash("danger", "Ce compte-rendu ne peut pas être validé");
            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }
        //Vérifier que la structure est une SLA ou une SLAN
        if (!$compteRendu->getStructure()->estUneSLA() && !$compteRendu->getStructure()->estUneSLAN()) {
            $this->addFlash('danger', "Ce compte-rendu n'est pas celui d'une SLA ou SLAN, il ne peut pas être validé");
            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }
        /** @var Personne $personne */
        $personne = $this->getUser();
        $personne = $personneRepository->findOneBy(["id" => $personne->getId()]);
        $dateCourante = new DateTime();



        if ($compteRendu->getSaison()->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
            $form_validation_cr_apl = $this->createForm(ValidationCompteRenduAPLPreReformeType::class, $compteRendu);
        } else {
            $form_validation_cr_apl = $this->createForm(ValidationCompteRenduAPLType::class, $compteRendu);
        }

        $form_validation_cr_apl->handleRequest($request);

        if ($form_validation_cr_apl->isSubmitted()) {
            if ($form_validation_cr_apl->isValid() && ($compteRenduInstanceWorkflow->can($compteRendu, CompteRenduInstance::TRANSITION_VALIDATION))) {


                $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_VALIDATION);

                if ($compteRendu->getSaison()->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
                    if ($form_validation_cr_apl->has('validationResponsable')) {
                        if ($form_validation_cr_apl->get('validationResponsable')->getData()) {
                            $statusResponsable = ElectionNominative::STATUS_VALIDE;
                        } else {
                            $statusResponsable = ElectionNominative::STATUS_REFUSE;
                        }
                        foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_RESLA) as $electionRESLA) {
                            $electionRESLA->setStatus($statusResponsable);
                        }
                    }


                    if ($form_validation_cr_apl->has('validationTresorier')) {
                        if ($form_validation_cr_apl->get('validationTresorier')->getData()) {
                            $statusTresorier = ElectionNominative::STATUS_VALIDE;
                        } else {
                            $statusTresorier = ElectionNominative::STATUS_REFUSE;
                        }
                        foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TSLA) as $electionTSLA) {
                            $electionTSLA->setStatus($statusTresorier);
                        }
                    }
                    //Pour les autres postes, la validation est automatique
                    foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_DELEGUE_AG) as $electionDelegue) {
                        $electionDelegue->setStatus(ElectionNominative::STATUS_VALIDE);
                    }
                    foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_SUPPLEANT_AG) as $electionSuppleant) {
                        $electionSuppleant->setStatus(ElectionNominative::STATUS_VALIDE);
                    }
                } else {

                    if ($form_validation_cr_apl->has('validationMissionOrganisation')) {
                        if ($form_validation_cr_apl->get('validationMissionOrganisation')->getData()) {
                            $statusOrganisation = ElectionNominative::STATUS_VALIDE;
                        } else {
                            $statusOrganisation = ElectionNominative::STATUS_REFUSE;
                        }
                        foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_ORGANISATION_SLA) as $electionOrganisation) {
                            $electionOrganisation->setStatus($statusOrganisation);
                        }
                    }


                    if ($form_validation_cr_apl->has('validationMissionTresorerie')) {
                        if ($form_validation_cr_apl->get('validationMissionTresorerie')->getData()) {
                            $statusTresorerie = ElectionNominative::STATUS_VALIDE;
                        } else {
                            $statusTresorerie = ElectionNominative::STATUS_REFUSE;
                        }
                        foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TRESORERIE_SLA) as $electionTresorerie) {
                            $electionTresorerie->setStatus($statusTresorerie);
                        }
                    }
                    //Pour les autres postes, la validation est automatique
                    foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_REPRESENTATION_SLA) as $electionRepresentation) {
                        $electionRepresentation->setStatus(ElectionNominative::STATUS_VALIDE);
                    }
                    foreach ($compteRendu->getElectionsNominativesPourPoste(ElectionNominative::POSTE_COORDINATION_SLA) as $electionCoordinateur) {
                        $electionCoordinateur->setStatus(ElectionNominative::STATUS_VALIDE);
                    }
                }


                $compteRendu = $attributionFonctionHelper->genererAttributionFonctionsElectives($compteRendu);

                $compteRendu->setDateValidation($dateCourante);
                $compteRendu->setPersonneValideuse($personne);
                $compteRendu->setCommentaireDestinationValidation(null);
                $compteRendu->setCommentaireInvalidation(null);
                $manager->persist($compteRendu);
                $manager->flush();
                try {
                    $ecritureFonctionJeitoHelper->ecritureCompteRendu($compteRendu);
                } catch (Exception $e) {
                    $this->envoiMailErreurSupport($e, $compteRendu, $mailer, $adresseMailSupport);
                }

                $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_001, [
                    NotificationHelper::PERIMETRE_COMPTERENDU => $compteRendu
                ]);

                $this->addFlash("success", "Le compte rendu d'APL a été validé");
                return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
            } else {
                $this->addFlash('danger', "Données incorrectes, veuillez corriger ");
            }
        }

        return $this->render('compte_rendu/apl/validation.html.twig', [
            'form_validation_cr_apl' => $form_validation_cr_apl->createView(),
            'compteRendu' => $compteRendu
        ]);
    }



    #[Route('/compte/rendu/region/{compteRendu}/invalidation', name: 'invalidation_cr_congres')]
    #[Route('/compte/rendu/sla/{compteRendu}/invalidation', name: 'invalidation_cr_apl_sla')]
    public function invalidationCompteRendu(Request $request, RedirectionHelper $redirectionHelper, NotificationHelper $notificationHelper, EntityManagerInterface $manager, PersonneRepository $personneRepository, CompteRenduInstance $compteRendu, WorkflowInterface $compteRenduInstanceWorkflow): Response
    {
        $this->denyAccessUnlessGranted(StructureVoter::VALIDER, $compteRendu->getStructure());

        /** @var Personne $personne */
        $personne = $this->getUser();
        $personne = $personneRepository->findOneBy(["id" => $personne->getId()]);
        $dateCourante = new DateTime();

        $form_invalidation_cr = $this->createForm(InvalidationCompteRenduType::class, $compteRendu);
        $form_invalidation_cr->handleRequest($request);

        if ($form_invalidation_cr->isSubmitted()) {
            if ($form_invalidation_cr->isValid() && ($compteRenduInstanceWorkflow->can($compteRendu, CompteRenduInstance::TRANSITION_NON_VALIDATION))) {
                try {

                    $compteRenduInstanceWorkflow->apply($compteRendu, CompteRenduInstance::TRANSITION_NON_VALIDATION);
                    $compteRendu->setDateValidation($dateCourante);
                    $compteRendu->setPersonneValideuse($personne);
                    $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_002, [
                        NotificationHelper::PERIMETRE_COMPTERENDU => $compteRendu
                    ]);
                    $manager->persist($compteRendu);
                    $manager->flush();
                    if ($compteRendu->getType() == CompteRenduInstance::TYPE_CONGRES) {
                        $this->addFlash("success", "Le compte rendu de congrès a été invalidé");
                    } else {
                        $this->addFlash("success", "Le compte rendu d'APL a été invalidé");
                    }
                    return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de la validation :" . $e->getMessage());
                }
            } else {
                $this->addFlash('danger', "Données incorrectes, veuillez corriger ");
            }
        }

        return $this->render('compte_rendu/invalidation.html.twig', [
            'form_invalidation_cr' => $form_invalidation_cr->createView(),
            'compteRendu' => $compteRendu
        ]);
    }

    function envoiMailErreurSupport(Exception $e, CompteRenduInstance $compteRendu, MailerInterface $mailer, String $adresseMailSupport)
    {
        //envoi du mail à l'adresse support:

        $message = (new TemplatedEmail())


            // On attribue le destinataire
            ->to($adresseMailSupport)
            ->subject("[Support Agora] Erreur lors de l'écriture des missions électives et des statuts d'autonomie de structures lors de la validation")
            ->htmlTemplate('mails_monitoring_support/erreur_ecriture_compte_rendu_jeito.html.twig')
            ->context([
                'exception' => $e,
                "structure" => $compteRendu->getStructure()
            ]);

        $mailer->send($message);
    }
}
