<?php

namespace App\Controller;

use App\Entity\ElectionNominative;
use App\Entity\Personne;
use App\Entity\Structure;
use App\Repository\PersonneRepository;
use App\Repository\SaisonDemocratiqueRepository;
use App\Repository\StructureRepository;
use App\Security\Voter\StructureVoter;
use App\Service\BoutonsAccesRapideHomePageHelper;
use App\Service\DroitsHelper;
use App\Service\ExportDeleguesSuppleantsHelper;
use App\Service\RedirectionHelper;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class RegionController extends AbstractController
{
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/region', name: 'home_region')]
    public function homePageRegion(Request $request, StructureRepository $structureRepository, BoutonsAccesRapideHomePageHelper $boutonsAccesRapideHomePageHelper, PersonneRepository $personneRepository, SaisonDemocratiqueRepository $saisonDemocratiqueRepository, DroitsHelper $droitsHelper): Response
    {
        $perimetreActif = $request->getSession()->get('perimetre');
        $typePerimetre = $request->getSession()->get('type_perimetre');
        if ($perimetreActif == null) {
            return $this->redirectToRoute('selection_perimetre');
        }

        /** @var Personne $user */
        $user = $this->getUser();
        $personne = $personneRepository->findOneBy(['id' => $user->getId()]);

        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        /** @var Structure $structure */
        $structureActive = $structureRepository->findOneById($perimetreActif);


        $boutonsAccesRapide = $boutonsAccesRapideHomePageHelper->genererListeBoutonsAccesRapide($structureActive, $saisonCourante, $typePerimetre);
        return $this->render('agora/homepage_region.html.twig', [
            'structurePerimetre' =>  $structureActive,
            'boutonsAccesRapide' => $boutonsAccesRapide,
            'droitsUser' => $droitsHelper->getDroitsPersonne($personne),
            'saisonCourante' => $saisonCourante,
        ]);
    }
    #[Route('/region/delegation/ag', name: 'region_delegation_ag')]
    public function delegationAG(Request $request, StructureRepository $structureRepository, RedirectionHelper $redirectionHelper, SaisonDemocratiqueRepository $saisonDemocratiqueRepository): Response
    {

        $perimetreActif = $request->getSession()->get('perimetre');
        $typePerimetre = $request->getSession()->get('type_perimetre');
        if ($perimetreActif == null) {
            return $this->redirectToRoute('selection_perimetre');
        }

        if ($typePerimetre !== Personne::TYPE_PERIMETRE_REGION) {
            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }
        /** @var Structure $region */
        $region = $structureRepository->findOneById($perimetreActif);
        $this->denyAccessUnlessGranted(StructureVoter::CONSULTER, $region);
        $listeStructuresDelegationRegionale = array();
        $listeDelegueOuSuppleant = array();

        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        //Parmi tous les CR de la saison courante: 

        $listeStructuresDelegationRegionale[] = $region;



        foreach ($region->getStructuresFilles() as $sla) {
            $listeStructuresDelegationRegionale[] = $sla;
        }
        foreach ($region->getStructureSlanAffiliees() as $slan) {
            $listeStructuresDelegationRegionale[] = $slan;
        }

        foreach ($listeStructuresDelegationRegionale as $structure) {
            $compterenduAnneeCourante = $structure->getCompteRenduInstancePourSaison($saisonCourante);
            if ($compterenduAnneeCourante) {
                foreach ($compterenduAnneeCourante->getElectionsNominatives() as $electionNominative) {
                    if ((
                            $electionNominative->getPoste() == ElectionNominative::POSTE_SUPPLEANT_AG
                            || $electionNominative->getPoste() == ElectionNominative::POSTE_DELEGUE_AG
                            || $electionNominative->getPoste() == ElectionNominative::POSTE_REPRESENTATION_REGION
                            || $electionNominative->getPoste() == ElectionNominative::POSTE_REPRESENTATION_SLA
                        )
                        && $electionNominative->getStatus() == ElectionNominative::STATUS_VALIDE && $electionNominative->getPersonne()
                    ) {
                        $listeDelegueOuSuppleant[] = $electionNominative;
                    }
                }
            }
        }
        return $this->render('agora/delegation_ag.html.twig', [
            'listeDelegueOuSuppleant' => $listeDelegueOuSuppleant,
            'saisonCourante' => $saisonCourante,
            'region' => $region
        ]);
    }

    #[Route('/region/delegation/ag/export', name: 'region_delegation_ag_export')]
    public function exportDelegationAG(Request $request, StructureRepository $structureRepository, RedirectionHelper $redirectionHelper, ExportDeleguesSuppleantsHelper $exportDeleguesSuppleantsHelper): Response
    {
        $perimetreActif = $request->getSession()->get('perimetre');
        $typePerimetre = $request->getSession()->get('type_perimetre');
        if ($perimetreActif == null) {
            return $this->redirectToRoute('selection_perimetre');
        }
        if ($typePerimetre !== Personne::TYPE_PERIMETRE_REGION) {
            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }
        /** @var Structure $region */
        $region = $structureRepository->findOneById($perimetreActif);
        $this->denyAccessUnlessGranted(StructureVoter::SAISIR, $region);
        $dateCourante = new DateTime();
        $fileName = 'Délégation AG région ' . $region->getNom() . ' au ' . $dateCourante->format('d-m-y') . '.xlsx';
        $temp_file = $exportDeleguesSuppleantsHelper->exportDeleguesSuppleants($fileName, $region);
        // Return the excel file as an attachment
        return $this->file($temp_file, $exportDeleguesSuppleantsHelper->filter_filename($fileName), ResponseHeaderBag::DISPOSITION_ATTACHMENT);
    }
}
