<?php

namespace App\Controller;

use App\Repository\NotificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('IS_AUTHENTICATED_FULLY')]
class NotificationController extends AbstractController
{
    #[Route('/notification', name: 'notifications')]
    public function notifications(NotificationRepository $notificationRepository): Response
    {
        $notifications = $notificationRepository->findBy(['personne' => $this->getUser()], ["date" => "DESC"]);
        return $this->render('notification/notifications.html.twig', [
            'notifications' => $notifications,
        ]);
    }


    #[Route('/notification/toutes/lues', name: 'marquer_toutes_notification_comme_lues')]
    public function marquerToutesNotificationsCommeLues(NotificationRepository $notificationRepository): Response
    {
        $notificationRepository->updateNotificationsCommeLues($this->getUser());

        return $this->redirectToRoute('notifications');
    }
}
