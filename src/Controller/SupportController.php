<?php

namespace App\Controller;

use App\Form\EnvoiMessageSupportType;
use App\Service\RedirectionHelper;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller pour le support utilisateur
 * @author  Antoine Faysse - ARIAC GWENAN <antoine.faysse@gwenan.net>
 */
class SupportController extends AbstractController
{

    /**
     * Permet la saisie d'un message et son envoi par mail à l'adresse mail de support
     * Si l'utilisat·eur est connecté·e, les données personnelles sont remplies automatiquement avec les données du compte. Sinon un formulaire est proposé
     * @param  Request $request la requete HTTP
     * @param  MailerInterface $mailer utilitaire d'envoi de mail
     * @return Response
     * @uses EnvoiMessageSupportType formulaire de saisie du message de support
     */
    #[Route("/support", name: "support")]
    public function envoiMessageSupportUtilisateursConnectes(Request $request, RedirectionHelper $redirectionHelper, MailerInterface $mailer): Response
    {
        /** @var Personne $user */
        $user = $this->getUser();
        $besoinCoordonnees = true;
        if ($user !== null) {
            $besoinCoordonnees = false;
        }
        $form = $this->createForm(EnvoiMessageSupportType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $adresseSupport = $this->getParameter('adresse_mail_support');
                if ($besoinCoordonnees) {
                    $expediteur = $form->get('email')->getData();
                    $nomExpediteur = $form->get('nom')->getData();
                    $prenomExpediteur = $form->get('prenom')->getData();
                    $numeroAdherentOuMatricule = $form->get('numeroAdherentOuMatricule')->getData();
                } else {
                    $expediteur = $user->getAdresseMailUsage();
                    $nomExpediteur = $user->getNom();
                    $prenomExpediteur = $user->getPrenom();
                    $numeroAdherentOuMatricule = $user->getNumeroAdherentOuMatricule();
                }
                $contenuMessage = $form->get('message')->getData();
                $rubrique = $form->get('rubrique')->getData();

                // Ici nous enverrons l'e-mail
                $message = (new TemplatedEmail())
                    // On attribue l'expéditeur
                    ->replyTo($expediteur)

                    // On attribue le destinataire
                    ->to($adresseSupport)
                    ->subject("[Support Agora] Demande de support de la part de $prenomExpediteur $nomExpediteur")
                    ->htmlTemplate('support/support_email.html.twig')
                    ->context([
                        'numeroAdherentOuMatricule' => $numeroAdherentOuMatricule,
                        'nomExpediteur' => $nomExpediteur,
                        'prenomExpediteur' => $prenomExpediteur,
                        'adresseExpediteur' => $expediteur,
                        'contenuMessage' => $contenuMessage,
                        'rubrique' => $rubrique,
                    ]);

                $mailer->send($message);

                $this->addFlash("success", "Le message a été envoyé avec succès, merci.");
            } catch (Exception $e) {
                $this->addFlash('danger', "Erreur lors de l'envoi du message: " . $e->getMessage());
            }


            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }
        return $this->render('support/saisie_message.html.twig', [
            'formSupport' => $form->createView(),
            'besoinCoordonnees' => $besoinCoordonnees
        ]);
    }
}
