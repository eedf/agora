<?php

namespace App\Controller;

use App\Entity\DocumentRessource;
use App\Repository\DocumentRessourceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RessourcesController extends AbstractController
{
    #[Route('/ressources/apl', name: 'ressources_apl')]
    public function documentsAPL(DocumentRessourceRepository $documentRessourceRepository): Response
    {
        $documents = $documentRessourceRepository->findBy(["categorie" => DocumentRessource::CATEGORIE_APL]);
        return $this->render('ressources/apl.html.twig', [
            'documents' => $documents,
        ]);
    }

    #[Route('/ressources/congres', name: 'ressources_congres')]
    public function documentsCongres(DocumentRessourceRepository $documentRessourceRepository): Response
    {
        $documents = $documentRessourceRepository->findBy(["categorie" => DocumentRessource::CATEGORIE_CONGRES]);
        return $this->render('ressources/congres.html.twig', [
            'documents' => $documents,
        ]);
    }
}
