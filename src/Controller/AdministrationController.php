<?php

namespace App\Controller;

use App\Entity\AttributionFonctionElective;
use App\Entity\DocumentRessource;
use App\Entity\Droit;
use App\Entity\ElectionNominative;
use App\Entity\Personne;
use App\Entity\SaisonDemocratique;
use App\Entity\Structure;
use App\Form\AdherentOuSalarieAutocompleteField;
use App\Form\AdminAffectationSlanType;
use App\Form\AdminDocumentsRessourceType;
use App\Form\DefinitionResponsabiliteRegionaleType;
use App\Form\DroitManuelPersonneType;
use App\Form\DroitManuelStructureType;
use App\Form\FinFonctionType;
use App\Repository\DocumentRessourceRepository;
use App\Repository\DroitRepository;
use App\Repository\PersonneRepository;
use App\Repository\SaisonDemocratiqueRepository;
use App\Repository\StructureRepository;
use App\Service\DroitsHelper;
use App\Service\EcritureFonctionJeitoHelper;
use App\Service\ExportDeleguesSuppleantsHelper;
use App\Service\NotificationHelper;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Exception;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_ADMIN')]
class AdministrationController extends AbstractController
{
    #[Route('/administration/administrateurs', name: 'administration_droits_admin')]
    public function administration(Request $request, PersonneRepository $personneRepository, EntityManagerInterface $manager): Response
    {


        $admins = $personneRepository->findByRole(Personne::ROLE_ADMIN);

        //Formulaire de recherche d'utilisateurs unique
        $formAjoutAdministrateur =  $this->createFormBuilder(null)
            ->add('personne', AdherentOuSalarieAutocompleteField::class, [
                'label' => "Nouvel·le administrat·eur·rice",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ]
            ])
            ->add('save', SubmitType::class, ['label' => 'Ajouter'])
            ->getForm();

        $formAjoutAdministrateur->handleRequest($request);

        if ($formAjoutAdministrateur->isSubmitted() && $formAjoutAdministrateur->isValid()) {
            /** @var Personne $personne */
            $personne = $formAjoutAdministrateur->get('personne')->getData();
            if ($personne->hasRole(Personne::ROLE_ADMIN)) {
                $this->addFlash('danger', "La personne " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ") a déjà le rôle d'administration !");
            } else {
                $personne->addRole(Personne::ROLE_ADMIN);

                if ($personne->estSalarieValide()  || $personne->estAdherentValide()) {
                    $this->addFlash('success', "Rôle d'administration ajouté pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ").");
                } else {
                    $this->addFlash('warning', "Rôle d'administration ajouté pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . "), dont l'adhésion ou le contrat de travail n'est pas valide.");
                }

                $manager->persist($personne);
                $manager->flush();
                return $this->redirectToRoute('administration_droits_admin');
            }
        }
        return $this->render('administration/droitsAdmin.html.twig', [
            'admins' => $admins,
            'formAjoutAdministrateur' => $formAjoutAdministrateur->createView(),
        ]);
    }

    #[Route('/administration/role_admin/{personne}/suppression', name: 'administration_supprimer_admin')]
    public function suppressionAdmin(Personne $personne, EntityManagerInterface $manager): Response
    {

        $personne->removeRole(Personne::ROLE_ADMIN);
        $this->addFlash('success', "Rôle d'administration supprimé pour " . $personne->getPrenom() . " " . $personne->getNom() . " (" . $personne->getNumeroAdherentOuMatricule() . ").");
        $manager->persist($personne);
        $manager->flush();


        //Cas spécifique ou l'admin se supprime en tant qu'admin
        if ($this->getUser() == $personne) {
            return $this->redirectToRoute('app_logout');
        }


        return $this->redirectToRoute('administration_droits_admin');
    }
    #[Route('/administration/delegues/suppleants', name: 'administration_delegues_suppleants_ag')]
    public function listingSuppleantsAG(SaisonDemocratiqueRepository $saisonDemocratiqueRepository, StructureRepository $structureRepository): Response
    {
        $listeDelegueOuSuppleant = array();
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        //Parmi tous les CR de la saison courante: 
        foreach ($saisonCourante->getComptesRendusInstances() as $compteRendu) {
            foreach ($compteRendu->getElectionsNominatives() as $electionNominative) {
                if ((
                        $electionNominative->getPoste() == ElectionNominative::POSTE_SUPPLEANT_AG
                        || $electionNominative->getPoste() == ElectionNominative::POSTE_DELEGUE_AG
                        || $electionNominative->getPoste() == ElectionNominative::POSTE_REPRESENTATION_REGION
                        || $electionNominative->getPoste() == ElectionNominative::POSTE_REPRESENTATION_SLA
                    )
                    && $electionNominative->getStatus() == ElectionNominative::STATUS_VALIDE && $electionNominative->getPersonne()
                ) {
                    $listeDelegueOuSuppleant[] = $electionNominative;
                }
            }
        }

        //On rajoute les membres du CD
        $listeMembresCD = array();
        $structureNationale = $structureRepository->findOneBy(["echelon" => Structure::ECHELON_STRUCTURE_NATIONAL]);
        $comiteDirecteur = $structureNationale->getEquipeGestion();
        foreach ($comiteDirecteur->getFonctionsActives() as $membreComiteDirecteur) {
            $listeMembresCD[] = $membreComiteDirecteur->getPersonne();
        }



        return $this->render('agora/delegation_ag.html.twig', [
            'listeDelegueOuSuppleant' => $listeDelegueOuSuppleant,
            'saisonCourante' => $saisonCourante,
            'listeMembresCD' => array_unique($listeMembresCD)
        ]);
    }
    #[Route('/administration/delegues/suppleants/export', name: 'administration_delegues_suppleants_ag_export')]
    public function exportDeleguesSuppleantsAG(ExportDeleguesSuppleantsHelper $exportDeleguesSuppleantsHelper): Response
    {
        $dateCourante = new DateTime();
        $fileName = 'Délégué·e·s et suppléant·e·s AG au ' . $dateCourante->format('d-m-y') . '.xlsx';
        $temp_file = $exportDeleguesSuppleantsHelper->exportDeleguesSuppleants($fileName);
        // Return the excel file as an attachment
        return $this->file($temp_file, $exportDeleguesSuppleantsHelper->filter_filename($fileName), ResponseHeaderBag::DISPOSITION_ATTACHMENT);
    }


    #[Route('/administration/responsabilite/sla', name: 'administration_responsabilite_sla')]
    public function responsabiliteSla(Request $request, StructureRepository $structureRepository, EntityManagerInterface $manager): Response
    {

        $structuresSlan = $structureRepository->findStructuresSLAN();
        $form_affectation_slans = $this->createForm(AdminAffectationSlanType::class, null, [
            "structuresSlan" => $structuresSlan
        ]);
        $form_affectation_slans->handleRequest($request);
        if ($form_affectation_slans->isSubmitted()) {
            if ($form_affectation_slans->isValid()) {
                try {
                    $structures = $form_affectation_slans->get('slans')->getData();
                    foreach ($structures as $structure) {
                        $manager->persist($structure);
                    }
                    $manager->flush();
                    $this->addFlash("success", "Les affiliations ont été enregistrées");
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'enregistrement: " . $e->getMessage());
                }
            }
        }

        $form_definition_responsabilite_regionale_sla = $this->createForm(DefinitionResponsabiliteRegionaleType::class);
        $form_definition_responsabilite_regionale_sla->handleRequest($request);
        if ($form_definition_responsabilite_regionale_sla->isSubmitted()) {
            if ($form_definition_responsabilite_regionale_sla->isValid()) {
                try {
                    /** @var Structure $structure  */
                    $structure = $form_definition_responsabilite_regionale_sla->get('structure')->getData();
                    $structure->setResponsabiliteRegionale(true);
                    $manager->persist($structure);

                    $manager->flush();
                    $this->addFlash("success", "La structure " . $structure->getNom() . " a été définie comme de responsabilité régionale");
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'enregistrement: " . $e->getMessage());
                }
            }
        }
        return $this->render('administration/niveauResponsabilitesSla.html.twig', [
            'form_affectation_slans' => $form_affectation_slans->createView(),
            'form_definition_responsabilite_regionale_sla' => $form_definition_responsabilite_regionale_sla->createView(),
            'slaResonsabiliteRegionale' => $structureRepository->findStructuresResponsabiliteRegionale()
        ]);
    }
    #[Route('/administration/responsabilite/regionale/{structure}/suppression', name: 'administration_supprimer_responsabilite_regionale')]
    public function suppressionSLAResponsabiliteRegionale(Structure $structure, EntityManagerInterface $manager): Response
    {

        $structure->setResponsabiliteRegionale(false);
        $manager->persist($structure);
        $manager->flush();
        $this->addFlash('success', "La responsabilité régionale de la structure" . $structure->getNom() . " a été supprimée");

        return $this->redirectToRoute('administration_responsabilite_sla');
    }


    #[Route('/administration/fin/fonction', name: 'administration_fin_fonction')]
    public function gestionFinFonction(Request $request, EntityManagerInterface $manager, EcritureFonctionJeitoHelper $ecritureFonctionJeitoHelper, NotificationHelper $notificationHelper): Response
    {

        $formFinFonction = $this->createForm(FinFonctionType::class, ['action' => $request->getRequestUri()]);

        $formFinFonction->handleRequest($request);
        if ($formFinFonction->isSubmitted()) {
            if ($formFinFonction->isValid()) {
                try {
                    $dateFin = $formFinFonction->get('dateFinMandat')->getData();
                    /** @var AttributionFonctionElective $attribution */
                    $attribution = $formFinFonction->get('mandatElectif')->getData();
                    $attribution->setDateFin($dateFin);

                    $manager->persist($attribution);
                    $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_008, [
                        NotificationHelper::PERIMETRE_ATTRIBUTION_FONCTION_ELECTIVE => $attribution
                    ]);
                    $manager->flush();
                    $ecritureFonctionJeitoHelper->raccourcirFonction($attribution);
                    $this->addFlash("success", "Le mandat électif a été terminé");
                    return $this->redirectToRoute('administration_fin_fonction');
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de la fin du mandat électif: " . $e->getMessage());
                }
            }
        }
        return $this->render('administration/finFonction.html.twig', [
            'formFinFonction' => $formFinFonction->createView(),
        ]);
    }

    #[Route('/administration/droits/personne/{personne}', name: 'administration_droits_personne')]
    public function gestionDroitsFinPersonneParticuliere(Request $request, NotificationHelper $notificationHelper, DroitsHelper $droitsHelper, Personne $personne, DroitRepository $droitRepository, EntityManagerInterface $manager): Response
    {
        $droitManuel = new Droit();
        $droitManuel->setPersonne($personne);
        $formDroitManuelPersonne = $this->createForm(DroitManuelPersonneType::class, $droitManuel, []);

        $formDroitManuelPersonne->handleRequest($request);
        if ($formDroitManuelPersonne->isSubmitted()) {
            if ($formDroitManuelPersonne->isValid()) {
                try {
                    $duree = $formDroitManuelPersonne->get('duree')->getData();
                    $dateExpiration = new DateTime();
                    $dateExpiration = $dateExpiration->add(new DateInterval($duree));
                    $droitManuel->setDateExpiration($dateExpiration);
                    if ($droitManuel->getPersonne()->aDroit($droitManuel->getDroit(), $droitManuel->getStructure())) {
                        if ($droitManuel->getPersonne()->aDroit($droitManuel->getDroit(), $droitManuel->getStructure(), $dateExpiration)) {
                            $this->addFlash("warning", "Cette personne a déjà ce droit, il ne sera pas ajouté");
                        } else {
                            /** @var Droit $droitStructure */
                            foreach ($droitManuel->getPersonne()->getDroitsPourStructure($droitManuel->getStructure()) as $droitStructure) {
                                if (
                                    $droitManuel->getDroit() == $droitStructure->getDroit()
                                    && $droitStructure->getType() == Droit::TYPE_DROIT_MANUEL
                                ) {
                                    $droitStructure->setDateExpiration($dateExpiration);
                                    $manager->persist($droitStructure);
                                    $manager->flush();
                                    $this->addFlash("success", "Le droit manuel étant existant, il a été rallongé");
                                }
                            }
                        }
                    } else {
                        $droitManuel->setType(Droit::TYPE_DROIT_MANUEL);
                        $manager->persist($droitManuel);
                        $manager->flush();
                        $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_007, [
                            NotificationHelper::PERIMETRE_DROIT => $droitManuel
                        ]);
                        $this->addFlash("success", "Le droit a été ajouté");
                    }
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'ajout du droit: " . $e->getMessage());
                }
            }
        }
        $droitsAutos = $droitRepository->findBy(["personne" => $personne, 'type' => Droit::TYPE_DROIT_AUTO]);
        $droitsManuels = $droitRepository->findBy(["personne" => $personne, 'type' => Droit::TYPE_DROIT_MANUEL]);
        return $this->render('administration/droits/personne.html.twig', [
            'formDroitManuelPersonne' => $formDroitManuelPersonne->createView(),
            "droitsManuels" => $droitsHelper->getListeDroitsLePlusFortPourCoupleStructurePersonne($droitsManuels),
            "droitsAutos" => $droitsHelper->getListeDroitsLePlusFortPourCoupleStructurePersonne($droitsAutos),
            'personne' => $personne
        ]);
    }

    #[Route('/administration/droits/personne', name: 'administration_droits_personnes')]
    public function gestionDroitsFinPersonnes(Request $request): Response
    {

        $formSelectionPersonne =  $this->createFormBuilder(null)
            ->add('personne', AdherentOuSalarieAutocompleteField::class, [
                'label' => "Personne",
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ]
            ])
            ->add('save', SubmitType::class, ['label' => 'Rechercher'])
            ->getForm();

        $formSelectionPersonne->handleRequest($request);

        if ($formSelectionPersonne->isSubmitted()) {
            if ($formSelectionPersonne->isValid()) {
                /** @var Personne $personne */
                $personne = $formSelectionPersonne->get('personne')->getData();
                return $this->redirectToRoute('administration_droits_personne', ['personne' => $personne->getId()]);
            }
        }
        return $this->render('administration/droits/selection_personne.html.twig', [
            'formSelectionPersonne' => $formSelectionPersonne->createView(),
        ]);
    }

    #[Route('/administration/droits/structure', name: 'administration_droits_structures')]
    public function gestionDroitsFinStructures(Request $request): Response
    {

        $formSelectionStructure =  $this->createFormBuilder(null)
            ->add('structure', EntityType::class, [
                'class' => Structure::class,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'placeholder' => 'Sélectionner la structure',
                'mapped' => false,
                'expanded' => false,
                'multiple' => false,
                'choice_label' => 'nom',
                'label' => "Structure",
                'query_builder' => function (EntityRepository $er) {
                    /** @var StructureRepository $er */
                    return $er->createQueryBuilder('s')
                        ->andWhere('s.statut in (:statutsActifs)')
                        ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])
                        ->orderBy('s.nom', 'ASC');
                },

                'group_by' => function ($choice, $nom, $value) {
                    /** @var Structure $choice*/
                    if ($choice->estUneSLA()) {
                        return ("SLA");
                    } else if ($choice->estUneSLAN()) {
                        return ("SLAN");
                    } else if ($choice->estUneRegion()) {
                        return ("Région");
                    } else {
                        return ("Autre");
                    }
                }
            ])
            ->add('save', SubmitType::class, ['label' => 'Rechercher'])
            ->getForm();

        $formSelectionStructure->handleRequest($request);

        if ($formSelectionStructure->isSubmitted()) {
            if ($formSelectionStructure->isValid()) {
                /** @var Personne $personne */
                $structure = $formSelectionStructure->get('structure')->getData();
                return $this->redirectToRoute('administration_droits_structure', ['structure' => $structure->getId()]);
            }
        }
        return $this->render('administration/droits/selection_structure.html.twig', [
            'formSelectionStructure' => $formSelectionStructure->createView(),
        ]);
    }


    #[Route('/administration/droits/structure/{structure}', name: 'administration_droits_structure')]
    public function gestionDroitsFinStructure(Request $request, NotificationHelper $notificationHelper, DroitsHelper $droitsHelper, Structure $structure, DroitRepository $droitRepository, EntityManagerInterface $manager): Response
    {
        $droitManuel = new Droit();
        $droitManuel->setStructure($structure);
        $formDroitManuelStructure = $this->createForm(DroitManuelStructureType::class, $droitManuel, []);

        $formDroitManuelStructure->handleRequest($request);
        if ($formDroitManuelStructure->isSubmitted()) {
            if ($formDroitManuelStructure->isValid()) {
                try {
                    $duree = $formDroitManuelStructure->get('duree')->getData();
                    $dateExpiration = new DateTime();
                    $dateExpiration = $dateExpiration->add(new DateInterval($duree));
                    $droitManuel->setDateExpiration($dateExpiration);
                    if ($droitManuel->getPersonne()->aDroit($droitManuel->getDroit(), $droitManuel->getStructure())) {
                        if ($droitManuel->getPersonne()->aDroit($droitManuel->getDroit(), $droitManuel->getStructure(), $dateExpiration)) {
                            $this->addFlash("warning", "Cette personne a déjà ce droit, il ne sera pas ajouté");
                        } else {
                            /** @var Droit $droitStructure */
                            foreach ($droitManuel->getPersonne()->getDroitsPourStructure($droitManuel->getStructure()) as $droitStructure) {
                                if (
                                    $droitManuel->getDroit() == $droitStructure->getDroit()
                                    && $droitStructure->getType() == Droit::TYPE_DROIT_MANUEL
                                ) {
                                    $droitStructure->setDateExpiration($dateExpiration);
                                    $manager->persist($droitStructure);
                                    $manager->flush();
                                    $this->addFlash("success", "Le droit manuel étant existant, il a été rallongé");
                                }
                            }
                        }
                    } else {
                        $droitManuel->setType(Droit::TYPE_DROIT_MANUEL);
                        $manager->persist($droitManuel);
                        $manager->flush();
                        $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_007, [
                            NotificationHelper::PERIMETRE_DROIT => $droitManuel
                        ]);
                        $this->addFlash("success", "Le droit a été ajouté");
                    }
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'ajout du droit: " . $e->getMessage());
                }
            }
        }

        $droitsAutos = $droitRepository->findBy(["structure" => $structure, 'type' => Droit::TYPE_DROIT_AUTO], ["personne" => 'ASC']);
        $droitsManuels = $droitRepository->findBy(["structure" => $structure, 'type' => Droit::TYPE_DROIT_MANUEL], ["personne" => 'ASC']);
        return $this->render('administration/droits/structure.html.twig', [
            'formDroitManuelStructure' => $formDroitManuelStructure->createView(),
            "droitsManuels" => $droitsHelper->getListeDroitsLePlusFortPourCoupleStructurePersonne($droitsManuels),
            "droitsAutos" => $droitsHelper->getListeDroitsLePlusFortPourCoupleStructurePersonne($droitsAutos),
            'structure' => $structure
        ]);
    }
    #[Route('/administration/droits/{droitManuel}/personne/{personne}/suppression', name: 'administration_supprimer_droit_manuel_personne')]
    public function suppressionDroitPersonne(Droit $droitManuel, NotificationHelper $notificationHelper, Personne $personne, EntityManagerInterface $manager): Response
    {
        if ($droitManuel->getType() == Droit::TYPE_DROIT_AUTO) {
            $this->addFlash('danger', "Ce droit est un droit automatique, il ne peut pas être suprimé");
        } else {
            $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_005, [
                NotificationHelper::PERIMETRE_DROIT => $droitManuel
            ]);
            $manager->remove($droitManuel);
            $manager->flush();
            $this->addFlash('success', "Le droit a été supprimé");
        }


        return $this->redirectToRoute('administration_droits_personne', ['personne' => $personne->getId()]);
    }

    #[Route('/administration/droits/{droitManuel}/structure/{structure}/suppression', name: 'administration_supprimer_droit_manuel_structure')]
    public function suppressionDroitStructure(Droit $droitManuel, Structure $structure, NotificationHelper $notificationHelper, EntityManagerInterface $manager): Response
    {

        if ($droitManuel->getType() == Droit::TYPE_DROIT_AUTO) {
            $this->addFlash('danger', "Ce droit est un droit automatique, il ne peut pas être suprimé");
        } else {
            $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_005, [
                NotificationHelper::PERIMETRE_DROIT => $droitManuel
            ]);
            $manager->remove($droitManuel);
            $manager->flush();
            $this->addFlash('success', "Le droit a été supprimé");
        }
        return $this->redirectToRoute('administration_droits_structure', ['structure' => $structure->getId()]);
    }
    #[Route('/administration/documents', name: 'administration_documents_ressources')]
    public function gestionDocumentsRessources(Request $request, EntityManagerInterface $manager, DocumentRessourceRepository $documentRessourceRepository)
    {
        $documents = $documentRessourceRepository->findAll();
        $form_documents_ressources = $this->createForm(AdminDocumentsRessourceType::class, null, [
            "documents" => $documents
        ]);
        $form_documents_ressources->handleRequest($request);
        if ($form_documents_ressources->isSubmitted()) {
            if ($form_documents_ressources->isValid()) {
                try {
                    $documents = $form_documents_ressources->get('documents')->getData();
                    foreach ($documents as $document) {
                        $manager->persist($document);
                    }
                    $manager->flush();
                    $this->addFlash("success", "Les documents ont été mis à jour");
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'enregistrement: " . $e->getMessage());
                }
            }
        }
        return $this->render('administration/documentsRessource.html.twig', [
            'form_documents_ressources' => $form_documents_ressources->createView(),
        ]);
    }
}
