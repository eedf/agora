<?php

namespace App\Controller;

use App\Entity\CompteRenduInstance;
use App\Entity\Droit;
use App\Entity\Personne;
use App\Entity\SaisonDemocratique;
use App\Entity\Structure;
use App\Form\DelegationDroitNonAdminType;
use App\Form\SelectionPerimetreForNavBarType;
use App\Form\SelectionPerimetreType;
use App\Repository\CompteRenduInstanceRepository;
use App\Repository\DroitRepository;
use App\Repository\PersonneRepository;
use App\Repository\SaisonDemocratiqueRepository;
use App\Repository\StructureRepository;
use App\Security\Voter\StructureVoter;
use App\Service\BoutonsAccesRapideHomePageHelper;
use App\Service\DroitsHelper;
use App\Service\NotificationHelper;
use App\Service\RedirectionHelper;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Drenso\OidcBundle\OidcClientInterface;
use Exception;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Workflow\WorkflowInterface;

class AgoraController extends AbstractController
{
    #[Route('/', name: 'agora')]
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    public function index(Request $request, RedirectionHelper $redirectionHelper): Response
    {
        return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
    }

    #[Route("/agora/accueil", name: "accueil_non_connecte")]
    public function accueil_non_connecte(Request $request, AuthenticationUtils $authenticationUtils)
    {

        /** @var Personne $userExistant */
        if ($this->getUser()) {
            return $this->redirectToRoute('agora');
        }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();


        return $this->render('agora/accueil_non_connecte.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    #[Route("/privacy", name: "privacy")]
    public function mentionsLegales(Request $request)
    {
        return $this->render('privacy/mentions_legales.html.twig');
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route("/select/perimetre", name: "selection_perimetre")]
    public function selectionPermietre(Request $request, RedirectionHelper $redirectionHelper, StructureRepository $structureRepository, PersonneRepository $personneRepository)
    {
        /** @var Personne $utilisateurConnecte */
        $utilisateurConnecte = $this->getUser();
        $utilisateurConnecte = $personneRepository->findOneBy(["id" => $utilisateurConnecte->getId()]);
        $perimetreActif = $request->getSession()->get('perimetre');
        $typePerimetre = $request->getSession()->get('type_perimetre');


        //Si l'utilisateru n'a qu'un périmètre dispo, on redirige directement vers la home page du périmètre
        if (count($utilisateurConnecte->getPerimetresDisponibles()) == 1) {
            $perimetreActif = array_values($utilisateurConnecte->getPerimetresDisponibles())[0];
            $typePerimetre = $this->deduitTypePerimetre($perimetreActif, $structureRepository);
            $request->getSession()->set('perimetre', $perimetreActif);
            $request->getSession()->set('type_perimetre', $typePerimetre);

            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }

        $formSelectionPerimetre = $this->createForm(SelectionPerimetreType::class, null, [
            "personne" => $utilisateurConnecte
        ]);

        $formSelectionPerimetre->handleRequest($request);
        if ($formSelectionPerimetre->isSubmitted() && $formSelectionPerimetre->isValid()) {

            $perimetreActif = $formSelectionPerimetre->get('perimetre')->getData();
            $typePerimetre = $this->deduitTypePerimetre($perimetreActif, $structureRepository);
            $request->getSession()->set('perimetre', $perimetreActif);
            $request->getSession()->set('type_perimetre', $typePerimetre);

            return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
        }
        return $this->render('agora/selection_perimetre.html.twig', [
            'formSelectionPerimetre' => $formSelectionPerimetre->createView()
        ]);
    }
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/sla', name: 'home_sla', methods: ['GET', 'POST'])]
    public function homePageSLA(Request $request, PersonneRepository $personneRepository,  StructureRepository $structureRepository, WorkflowInterface $compteRenduInstanceWorkflow, BoutonsAccesRapideHomePageHelper $boutonsAccesRapideHomePageHelper, SaisonDemocratiqueRepository $saisonDemocratiqueRepository, DroitsHelper $droitsHelper): Response
    {
        $perimetreActif = $request->getSession()->get('perimetre');
        $typePerimetre = $request->getSession()->get('type_perimetre');
        if ($perimetreActif == null) {
            return $this->redirectToRoute('selection_perimetre');
        }
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        /** @var Personne $user */
        $user = $this->getUser();

        $personne = $personneRepository->findOneBy(['id' => $user->getId()]);
        /** @var Structure $structure */
        $structureActive = $structureRepository->findOneById($perimetreActif);

        $droitsUtilisateur = $droitsHelper->getDroitsPersonne($personne);

        $boutonsAccesRapide = $boutonsAccesRapideHomePageHelper->genererListeBoutonsAccesRapide($structureActive, $saisonCourante, $typePerimetre);

        return $this->render('agora/homepage_sla.html.twig', [
            'structurePerimetre' =>  $structureActive,
            'boutonsAccesRapide' => $boutonsAccesRapide,
            'droitsUser' => $droitsUtilisateur,
            'saisonCourante' => $saisonCourante,
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/slan', name: 'home_slan')]
    public function homePageSLAN(Request $request, PersonneRepository $personneRepository,  StructureRepository $structureRepository, BoutonsAccesRapideHomePageHelper $boutonsAccesRapideHomePageHelper, SaisonDemocratiqueRepository $saisonDemocratiqueRepository, DroitsHelper $droitsHelper): Response
    {
        $perimetreActif = $request->getSession()->get('perimetre');
        $typePerimetre = $request->getSession()->get('type_perimetre');
        if ($perimetreActif == null) {
            return $this->redirectToRoute('selection_perimetre');
        }
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        /** @var Personne $user */
        $user = $this->getUser();

        $personne = $personneRepository->findOneBy(['id' => $user->getId()]);
        /** @var Structure $structureActive */
        $structureActive = $structureRepository->findOneById($perimetreActif);

        $droitsUtilisateur = $droitsHelper->getDroitsPersonne($personne);
        $boutonsAccesRapide = $boutonsAccesRapideHomePageHelper->genererListeBoutonsAccesRapide($structureActive, $saisonCourante, $typePerimetre);

        return $this->render('agora/homepage_slan.html.twig', [
            'structurePerimetre' =>  $structureActive,
            'boutonsAccesRapide' => $boutonsAccesRapide,
            'droitsUser' => $droitsUtilisateur,
            'saisonCourante' => $saisonCourante,
        ]);
    }



    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route("/agora/resume/structure", name: "resume_structure_homepage", methods: ["GET", "POST"])]
    public function selectionStructureHomePage(Request $request, StructureRepository $structureRepository, SaisonDemocratiqueRepository $saisonDemocratiqueRepository)
    {
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        $typePerimetre = $request->getSession()->get('type_perimetre');
        $perimetreActif = $request->getSession()->get('perimetre');
        /** @var Structure $structureActive */
        $structureActive = $structureRepository->findOneById($perimetreActif);


        $dataStructures = null;
        switch ($typePerimetre) {
            case Personne::TYPE_PERIMETRE_REGION:
                $dataStructures = $structureRepository->findStructuresFillesSLAActives($structureActive);
                $dataStructures[] = $structureActive; //On rajoute la structure régionale elle-même
                $structureSelectionnee = $structureActive;
                break;
            case Personne::TYPE_PERIMETRE_NATIONAL:
            case Personne::TYPE_PERIMETRE_ADMIN:
                $dataStructures = $structureRepository->findAllStructuresActives(true);
                $structureSelectionnee = null;
                break;
            default:
                $structureSelectionnee = $structureActive;
                break;
        }
        if ($dataStructures !== null) {
            $formSelectionStructure =  $this->createFormBuilder(null, [
                'action' => $this->generateUrl('resume_structure_homepage'),
            ])
                ->add('structure', EntityType::class, [
                    'class' => Structure::class,
                    'label_attr' => [
                        'class' => 'fw-bold text-primary text-center',
                    ],
                    'placeholder' => 'Sélectionner la structure',
                    'mapped' => false,
                    'expanded' => false,
                    'multiple' => false,
                    'data' =>  $structureSelectionnee,
                    'choice_label' => 'nom',
                    'label' => "Structure",
                    "attr" => [
                        "class" => "text-center bg-light text-dark fw-bold border-secondary"
                    ],

                    'choices' => $dataStructures,

                    'group_by' => function ($choice, $nom, $value) {
                        /** @var Structure $choice*/
                        if ($choice->estUneSLA()) {
                            return ("SLA");
                        } else if ($choice->estUneSLAN()) {
                            return ("SLAN");
                        } else if ($choice->estUneRegion()) {
                            return ("Région");
                        } else {
                            return ("Autre");
                        }
                    }
                ])

                ->getForm();
            $formSelectionStructure->handleRequest($request);

            if ($formSelectionStructure->isSubmitted()) {
                if ($formSelectionStructure->isValid()) {
                    /** @var Personne $personne */
                    $structureSelectionnee = $formSelectionStructure->get('structure')->getData();
                }
            }
        }


        return $this->render('components/_resume_structure.html.twig', [
            "formSelectionStructure" => $formSelectionStructure ? $formSelectionStructure->createView() : null,
            'saisonCourante' => $saisonCourante,
            "structure" => $structureSelectionnee
        ]);
    }


    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/national', name: 'home_national')]
    public function homePageNational(Request $request, DroitsHelper $droitsHelper, PersonneRepository $personneRepository, BoutonsAccesRapideHomePageHelper $boutonsAccesRapideHomePageHelper, SaisonDemocratiqueRepository $saisonDemocratiqueRepository): Response
    {
        $perimetreActif = $request->getSession()->get('perimetre');
        $typePerimetre = $request->getSession()->get('type_perimetre');
        if ($perimetreActif == null) {
            return $this->redirectToRoute('selection_perimetre');
        }

        /** @var Personne $user */
        $user = $this->getUser();
        $personne = $personneRepository->findOneBy(['id' => $user->getId()]);
        $droitsUtilisateur = $droitsHelper->getDroitsPersonne($personne);
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        $boutonsAccesRapide = $boutonsAccesRapideHomePageHelper->genererListeBoutonsAccesRapide(null, $saisonCourante, $typePerimetre);

        return $this->render('agora/homepage_national.html.twig', [
            'saisonCourante' => $saisonCourante,
            'droitsUser' => $droitsUtilisateur,
            'boutonsAccesRapide' => $boutonsAccesRapide
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/agora/admin', name: 'home_admin')]
    public function homePageAdmin(Request $request, DroitsHelper $droitsHelper, BoutonsAccesRapideHomePageHelper $boutonsAccesRapideHomePageHelper, PersonneRepository $personneRepository, SaisonDemocratiqueRepository $saisonDemocratiqueRepository): Response
    {
        $perimetreActif = $request->getSession()->get('perimetre');
        $typePerimetre = $request->getSession()->get('type_perimetre');
        if ($perimetreActif == null) {
            return $this->redirectToRoute('selection_perimetre');
        }

        /** @var Personne $user */
        $user = $this->getUser();
        $user->getNotifications();
        $personne = $personneRepository->findOneBy(['id' => $user->getId()]);

        $droitsUtilisateur = $droitsHelper->getDroitsPersonne($personne);
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        $boutonsAccesRapide = $boutonsAccesRapideHomePageHelper->genererListeBoutonsAccesRapide(null, $saisonCourante, $typePerimetre);
        return $this->render('agora/homepage_admin.html.twig', [
            'boutonsAccesRapide' => $boutonsAccesRapide,
            'droitsUser' => $droitsUtilisateur,
            'saisonCourante' => $saisonCourante,
        ]);
    }
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/sla/apl', name: 'gestion_apl_sla')]
    public function gestion_sla(Request $request, StructureRepository $structureRepository): Response
    {
        $perimetreActif = $request->getSession()->get('perimetre');
        /** @var Structure $structureActive */
        $structureActive = $structureRepository->findOneById($perimetreActif);
        if ($structureActive != null && $structureActive->estUneSLA()) {
            //Si c'est une SLA, on va vers la gestion de l'APL de la structue Active en particulier
            return $this->redirectToRoute('gestion_structure_sla', ['structure' => $structureActive->getId()]);
        } else {
            //Si c'est pour d'autres, on va vers la gestion des differentes structures
            return $this->redirectToRoute('gestion_structures_sla');
        }
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/slan/apl', name: 'gestion_apl_slan')]
    public function gestion_slan(): Response
    {
        //Dans tous les cas, on va vers la gestion de toutes les SLAN
        return $this->redirectToRoute('gestion_structures_slan');
    }
    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/region/congres', name: 'gestion_congres_region')]
    public function gestion_congres_region(Request $request, StructureRepository $structureRepository): Response
    {
        $perimetreActif = $request->getSession()->get('perimetre');
        /** @var Structure $structureActive */
        $structureActive = $structureRepository->findOneById($perimetreActif);
        if ($structureActive !== null) {
            if ($structureActive->estUneRegion()) {
                //Si c'est une région, on va vers la gestion du congres de la structue Active en particulier
                return $this->redirectToRoute('gestion_structure_region', ['structure' => $structureActive->getId()]);
            } else if ($structureActive->estUneSLA()) {
                //Si c'est une SLA, on va vers la gestion du congres de la structure parent en particulier
                return $this->redirectToRoute('gestion_structure_region', ['structure' => $structureActive->getStructureParent()->getId()]);
            } else if ($structureActive->estUneSLAN()) {
                if ($structureActive->getStructureRegionaleAffiliee() !== null) {
                    //Si c'est une SLAN, on va vers la gestion du congres de la structure régionale affiliée si elle existe en particulier
                    return $this->redirectToRoute('gestion_structure_region', ['structure' => $structureActive->getStructureRegionaleAffiliee()->getId()]);
                } else { // Si pas de structure régionale affiliée on a rien a faire là!
                    return $this->redirectToRoute('home_slan');
                }
            }
        }
        //Si c'est pour d'autres, on va vers la gestion de toutes les régions
        return $this->redirectToRoute('gestion_structures_regions');
    }


    #[Route('/agora/slans', name: 'gestion_structures_slan')]
    public function gestion_structures_slan(Request $request, StructureRepository $structureRepository, SaisonDemocratiqueRepository $saisonDemocratiqueRepository): Response
    {
        $listesStructuresAAfficher = array();
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        //On récuperes les SLAN à afficher en fonction du périmètre:
        $listesStructuresAAfficher = $structureRepository->findStructuresSLAN();

        $comptesRendusAPL = [];

        /** @var Structure $structure */
        foreach ($listesStructuresAAfficher as $structure) {
            $comptesRendusAPL[$structure->getNom()] = $structure->getCompteRenduInstancePourSaison($saisonCourante);
        }
        return $this->render('agora/resume_structures_slan.html.twig', [
            'comptesRendusAPL' => $comptesRendusAPL,
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/slas', name: 'gestion_structures_sla')]
    public function gestion_structures_sla(Request $request, StructureRepository $structureRepository, SaisonDemocratiqueRepository $saisonDemocratiqueRepository): Response
    {
        $region = null;
        $listesStructuresAAfficher = array();
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        //On récuperes les SLA à afficher en fonction du périmètre:
        $perimetreActif = $request->getSession()->get('perimetre');
        $typePerimetre = $request->getSession()->get('type_perimetre');
        switch ($typePerimetre) {
            case Personne::TYPE_PERIMETRE_SLAN:
            case Personne::TYPE_PERIMETRE_SLA:
                $structureActive = $structureRepository->findOneById($perimetreActif);
                return $this->redirectToRoute('gestion_structure_sla', ['structure' => $structureActive->getId()]);
                break;
            case Personne::TYPE_PERIMETRE_REGION:
                $structureActive = $structureRepository->findOneById($perimetreActif);
                $region = $structureActive;
                break;
        }

        return $this->render('agora/resume_structures_sla.html.twig', [
            "region" => $region
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/regions', name: 'gestion_structures_regions')]
    public function gestion_structures_regions(Request $request, StructureRepository $structureRepository, SaisonDemocratiqueRepository $saisonDemocratiqueRepository): Response
    {
        $region = null;
        $listesStructuresAAfficher = array();
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        //On récuperes les SLA à afficher en fonction du périmètre:
        $perimetreActif = $request->getSession()->get('perimetre');
        if ($perimetreActif == Personne::ROLE_ADMIN) {
            $listesStructuresAAfficher = $structureRepository->findAllStructuresRegionalesActives();
        } else {
            /** @var Structure $structureActive */
            $structureActive = $structureRepository->findOneById($perimetreActif);
            if ($structureActive->estNational()) {
                $listesStructuresAAfficher = $structureRepository->findAllStructuresRegionalesActives();
            } else if ($structureActive->estUneRegion()) {
                return $this->redirectToRoute('gestion_structure_region', ['structure' => $structureActive->getId()]);;
            }
        }

        $comptesRendusRegion = [];

        /** @var Structure $structure */
        foreach ($listesStructuresAAfficher as $structure) {
            $comptesRendusRegion[$structure->getNom()] = $structure->getCompteRenduInstancePourSaison($saisonCourante);
        }
        return $this->render('agora/resume_structures_regions.html.twig', [
            'comptesRendusRegion' => $comptesRendusRegion,
        ]);
    }




    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/sla/{structure}', name: 'gestion_structure_sla')]
    #[Route('/agora/region/{structure}', name: 'gestion_structure_region')]
    #[Route('/agora/slan/{structure}', name: 'gestion_structure_slan')]
    public function gestion_structure_specifique(Structure $structure, SaisonDemocratiqueRepository $saisonDemocratiqueRepository, WorkflowInterface $compteRenduInstanceWorkflow): Response
    {
        //Recherche du CR de la saison active
        $saisonCourante = $saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        $saisonPrecedente = $saisonCourante->getSaisonPrecedente();
        $compteRendu = $structure->getCompteRenduInstancePourSaison($saisonCourante);
        if ($saisonPrecedente) {
            $compteRenduPrecedent = $structure->getCompteRenduInstancePourSaison($saisonPrecedente);
        } else {
            $compteRenduPrecedent = null;
        }

        $etatCompteRendu = $compteRenduInstanceWorkflow->getMarking($compteRendu);
        if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_SAISI)) {
            if ($this->isGranted(StructureVoter::SAISIR, $structure)) {
                return $this->redirectToRoute("compte_rendu_action", [
                    'compteRendu' => $compteRendu->getId(),
                    'action' => 'saisie'
                ]);
            } else { // Si le CR n'est pas encore saisi, on peut aller consulter le CR de l'année précédente s'il exsite
                if ($compteRenduPrecedent !== null) {
                    return $this->redirectToRoute("compte_rendu_action", [
                        'compteRendu' => $compteRenduPrecedent->getId(),
                        'action' => 'consultation'
                    ]);
                }
            }
        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_PAS_D_INSTANCE)) {
            if ($this->isGranted(StructureVoter::SAISIR, $structure)) {
                return $this->redirectToRoute("compte_rendu_action", [
                    'compteRendu' => $compteRendu->getId(),
                    'action' => 'consultation'
                ]);
            } else if ($this->isGranted(StructureVoter::VALIDER, $structure)) {
                return $this->redirectToRoute("compte_rendu_action", [
                    'compteRendu' => $compteRendu->getId(),
                    'action' => 'saisie'
                ]);
            }
        } else if (
            $etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_ATTENTE_VALIDATION)
            || $etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_NON_VALIDE)
        ) {
            if ($this->isGranted(StructureVoter::VALIDER, $structure)) {
                return $this->redirectToRoute("compte_rendu_action", [
                    'compteRendu' => $compteRendu->getId(),
                    'action' => 'saisie'
                ]);
            } else  if ($this->isGranted(StructureVoter::SAISIR, $structure)) {
                return $this->redirectToRoute("compte_rendu_action", [
                    'compteRendu' => $compteRendu->getId(),
                    'action' => 'saisie'
                ]);
            } else { // Si le CR n'est pas encore saisi, on peut aller consulter le CR de l'année précédente s'il exsite
                if ($compteRenduPrecedent !== null) {
                    return $this->redirectToRoute("compte_rendu_action", [
                        'compteRendu' => $compteRenduPrecedent->getId(),
                        'action' => 'consultation'
                    ]);
                }
            }
        } else if ($etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_ARCHIVE)) {
            if ($this->isGranted(StructureVoter::CONSULTER_ARCHIVE, $structure)) {
                return $this->redirectToRoute("compte_rendu_action", [
                    'compteRendu' => $compteRendu->getId(),
                    'action' => 'consultation'
                ]);
            }
        } else if (
            $etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_VALIDE)
            || $etatCompteRendu->has(CompteRenduInstance::STATUT_CR_INSTANCE_SAISON_PASSEE)
        ) {
            //Dans ces deux cas, uniquement les personnes avec le droit de validation peuvent modifier
            if ($this->isGranted(StructureVoter::MODIFIER_APRES_VALIDATION, $structure)) {
                return $this->redirectToRoute("compte_rendu_action", [
                    'compteRendu' => $compteRendu->getId(),
                    'action' => 'saisie'
                ]);
                //Les autres peuvent seulement consulter
            } else if ($this->isGranted(StructureVoter::CONSULTER, $structure)) {
                return $this->redirectToRoute("compte_rendu_action", [
                    'compteRendu' => $compteRendu->getId(),
                    'action' => 'consultation'
                ]);
            }
        }

        return $this->redirectToRoute("compte_rendu_indisponible", [
            'structure' => $structure,
            'saison' => $saisonCourante
        ]);
    }


    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/archive/sla', name: 'archives_apl_sla')]
    public function archivesSLA(Request $request, StructureRepository $structureRepository): Response
    {
        $typePerimetre = $request->getSession()->get('type_perimetre');
        $perimetreActif = $request->getSession()->get('perimetre');
        /** @var Structure $structureActive */
        $structureActive = $structureRepository->findOneById($perimetreActif);
        $structureParametre = null;
        switch ($typePerimetre) {
            case Personne::TYPE_PERIMETRE_SLAN:
            case Personne::TYPE_PERIMETRE_SLA:
            case Personne::TYPE_PERIMETRE_REGION:
                $structureParametre = $structureActive;
                break;
        }
        return $this->render('agora/archives_structures.html.twig', [
            "structure" => $structureParametre,
            "typeCompteRendu" => CompteRenduInstance::TYPE_APL_SLA
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/archive/slan', name: 'archives_apl_slan')]
    public function archivesSLAN(): Response
    {
        return $this->render('agora/archives_structures.html.twig', [
            "structure" => null,
            "typeCompteRendu" => CompteRenduInstance::TYPE_APL_SLAN
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/agora/archive/region', name: 'archives_congres_region')]
    public function archivesRegion(Request $request, StructureRepository $structureRepository): Response
    {
        $typePerimetre = $request->getSession()->get('type_perimetre');
        $perimetreActif = $request->getSession()->get('perimetre');
        /** @var Structure $structureActive */
        $structureActive = $structureRepository->findOneById($perimetreActif);
        $structureParametre = null;
        switch ($typePerimetre) {
            case Personne::TYPE_PERIMETRE_SLAN:
                $structureParametre = $structureActive->getStructureRegionaleAffiliee();
                break;
            case Personne::TYPE_PERIMETRE_SLA:
                $structureParametre = $structureActive->getStructureParent();
                break;
            case Personne::TYPE_PERIMETRE_REGION:
                $structureParametre = $structureActive;
                break;
        }
        return $this->render('agora/archives_structures.html.twig', [
            "structure" => $structureParametre,
            "typeCompteRendu" => CompteRenduInstance::TYPE_CONGRES
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route("/agora/archive/selection/{typeCompteRendu}", name: "archives", methods: ["GET", "POST"])]
    public function selectionPerimetreArchive(Request $request, RedirectionHelper $redirectionHelper, String $typeCompteRendu, StructureRepository $structureRepository, SaisonDemocratiqueRepository $saisonDemocratiqueRepository, CompteRenduInstanceRepository $compteRenduInstanceRepository)
    {
        $structuresSelectionnees = [];
        $saisonsInactives = $saisonDemocratiqueRepository->findBy(['actif' => false]);
        $saisonsSelectionnees = $saisonsInactives;
        $typePerimetre = $request->getSession()->get('type_perimetre');
        $perimetreActif = $request->getSession()->get('perimetre');
        /** @var Structure $structureActive */
        $structureActive = $structureRepository->findOneById($perimetreActif);
        $filtreStructure = false;
        $dataStructures = null;
        switch ($typeCompteRendu) {
            case CompteRenduInstance::TYPE_APL_SLA:
                switch ($typePerimetre) {
                    case Personne::TYPE_PERIMETRE_REGION:
                        $structuresSelectionnees = $structureRepository->findStructuresFillesSLAActives($structureActive);
                        $filtreStructure = false;
                        break;
                    case Personne::TYPE_PERIMETRE_SLA:
                    case Personne::TYPE_PERIMETRE_SLAN:
                        $filtreStructure = false;
                        $structuresSelectionnees[] = $structureActive; //On rajoute la SLAN
                        break;
                    case Personne::TYPE_PERIMETRE_NATIONAL:
                    case Personne::TYPE_PERIMETRE_ADMIN:
                        $dataStructures = $structureRepository->findStructuresRegion();
                        $structuresSelectionnees = $structureRepository->findAllStructuresSLAActives();
                        $filtreStructure = true;
                        break;
                    default:
                        break;
                }
                break;
            case CompteRenduInstance::TYPE_APL_SLAN:

                $structuresSelectionnees = $structureRepository->findStructuresSLAN();
                $filtreStructure = false;
                break;

            case CompteRenduInstance::TYPE_CONGRES:
                switch ($typePerimetre) {
                    case Personne::TYPE_PERIMETRE_REGION:
                        $filtreStructure = false;
                        $structuresSelectionnees[] = $structureActive; //On rajoute la structure régionale elle-même
                        break;
                    case Personne::TYPE_PERIMETRE_SLA:
                        $filtreStructure = false;
                        $structuresSelectionnees[] = $structureActive->getStructureParent(); //On rajoute la structure parent
                        break;
                    case Personne::TYPE_PERIMETRE_SLAN:
                        $filtreStructure = false;
                        $structuresSelectionnees[] = $structureActive->getStructureRegionaleAffiliee(); //On rajoute la structure régionale affiliée

                        break;
                    case Personne::TYPE_PERIMETRE_NATIONAL:
                    case Personne::TYPE_PERIMETRE_ADMIN:
                        $filtreStructure = false;
                        $structuresSelectionnees = $structureRepository->findStructuresRegion();
                        break;
                    default:
                        break;
                }
                break;
        }

        $formSelectionPerimetreArchive =  $this->createFormBuilder(null, [
            'action' => $this->generateUrl('archives', ["typeCompteRendu" => $typeCompteRendu]),
        ])
            ->add('saison', EntityType::class, [
                'class' => SaisonDemocratique::class,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'placeholder' => 'Toutes les saisons',
                'mapped' => false,
                'expanded' => false,
                'multiple' => false,
                'choice_label' => 'nom',
                'label' => "Saison",
                "attr" => [
                    "class" => "text-center border-secondary",
                ],
                'choices' => $saisonsInactives,
            ])->getForm();

        if ($filtreStructure) {
            $formSelectionPerimetreArchive->add('structure', EntityType::class, [
                'class' => Structure::class,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'placeholder' => 'Toutes les régions',
                'mapped' => false,
                'expanded' => false,
                'multiple' => false,
                'choice_label' => 'nom',
                'label' => "Structure",
                "attr" => [
                    "class" => "text-center border-secondary",
                ],
                'choices' => $dataStructures,

            ]);
        }
        $formSelectionPerimetreArchive->handleRequest($request);

        if ($formSelectionPerimetreArchive->isSubmitted()) {
            if ($formSelectionPerimetreArchive->isValid()) {
                /** @var Personne $personne */
                if ($filtreStructure) {
                    $structureSelectionnee = $formSelectionPerimetreArchive->get('structure')->getData();
                    if ($structureSelectionnee !== null) {
                        $structuresSelectionnees = $structureRepository->findStructuresFillesSLAActives($structureSelectionnee);
                    }
                }
                $saisonSelectionnee = $formSelectionPerimetreArchive->get('saison')->getData();
                if ($saisonSelectionnee !== null) {
                    $saisonsSelectionnees = [$saisonSelectionnee];
                }
            }
        }

        $comptesRendusArchives = $compteRenduInstanceRepository->findByStructuresBySaisons($structuresSelectionnees, $saisonsSelectionnees);
        return $this->render('components/_resume_archives.html.twig', [
            "formSelectionPerimetreArchive" => $formSelectionPerimetreArchive ? $formSelectionPerimetreArchive->createView() : null,
            "comptesRendusArchives" => $comptesRendusArchives,
            "typeCompteRendu" => $typeCompteRendu,

        ]);
    }


    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route("/agora/resume/apl/selection", name: "resume_apl", methods: ["GET", "POST"])]
    public function selectionPerimetreAPL(Request $request, StructureRepository $structureRepository, CompteRenduInstanceRepository $compteRenduInstanceRepository)
    {
        $structuresSelectionnees = [];
        $typePerimetre = $request->getSession()->get('type_perimetre');
        $perimetreActif = $request->getSession()->get('perimetre');
        /** @var Structure $structureActive */
        $structureActive = $structureRepository->findOneById($perimetreActif);
        $filtreStructure = false;
        switch ($typePerimetre) {
            case Personne::TYPE_PERIMETRE_REGION:
                $structuresSelectionnees = $structureRepository->findStructuresFillesSLAActives($structureActive);
                $filtreStructure = false;
                break;
            case Personne::TYPE_PERIMETRE_SLA:
            case Personne::TYPE_PERIMETRE_SLAN:
                $filtreStructure = false;
                $structuresSelectionnees[] = $structureActive; //On rajoute la SLAN
                break;
            case Personne::TYPE_PERIMETRE_NATIONAL:
            case Personne::TYPE_PERIMETRE_ADMIN:
                $dataStructures = $structureRepository->findStructuresRegion();
                $structuresSelectionnees = $structureRepository->findAllStructuresSLAActives();
                $filtreStructure = true;
                break;
            default:
                break;
        }
        $formSelectionPerimetreAPL = null;
        if ($filtreStructure) {
            $formSelectionPerimetreAPL =  $this->createFormBuilder(null, [
                'action' => $this->generateUrl('resume_apl'),
            ])->add('structure', EntityType::class, [
                'class' => Structure::class,
                'label_attr' => [
                    'class' => 'fw-bold text-primary text-center',
                ],
                'placeholder' => 'Sélectionner la région',
                'mapped' => false,
                'expanded' => false,
                'multiple' => false,
                'choice_label' => 'nom',
                'label' => "Structure",
                "attr" => [
                    "class" => "text-center border-secondary",
                ],
                'choices' => $dataStructures,
            ])->getForm();


            $formSelectionPerimetreAPL->handleRequest($request);

            if ($formSelectionPerimetreAPL->isSubmitted()) {
                if ($formSelectionPerimetreAPL->isValid()) {
                    /** @var Personne $personne */

                    $structureSelectionnee = $formSelectionPerimetreAPL->get('structure')->getData();
                    if ($structureSelectionnee !== null) {
                        $structuresSelectionnees = $structureRepository->findStructuresFillesSLAActives($structureSelectionnee);
                    }
                }
            }
        }


        $comptesRendus = $compteRenduInstanceRepository->findByStructuresForSaisonActive($structuresSelectionnees);
        return $this->render('components/_resume_apl_sla.html.twig', [
            "formSelectionPerimetreAPL" => $formSelectionPerimetreAPL ? $formSelectionPerimetreAPL->createView() : null,
            "comptesRendus" => $comptesRendus,

        ]);
    }




    #[Route('/agora/redirect/perimetre', name: 'redirectPerimetre')]
    public function redirectPerimetre(Request $request, RedirectionHelper $redirectionHelper): Response
    {
        return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route("/nav/bar/select/perimetre", name: "nav_bar_selection_perimetre", methods: ["GET", "POST"])]
    public function selectionPerimetreNavbar(Request $request, RedirectionHelper $redirectionHelper, StructureRepository $structureRepository, PersonneRepository $personneRepository)
    {
        /** @var Personne $personne */
        $personne = $this->getUser();
        $personne = $personneRepository->findOneBy(["id" => $personne->getId()]);
        $perimetreActif = $request->getSession()->get('perimetre');
        if ($perimetreActif !== null) {
            $formSelectionPerimetreForNavBar = $this->createForm(SelectionPerimetreForNavBarType::class, $personne, [
                "personne" => $personne,
                "perimetreActif" => $perimetreActif,
                'action' => $this->generateUrl('nav_bar_selection_perimetre'),
                'doneRedirect' => $this->generateUrl('redirectPerimetre')
            ]);

            $formSelectionPerimetreForNavBar->handleRequest($request);
            if ($formSelectionPerimetreForNavBar->isSubmitted() && $formSelectionPerimetreForNavBar->isValid()) {

                $perimetreActif = $formSelectionPerimetreForNavBar->get('perimetre')->getData();
                $typePerimetre = $this->deduitTypePerimetre($perimetreActif, $structureRepository);
                $request->getSession()->set('perimetre', $perimetreActif);
                $request->getSession()->set('type_perimetre', $typePerimetre);
                return $this->redirectToRoute($redirectionHelper->retourHomePagePerimetre($request));
            }
        }

        return $this->render('nav_bar/_selection_perimetre.html.twig', [
            'formSelectionPerimetreForNavBar' => $formSelectionPerimetreForNavBar->createView()
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route("/agora/home/delegation/droits", name: "delegation_droits_non_admin", methods: ["GET", "POST"])]
    public function delegationDroits(Request $request, DroitsHelper $droitsHelper, NotificationHelper $notificationHelper, PersonneRepository $personneRepository, DroitRepository $droitRepository, EntityManagerInterface $manager)
    {
        /** @var Personne $personne */
        $personne = $this->getUser();
        $personne = $personneRepository->findOneBy(["id" => $personne->getId()]);
        $droitsManuelsSupprimables = [];
        //On récupere le droit le plus fort pour chaque structure
        $arrayDroitLePlusFortParStructure = $droitsHelper->getListeDroitsLePlusFortPourCoupleStructurePersonne($personne->getDroitsActifs()->toArray());
        /** @var Droit $droit */
        foreach ($arrayDroitLePlusFortParStructure as $droit) {
            $droitsManuelsSupprimables = array_merge($droitsManuelsSupprimables, $droitRepository->findDroitsManuelsActifsForStructureEtDroits(null, $droit->getStructure(), $droitsHelper->getDroitsPlusFaibles($droit->getDroit())));
        }

        $structuresAvecDroits = $droitsHelper->getStructuresDroitsPersonne($personne);
        $droitManuel = new Droit();
        $form_delegation_droit = $this->createForm(DelegationDroitNonAdminType::class, $droitManuel, [
            "structuresDisponibles" => $structuresAvecDroits,
            'action' => $request->getRequestUri()
        ]);
        $form_delegation_droit->handleRequest($request);

        if ($form_delegation_droit->isSubmitted()) {
            if ($form_delegation_droit->isValid()) {
                try {
                    $duree = $form_delegation_droit->get('duree')->getData();
                    $dateExpiration = new DateTime();
                    $dateExpiration = $dateExpiration->add(new DateInterval($duree));
                    if ($droitManuel->getPersonne()->aDroit($droitManuel->getDroit(), $droitManuel->getStructure())) {
                        if ($droitManuel->getPersonne()->aDroit($droitManuel->getDroit(), $droitManuel->getStructure(), $dateExpiration)) {
                            $this->addFlash("warning", "Cette personne a déjà ce droit, il ne sera pas ajouté");
                            return $this->redirectToRoute('delegation_droits_non_admin');
                        } else {
                            /** @var Droit $droitStructure */
                            foreach ($droitManuel->getPersonne()->getDroitsPourStructure($droitManuel->getStructure()) as $droitStructure) {
                                if (
                                    $droitManuel->getDroit() == $droitStructure->getDroit()
                                    && $droitStructure->getType() == Droit::TYPE_DROIT_MANUEL
                                ) {
                                    $droitStructure->setDateExpiration($dateExpiration);
                                    $manager->persist($droitStructure);
                                    $manager->flush();
                                    $this->addFlash("success", "Le droit manuel étant existant, il a été rallongé");
                                    return $this->redirectToRoute('delegation_droits_non_admin');
                                }
                            }
                        }
                    }
                    $droitManuel->setDateExpiration($dateExpiration);
                    $droitManuel->setType(Droit::TYPE_DROIT_MANUEL);
                    $manager->persist($droitManuel);
                    $manager->flush();


                    $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_007, [
                        NotificationHelper::PERIMETRE_DROIT => $droitManuel
                    ]);
                    $this->addFlash("success", "Le droit a été ajouté");
                    return $this->redirectToRoute('delegation_droits_non_admin');
                } catch (Exception $e) {
                    $this->addFlash('danger', "Erreur lors de l'ajout du droit: " . $e->getMessage());
                }
            }
        }

        return $this->render('agora/delegation_droits.html.twig', [
            'form' => $form_delegation_droit->createView(),
            'droitsManuelsSupprimables' => $droitsManuelsSupprimables
        ]);
    }

    #[Route('/agora/home/supprimer/droit/{droitManuel}', name: 'supprimer_droit_non_admin')]
    public function suppressionDroit(Droit $droitManuel, NotificationHelper $notificationHelper, EntityManagerInterface $manager): Response
    {

        if ($droitManuel->getType() == Droit::TYPE_DROIT_AUTO) {
            $this->addFlash('danger', "Ce droit est un droit automatique, il ne peut pas être suprimé");
        } else {
            $notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_005, [
                NotificationHelper::PERIMETRE_DROIT => $droitManuel
            ]);
            $manager->remove($droitManuel);
            $manager->flush();

            $this->addFlash('success', "Le droit a été supprimé");
        }
        return $this->redirectToRoute('delegation_droits_non_admin');
    }

    private function deduitTypePerimetre($perimetreActif, StructureRepository $structureRepository): string
    {
        if ($perimetreActif == Personne::ROLE_ADMIN) {
            $typePerimetre = Personne::TYPE_PERIMETRE_ADMIN;
        } else {
            $structureActive = $structureRepository->findOneById($perimetreActif);
            if ($structureActive->estUneSLA()) {
                $typePerimetre =  Personne::TYPE_PERIMETRE_SLA;
            } else if ($structureActive->estUneSLAN()) {
                $typePerimetre = Personne::TYPE_PERIMETRE_SLAN;
            } else if ($structureActive->estUneRegion()) {
                $typePerimetre = Personne::TYPE_PERIMETRE_REGION;
            } else {
                $typePerimetre =  Personne::TYPE_PERIMETRE_NATIONAL;
            }
        }
        return $typePerimetre;
    }
}
