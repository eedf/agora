<?php

namespace App\Security\Provider;

use App\Entity\Personne;
use App\Repository\PersonneRepository;
use Drenso\OidcBundle\Model\OidcUserData;
use Drenso\OidcBundle\Security\Exception\OidcUserNotFoundException;
use Drenso\OidcBundle\Security\UserProvider\OidcUserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;

class JeitoUserProvider implements OidcUserProviderInterface
{
    private PersonneRepository $personneRepository;


    public function __construct(PersonneRepository $personneRepository)
    {
        $this->personneRepository = $personneRepository;
    }
    /** @throws OidcException Can be thrown when the user cannot be created */
    public function ensureUserExists(string $userIdentifier, OidcUserData $userData)
    {
    }

    /** Custom user loader method to be able to distinguish oidc authentications */
    public function loadOidcUser(string $userIdentifier): UserInterface
    {
        $personne = $this->personneRepository->findOneBy(["uuid" => $userIdentifier]);
        if ($personne == null) {
            throw new OidcUserNotFoundException($userIdentifier . " ne correspond à aucune personne connue dans Agora");
        }
        return $personne;
    }

    /**
     * Refreshes the user.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @return Personne
     *
     * @throws UnsupportedUserException if the user is not supported
     * @throws UserNotFoundException    if the user is not found
     */
    public function refreshUser(UserInterface $user): Personne
    {
        if (!$user instanceof Personne) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        $personne = $this->personneRepository->findOneBy(["id" => $user->getId()]);
        if ($personne == null) {
            throw new OidcUserNotFoundException($user->getId() . " ne correspond à aucune personne connue dans Agora");
        }
        return $personne;
    }




    /**
     * Whether this provider supports the given user class.
     *
     * @return bool
     */
    public function supportsClass(string $class): bool
    {
        return Personne::class === $class || is_subclass_of($class, Personne::class);
    }

    /**
     * Loads the user for the given user identifier (e.g. username or email).
     *
     * This method must throw UserNotFoundException if the user is not found.
     *
     * @throws UserNotFoundException
     */
    public function loadUserByIdentifier(string $identifier): Personne
    {
        $personne = $this->personneRepository->findOneBy(["uuid" => $identifier]);
        if ($personne == null) {
            throw new OidcUserNotFoundException($identifier . " ne correspond à aucune personne connue dans Agora");
        }
        return $personne;
    }
}
