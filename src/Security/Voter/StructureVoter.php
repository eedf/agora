<?php

// src/Security/StructureVoter.php
namespace App\Security\Voter;

use App\Entity\Personne;
use App\Entity\Structure;
use App\Repository\PersonneRepository;
use App\Repository\StructureRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class StructureVoter extends Voter
{
    // these strings are just invented: you can use anything
    const CONSULTER = 'consulter';
    const CONSULTER_ARCHIVE = 'consulter_archive';
    const SAISIR = 'saisir';
    const VALIDER = 'valider';
    const MODIFIER_APRES_VALIDATION = 'modifier_apres_validation';

    private StructureRepository $structureRepository;
    private PersonneRepository $personneRepository;

    public function __construct(
        StructureRepository $structureRepository,
        PersonneRepository $personneRepository
    ) {
        $this->structureRepository = $structureRepository;
        $this->personneRepository = $personneRepository;
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::CONSULTER, self::CONSULTER_ARCHIVE, self::SAISIR, self::VALIDER, self::CONSULTER, self::MODIFIER_APRES_VALIDATION])) {
            return false;
        }

        // only vote on `Structure` objects
        if (!$subject instanceof Structure) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        /** @var Personne $user */
        $user = $token->getUser();


        if (!$user instanceof Personne) {
            // the user must be logged in; if not, deny access
            return false;
        }
        $personne = $this->personneRepository->findOneBy(['id' =>  $user->getId()]);

        // you know $subject is a Structure object, thanks to `supports()`
        /** @var Structure $structure */
        $structure = $subject;

        return match ($attribute) {
            self::CONSULTER => $this->canConsulter($structure, $personne),
            self::CONSULTER_ARCHIVE => $this->canConsulterArchive($structure, $personne),
            self::SAISIR => $this->canSaisir($structure, $personne),
            self::VALIDER => $this->canValider($structure, $personne),
            self::MODIFIER_APRES_VALIDATION => $this->canModifierApresValidation($structure, $personne),
            default => throw new \LogicException('This code should not be reached!')
        };
    }

    private function canConsulter(Structure $structure, Personne $personne): bool
    {

        if ($this->canConsulterArchive($structure, $personne)) {
            return true;
        }
        if ($personne->aDroit(StructureVoter::CONSULTER, $structure)) {
            return true;
        }
        return false;
    }

    private function canConsulterArchive(Structure $structure, Personne $personne): bool
    {
        if ($this->canSaisir($structure, $personne)) {
            return true;
        }

        //Si la personne a un droit manuel
        if ($personne->aDroit(StructureVoter::CONSULTER_ARCHIVE, $structure)) {
            return true;
        }

        return false;
    }
    private function canSaisir(Structure $structure, Personne $personne): bool
    {
        if ($this->canValider($structure, $personne)) {
            return true;
        }

        //Si la personne a un droit manuel
        if ($personne->aDroit(StructureVoter::SAISIR, $structure)) {
            return true;
        }

        return false;
    }
    private function canValider(Structure $structure, Personne $personne): bool
    {
        if ($this->canModifierApresValidation($structure, $personne)) {
            return true;
        }
        //Si la personne a un droit manuel
        if ($personne->aDroit(StructureVoter::VALIDER, $structure)) {
            return true;
        }
        return false;
    }
    private function canModifierApresValidation(Structure $structure, Personne $personne): bool
    {
        //Si la personne a un droit manuel
        if ($personne->aDroit(StructureVoter::MODIFIER_APRES_VALIDATION, $structure)) {
            return true;
        }
        return $personne->hasRole(Personne::ROLE_ADMIN);
    }
}
