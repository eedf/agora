<?php

namespace App\Message;

use PhpParser\Node\Expr\Cast\String_;

class MessageNotification
{
    public function __construct(
        private String $titre,
        private ?String $lien,
        private String $contenu,
        private array $destinataires
    ) {
    }

    public function getTitre(): String
    {
        return $this->titre;
    }

    public function getContenu(): String
    {
        return $this->contenu;
    }

    public function getLien(): ?String
    {
        return $this->lien;
    }

    public function getDestinataires(): ?array
    {
        return $this->destinataires;
    }
}
