<?php

namespace App\Entity;

use App\Repository\VoteInstanceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity(repositoryClass: VoteInstanceRepository::class)]
class VoteInstance
{
    /** @deprecated changement des status de l'AG 2024 */
    const TYPE_RAPPORT_MORAL_ACTIVITE = "Rapport Moral et d'Activités";

    const TYPE_RAPPORT_MORAL = "Rapport Moral";
    const TYPE_RAPPORT_FINANCIER = 'Rapport Financier';
    const TYPE_RAPPORT_PLAN_ACTION_LOCAL = "Plan d'Action Local";
    const TYPE_RAPPORT_PLAN_ACTION_REGIONAL = "Plan d'Action Régional";
    const TYPE_RAPPORT_BUDGET_PREVISIONNEL = 'Budget prévisionnel';

    const LISTE_RAPPORTS_SLA = [
        self::TYPE_RAPPORT_MORAL,
        self::TYPE_RAPPORT_FINANCIER,
        self::TYPE_RAPPORT_PLAN_ACTION_LOCAL,
        self::TYPE_RAPPORT_BUDGET_PREVISIONNEL
    ];

    const LISTE_RAPPORTS_CONGRES = [
        self::TYPE_RAPPORT_MORAL,
        self::TYPE_RAPPORT_FINANCIER,
        self::TYPE_RAPPORT_PLAN_ACTION_REGIONAL,
        self::TYPE_RAPPORT_BUDGET_PREVISIONNEL
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'votesInstances')]
    #[ORM\JoinColumn(nullable: false)]
    private ?CompteRenduInstance $instance = null;

    #[ORM\OneToOne(inversedBy: 'voteInstance', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?Vote $vote = null;

    #[ORM\Column(length: 255)]
    private ?string $type = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInstance(): ?CompteRenduInstance
    {
        return $this->instance;
    }

    public function setInstance(?CompteRenduInstance $instance): static
    {
        $this->instance = $instance;

        return $this;
    }

    public function getVote(): ?Vote
    {
        return $this->vote;
    }

    public function setVote(?Vote $vote): static
    {
        $this->vote = $vote;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    #[Assert\Callback]
    protected function verificationVote(ExecutionContextInterface $context, mixed $payload)
    {
        if ($this->getVote()) {
            $nombreVotants = $this->getInstance()->getNombreVotantsPourCategorie($this->getType());
            if ($this->getVote()->getNombreVoixDefavorables() === null && ($this->getVote()->getNombreVoixFavorables() || $this->getVote()->getNombreVoixBlancs())) {
                $context->buildViolation("Le nombre de votes défavorables doit être indiqué")
                    ->addViolation();
            } else if ($this->getVote()->getNombreVoixFavorables() === null && ($this->getVote()->getNombreVoixDefavorables() || $this->getVote()->getNombreVoixBlancs())) {
                $context->buildViolation("Le nombre de votes favorables doit être indiqué")
                    ->addViolation();
            } else if ($this->getVote()->getNombreVoixBlancs() === null && ($this->getVote()->getNombreVoixDefavorables() || $this->getVote()->getNombreVoixFavorables())) {
                $context->buildViolation("Le nombre de votes blancs doit être indiqué")
                    ->addViolation();
            } else if (
                ($this->getVote()->getNombreVoixFavorables()
                    + $this->getVote()->getNombreVoixDefavorables()
                    + $this->getVote()->getNombreVoixBlancs()
                ) > $nombreVotants
            ) {
                $context->buildViolation("Le nombre de total de voix (favorables, défavorables et blancs) doit être inférieur ou égal au nombre total de votant·e·s")
                    ->addViolation();
            }
        }
    }
}
