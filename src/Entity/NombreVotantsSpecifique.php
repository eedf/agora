<?php

namespace App\Entity;

use App\Repository\NombreVotantsSpecifiqueRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: NombreVotantsSpecifiqueRepository::class)]
class NombreVotantsSpecifique
{

    const CATEGORIE_ELECTION_EGA = "Equipe de gestion et d'animation";
    const CATEGORIE_ELECTION_ER = "Equipe régionale";
    const CATEGORIE_VOEUX = "Voeux";
    const CATEGORIE_MOTIONS = "Motions";
    const CATEGORIE_DELEGUES_AG = "Délégué·es à l'Assemblée Générale";
    const CATEGORIE_SUPPLEANTS_AG = "Délégué·es suppléant·es à l'Assemblée Générale";
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreVotants = null;

    #[ORM\Column(length: 255)]
    private ?string $categorie = null;

    #[ORM\ManyToOne(inversedBy: 'nombreVotantsSpecifiques')]
    #[ORM\JoinColumn(nullable: false)]
    private ?CompteRenduInstance $compteRendu = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreVotants(): ?int
    {
        return $this->nombreVotants;
    }

    public function setNombreVotants(?int $nombreVotants): static
    {
        $this->nombreVotants = $nombreVotants;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): static
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getCompteRendu(): ?CompteRenduInstance
    {
        return $this->compteRendu;
    }

    public function setCompteRendu(?CompteRenduInstance $compteRendu): static
    {
        $this->compteRendu = $compteRendu;

        return $this;
    }
}
