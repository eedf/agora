<?php

namespace App\Entity;

use App\Repository\VoteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VoteRepository::class)]
class Vote
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreVoixFavorables = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreVoixDefavorables = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreDeVoixAbstention = null;

    #[ORM\OneToOne(mappedBy: 'vote', cascade: ['persist', 'remove'])]
    private ?VoteInstance $voteInstance = null;

    #[ORM\OneToOne(mappedBy: 'vote', cascade: ['persist', 'remove'])]
    private ?ElectionNominative $electionNominative = null;

    #[ORM\OneToOne(mappedBy: 'vote', cascade: ['persist', 'remove'])]
    private ?ElectionEquipe $electionEquipe = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreVoixBlancs = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreVoixFavorables(): ?int
    {
        return $this->nombreVoixFavorables;
    }

    public function setNombreVoixFavorables(?int $nombreVoixFavorables): static
    {
        $this->nombreVoixFavorables = $nombreVoixFavorables;

        return $this;
    }

    public function getNombreVoixDefavorables(): ?int
    {
        return $this->nombreVoixDefavorables;
    }

    public function setNombreVoixDefavorables(?int $nombreVoixDefavorables): static
    {
        $this->nombreVoixDefavorables = $nombreVoixDefavorables;

        return $this;
    }

    public function getNombreDeVoixAbstention(): ?int
    {
        return $this->nombreDeVoixAbstention;
    }

    public function setNombreDeVoixAbstention(?int $nombreDeVoixAbstention): static
    {
        $this->nombreDeVoixAbstention = $nombreDeVoixAbstention;

        return $this;
    }

    public function getVoteInstance(): ?VoteInstance
    {
        return $this->voteInstance;
    }

    public function setVoteInstance(VoteInstance $voteInstance): static
    {
        // set the owning side of the relation if necessary
        if ($voteInstance->getVote() !== $this) {
            $voteInstance->setVote($this);
        }

        $this->voteInstance = $voteInstance;

        return $this;
    }

    public function getElectionNominative(): ?ElectionNominative
    {
        return $this->electionNominative;
    }

    public function setElectionNominative(ElectionNominative $electionNominative): static
    {
        // set the owning side of the relation if necessary
        if ($electionNominative->getVote() !== $this) {
            $electionNominative->setVote($this);
        }

        $this->electionNominative = $electionNominative;

        return $this;
    }

    public function getElectionEquipe(): ?ElectionEquipe
    {
        return $this->electionEquipe;
    }

    public function setElectionEquipe(ElectionEquipe $electionEquipe): static
    {
        // set the owning side of the relation if necessary
        if ($electionEquipe->getVote() !== $this) {
            $electionEquipe->setVote($this);
        }

        $this->electionEquipe = $electionEquipe;

        return $this;
    }

    public function getNombreVoixBlancs(): ?int
    {
        return $this->nombreVoixBlancs;
    }

    public function setNombreVoixBlancs(?int $nombreVoixBlancs): static
    {
        $this->nombreVoixBlancs = $nombreVoixBlancs;

        return $this;
    }
}
