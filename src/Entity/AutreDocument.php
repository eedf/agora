<?php

namespace App\Entity;

use App\Repository\AutreDocumentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AutreDocumentRepository::class)]
class AutreDocument
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?DocumentFile $document = null;

    #[ORM\Column(length: 255)]
    private ?string $titre = null;

    #[ORM\ManyToOne(inversedBy: 'autresDocuments')]
    #[ORM\JoinColumn(nullable: false)]
    private ?CompteRenduInstance $compteRenduInstance = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocument(): ?DocumentFile
    {
        return $this->document;
    }

    public function setDocument(DocumentFile $document): static
    {
        $this->document = $document;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }

    public function getCompteRenduInstance(): ?CompteRenduInstance
    {
        return $this->compteRenduInstance;
    }

    public function setCompteRenduInstance(?CompteRenduInstance $compteRenduInstance): static
    {
        $this->compteRenduInstance = $compteRenduInstance;

        return $this;
    }

    #[Assert\Callback]
    protected function verificationDateInstance(ExecutionContextInterface $context, mixed $payload)
    {

        if ($this->getDocument()->getDocumentSize() == 0 && !$this->getDocument()->getDocumentFile()) {
            $context->buildViolation("Le fichier doit être sélectionné.")
                ->addViolation();
        }
    }
}
