<?php

namespace App\Entity;

use App\Repository\EquipeRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: EquipeRepository::class)]
class Equipe
{

    const TYPE_EQUIPE_SUPPORT_DEVELOPPEMENT = 1;
    const TYPE_EQUIPE_EQUIPE_NATIONALE = 2;
    const TYPE_EQUIPE_COMMISSION_CONTROLE = 4;
    const TYPE_EQUIPE_COMMISSION = 5;
    const TYPE_EQUIPE_EQUIPE_GESTION = 6;
    const TYPE_EQUIPE_FORMATION = 7;
    const TYPE_EQUIPE_ACTIVITE_ADAPTEE = 8;
    const TYPE_EQUIPE_UNITE_AINE = 9;
    const TYPE_EQUIPE_UNITE_ECLE = 10;
    const TYPE_EQUIPE_CERCLE_LOUVETEAUX = 11;
    const TYPE_EQUIPE_RONDE_LUTINS = 12;
    const TYPE_EQUIPE_UNITE_NOMADE = 13;
    const TYPE_EQUIPE_ACTIVITE_COMPLEMENTAIRE = 14;
    const TYPE_EQUIPE_PERSONNALISEE = 15;


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'equipes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Structure $structure = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateCreation = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $type = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDesactivation = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    #[Groups(['jeito'])]
    private ?Uuid $uuid = null;

    #[ORM\OneToMany(mappedBy: 'equipe', targetEntity: Fonction::class, orphanRemoval: true)]
    private Collection $fonctions;

    #[ORM\OneToMany(mappedBy: 'equipe', targetEntity: AttributionFonctionElective::class, orphanRemoval: true)]
    private Collection $attributionsFonctionsElectives;


    public function __construct()
    {
        $this->fonctions = new ArrayCollection();
        $this->attributionsFonctionsElectives = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): static
    {
        $this->structure = $structure;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTimeInterface $dateCreation): static
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(?\DateTimeInterface $dateModification): static
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getDateDesactivation(): ?\DateTimeInterface
    {
        return $this->dateDesactivation;
    }

    public function setDateDesactivation(?\DateTimeInterface $dateDesactivation): static
    {
        $this->dateDesactivation = $dateDesactivation;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Fonction>
     */
    public function getFonctions(): Collection
    {
        return $this->fonctions;
    }
    /**
     * @return Collection<int, Fonction>
     */
    public function getFonctionsActives(?String $typeFonction = null, ?DateTime $date = null): Collection
    {
        if ($date == null) {
            $date = new DateTime();
        }

        $fonctionsActives = $this->fonctions->filter(function (Fonction $fonction) use ($typeFonction, $date) {
            if ($typeFonction == null) {
                return ($fonction->estActive($date));
            } else {
                return ($fonction->estActive($date)
                    && $fonction->getType() == $typeFonction
                );
            }
        });
        return $fonctionsActives;
    }

    public function addFonction(Fonction $fonction): static
    {
        if (!$this->fonctions->contains($fonction)) {
            $this->fonctions->add($fonction);
            $fonction->setEquipe($this);
        }

        return $this;
    }

    public function removeFonction(Fonction $fonction): static
    {
        if ($this->fonctions->removeElement($fonction)) {
            // set the owning side to null (unless already changed)
            if ($fonction->getEquipe() === $this) {
                $fonction->setEquipe(null);
            }
        }

        return $this;
    }



    public function getActif(): ?bool
    {
        return $this->dateDesactivation == null;
    }

    /**
     * @return Collection<int, AttributionFonctionElective>
     */
    public function getAttributionsFonctionsElectives(): Collection
    {
        return $this->attributionsFonctionsElectives;
    }

    public function addAttributionsFonctionsElective(AttributionFonctionElective $attributionsFonctionsElective): static
    {
        if (!$this->attributionsFonctionsElectives->contains($attributionsFonctionsElective)) {
            $this->attributionsFonctionsElectives->add($attributionsFonctionsElective);
            $attributionsFonctionsElective->setEquipe($this);
        }

        return $this;
    }

    public function removeAttributionsFonctionsElective(AttributionFonctionElective $attributionsFonctionsElective): static
    {
        if ($this->attributionsFonctionsElectives->removeElement($attributionsFonctionsElective)) {
            // set the owning side to null (unless already changed)
            if ($attributionsFonctionsElective->getEquipe() === $this) {
                $attributionsFonctionsElective->setEquipe(null);
            }
        }

        return $this;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }
}
