<?php

namespace App\Entity;

use App\Repository\RoleEquipeElueRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RoleEquipeElueRepository::class)]
class RoleEquipeElue
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'rolesEquipeElue')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    private ?Personne $personne = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $fonctionSpecifique = null;

    #[ORM\ManyToOne(inversedBy: 'roles')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ElectionEquipe $electionEquipe = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): static
    {
        $this->personne = $personne;

        return $this;
    }

    public function getFonctionSpecifique(): ?string
    {
        return $this->fonctionSpecifique;
    }

    public function setFonctionSpecifique(?string $fonctionSpecifique): static
    {
        $this->fonctionSpecifique = $fonctionSpecifique;

        return $this;
    }

    public function getElectionEquipe(): ?ElectionEquipe
    {
        return $this->electionEquipe;
    }

    public function setElectionEquipe(?ElectionEquipe $electionEquipe): static
    {
        $this->electionEquipe = $electionEquipe;

        return $this;
    }
}
