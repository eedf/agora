<?php

namespace App\Entity;

use App\Repository\EmployeRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: EmployeRepository::class)]
class Employe
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::BIGINT, unique: true)]
    #[Groups(['recherche_auto_complete'])]
    private ?string $numeroSalarie = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\OneToOne(inversedBy: 'employe', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Personne $personne = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Email(
        message: "Cette adresse mail '{{ value }}' n'est pas valide."
    )]
    private ?string $email = null;

    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[AssertPhoneNumber(defaultRegion: 'FR')]
    private $telephone = null;

    #[ORM\OneToMany(mappedBy: 'employe', targetEntity: ContratTravail::class, orphanRemoval: true, fetch: "EAGER")]
    private Collection $contratsTravail;

    #[ORM\Column(type: 'uuid', unique: true)]
    private ?Uuid $uuid = null;

    public function __construct()
    {
        $this->contratsTravail = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroSalarie(): ?string
    {
        return $this->numeroSalarie;
    }

    public function setNumeroSalarie(string $numeroSalarie): static
    {
        $this->numeroSalarie = $numeroSalarie;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): static
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(Personne $personne): static
    {
        $this->personne = $personne;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($telephone): static
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return Collection<int, ContratTravail>
     */
    public function getContratsTravail(): Collection
    {
        return $this->contratsTravail;
    }

    /**
     * @return ContratTravail
     */
    public function getContratTravailCourantActif(DateTime $date = null): ?ContratTravail
    {
        if (!$date) {
            $date = new DateTime();
        }
        if ($this->getContratsTravail()->count() > 0) {
            $contratsActifs = $this->contratsTravail->filter(function (ContratTravail $contratTravail) use ($date) {
                return ($contratTravail->estActif());
            });


            if ($contratsActifs->count() > 0) {
                return $contratsActifs->first();
            }
        }
        return null;
    }

    public function addContratsTravail(ContratTravail $contratsTravail): static
    {
        if (!$this->contratsTravail->contains($contratsTravail)) {
            $this->contratsTravail->add($contratsTravail);
            $contratsTravail->setEmploye($this);
        }

        return $this;
    }

    public function removeContratsTravail(ContratTravail $contratsTravail): static
    {
        if ($this->contratsTravail->removeElement($contratsTravail)) {
            // set the owning side to null (unless already changed)
            if ($contratsTravail->getEmploye() === $this) {
                $contratsTravail->setEmploye(null);
            }
        }

        return $this;
    }

    //Vérifie si l'employé à un contrat actif a la date indiquée (si pas de date, on prend la date courante)
    public function hasContratActif(?DateTime $date = null): bool
    {
        if (!$date) {
            $date = new DateTime();
        }
        $contratsActifs = $this->contratsTravail->filter(function (ContratTravail $contratTravail) use ($date) {
            return (($contratTravail->getDateDebut() <= $date
                && $contratTravail->getDateFin() >= $date
            ) || ($contratTravail->getDateDebut() <= $date
                && $contratTravail->getDateFin() == null)
            );
        });

        return count($contratsActifs) > 0;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }
}
