<?php

namespace App\Entity;

use App\Repository\StructureRepository;
use App\Service\DroitsHelper;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bridge\Doctrine\Types\UuidType;

#[ORM\Entity(repositoryClass: StructureRepository::class)]
class Structure
{

    const ECHELON_STRUCTURE_NATIONAL = 1;
    const ECHELON_STRUCTURE_REGIONAL = 2;
    const ECHELON_STRUCTURE_LOCAL = 3;

    const TYPE_STRUCTURE_ASSOCIATION = 9;
    const TYPE_STRUCTURE_REGION = 6;
    const TYPE_STRUCTURE_GROUPE_LOCAL = 10;
    const TYPE_STRUCTURE_CENTRE = 15;
    const TYPE_STRUCTURE_SERVICE_VACANCES = 16;
    const TYPE_STRUCTURE_AUTRES = 4;

    const MAP_ECHELON_BY_TYPE_STRUCTURE = [
        self::TYPE_STRUCTURE_ASSOCIATION => self::ECHELON_STRUCTURE_NATIONAL,
        self::TYPE_STRUCTURE_REGION => self::ECHELON_STRUCTURE_REGIONAL,
        self::TYPE_STRUCTURE_GROUPE_LOCAL => self::ECHELON_STRUCTURE_LOCAL,
        self::TYPE_STRUCTURE_CENTRE => self::ECHELON_STRUCTURE_LOCAL,
        self::TYPE_STRUCTURE_SERVICE_VACANCES => self::ECHELON_STRUCTURE_LOCAL,
        self::TYPE_STRUCTURE_AUTRES => self::ECHELON_STRUCTURE_LOCAL
    ];

    const STATUT_STRUCTURE_AUTONOME = 1;
    const STATUT_STRUCTURE_RATTACHEE = 2;
    const STATUT_STRUCTURE_EN_SOMMEIL = 3;
    const STATUT_STRUCTURE_FERMEE = 4;


    const LISTE_TYPES_STRUCTURES_SLA_OU_SLAN = [
        self::TYPE_STRUCTURE_GROUPE_LOCAL,
        self::TYPE_STRUCTURE_CENTRE,
        self::TYPE_STRUCTURE_SERVICE_VACANCES,
        self::TYPE_STRUCTURE_AUTRES
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: UuidType::NAME, unique: true)]
    private ?Uuid $uuid = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 100)]
    private ?string $type = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'structuresFilles', fetch: "EAGER")]
    private ?self $structureParent = null;

    #[ORM\OneToMany(mappedBy: 'structureParent', targetEntity: self::class)]
    private Collection $structuresFilles;

    #[ORM\Column(length: 255)]
    private ?string $statut = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $echelon = null;

    #[ORM\OneToMany(mappedBy: 'structure', targetEntity: Equipe::class, orphanRemoval: true)]
    private Collection $equipes;

    #[ORM\OneToMany(mappedBy: 'structure', targetEntity: Adhesion::class, orphanRemoval: true)]
    private Collection $adhesions;

    #[ORM\OneToMany(mappedBy: 'structure', targetEntity: CompteRenduInstance::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $compteRendusInstances;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'structureSlanAffiliees')]
    private ?self $structureRegionaleAffiliee = null;

    #[ORM\OneToMany(mappedBy: 'structureRegionaleAffiliee', targetEntity: self::class)]
    private Collection $structureSlanAffiliees;

    #[ORM\OneToMany(mappedBy: 'structure', targetEntity: Droit::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $droits;

    #[ORM\Column]
    private ?bool $responsabiliteRegionale = false;

    public function __construct()
    {
        $this->structuresFilles = new ArrayCollection();
        $this->structureSlanAffiliees = new ArrayCollection();
        $this->equipes = new ArrayCollection();
        $this->adhesions = new ArrayCollection();
        $this->compteRendusInstances = new ArrayCollection();
        $this->droits = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getNom() . " (" . $this->getType() . ")";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): static
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getStructureParent(): ?self
    {
        return $this->structureParent;
    }

    public function setStructureParent(?self $structureParent): static
    {
        $this->structureParent = $structureParent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getStructureSlanAffiliees(): Collection
    {
        return $this->structureSlanAffiliees;
    }

    public function addStructureSlanAffiliee(self $structureSlanAffiliee): static
    {
        if (!$this->structureSlanAffiliees->contains($structureSlanAffiliee)) {
            $this->structureSlanAffiliees->add($structureSlanAffiliee);
            $structureSlanAffiliee->setStructureRegionaleAffiliee($this);
        }

        return $this;
    }

    public function removeStructureSlanAffiliee(self $structureSlanAffiliee): static
    {
        if ($this->structureSlanAffiliees->removeElement($structureSlanAffiliee)) {
            // set the owning side to null (unless already changed)
            if ($structureSlanAffiliee->getStructureRegionaleAffiliee() === $this) {
                $structureSlanAffiliee->setStructureRegionaleAffiliee(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, self>
     */
    public function getStructuresFilles(): Collection
    {
        return $this->structuresFilles;
    }

    public function addStructuresFille(self $structuresFille): static
    {
        if (!$this->structuresFilles->contains($structuresFille)) {
            $this->structuresFilles->add($structuresFille);
            $structuresFille->setStructureParent($this);
        }

        return $this;
    }

    public function removeStructuresFille(self $structuresFille): static
    {
        if ($this->structuresFilles->removeElement($structuresFille)) {
            // set the owning side to null (unless already changed)
            if ($structuresFille->getStructureParent() === $this) {
                $structuresFille->setStructureParent(null);
            }
        }

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): static
    {
        $this->statut = $statut;

        return $this;
    }

    public function getEchelon(): ?string
    {
        return $this->echelon;
    }

    public function setEchelon(?string $echelon): static
    {
        $this->echelon = $echelon;

        return $this;
    }

    /**
     * @return Collection<int, Equipe>
     */
    public function getEquipes(): Collection
    {
        return $this->equipes;
    }

    public function addEquipe(Equipe $equipe): static
    {
        if (!$this->equipes->contains($equipe)) {
            $this->equipes->add($equipe);
            $equipe->setStructure($this);
        }

        return $this;
    }

    public function removeEquipe(Equipe $equipe): static
    {
        if ($this->equipes->removeElement($equipe)) {
            // set the owning side to null (unless already changed)
            if ($equipe->getStructure() === $this) {
                $equipe->setStructure(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Adhesion>
     */
    public function getAdhesions(): Collection
    {
        return $this->adhesions;
    }

    public function addAdhesion(Adhesion $adhesion): static
    {
        if (!$this->adhesions->contains($adhesion)) {
            $this->adhesions->add($adhesion);
            $adhesion->setStructure($this);
        }

        return $this;
    }

    public function removeAdhesion(Adhesion $adhesion): static
    {
        if ($this->adhesions->removeElement($adhesion)) {
            // set the owning side to null (unless already changed)
            if ($adhesion->getStructure() === $this) {
                $adhesion->setStructure(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CompteRenduInstance>
     */
    public function getCompteRendusInstances(): Collection
    {
        return $this->compteRendusInstances;
    }

    public function getCompteRenduInstancePourSaison(SaisonDemocratique $saison): ?CompteRenduInstance
    {
        $compteRendusTrouvees = $this->getCompteRendusInstances()->filter(function (CompteRenduInstance $compteRenduInstance) use ($saison) {
            return $compteRenduInstance->getSaison() == $saison;
        });


        if (count($compteRendusTrouvees) == 1) {
            return $compteRendusTrouvees->first();
        } else {
            return null;
        }
    }

    public function addCompteRendusInstance(CompteRenduInstance $compteRendusInstance): static
    {
        if (!$this->compteRendusInstances->contains($compteRendusInstance)) {
            $this->compteRendusInstances->add($compteRendusInstance);
            $compteRendusInstance->setStructure($this);
        }

        return $this;
    }

    public function removeCompteRendusInstance(CompteRenduInstance $compteRendusInstance): static
    {
        if ($this->compteRendusInstances->removeElement($compteRendusInstance)) {
            // set the owning side to null (unless already changed)
            if ($compteRendusInstance->getStructure() === $this) {
                $compteRendusInstance->setStructure(null);
            }
        }

        return $this;
    }

    public function estUneSLA(): bool
    {
        return (in_array($this->type, self::LISTE_TYPES_STRUCTURES_SLA_OU_SLAN)
            && ($this->structureParent)
            && ($this->structureParent->echelon == self::ECHELON_STRUCTURE_REGIONAL));
    }

    public function estUneSLAN(): bool
    {
        return (in_array($this->type, self::LISTE_TYPES_STRUCTURES_SLA_OU_SLAN)
            && ($this->structureParent)
            && ($this->structureParent->echelon == self::ECHELON_STRUCTURE_NATIONAL));
    }
    public function estUneRegion(): bool
    {
        return ($this->type == self::TYPE_STRUCTURE_REGION
            && ($this->structureParent)
            && ($this->echelon == self::ECHELON_STRUCTURE_REGIONAL));
    }

    public function estNational(): bool
    {
        return ($this->type == self::TYPE_STRUCTURE_ASSOCIATION
            && ($this->echelon == self::ECHELON_STRUCTURE_NATIONAL));
    }

    /**
     * getEquipeGestion
     *  Retourne l'équipe de la structure qui correspond à l'équipe de gestion. S'il n'y a pas qu'un résultat, retourne null
     * @return Equipe
     */
    public function getEquipeGestion(): ?Equipe
    {
        return $this->getEquipeSpecifique(Equipe::TYPE_EQUIPE_EQUIPE_GESTION);
    }

    /**
     * getEquipeSpecifique
     *  Retourne l'équipe de la structure qui correspond à l'équipe de gestion. S'il n'y a pas qu'un résultat, retourne null
     * @return Equipe
     */
    public function getEquipeSpecifique(String $typeEquipe, $actif = true): ?Equipe
    {
        if ($actif) {
            $equipes_trouvees = $this->equipes->filter(function (Equipe $equipe) use ($typeEquipe) {
                return $equipe->getActif() && $equipe->getType() == $typeEquipe;
            });
        } else {
            $equipes_trouvees = $this->equipes->filter(function (Equipe $equipe) use ($typeEquipe) {
                return $equipe->getType() == $typeEquipe;
            });
        }


        if (count($equipes_trouvees) == 1) {
            return $equipes_trouvees->first();
        } else {
            return null;
        }
    }

    /**
     * getStructuresFillesDeResponsabiliteRegionale
     * Retourne la liste des structure fille de la structure courante qui sont de responsabilité régionale (SLAR)
     * @return Equipe
     */
    public function getStructuresFillesDeResponsabiliteRegionale(): Collection
    {
        $structuresTrouvees = $this->structuresFilles->filter(function (Structure $structureFille) {
            return $structureFille->isResponsabiliteRegionale();
        });
        return $structuresTrouvees;
    }

    public function getStructureRegionaleAffiliee(): ?self
    {
        return $this->structureRegionaleAffiliee;
    }

    public function setStructureRegionaleAffiliee(?self $structureRegionaleAffiliee): static
    {
        $this->structureRegionaleAffiliee = $structureRegionaleAffiliee;

        return $this;
    }

    /**
     * @return Collection<int, Droit>
     */
    public function getDroits(): Collection
    {
        return $this->droits;
    }

    public function getDroitsSuperieursOuEquivalents(String $droit): Collection
    {
        $droitsSuperieursOuEquivalents = $this->droits->filter(function (Droit $droitAComparer) use ($droit) {
            return (DroitsHelper::ARRAY_HIERARCHY_DROITS[$droitAComparer->getDroit()] >= DroitsHelper::ARRAY_HIERARCHY_DROITS[$droit]);
        });
        return $droitsSuperieursOuEquivalents;
    }

    public function addDroit(Droit $droit): static
    {
        if (!$this->droits->contains($droit)) {
            $this->droits->add($droit);
            $droit->setStructure($this);
        }

        return $this;
    }

    public function removeDroit(Droit $droit): static
    {
        if ($this->droits->removeElement($droit)) {
            // set the owning side to null (unless already changed)
            if ($droit->getStructure() === $this) {
                $droit->setStructure(null);
            }
        }

        return $this;
    }

    public function getNombreAdherents(?DateTime $date = null): int
    {
        if (!$date) {
            $date = new DateTime();
        }
        $nombre = 0;
        if ($this->estUneSLA() || $this->estUneSLAN()) {

            if ($this->adhesions->count() > 0) {
                $adhesionsActives = $this->adhesions->filter(function (Adhesion $adhesion) use ($date) {

                    return ($adhesion->getDateDebut() <= $date
                        && ($adhesion->getDateFin() == null || $adhesion->getDateFin() >= $date)
                    );
                });

                $nombre = $adhesionsActives->count();
            }
        } else if ($this->estUneRegion()) {
            if ($this->adhesions->count() > 0) {
                $adhesionsActives = $this->adhesions->filter(function (Adhesion $adhesion) use ($date) {

                    return ($adhesion->getDateDebut() <= $date
                        && ($adhesion->getDateFin() == null || $adhesion->getDateFin() >= $date)
                    );
                });

                $nombre = $adhesionsActives->count();
            }
            foreach ($this->getStructuresFilles() as $structureFille) {
                $nombre += $structureFille->getNombreAdherents($date);
            }
            foreach ($this->getStructureSlanAffiliees() as $structureSlanAffiliee) {
                $nombre += $structureSlanAffiliee->getNombreAdherents($date);
            }
        }
        return $nombre;
    }

    public function getDecompositionsNombreAdherents(?DateTime $date = null): array
    {
        $result = array();
        if (!$date) {
            $date = new DateTime();
        }

        if ($this->estUneRegion()) {

            //Pour la structure régionale
            $nombreStructureRegionale = 0;
            if ($this->adhesions->count() > 0) {
                $adhesionsActives = $this->adhesions->filter(function (Adhesion $adhesion) use ($date) {

                    return ($adhesion->getDateDebut() <= $date
                        && ($adhesion->getDateFin() == null || $adhesion->getDateFin() >= $date)
                    );
                });

                $nombreStructureRegionale = $adhesionsActives->count();
            }
            $result["Dans la structure régionale"] = $nombreStructureRegionale;

            //Pour les SLA
            $nombreSLA = 0;
            foreach ($this->getStructuresFilles() as $structureFille) {
                $nombreSLA += $structureFille->getNombreAdherents($date);
            }
            $result["Dans les groupes locaux et centres"] = $nombreSLA;

            //Pour les SLAN affiliées
            $nombreSLAN = 0;
            foreach ($this->getStructureSlanAffiliees() as $structureSlanAffiliee) {
                $nombreSLAN += $structureSlanAffiliee->getNombreAdherents($date);
            }
            $result["Dans les structures nationales affiliées"] = $nombreSLAN;
        }
        return $result;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function isResponsabiliteRegionale(): ?bool
    {
        return $this->responsabiliteRegionale;
    }

    public function setResponsabiliteRegionale(bool $responsabiliteRegionale): static
    {
        $this->responsabiliteRegionale = $responsabiliteRegionale;

        return $this;
    }
}
