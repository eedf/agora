<?php

namespace App\Entity;

use App\Repository\SaisonDemocratiqueRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SaisonDemocratiqueRepository::class)]
class SaisonDemocratique
{

    const DEFAULT_FORMAT_DATE_DEBUT_SAISON = 'first day of september this year';
    const DEFAULT_FORMAT_DATE_FIN_SAISON = 'last day of august next year';

    /** @deprecated changement des status de l'AG 2024 */
    const DATE_AG_2024 = "2024-06-04";


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateDebut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateFin = null;

    #[ORM\Column]
    private ?bool $actif = null;

    #[ORM\OneToMany(mappedBy: 'saison', targetEntity: CompteRenduInstance::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $comptesRendusInstances;

    #[ORM\OneToOne(inversedBy: 'saisonSuivante', targetEntity: self::class, cascade: ['persist', 'remove'])]
    private ?self $saisonPrecedente = null;

    #[ORM\OneToOne(mappedBy: 'saisonPrecedente', targetEntity: self::class, cascade: ['persist', 'remove'])]
    private ?self $saisonSuivante = null;

    public function __construct()
    {
        $this->comptesRendusInstances = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }


    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): static
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): static
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function isActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): static
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * @return Collection<int, CompteRenduInstance>
     */
    public function getComptesRendusInstances(): Collection
    {
        return $this->comptesRendusInstances;
    }

    public function addComptesRendusInstance(CompteRenduInstance $comptesRendusInstance): static
    {
        if (!$this->comptesRendusInstances->contains($comptesRendusInstance)) {
            $this->comptesRendusInstances->add($comptesRendusInstance);
            $comptesRendusInstance->setSaison($this);
        }

        return $this;
    }

    public function removeComptesRendusInstance(CompteRenduInstance $comptesRendusInstance): static
    {
        if ($this->comptesRendusInstances->removeElement($comptesRendusInstance)) {
            // set the owning side to null (unless already changed)
            if ($comptesRendusInstance->getSaison() === $this) {
                $comptesRendusInstance->setSaison(null);
            }
        }

        return $this;
    }

    public function getSaisonPrecedente(): ?self
    {
        return $this->saisonPrecedente;
    }

    public function setSaisonPrecedente(?self $saisonPrecedente): static
    {
        $this->saisonPrecedente = $saisonPrecedente;

        return $this;
    }

    public function getSaisonSuivante(): ?self
    {
        return $this->saisonSuivante;
    }

    public function setSaisonSuivante(?self $saisonSuivante): static
    {
        // unset the owning side of the relation if necessary
        if ($saisonSuivante === null && $this->saisonSuivante !== null) {
            $this->saisonSuivante->setSaisonPrecedente(null);
        }

        // set the owning side of the relation if necessary
        if ($saisonSuivante !== null && $saisonSuivante->getSaisonPrecedente() !== $this) {
            $saisonSuivante->setSaisonPrecedente($this);
        }

        $this->saisonSuivante = $saisonSuivante;

        return $this;
    }
}
