<?php

namespace App\Entity;

use App\Repository\ElectionEquipeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity(repositoryClass: ElectionEquipeRepository::class)]
class ElectionEquipe
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'electionEquipe', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?CompteRenduInstance $compteRendu = null;

    #[Assert\Valid()]
    #[ORM\OneToMany(mappedBy: 'electionEquipe', targetEntity: RoleEquipeElue::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $roles;

    #[ORM\OneToOne(inversedBy: 'electionEquipe', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Vote $vote = null;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteRendu(): ?CompteRenduInstance
    {
        return $this->compteRendu;
    }

    public function setCompteRendu(CompteRenduInstance $compteRendu): static
    {
        $this->compteRendu = $compteRendu;

        return $this;
    }

    /**
     * @return Collection<int, RoleEquipeElue>
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(RoleEquipeElue $role): static
    {
        if (!$this->roles->contains($role)) {
            $this->roles->add($role);
            $role->setElectionEquipe($this);
        }

        return $this;
    }

    public function removeRole(RoleEquipeElue $role): static
    {
        if ($this->roles->removeElement($role)) {
            // set the owning side to null (unless already changed)
            if ($role->getElectionEquipe() === $this) {
                $role->setElectionEquipe(null);
            }
        }

        return $this;
    }

    public function getVote(): ?Vote
    {
        return $this->vote;
    }

    public function setVote(Vote $vote): static
    {
        $this->vote = $vote;

        return $this;
    }

    #[Assert\Callback]
    protected function verificationVote(ExecutionContextInterface $context, mixed $payload)
    {
        if (($this->getRoles()->count() > 0) && (!$this->getVote()
            || ($this->getVote()->getNombreVoixDefavorables() === null)
            || ($this->getVote()->getNombreVoixFavorables() === null))) {
            $context->buildViolation("Le résultat du vote doit être renseigné")
                ->addViolation();
        }

        if ($this->getVote() && ($this->getVote()->getNombreVoixDefavorables() || $this->getVote()->getNombreVoixFavorables() || $this->getVote()->getNombreVoixBlancs())) {
            $nombreVotants = $this->getCompteRendu()->getNombreVotantsPourCategorie($this->getCompteRendu()->getType() == CompteRenduInstance::TYPE_CONGRES ? NombreVotantsSpecifique::CATEGORIE_ELECTION_ER : NombreVotantsSpecifique::CATEGORIE_ELECTION_EGA);

            if ($this->getRoles()->count() == 0) {
                $context->buildViolation("Au moins une personne doit être renseignée")
                    ->addViolation();
            } else if ($this->getVote()->getNombreVoixDefavorables() === null && ($this->getVote()->getNombreVoixFavorables() || $this->getVote()->getNombreVoixBlancs())) {
                $context->buildViolation("Le nombre de votes défavorables doit être indiqué")
                    ->addViolation();
            } else if ($this->getVote()->getNombreVoixFavorables() === null && ($this->getVote()->getNombreVoixDefavorables() || $this->getVote()->getNombreVoixBlancs())) {
                $context->buildViolation("Le nombre de votes favorables doit être indiqué")
                    ->addViolation();
            } else if ($this->getVote()->getNombreVoixBlancs() === null && ($this->getVote()->getNombreVoixDefavorables() || $this->getVote()->getNombreVoixFavorables())) {
                $context->buildViolation("Le nombre de votes blancs doit être indiqué")
                    ->addViolation();
            } else if (
                ($this->getVote()->getNombreVoixFavorables()
                    + $this->getVote()->getNombreVoixDefavorables()
                    + $this->getVote()->getNombreVoixBlancs()) > $nombreVotants
            ) {
                $context->buildViolation("Le nombre de total de voix (favorables, défavorables et blancs ) doit être inférieur ou égal au nombre total de votant·es")
                    ->addViolation();
            } else if ($this->getVote()->getNombreVoixFavorables() < (floor($nombreVotants / 2) + 1)) {
                $context->buildViolation("Le nombre de voix favorables doit être supérieur à la moitié du nombre total de votant·e·s (50%) +1")
                    ->addViolation();
            }

            //Vérifie que des memebres déclarés dans l'équipe de gestion n'étaient pas déjà élus pour deux ans lors de l'APL précédente
            if ($this->getRoles()->count() > 0) {
                $saisonPrecedente = $this->getCompteRendu()->getSaison()->getSaisonPrecedente();
                if ($saisonPrecedente) {
                    $compteRenduAnneePrecedente = $this->getCompteRendu()->getStructure()->getCompteRenduInstancePourSaison($saisonPrecedente);
                    if ($compteRenduAnneePrecedente) {
                        $personnesDejaEluesPrecedentes = [];
                        foreach ($compteRenduAnneePrecedente->getElectionsNominatives() as $electionNominativeSaisonPrecedente) {
                            if (
                                $electionNominativeSaisonPrecedente->getPersonne()
                                && ($electionNominativeSaisonPrecedente->getStatus() == ElectionNominative::STATUS_VALIDE)
                                && in_array($electionNominativeSaisonPrecedente->getPoste(), ElectionNominative::LISTE_POSTES_ELECTIFS_PLUSIEURS_ANNEES)
                            ) {
                                //Election nominative pour la saison courante avec le meme poste
                                $electionsNominativesSaisonCouranteCorrespondantes = $this->getCompteRendu()->getElectionsNominativesPourPoste($electionNominativeSaisonPrecedente->getPoste());
                                if ($electionsNominativesSaisonCouranteCorrespondantes->count() > 0 && $electionsNominativesSaisonCouranteCorrespondantes->first()->isDejaElu()) {
                                    $personnesDejaEluesPrecedentes[] = $electionNominativeSaisonPrecedente->getPersonne();
                                }
                            }
                        }
                        foreach ($this->getRoles() as $membreEquipeElue) {
                            if (in_array($membreEquipeElue->getPersonne(), $personnesDejaEluesPrecedentes)) {
                                $context->buildViolation($membreEquipeElue->getPersonne()->getPrenom() . " " . $membreEquipeElue->getPersonne()->getNom()
                                    . " a été déjà élu·e l'an dernier pour deux ans. Elle/Il fait de fait partie de l'équipe et ne peut pas y être ajouté·e.")
                                    ->addViolation();
                            }
                        }
                    }
                }
            }
        }
    }
}
