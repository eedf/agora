<?php

namespace App\Entity;

use App\Repository\AttributionFonctionElectiveRepository;
use DateTime;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AttributionFonctionElectiveRepository::class)]
class AttributionFonctionElective
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'attributionsFonctionElective')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Personne $personne = null;

    #[ORM\ManyToOne(inversedBy: 'attributionsFonctionsElectives')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Equipe $equipe = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDebut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateFin = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $status = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $type = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nom = null;

    #[ORM\ManyToOne(inversedBy: 'attributionsFonctionsElectives')]
    #[ORM\JoinColumn(nullable: false)]
    private ?CompteRenduInstance $compteRendu = null;

    #[ORM\ManyToOne(inversedBy: 'attributionsFonctionsElectives')]
    private ?Fonction $fonction = null;

    #[ORM\Column(length: 255)]
    private ?string $operation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): static
    {
        $this->personne = $personne;

        return $this;
    }

    public function getEquipe(): ?Equipe
    {
        return $this->equipe;
    }

    public function setEquipe(?Equipe $equipe): static
    {
        $this->equipe = $equipe;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(?\DateTimeInterface $dateDebut): static
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): static
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCompteRendu(): ?CompteRenduInstance
    {
        return $this->compteRendu;
    }

    public function setCompteRendu(?CompteRenduInstance $compteRendu): static
    {
        $this->compteRendu = $compteRendu;

        return $this;
    }

    public function getFonction(): ?Fonction
    {
        return $this->fonction;
    }

    public function setFonction(?Fonction $fonction): static
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function estActive(?DateTime $date = null): bool
    {
        if ($date == null) {
            $date = new DateTime();
        }
        return ($this->getDateDebut() <= $date
            && ($this->getDateFin() == null || $this->getDateFin() >= $date)
        );
    }

    public function getOperation(): ?string
    {
        return $this->operation;
    }

    public function setOperation(string $operation): static
    {
        $this->operation = $operation;

        return $this;
    }
}
