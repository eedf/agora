<?php

namespace App\Entity;

use App\Repository\VoeuRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity(repositoryClass: VoeuRepository::class)]
class Voeu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $titre = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $contenu = null;

    #[ORM\ManyToOne(inversedBy: 'voeux')]
    #[ORM\JoinColumn(nullable: false)]
    private ?CompteRenduInstance $compteRendu = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Vote $vote = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): static
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getCompteRendu(): ?CompteRenduInstance
    {
        return $this->compteRendu;
    }

    public function setCompteRendu(?CompteRenduInstance $compteRendu): static
    {
        $this->compteRendu = $compteRendu;

        return $this;
    }

    #[Assert\Callback]
    protected function verificationVote(ExecutionContextInterface $context, mixed $payload)
    {
        if ($this->getVote()) {
            $nombreVotants = $this->getCompteRendu()->getNombreVotantsPourCategorie(NombreVotantsSpecifique::CATEGORIE_VOEUX);
            if ($this->getVote()->getNombreVoixDefavorables() === null && ($this->getVote()->getNombreVoixFavorables() || $this->getVote()->getNombreVoixBlancs())) {
                $context->buildViolation("Le nombre de votes défavorables doit être indiqué")
                    ->addViolation();
            } else if ($this->getVote()->getNombreVoixFavorables() === null && ($this->getVote()->getNombreVoixDefavorables() || $this->getVote()->getNombreVoixBlancs())) {
                $context->buildViolation("Le nombre de votes favorables doit être indiqué")
                    ->addViolation();
            } else if ($this->getVote()->getNombreVoixBlancs() === null && ($this->getVote()->getNombreVoixDefavorables() || $this->getVote()->getNombreVoixFavorables())) {
                $context->buildViolation("Le nombre de votes blancs doit être indiqué")
                    ->addViolation();
            } else if (
                ($this->getVote()->getNombreVoixFavorables()
                    + $this->getVote()->getNombreVoixDefavorables()
                    + $this->getVote()->getNombreVoixBlancs()
                ) > $nombreVotants
            ) {
                $context->buildViolation("Le nombre de total de voix (favorables, défavorables et blancs) doit être inférieur ou égal au nombre total de votant·e·s")
                    ->addViolation();
            }
        }
    }

    public function getVote(): ?Vote
    {
        return $this->vote;
    }

    public function setVote(?Vote $vote): static
    {
        $this->vote = $vote;

        return $this;
    }
}
