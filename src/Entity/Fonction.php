<?php

namespace App\Entity;

use App\Repository\FonctionRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: FonctionRepository::class)]
class Fonction
{

    const CATEGORY_FONCTION_BENEVOLE = 1;
    const CATEGORY_FONCTION_PARTICIPANT = 2;
    const CATEGORY_FONCTION_SALARIE = 3;

    const TYPE_FONCTION_PARTICIPANT_SIMPLE = 1;
    const TYPE_FONCTION_MEMBRE = 2;
    const TYPE_FONCTION_RESPONSABLE = 3;
    const TYPE_FONCTION_TRESORIER = 4;
    const TYPE_FONCTION_DELEGUE_AG = 5;
    const TYPE_FONCTION_SALARIE = 6;
    const TYPE_MISSION_ORGANISATION = 7;
    const TYPE_MISSION_COORDINATION = 8;
    const TYPE_MISSION_REPRESENTATION = 9;
    const TYPE_MISSION_TRESORERIE = 10;

    const MAP_CATEGORY_BY_TYPE_FONCTION = [
        self::TYPE_FONCTION_PARTICIPANT_SIMPLE => self::CATEGORY_FONCTION_PARTICIPANT,
        self::TYPE_FONCTION_MEMBRE => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_FONCTION_RESPONSABLE => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_FONCTION_TRESORIER => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_FONCTION_SALARIE => self::CATEGORY_FONCTION_SALARIE,
        self::TYPE_MISSION_ORGANISATION => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_MISSION_COORDINATION => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_MISSION_REPRESENTATION => self::CATEGORY_FONCTION_BENEVOLE,
        self::TYPE_MISSION_TRESORERIE => self::CATEGORY_FONCTION_BENEVOLE,
    ];



    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    private ?Uuid $uuid = null;

    #[ORM\ManyToOne(inversedBy: 'fonctions')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['jeito'])]
    private ?Personne $personne = null;

    #[ORM\ManyToOne(inversedBy: 'fonctions', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['jeito'])]
    private ?Equipe $equipe = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['jeito'])]
    private ?\DateTimeInterface $dateDebut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['jeito'])]
    private ?\DateTimeInterface $dateFin = null;

    #[ORM\Column(length: 255)]
    #[Groups(['jeito'])]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    #[Groups(['jeito'])]
    private ?string $type = null;

    #[ORM\Column(length: 255)]
    #[Groups(['jeito'])]
    private ?string $statut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\OneToMany(mappedBy: 'fonction', targetEntity: Droit::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $droitsAssocies;

    #[ORM\OneToMany(mappedBy: 'fonction', targetEntity: AttributionFonctionElective::class, orphanRemoval: true, cascade: ['remove'])]
    private Collection $attributionsFonctionsElectives;



    public function __construct()
    {
        $this->droitsAssocies = new ArrayCollection();
        $this->attributionsFonctionsElectives = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): static
    {
        $this->personne = $personne;

        return $this;
    }

    public function getEquipe(): ?Equipe
    {
        return $this->equipe;
    }

    public function setEquipe(?Equipe $equipe): static
    {
        $this->equipe = $equipe;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): static
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): static
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): static
    {
        $this->statut = $statut;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): static
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function estActive(?DateTime $date = null): bool
    {
        if ($date == null) {
            $date = new DateTime();
        }
        return ($this->getDateDebut() <= $date
            && ($this->getDateFin() == null || $this->getDateFin() >= $date)
        );
    }

    /**
     * @return Collection<int, Droit>
     */
    public function getDroitsAssocies(): Collection
    {
        return $this->droitsAssocies;
    }

    public function addDroitAssocie(Droit $droitAssocie): static
    {
        if (!$this->droitsAssocies->contains($droitAssocie)) {
            $this->droitsAssocies->add($droitAssocie);
            $droitAssocie->setFonction($this);
        }

        return $this;
    }

    public function removeDroitAssocie(Droit $droitAssocie): static
    {
        if ($this->droitsAssocies->removeElement($droitAssocie)) {
            // set the owning side to null (unless already changed)
            if ($droitAssocie->getFonction() === $this) {
                $droitAssocie->setFonction(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AttributionFonctionElective>
     */
    public function getAttributionsFonctionsElectives(): Collection
    {
        return $this->attributionsFonctionsElectives;
    }

    public function addAttributionsFonctionsElective(AttributionFonctionElective $attributionsFonctionsElective): static
    {
        if (!$this->attributionsFonctionsElectives->contains($attributionsFonctionsElective)) {
            $this->attributionsFonctionsElectives->add($attributionsFonctionsElective);
            $attributionsFonctionsElective->setFonction($this);
        }

        return $this;
    }

    public function removeAttributionsFonctionsElective(AttributionFonctionElective $attributionsFonctionsElective): static
    {
        if ($this->attributionsFonctionsElectives->removeElement($attributionsFonctionsElective)) {
            // set the owning side to null (unless already changed)
            if ($attributionsFonctionsElective->getFonction() === $this) {
                $attributionsFonctionsElective->setFonction(null);
            }
        }

        return $this;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }
}
