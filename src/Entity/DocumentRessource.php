<?php

namespace App\Entity;

use App\Repository\DocumentRessourceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DocumentRessourceRepository::class)]
class DocumentRessource
{

    const TYPE_DOCUMENT_LIEN = 'Lien';
    const TYPE_DOCUMENT_FICHIER = 'Fichier';

    const CATEGORIE_APL = "apl";
    const CATEGORIE_CONGRES = "congres";

    const DOC_APL_BUDGET = "DOC_APL_BUDGET";
    const DOC_APL_CALENDRIER = "DOC_APL_CALENDRIER";
    const DOC_APL_RAPPORT_MORAL = "DOC_APL_RAPPORT_MORAL";
    const DOC_APL_RAPPORT_ACTIVITES = "DOC_APL_RAPPORT_ACTIVITES";
    const DOC_APL_RAPPORT_FINANCIER = "DOC_APL_RAPPORT_FINANCIER";
    const DOC_APL_REGLES_FONCTIONNEMENT = "DOC_APL_REGLES_FONCTIONNEMENT";
    const DOC_APL_PROJET_PEDA = "DOC_APL_PROJET_PEDA";
    const DOC_APL_LISTE_EMARGEMENT = "DOC_APL_LISTE_EMARGEMENT";
    const DOC_APL_CAMPAGNE = "DOC_APL_CAMPAGNE";

    const DOC_CONGRES_BUDGET = "DOC_CONGRES_BUDGET";
    const DOC_CONGRES_CALENDRIER = "DOC_CONGRES_CALENDRIER";
    const DOC_CONGRES_RAPPORT_MORAL = "DOC_CONGRES_RAPPORT_MORAL";
    const DOC_CONGRES_RAPPORT_ACTIVITES = "DOC_CONGRES_RAPPORT_ACTIVITES";
    const DOC_CONGRES_RAPPORT_FINANCIER = "DOC_CONGRES_RAPPORT_FINANCIER";
    const DOC_CONGRES_PLAN_ACTION_REGIONAL = "DOC_CONGRES_PLAN_ACTION_REGIONAL";
    const DOC_CONGRES_LISTE_EMARGEMENT = "DOC_CONGRES_LISTE_EMARGEMENT";
    const DOC_CONGRES_CAMPAGNE = "DOC_CONGRES_CAMPAGNE";

    const LISTE_DOCUMENTS_RESSOURCES =
    [
        self::CATEGORIE_APL => [
            self::DOC_APL_BUDGET => "Budget prévisionnel de SLA vierge",
            self::DOC_APL_CALENDRIER => "Calendrier prévisionnel de SLA vierge OU exemple modifiable de calendrier prévisionnel de SLA",
            self::DOC_APL_RAPPORT_MORAL => "Document d’aide à l’écriture d’un rapport moral de SLA",
            self::DOC_APL_RAPPORT_ACTIVITES => "Document d’aide à l’écriture d’un rapport d'activités de SLA",
            self::DOC_APL_RAPPORT_FINANCIER => "Document d’aide à l’écriture d’un rapport financier de SLA",
            self::DOC_APL_REGLES_FONCTIONNEMENT => "Règles de fonctionnement de SLA type",
            self::DOC_APL_PROJET_PEDA => "Projet pédagogique d’année type",
            self::DOC_APL_LISTE_EMARGEMENT => "Liste d’émargement vierge",
            self::DOC_APL_CAMPAGNE => "Campagne des APL"
        ],
        self::CATEGORIE_CONGRES => [
            self::DOC_CONGRES_BUDGET => "Budget prévisionnel de région vierge",
            self::DOC_CONGRES_PLAN_ACTION_REGIONAL => "Document d’aide à l’écriture de plan d’action régional",
            self::DOC_CONGRES_RAPPORT_MORAL => "Document d’aide à l’écriture d’un rapport moral de région",
            self::DOC_CONGRES_RAPPORT_ACTIVITES => "Document d’aide à l’écriture d’un rapport d'activités de région",
            self::DOC_CONGRES_RAPPORT_FINANCIER => "Document d’aide à l’écriture d’un rapport financier de région",
            self::DOC_CONGRES_LISTE_EMARGEMENT => "Liste d’émargement vierge",
            self::DOC_CONGRES_CAMPAGNE => "Campagne des Congrès"
        ]
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $document = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lien = null;

    #[ORM\Column(length: 255)]
    private ?string $typeDocument = self::TYPE_DOCUMENT_LIEN;

    #[ORM\Column(length: 255)]
    private ?string $categorie = null;

    #[ORM\Column(length: 255)]
    private ?string $Titre = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDocument(): ?DocumentFile
    {
        return $this->document;
    }

    public function setDocument(?DocumentFile $document): static
    {
        $this->document = $document;

        return $this;
    }

    public function getLien(): ?string
    {
        return $this->lien;
    }

    public function setLien(?string $lien): static
    {
        $this->lien = $lien;

        return $this;
    }

    public function getTypeDocument(): ?string
    {
        return $this->typeDocument;
    }

    public function setTypeDocument(string $typeDocument): static
    {
        $this->typeDocument = $typeDocument;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): static
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): static
    {
        $this->Titre = $Titre;

        return $this;
    }
}
