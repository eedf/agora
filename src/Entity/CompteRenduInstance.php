<?php

namespace App\Entity;

use App\Repository\CompteRenduInstanceRepository;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity(repositoryClass: CompteRenduInstanceRepository::class)]
class CompteRenduInstance
{

    const STATUT_CR_INSTANCE_NON_SAISI = 'non_saisi';
    const STATUT_CR_INSTANCE_PAS_D_INSTANCE = 'pas_d_instance';
    const STATUT_CR_INSTANCE_ATTENTE_VALIDATION = 'attente_validation';
    const STATUT_CR_INSTANCE_NON_VALIDE = 'non_valide';
    const STATUT_CR_INSTANCE_VALIDE = 'valide';
    const STATUT_CR_INSTANCE_SAISON_PASSEE = 'saison_passee';
    const STATUT_CR_INSTANCE_ARCHIVE = 'archive';

    const MAP_LABEL_PAR_STATUT_CR = [
        self::STATUT_CR_INSTANCE_NON_SAISI => "Non saisi",
        self::STATUT_CR_INSTANCE_PAS_D_INSTANCE => "N'a pas eu lieu",
        self::STATUT_CR_INSTANCE_ATTENTE_VALIDATION => 'En attente de validation',
        self::STATUT_CR_INSTANCE_NON_VALIDE => 'Invalidé',
        self::STATUT_CR_INSTANCE_VALIDE => 'Validé',
        self::STATUT_CR_INSTANCE_SAISON_PASSEE => 'Saison passée',
        self::STATUT_CR_INSTANCE_ARCHIVE => 'Archivé',
    ];


    const TRANSITION_SOUMISSION_VALIDATION = 'soumission_validation';
    const TRANSITION_NON_SAISI_SAISON_PASSEE = 'non_saisi_saison_passee';
    const TRANSITION_NON_VALIDATION = 'non_validation';
    const TRANSITION_VALIDATION = 'validation';
    const TRANSITION_ATTENTE_VALIDATION_SAISON_PASSEE = 'attente_validation_saison_passee';
    const TRANSITION_RESSOUMISSION = 'resoumission';
    const TRANSITION_NON_VALIDE_SAISON_PASSEE = "non_valide_saison_passee";
    const TRANSITION_VALIDE_SAISON_PASSEE = "valide_saison_passee";
    const TRANSITION_ARCHIVAGE = "archivage";
    const TRANSITION_NON_SAISI_PAS_D_INSTANCE = 'non_saisi_pas_d_instance';
    const TRANSITION_VALIDE_PAS_D_INSTANCE = 'valide_pas_d_instance';
    const TRANSITION_INVALIDE_PAS_D_INSTANCE = 'invalide_pas_d_instance';
    const TRANSITION_PAS_D_INSTANCE_ATTENTE_VALIDATION = 'pas_d_instance_attente_validation';
    const TRANSITION_PAS_D_INSTANCE_SAISON_PASSEE = 'pas_d_instance_saison_passee';
    const TRANSITION_ATTENTE_VALIDATION_PAS_D_INSTANCE = 'attente_validation_pas_d_instance';

    const TYPE_APL_SLA = "apl_sla";
    const TYPE_APL_SLAN = "apl_slan";
    const TYPE_CONGRES = "congres";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 20)]
    private ?string $type = null;

    #[ORM\ManyToOne(inversedBy: 'compteRendusInstances')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Structure $structure = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateValidation = null;

    #[ORM\ManyToOne(inversedBy: 'comptesRendusSoumis')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private ?Personne $dernierePersonneSoummetteur = null;

    #[ORM\ManyToOne(inversedBy: 'compteRendusValides')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private ?Personne $personneValideuse = null;

    #[ORM\ManyToOne(inversedBy: 'comptesRendusInstances')]
    #[ORM\JoinColumn(nullable: false)]
    private ?SaisonDemocratique $saison = null;

    #[ORM\Column(type: Types::ARRAY)]
    private array $status = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateInstance = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreVotantsTotal = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreVoixPourBudgetPrevisionnel = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreVoixPourRapportMoral = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreVoixPourPAR = null;

    #[ORM\Column(nullable: true)]
    private ?int $nombreVoixPourRapportFinancier = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $budgetPrevisionnel = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $projetPedagogique = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $planAction = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $calendrierPrevisionnel = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $rapportMoral = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $rapportActivites = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $rapportFinancier = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $fonctionnementStructure = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?DocumentFile $listeEmargement = null;

    #[Assert\Valid()]
    #[ORM\OneToMany(mappedBy: 'instance', targetEntity: VoteInstance::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $votesInstances;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $commentaireTenueInstance = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $commentaireDestinationValidation = null;

    #[Assert\Valid()]
    #[ORM\OneToMany(mappedBy: 'compteRendu', targetEntity: ElectionNominative::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $electionsNominatives;

    #[Assert\Valid()]
    #[ORM\OneToOne(mappedBy: 'compteRendu', cascade: ['persist', 'remove'])]
    private ?ElectionEquipe $electionEquipe = null;

    #[ORM\OneToMany(mappedBy: 'compteRendu', targetEntity: AttributionFonctionElective::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $attributionsFonctionsElectives;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $commentaireInvalidation = null;

    #[ORM\OneToMany(mappedBy: 'compteRendu', targetEntity: Voeu::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $voeux;

    #[ORM\OneToMany(mappedBy: 'compteRendu', targetEntity: Motion::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $motions;

    #[ORM\Column(nullable: true)]
    private ?int $autonomieStructure = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateFinInstance = null;

    #[ORM\OneToMany(mappedBy: 'compteRendu', targetEntity: NombreVotantsSpecifique::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $nombreVotantsSpecifiques;

    #[Assert\Unique(message: "Il n'est pas possible d'indiquer deux fois la même personne")]
    #[ORM\ManyToMany(targetEntity: Personne::class)]
    private Collection $representantsEchelonSuperieur;

    #[Assert\Count(
        max: 6,
        maxMessage: 'Vous ne pouvez pas ajouter plus de {{ limit }} documents',
    )]
    #[Assert\Valid()]
    #[ORM\OneToMany(mappedBy: 'compteRenduInstance', targetEntity: AutreDocument::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $autresDocuments;

    #[ORM\Column(nullable: true)]
    private ?bool $naPasEuLieu = null;

    public function __construct($typeInstance)
    {
        $this->votesInstances = new ArrayCollection();
        $this->electionsNominatives = new ArrayCollection();
        $this->type = $typeInstance;
        if ($typeInstance == CompteRenduInstance::TYPE_CONGRES) {
            $listeVotesInstance = VoteInstance::LISTE_RAPPORTS_CONGRES;
        } else {
            $listeVotesInstance = VoteInstance::LISTE_RAPPORTS_SLA;
        }
        //Ajout des rapports
        foreach ($listeVotesInstance as $typeRapport) {
            $voteRapport = new VoteInstance();
            $voteRapport->setType($typeRapport);
            $this->addVotesInstance($voteRapport);
        }
        $this->nombreVotantsSpecifiques = new ArrayCollection();
        //Ajout des nombresDeVotantsSpecifiques
        $this->ajouterNombreVotantsSpecifiques();
        $this->attributionsFonctionsElectives = new ArrayCollection();
        $this->voeux = new ArrayCollection();
        $this->motions = new ArrayCollection();
        $this->representantsEchelonSuperieur = new ArrayCollection();
        $this->autresDocuments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): static
    {
        $this->structure = $structure;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(?\DateTimeInterface $dateModification): static
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getDateValidation(): ?\DateTimeInterface
    {
        return $this->dateValidation;
    }

    public function setDateValidation(?\DateTimeInterface $dateValidation): static
    {
        $this->dateValidation = $dateValidation;

        return $this;
    }

    public function getDernierePersonneSoummetteur(): ?Personne
    {
        return $this->dernierePersonneSoummetteur;
    }

    public function setDernierePersonneSoummetteur(?Personne $dernierePersonneSoummetteur): static
    {
        $this->dernierePersonneSoummetteur = $dernierePersonneSoummetteur;

        return $this;
    }

    public function getPersonneValideuse(): ?Personne
    {
        return $this->personneValideuse;
    }

    public function setPersonneValideuse(?Personne $personneValideuse): static
    {
        $this->personneValideuse = $personneValideuse;

        return $this;
    }

    public function getSaison(): ?SaisonDemocratique
    {
        return $this->saison;
    }

    public function setSaison(?SaisonDemocratique $saison): static
    {
        $this->saison = $saison;

        return $this;
    }

    public function getStatus(): ?array
    {
        return $this->status;
    }

    public function setStatus(array $status): void
    {
        $this->status = $status;
    }

    public function getDateInstance(): ?\DateTimeInterface
    {
        return $this->dateInstance;
    }

    public function setDateInstance(?\DateTimeInterface $dateInstance): static
    {
        $this->dateInstance = $dateInstance;

        return $this;
    }

    public function getNombreVotantsTotal(): ?int
    {
        return $this->nombreVotantsTotal;
    }

    public function setNombreVotantsTotal(?int $nombreVotantsTotal): static
    {
        $this->nombreVotantsTotal = $nombreVotantsTotal;

        return $this;
    }

    public function getNombreVoixPourBudgetPrevisionnel(): ?int
    {
        return $this->nombreVoixPourBudgetPrevisionnel;
    }

    public function setNombreVoixPourBudgetPrevisionnel(?int $nombreVoixPourBudgetPrevisionnel): static
    {
        $this->nombreVoixPourBudgetPrevisionnel = $nombreVoixPourBudgetPrevisionnel;

        return $this;
    }

    public function getNombreVoixPourRapportMoral(): ?int
    {
        return $this->nombreVoixPourRapportMoral;
    }

    public function setNombreVoixPourRapportMoral(?int $nombreVoixPourRapportMoral): static
    {
        $this->nombreVoixPourRapportMoral = $nombreVoixPourRapportMoral;

        return $this;
    }

    public function getNombreVoixPourPAR(): ?int
    {
        return $this->nombreVoixPourPAR;
    }

    public function setNombreVoixPourPAR(?int $nombreVoixPourPAR): static
    {
        $this->nombreVoixPourPAR = $nombreVoixPourPAR;

        return $this;
    }

    public function getNombreVoixPourRapportFinancier(): ?int
    {
        return $this->nombreVoixPourRapportFinancier;
    }

    public function setNombreVoixPourRapportFinancier(?int $nombreVoixPourRapportFinancier): static
    {
        $this->nombreVoixPourRapportFinancier = $nombreVoixPourRapportFinancier;

        return $this;
    }


    //Permet de supprimer les valeurs d'un compte rendu
    public function supprimerValeurs()
    {

        $this->setDateModification(null);
        $this->setDateValidation(null);
        $this->setDernierePersonneSoummetteur(null);
        $this->setPersonneValideuse(null);
        $this->setDateInstance(null);

        $this->setnombreVotantsTotal(null);
        $this->setnombreVoixPourBudgetPrevisionnel(null);
        $this->setnombreVoixPourRapportMoral(null);
        $this->setnombreVoixPourPAR(null);
        $this->setNombreVoixPourRapportFinancier(null);

        $this->setBudgetPrevisionnel(null);
        $this->setProjetPedagogique(null);
        $this->setPlanAction(null);
        $this->setCalendrierPrevisionnel(null);
        $this->setRapportMoral(null);
        $this->setRapportActivites(null);
        $this->setRapportFinancier(null);
        $this->setFonctionnementStructure(null);
        $this->setListeEmargement(null);

        $this->setCommentaireTenueInstance(null);
        $this->setCommentaireDestinationValidation(null);
        if ($this->getElectionEquipe()) {
            foreach ($this->getElectionEquipe()->getRoles() as $role) {
                $this->getElectionEquipe()->removeRole($role);
            }
            if ($this->getElectionEquipe()->getVote()) {
                $this->getElectionEquipe()->getVote()->setNombreVoixDefavorables(null);
                $this->getElectionEquipe()->getVote()->setNombreVoixFavorables(null);
                $this->getElectionEquipe()->getVote()->setNombreDeVoixAbstention(null);
            }
        }


        $this->setCommentaireInvalidation(null);
        $this->setAutonomieStructure(null);

        foreach ($this->getVotesInstances() as $voteInstance) {
            $voteInstance->setVote(null);
        }

        foreach ($this->getElectionsNominatives() as $electionNominative) {
            $this->removeElectionsNominative($electionNominative);
        }

        foreach ($this->getNombreVotantsSpecifiques() as $nombreVotantSpecifique) {
            $this->removeNombreVotantsSpecifique($nombreVotantSpecifique);
        }

        foreach ($this->getAttributionsFonctionsElectives() as $attribution) {
            $this->removeAttributionsFonctionsElective($attribution);
        }
        foreach ($this->getMotions() as $motions) {
            $this->removeMotion($motions);
        }
        foreach ($this->getVoeux() as $voeux) {
            $this->removeVoeux($voeux);
        }

        foreach ($this->getRepresentantsEchelonSuperieur() as $representantEchelonSuperieur) {
            $this->removeRepresentantsEchelonSuperieur($representantEchelonSuperieur);
        }
    }

    public function getBudgetPrevisionnel(): ?DocumentFile
    {
        return $this->budgetPrevisionnel;
    }

    public function setBudgetPrevisionnel(?DocumentFile $budgetPrevisionnel): static
    {
        $this->budgetPrevisionnel = $budgetPrevisionnel;

        return $this;
    }

    public function getProjetPedagogique(): ?DocumentFile
    {
        return $this->projetPedagogique;
    }

    public function setProjetPedagogique(?DocumentFile $projetPedagogique): static
    {
        $this->projetPedagogique = $projetPedagogique;

        return $this;
    }

    public function getPlanAction(): ?DocumentFile
    {
        return $this->planAction;
    }

    public function setPlanAction(?DocumentFile $planAction): static
    {
        $this->planAction = $planAction;

        return $this;
    }

    public function getCalendrierPrevisionnel(): ?DocumentFile
    {
        return $this->calendrierPrevisionnel;
    }

    public function setCalendrierPrevisionnel(?DocumentFile $calendrierPrevisionnel): static
    {
        $this->calendrierPrevisionnel = $calendrierPrevisionnel;

        return $this;
    }

    public function getRapportMoral(): ?DocumentFile
    {
        return $this->rapportMoral;
    }

    public function setRapportMoral(?DocumentFile $rapportMoral): static
    {
        $this->rapportMoral = $rapportMoral;

        return $this;
    }

    public function getRapportActivites(): ?DocumentFile
    {
        return $this->rapportActivites;
    }

    public function setRapportActivites(?DocumentFile $rapportActivites): static
    {
        $this->rapportActivites = $rapportActivites;

        return $this;
    }

    public function getRapportFinancier(): ?DocumentFile
    {
        return $this->rapportFinancier;
    }

    public function setRapportFinancier(?DocumentFile $rapportFinancier): static
    {
        $this->rapportFinancier = $rapportFinancier;

        return $this;
    }

    public function getFonctionnementStructure(): ?DocumentFile
    {
        return $this->fonctionnementStructure;
    }

    public function setFonctionnementStructure(?DocumentFile $fonctionnementStructure): static
    {
        $this->fonctionnementStructure = $fonctionnementStructure;

        return $this;
    }

    public function getListeEmargement(): ?DocumentFile
    {
        return $this->listeEmargement;
    }

    public function setListeEmargement(?DocumentFile $listeEmargement): static
    {
        $this->listeEmargement = $listeEmargement;

        return $this;
    }

    /**
     * @return Collection<int, VoteInstance>
     */
    public function getVotesInstances(): Collection
    {
        return $this->votesInstances;
    }

    public function addVotesInstance(VoteInstance $votesInstance): static
    {
        if (!$this->votesInstances->contains($votesInstance)) {
            $this->votesInstances->add($votesInstance);
            $votesInstance->setInstance($this);
        }

        return $this;
    }

    public function removeVotesInstance(VoteInstance $votesInstance): static
    {
        if ($this->votesInstances->removeElement($votesInstance)) {
            // set the owning side to null (unless already changed)
            if ($votesInstance->getInstance() === $this) {
                $votesInstance->setInstance(null);
            }
        }

        return $this;
    }

    public function getCommentaireTenueInstance(): ?string
    {
        return $this->commentaireTenueInstance;
    }

    public function setCommentaireTenueInstance(?string $commentaireTenueInstance): static
    {
        $this->commentaireTenueInstance = $commentaireTenueInstance;

        return $this;
    }

    public function getCommentaireDestinationValidation(): ?string
    {
        return $this->commentaireDestinationValidation;
    }

    public function setCommentaireDestinationValidation(?string $commentaireDestinationValidation): static
    {
        $this->commentaireDestinationValidation = $commentaireDestinationValidation;

        return $this;
    }

    /**
     * @return Collection<int, ElectionNominative>
     */
    public function getElectionsNominatives(): Collection
    {
        return $this->electionsNominatives;
    }

    public function addElectionsNominative(ElectionNominative $electionsNominative): static
    {
        if (!$this->electionsNominatives->contains($electionsNominative)) {
            $this->electionsNominatives->add($electionsNominative);
            $electionsNominative->setCompteRendu($this);
        }

        return $this;
    }

    public function removeElectionsNominative(ElectionNominative $electionsNominative): static
    {
        if ($this->electionsNominatives->removeElement($electionsNominative)) {
            // set the owning side to null (unless already changed)
            if ($electionsNominative->getCompteRendu() === $this) {
                $electionsNominative->setCompteRendu(null);
            }
        }

        return $this;
    }

    public function getElectionsNominativesPourPoste(String $poste): Collection
    {


        $elections = $this->electionsNominatives->filter(function (ElectionNominative $electionNominative) use ($poste) {

            return ($electionNominative->getPoste() == $poste);
        });

        return $elections;
    }

    public function getElectionEquipe(): ?ElectionEquipe
    {
        return $this->electionEquipe;
    }

    public function setElectionEquipe(ElectionEquipe $electionEquipe): static
    {
        // set the owning side of the relation if necessary
        if ($electionEquipe->getCompteRendu() !== $this) {
            $electionEquipe->setCompteRendu($this);
        }

        $this->electionEquipe = $electionEquipe;

        return $this;
    }



    /**
     * @return Collection<int, AttributionFonctionElective>
     */
    public function getAttributionsFonctionsElectives(): Collection
    {
        return $this->attributionsFonctionsElectives;
    }

    public function getAttributionsFonctionsElectivesPourOperation(String $operation): Collection
    {
        $attributions = $this->attributionsFonctionsElectives->filter(function (AttributionFonctionElective $attribution) use ($operation) {

            return ($attribution->getOperation() == $operation);
        });

        return $attributions;
    }

    public function addAttributionsFonctionsElective(AttributionFonctionElective $attributionsFonctionsElective): static
    {
        if (!$this->attributionsFonctionsElectives->contains($attributionsFonctionsElective)) {
            $this->attributionsFonctionsElectives->add($attributionsFonctionsElective);
            $attributionsFonctionsElective->setCompteRendu($this);
        }

        return $this;
    }

    public function removeAttributionsFonctionsElective(AttributionFonctionElective $attributionsFonctionsElective): static
    {
        if ($this->attributionsFonctionsElectives->removeElement($attributionsFonctionsElective)) {
            // set the owning side to null (unless already changed)
            if ($attributionsFonctionsElective->getCompteRendu() === $this) {
                $attributionsFonctionsElective->setCompteRendu(null);
            }
        }

        return $this;
    }

    public function getCommentaireInvalidation(): ?string
    {
        return $this->commentaireInvalidation;
    }

    public function setCommentaireInvalidation(?string $commentaireInvalidation): static
    {
        $this->commentaireInvalidation = $commentaireInvalidation;

        return $this;
    }

    /**
     * @return Collection<int, Voeu>
     */
    public function getVoeux(): Collection
    {
        return $this->voeux;
    }

    public function addVoeux(Voeu $voeux): static
    {
        if (!$this->voeux->contains($voeux)) {
            $this->voeux->add($voeux);
            $voeux->setCompteRendu($this);
        }

        return $this;
    }

    public function removeVoeux(Voeu $voeux): static
    {
        if ($this->voeux->removeElement($voeux)) {
            // set the owning side to null (unless already changed)
            if ($voeux->getCompteRendu() === $this) {
                $voeux->setCompteRendu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Motion>
     */
    public function getMotions(): Collection
    {
        return $this->motions;
    }

    public function addMotion(Motion $motion): static
    {
        if (!$this->motions->contains($motion)) {
            $this->motions->add($motion);
            $motion->setCompteRendu($this);
        }

        return $this;
    }

    public function removeMotion(Motion $motion): static
    {
        if ($this->motions->removeElement($motion)) {
            // set the owning side to null (unless already changed)
            if ($motion->getCompteRendu() === $this) {
                $motion->setCompteRendu(null);
            }
        }

        return $this;
    }
    //Permet d'ajouter les fontion élecetives nominatives en fonction du nombre d'adhérents de la structure
    public function ajouterFonctionElectives()
    {
        $postesAPourvoir = [];
        if ($this->getSaison()->getSaisonPrecedente()) {
            $datePriseEnCompte = $this->getSaison()->getSaisonPrecedente()->getDateFin();
        } else {
            /** @var DateTime $datePriseEnCompte */
            $datePriseEnCompte = clone $this->getSaison()->getDateDebut();
            $datePriseEnCompte = $datePriseEnCompte->modify("-1 days");
        }
        if ($this->type == self::TYPE_CONGRES) {
            if ($this->saison->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
                //On Ajoute les postes de RR et TR:
                $postesAPourvoir[] = ElectionNominative::POSTE_RR;
                $postesAPourvoir[] = ElectionNominative::POSTE_TR;
            } else {
                $postesAPourvoir[] = ElectionNominative::POSTE_ORGANISATION_REGION;
                $postesAPourvoir[] = ElectionNominative::POSTE_TRESORERIE_REGION;
                $postesAPourvoir[] = ElectionNominative::POSTE_COORDINATION_REGION;
                $postesAPourvoir[] = ElectionNominative::POSTE_REPRESENTATION_REGION;
            }
            //Pour les Délégués et Suppléants, il faut déterminer le nombre en fonction du nombre d'adhérents.
            $nombreAdherents = $this->getStructure()->getNombreAdherents($datePriseEnCompte);
            if ($nombreAdherents <= 450) {
                $nbDelegues = 3;
            } else if ($nombreAdherents <= 600) {
                $nbDelegues = 4;
            } else if ($nombreAdherents <= 750) {
                $nbDelegues = 5;
            } else if ($nombreAdherents <= 900) {
                $nbDelegues = 6;
            } else if ($nombreAdherents <= 1050) {
                $nbDelegues = 7;
            } else {
                $nbDelegues = 8;
            }
            //Ajout des Elections nominatives: 
            for ($i = 0; $i < $nbDelegues; $i++) {
                $postesAPourvoir[] = ElectionNominative::POSTE_DELEGUE_AG;
            }
            for ($i = 0; $i < $nbDelegues; $i++) {
                $postesAPourvoir[] = ElectionNominative::POSTE_SUPPLEANT_AG;
            }
        } else {
            if ($this->saison->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
                $postesAPourvoir[] = ElectionNominative::POSTE_RESLA;
                $postesAPourvoir[] = ElectionNominative::POSTE_TSLA;
                $postesAPourvoir[] = ElectionNominative::POSTE_DELEGUE_AG;
                $postesAPourvoir[] = ElectionNominative::POSTE_SUPPLEANT_AG;
            } else {
                $postesAPourvoir[] = ElectionNominative::POSTE_ORGANISATION_SLA;
                $postesAPourvoir[] = ElectionNominative::POSTE_TRESORERIE_SLA;
                $postesAPourvoir[] = ElectionNominative::POSTE_COORDINATION_SLA;
                $postesAPourvoir[] = ElectionNominative::POSTE_REPRESENTATION_SLA;
            }
        }
        foreach ($postesAPourvoir as $poste) {
            $electionNominative = new ElectionNominative();
            $electionNominative->setPoste($poste);
            $this->addElectionsNominative($electionNominative);
        }
    }
    //Permet d'ajouter les objet NombreVotantsSpecifiques  avec les bonnes catégories au compteRendu
    public function ajouterNombreVotantsSpecifiques()
    {
        $listeCategories = [];
        if ($this->type == self::TYPE_CONGRES) {
            $listeCategories = array_merge(VoteInstance::LISTE_RAPPORTS_CONGRES, $listeCategories);
            $listeCategories[] = NombreVotantsSpecifique::CATEGORIE_ELECTION_ER;

            if ($this->saison && ($this->saison->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024))) {
                //On Ajoute les postes de RR et TR:
                $listeCategories[] = ElectionNominative::POSTE_RR;
                $listeCategories[] = ElectionNominative::POSTE_TR;
            } else {
                $listeCategories[] = ElectionNominative::POSTE_ORGANISATION_REGION;
                $listeCategories[] = ElectionNominative::POSTE_TRESORERIE_REGION;
                $listeCategories[] = ElectionNominative::POSTE_COORDINATION_REGION;
                $listeCategories[] = ElectionNominative::POSTE_REPRESENTATION_REGION;
            }
            $listeCategories[] = NombreVotantsSpecifique::CATEGORIE_DELEGUES_AG;
            $listeCategories[] = NombreVotantsSpecifique::CATEGORIE_SUPPLEANTS_AG;
            $listeCategories[] = NombreVotantsSpecifique::CATEGORIE_MOTIONS;
            $listeCategories[] = NombreVotantsSpecifique::CATEGORIE_VOEUX;
        } else {
            $listeCategories = array_merge(VoteInstance::LISTE_RAPPORTS_SLA, $listeCategories);
            $listeCategories[] = NombreVotantsSpecifique::CATEGORIE_ELECTION_EGA;
            if ($this->saison && ($this->saison->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024))) {
                $listeCategories[] = ElectionNominative::POSTE_RESLA;
                $listeCategories[] = ElectionNominative::POSTE_TSLA;
                $listeCategories[] = NombreVotantsSpecifique::CATEGORIE_DELEGUES_AG;
                $listeCategories[] = NombreVotantsSpecifique::CATEGORIE_SUPPLEANTS_AG;
            } else {
                $listeCategories[] = ElectionNominative::POSTE_ORGANISATION_SLA;
                $listeCategories[] = ElectionNominative::POSTE_TRESORERIE_SLA;
                $listeCategories[] = ElectionNominative::POSTE_COORDINATION_SLA;
                $listeCategories[] = ElectionNominative::POSTE_REPRESENTATION_SLA;
            }
        }
        foreach ($listeCategories as $categorie) {
            $nombreVotantSpecifique = new NombreVotantsSpecifique();
            $nombreVotantSpecifique->setCategorie($categorie);
            $this->addNombreVotantsSpecifique($nombreVotantSpecifique);
        }
    }
    public function getAutonomieStructure(): ?int
    {
        return $this->autonomieStructure;
    }

    public function setAutonomieStructure(?int $autonomieStructure): static
    {
        $this->autonomieStructure = $autonomieStructure;

        return $this;
    }

    #[Assert\Callback]
    protected function verificationDateInstance(ExecutionContextInterface $context, mixed $payload)
    {
        if (! $this->isNaPasEuLieu()) {


            $dateCourante = new DateTime();
            if ($this->getDateInstance() > $dateCourante) {
                $context->buildViolation("La date de l'instance ne peut pas être dans le futur.")
                    ->atPath('dateInstance')
                    ->addViolation();
            } else if (
                ($this->getDateInstance() > $this->getSaison()->getDateFin())
                || ($this->getDateInstance() < $this->getSaison()->getDateDebut())
            ) {

                $context->buildViolation("La date de l'instance doit être comprise entre le " . $this->getSaison()->getDateDebut()->format('d-m-y') . " et le " . $this->getSaison()->getDateFin()->format('d-m-y'))
                    ->atPath('dateInstance')
                    ->addViolation();
            }
            if ($this->getDateFinInstance())
                if ($this->getDateFinInstance() > $dateCourante) {
                    $context->buildViolation("La date de fin de l'instance ne peut pas être dans le futur.")
                        ->atPath('dateFinInstance')
                        ->addViolation();
                } else if (
                    ($this->getDateFinInstance() > $this->getSaison()->getDateFin())
                    || ($this->getDateFinInstance() < $this->getSaison()->getDateDebut())
                ) {

                    $context->buildViolation("La date de fin de l'instance doit être comprise entre le " . $this->getSaison()->getDateDebut()->format('d-m-y') . " et le " . $this->getSaison()->getDateFin()->format('d-m-y'))
                        ->atPath('dateFinInstance')
                        ->addViolation();
                } else if ($this->getDateFinInstance() <= $this->getDateInstance()) {
                    $context->buildViolation("La date de fin de l'instance doit être supérieure à la date de début.")
                        ->atPath('dateFinInstance')
                        ->addViolation();
                }
        }
    }

    #[Assert\Callback]
    protected function verificationCompteRendu(ExecutionContextInterface $context, mixed $payload)
    {
        $membresEquipesGestion = [];
        if ($this->getElectionEquipe()) {
            foreach ($this->getElectionEquipe()->getRoles() as $roleEquipeElue) {
                if (!($roleEquipeElue->getPersonne() === null)) {
                    if (in_array($roleEquipeElue->getPersonne(), $membresEquipesGestion)) {
                        if ($this->type == self::TYPE_CONGRES) {
                            $context->buildViolation("Une même personne ne peut pas apparaître plusieurs fois dans une équipe régionale.")
                                ->atPath("electionEquipe")->addViolation();
                        } else {
                            $context->buildViolation("Une même personne ne peut pas apparaître plusieurs fois dans une équipe de groupe.")
                                ->atPath("electionEquipe")->addViolation();
                        }
                    }
                    $membresEquipesGestion[] = $roleEquipeElue->getPersonne();
                }
            }
        }

        if ($this->type == self::TYPE_CONGRES) {

            if ($this->getSaison()->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
                /** @var ElectionNominative $electionNominativeRR */
                $electionNominativeRR = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_RR)->first();
                if ($electionNominativeRR && !($electionNominativeRR->getPersonne() === null)) {
                    if (in_array($electionNominativeRR->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas être à la fois responsable régional·e et membre de l'équipe régionale.")
                            ->atPath("electionsNominatives")
                            ->addViolation();
                    }
                }
                /** @var ElectionNominative $electionNominativeTR */
                $electionNominativeTR = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TR)->first();
                if ($electionNominativeTR && !($electionNominativeTR->getPersonne() === null)) {
                    if (in_array($electionNominativeTR->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas être à la fois trésorier·e régional·e et membre de l'équipe régionale.")
                            ->atPath("electionsNominatives")->addViolation();
                    }
                }

                if ($electionNominativeTR && $electionNominativeRR && !($electionNominativeRR->getPersonne() === null) && !($electionNominativeTR->getPersonne() === null)) {
                    if ($electionNominativeRR->getPersonne() == $electionNominativeTR->getPersonne()) {
                        $context->buildViolation("Une même personne ne peut pas être à la fois responsable et trésorière d'une même structure.")
                            ->atPath("electionsNominatives")->addViolation();
                    }
                }
            } else {
                /** @var ElectionNominative $electionNominativeCoordo */
                $electionNominativeCoordo = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_COORDINATION_REGION)->first();
                if (!($electionNominativeCoordo->getPersonne() === null)) {
                    if (in_array($electionNominativeCoordo->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas avoir la mission de coordination et être membre de l'équipe régionale.")
                            ->atPath("electionsNominatives")
                            ->addViolation();
                    }
                }
                /** @var ElectionNominative $electionNominativeTresorerie */
                $electionNominativeTresorerie = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TRESORERIE_REGION)->first();
                if (!($electionNominativeTresorerie->getPersonne() === null)) {
                    if (in_array($electionNominativeTresorerie->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas avoir la mission de trésorerie et être et membre de l'équipe régionale.")
                            ->atPath("electionsNominatives")
                            ->addViolation();
                    }
                }
                /** @var ElectionNominative $electionNominativeOrganisation */
                $electionNominativeOrganisation = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_ORGANISATION_REGION)->first();
                if (!($electionNominativeOrganisation->getPersonne() === null)) {
                    if (in_array($electionNominativeOrganisation->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas avoir la mission d'organisation et être membre de l'équipe régionale.")
                            ->atPath("electionsNominatives")
                            ->addViolation();
                    }
                }
                /** @var ElectionNominative $electionNominativeRepresentation */
                $electionNominativeRepresentation = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_REPRESENTATION_REGION)->first();
                if (!($electionNominativeRepresentation->getPersonne() === null)) {
                    if (in_array($electionNominativeRepresentation->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas avoir la mission de représentation et être membre de l'équipe régionale.")
                            ->atPath("electionsNominatives")
                            ->addViolation();
                    }
                }
                if (
                    ($electionNominativeRepresentation->getPersonne() !== null)
                    && ($electionNominativeRepresentation->getPersonne() == $electionNominativeOrganisation->getPersonne())
                    && ($electionNominativeRepresentation->getPersonne() == $electionNominativeCoordo->getPersonne())
                    && ($electionNominativeCoordo->getPersonne() == $electionNominativeTresorerie->getPersonne())
                ) {
                    $context->buildViolation("Une même personne ne peut pas cumuler les 4 missions (Organisation, Trésorerie, Coordination, Représentation).")
                        ->atPath("electionsNominatives")
                        ->addViolation();
                }
            }
        } else {
            if ($this->getSaison()->getDateDebut() < new DateTime(SaisonDemocratique::DATE_AG_2024)) {
                /** @var ElectionNominative $electionNominativeRESLA */
                $electionNominativeRESLA = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_RESLA)->first();
                if ($electionNominativeRESLA && !($electionNominativeRESLA->getPersonne() === null)) {
                    if (in_array($electionNominativeRESLA->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas être à la fois responsable de structure et membre de l'équipe de gestion.")
                            ->atPath("electionsNominatives")->addViolation();
                    }
                }

                /** @var ElectionNominative $electionNominativeTR */
                $electionNominativeTSLA = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TSLA)->first();
                if ($electionNominativeTSLA && !($electionNominativeTSLA->getPersonne() === null)) {
                    if (in_array($electionNominativeTSLA->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas être à la fois trésorier·e de structure et membre de l'équipe de gestion.")
                            ->atPath("electionsNominatives")->addViolation();
                    }
                }


                if ($electionNominativeRESLA && !($electionNominativeRESLA->getPersonne() === null) && $electionNominativeTSLA &&  !($electionNominativeTSLA->getPersonne() === null)) {
                    if ($electionNominativeRESLA->getPersonne() == $electionNominativeTSLA->getPersonne()) {
                        $context->buildViolation("Une même personne ne peut pas être à la fois responsable et trésorière d'une même structure.")
                            ->atPath("electionsNominatives")->addViolation();
                    }
                }
            } else {
                /** @var ElectionNominative $electionNominativeCoordo */
                $electionNominativeCoordo = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_COORDINATION_SLA)->first();
                if (!($electionNominativeCoordo->getPersonne() === null)) {
                    if (in_array($electionNominativeCoordo->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas avoir la mission de coordination et être membre de l'équipe de gestion.")
                            ->atPath("electionsNominatives")
                            ->addViolation();
                    }
                }
                /** @var ElectionNominative $electionNominativeTresorerie */
                $electionNominativeTresorerie = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_TRESORERIE_SLA)->first();
                if (!($electionNominativeTresorerie->getPersonne() === null)) {
                    if (in_array($electionNominativeTresorerie->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas avoir la mission de trésorerie et être et membre de l'équipe de gestion.")
                            ->atPath("electionsNominatives")
                            ->addViolation();
                    }
                }
                /** @var ElectionNominative $electionNominativeOrganisation */
                $electionNominativeOrganisation = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_ORGANISATION_SLA)->first();
                if (!($electionNominativeOrganisation->getPersonne() === null)) {
                    if (in_array($electionNominativeOrganisation->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas avoir la mission d'organisation et être membre de l'équipe de gestion.")
                            ->atPath("electionsNominatives")
                            ->addViolation();
                    }
                }
                /** @var ElectionNominative $electionNominativeRepresentation */
                $electionNominativeRepresentation = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_REPRESENTATION_SLA)->first();
                if (!($electionNominativeRepresentation->getPersonne() === null)) {
                    if (in_array($electionNominativeRepresentation->getPersonne(), $membresEquipesGestion)) {
                        $context->buildViolation("Une même personne ne peut pas avoir la mission de représentation et être membre de l'équipe de gestion.")
                            ->atPath("electionsNominatives")
                            ->addViolation();
                    }
                }
                if (
                    ($electionNominativeRepresentation->getPersonne() !== null)
                    && ($electionNominativeRepresentation->getPersonne() == $electionNominativeOrganisation->getPersonne())
                    && ($electionNominativeRepresentation->getPersonne() == $electionNominativeCoordo->getPersonne())
                    && ($electionNominativeCoordo->getPersonne() == $electionNominativeTresorerie->getPersonne())
                ) {
                    $context->buildViolation("Une même personne ne peut pas cumuler les 4 missions (Organisation, Trésorerie, Coordination, Représentation).")
                        ->atPath("electionsNominatives")
                        ->addViolation();
                }
            }
        }
        $deleguesAG = [];
        $electionsNominativeDeleguesAG = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_DELEGUE_AG);
        /** @var ElectionNominative $electionNominativeDelegueAG */
        foreach ($electionsNominativeDeleguesAG as $electionNominativeDelegueAG) {
            if (!($electionNominativeDelegueAG->getPersonne() === null)) {
                if (in_array($electionNominativeDelegueAG->getPersonne(), $deleguesAG)) {
                    $context->buildViolation("Une même personne ne peut pas être deux fois déléguée à l'AG.")
                        ->atPath("electionsNominatives")->addViolation();
                }
                $deleguesAG[] = $electionNominativeDelegueAG->getPersonne();
            }
        }

        $suppleantsAG = [];
        $electionsNominativeSuppleantsAG = $this->getElectionsNominativesPourPoste(ElectionNominative::POSTE_SUPPLEANT_AG);
        /** @var ElectionNominative $electionNominativeSuppleantAG */
        foreach ($electionsNominativeSuppleantsAG as $electionNominativeSuppleantAG) {
            if (!($electionNominativeSuppleantAG->getPersonne() === null)) {
                if (in_array($electionNominativeSuppleantAG->getPersonne(), $suppleantsAG)) {
                    $context->buildViolation("Une même personne ne peut pas être deux fois suppléante à l'AG.")
                        ->atPath("electionsNominatives")->addViolation();
                }
                if (in_array($electionNominativeSuppleantAG->getPersonne(), $deleguesAG)) {
                    $context->buildViolation("Une même personne ne peut pas être à la fois déléguée et suppléante à l'AG.")
                        ->atPath("electionsNominatives")->addViolation();
                }
                $suppleantsAG[] = $electionNominativeSuppleantAG->getPersonne();
            }
        }
        if ($this->getElectionEquipe() && $this->getElectionEquipe()->getRoles()) {
            foreach ($this->getElectionEquipe()->getRoles() as $roleEquipeElue) {
                //Vérification des limites d'âge pour l'équipe de goupe
                if ($this->getDateInstance() !== null && $roleEquipeElue->getPersonne() !== null && $roleEquipeElue->getPersonne()->getDateNaissance() !== null) {
                    $age = $roleEquipeElue->getPersonne()->getAge($this->getDateInstance());
                    if ($age < 16) {
                        $context->buildViolation("Toute personne élue dans une équipe de gestion doit avoir 16 ans révolus au moment de l'instance.")
                            ->atPath("electionEquipe")->addViolation();
                    }
                }
            }
        }
    }

    public function getDateFinInstance(): ?\DateTimeInterface
    {
        return $this->dateFinInstance;
    }

    public function setDateFinInstance(?\DateTimeInterface $dateFinInstance): static
    {
        $this->dateFinInstance = $dateFinInstance;

        return $this;
    }

    /**
     * @return Collection<int, NombreVotantsSpecifique>
     */
    public function getNombreVotantsSpecifiques(): Collection
    {
        return $this->nombreVotantsSpecifiques;
    }

    public function addNombreVotantsSpecifique(NombreVotantsSpecifique $nombreVotantsSpecifique): static
    {
        if (!$this->nombreVotantsSpecifiques->contains($nombreVotantsSpecifique)) {
            $this->nombreVotantsSpecifiques->add($nombreVotantsSpecifique);
            $nombreVotantsSpecifique->setCompteRendu($this);
        }

        return $this;
    }

    public function removeNombreVotantsSpecifique(NombreVotantsSpecifique $nombreVotantsSpecifique): static
    {
        if ($this->nombreVotantsSpecifiques->removeElement($nombreVotantsSpecifique)) {
            // set the owning side to null (unless already changed)
            if ($nombreVotantsSpecifique->getCompteRendu() === $this) {
                $nombreVotantsSpecifique->setCompteRendu(null);
            }
        }

        return $this;
    }
    //Renvoie le nombre de votants pour la catégorie passée en paramètre parmi les nombres de votants spécifiques
    public function getNombreVotantsSpecifiquePourCategorie(String $categorie): ?int
    {
        $nombreVotantSpes = $this->nombreVotantsSpecifiques->filter(function (NombreVotantsSpecifique $nombreVotantSpe) use ($categorie) {

            return ($nombreVotantSpe->getCategorie() == $categorie);
        });
        if ($nombreVotantSpes->count() > 0) {
            return $nombreVotantSpes->first()->getNombreVotants();
        } else {
            return null;
        }
    }
    //Renvoie le nombre de votants pour la catégorie passée en paramètre parmi les nombres de votants spécifiques ou le nombre total
    public function getNombreVotantsPourCategorie(String $categorie): ?int
    {
        if ($this->getNombreVotantsTotal() !== null) {
            return $this->getNombreVotantsTotal($categorie);
        } else {
            return $this->getNombreVotantsSpecifiquePourCategorie($categorie);
        }
    }
    //Renvoie le nombre de votants maximal parmi les nombres de votants spécifiques ou le nombre total
    public function getNombreVotantsMaximal(): ?int
    {
        if ($this->getNombreVotantsTotal() !== null) {
            return $this->getNombreVotantsTotal();
        } else {
            $max = 0;
            foreach ($this->nombreVotantsSpecifiques as $nombreVotantsSpecifique) {
                if ($max < $nombreVotantsSpecifique->getNombreVotants()) {
                    $max = $nombreVotantsSpecifique->getNombreVotants();
                }
            }
        }
        return $max;
    }

    /**
     * @return Collection<int, Personne>
     */
    public function getRepresentantsEchelonSuperieur(): Collection
    {
        return $this->representantsEchelonSuperieur;
    }

    public function addRepresentantsEchelonSuperieur(Personne $representantsEchelonSuperieur): static
    {
        $this->representantsEchelonSuperieur->add($representantsEchelonSuperieur);
        return $this;
    }

    public function removeRepresentantsEchelonSuperieur(Personne $representantsEchelonSuperieur): static
    {
        $this->representantsEchelonSuperieur->removeElement($representantsEchelonSuperieur);

        return $this;
    }

    /**
     * @return Collection<int, AutreDocument>
     */
    public function getAutresDocuments(): Collection
    {
        return $this->autresDocuments;
    }

    public function addAutresDocument(AutreDocument $autresDocument): static
    {
        if (!$this->autresDocuments->contains($autresDocument)) {
            $this->autresDocuments->add($autresDocument);
            $autresDocument->setCompteRenduInstance($this);
        }

        return $this;
    }

    public function removeAutresDocument(AutreDocument $autresDocument): static
    {
        if ($this->autresDocuments->removeElement($autresDocument)) {
            // set the owning side to null (unless already changed)
            if ($autresDocument->getCompteRenduInstance() === $this) {
                $autresDocument->setCompteRenduInstance(null);
            }
        }

        return $this;
    }

    public function isNaPasEuLieu(): ?bool
    {
        return $this->naPasEuLieu;
    }

    public function setNaPasEuLieu(?bool $naPasEuLieu): static
    {
        $this->naPasEuLieu = $naPasEuLieu;

        return $this;
    }
}
