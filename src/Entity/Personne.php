<?php

namespace App\Entity;

use App\Repository\PersonneRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use PhpParser\Node\Expr\BinaryOp\BooleanOr;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PersonneRepository::class)]
#[UniqueEntity(fields: ["uuid"], message: "There is already an account with this uuid")]
class Personne implements UserInterface
{

    //Roles dans AGORA
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const TYPE_PERIMETRE_ADMIN = "ADMINISTRATION";
    const TYPE_PERIMETRE_REGION = "Région";
    const TYPE_PERIMETRE_SLAN = "SLAN";
    const TYPE_PERIMETRE_SLA = "SLA";
    const TYPE_PERIMETRE_NATIONAL = "NATIONAL";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    #[ORM\Column(type: 'uuid', unique: true)]
    private ?Uuid $uuid = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $password;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\Column(length: 100)]
    #[Groups(['recherche_auto_complete'])]
    private ?string $nom = null;

    #[ORM\Column(length: 100)]
    #[Groups(['recherche_auto_complete'])]
    private ?string $prenom = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateNaissance = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'personnesDontEstResponsableLegal1')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private ?self $responsableLegal1 = null;

    #[ORM\OneToMany(mappedBy: 'responsableLegal1', targetEntity: self::class)]
    private Collection $personnesDontEstResponsableLegal1;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'personnesDontEstResponsableLegal2')]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private ?self $responsableLegal2 = null;

    #[ORM\OneToMany(mappedBy: 'responsableLegal2', targetEntity: self::class)]
    private Collection $personnesDontEstResponsableLegal2;

    #[ORM\Column(length: 20)]
    private ?string $genre = null;

    #[ORM\Column(length: 254)]
    #[Assert\Email(
        message: "Cette adresse mail '{{ value }}' n'est pas valide."
    )]
    private ?string $adresseMail = null;

    #[ORM\Column]
    private ?bool $droitImage = null;

    #[ORM\OneToOne(mappedBy: 'personne', cascade: ['persist', 'remove'])]
    private ?Coordonnees $coordonnees = null;

    #[ORM\OneToOne(mappedBy: 'personne', cascade: ['persist', 'remove'])]
    #[Groups(['recherche_auto_complete'])]
    private ?Adherent $adherent = null;

    #[ORM\OneToOne(mappedBy: 'personne', cascade: ['persist', 'remove'])]
    #[Groups(['recherche_auto_complete'])]
    private ?Employe $employe = null;

    #[ORM\OneToMany(mappedBy: 'personne', targetEntity: Fonction::class, orphanRemoval: true, fetch: "EAGER")]
    private Collection $fonctions;

    #[ORM\OneToMany(mappedBy: 'dernierePersonneSoummetteur', targetEntity: CompteRenduInstance::class)]
    private Collection $comptesRendusSoumis;

    #[ORM\OneToMany(mappedBy: 'personneValideuse', targetEntity: CompteRenduInstance::class)]
    private Collection $compteRendusValides;

    #[ORM\OneToMany(mappedBy: 'personne', targetEntity: Droit::class, orphanRemoval: true)]
    private Collection $droits;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDerniereConnexion = null;

    #[ORM\OneToMany(mappedBy: 'personne', targetEntity: RoleEquipeElue::class, cascade: ['persist', 'remove'])]
    private Collection $rolesEquipeElue;

    #[ORM\OneToMany(mappedBy: 'personne', targetEntity: AttributionFonctionElective::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $attributionsFonctionElective;

    #[ORM\OneToMany(mappedBy: 'personne', targetEntity: ElectionNominative::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $electionsNominatives;

    #[ORM\OneToMany(mappedBy: 'personne', targetEntity: Notification::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $notifications;

    public function __construct()
    {
        $this->personnesDontEstResponsableLegal1 = new ArrayCollection();
        $this->personnesDontEstResponsableLegal2 = new ArrayCollection();
        $this->fonctions = new ArrayCollection();
        $this->comptesRendusSoumis = new ArrayCollection();
        $this->compteRendusValides = new ArrayCollection();
        $this->droits = new ArrayCollection();
        $this->attributionsFonctionElective = new ArrayCollection();
        $this->rolesEquipeElue = new ArrayCollection();
        $this->electionsNominatives = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): static
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): static
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(?\DateTimeInterface $dateNaissance): static
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getResponsableLegal1(): ?self
    {
        return $this->responsableLegal1;
    }

    public function setResponsableLegal1(?self $responsableLegal1): static
    {
        $this->responsableLegal1 = $responsableLegal1;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getPersonnesDontEstResponsableLegal1(): Collection
    {
        return $this->personnesDontEstResponsableLegal1;
    }

    public function addPersonnesDontEstResponsableLegal1(self $personnesDontEstResponsableLegal1): static
    {
        if (!$this->personnesDontEstResponsableLegal1->contains($personnesDontEstResponsableLegal1)) {
            $this->personnesDontEstResponsableLegal1->add($personnesDontEstResponsableLegal1);
            $personnesDontEstResponsableLegal1->setResponsableLegal1($this);
        }

        return $this;
    }

    public function removePersonnesDontEstResponsableLegal1(self $personnesDontEstResponsableLegal1): static
    {
        if ($this->personnesDontEstResponsableLegal1->removeElement($personnesDontEstResponsableLegal1)) {
            // set the owning side to null (unless already changed)
            if ($personnesDontEstResponsableLegal1->getResponsableLegal1() === $this) {
                $personnesDontEstResponsableLegal1->setResponsableLegal1(null);
            }
        }

        return $this;
    }

    public function getResponsableLegal2(): ?self
    {
        return $this->responsableLegal2;
    }

    public function setResponsableLegal2(?self $responsableLegal2): static
    {
        $this->responsableLegal2 = $responsableLegal2;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getPersonnesDontEstResponsableLegal2(): Collection
    {
        return $this->personnesDontEstResponsableLegal2;
    }

    public function addPersonnesDontEstResponsableLegal2(self $personnesDontEstResponsableLegal2): static
    {
        if (!$this->personnesDontEstResponsableLegal2->contains($personnesDontEstResponsableLegal2)) {
            $this->personnesDontEstResponsableLegal2->add($personnesDontEstResponsableLegal2);
            $personnesDontEstResponsableLegal2->setResponsableLegal2($this);
        }

        return $this;
    }

    public function removePersonnesDontEstResponsableLegal2(self $personnesDontEstResponsableLegal2): static
    {
        if ($this->personnesDontEstResponsableLegal2->removeElement($personnesDontEstResponsableLegal2)) {
            // set the owning side to null (unless already changed)
            if ($personnesDontEstResponsableLegal2->getResponsableLegal2() === $this) {
                $personnesDontEstResponsableLegal2->setResponsableLegal2(null);
            }
        }

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): static
    {
        $this->genre = $genre;

        return $this;
    }

    public function getAdresseMail(): ?string
    {
        return $this->adresseMail;
    }

    public function setAdresseMail(string $adresseMail): static
    {
        $this->adresseMail = $adresseMail;

        return $this;
    }

    public function getAdresseMailUsage(): ?string
    {
        if ($this->getContratTravailValide() !== null) {
            if ($this->getEmploye()->getEmail() !== null) {
                return $this->getEmploye()->getEmail();
            }
        }
        return $this->adresseMail;
    }

    public function getTelephoneUsage(): ?string
    {
        if ($this->getContratTravailValide() !== null) {
            if ($this->getEmploye()->getTelephone() !== null) {
                return $this->getEmploye()->getTelephone();
            }
        }
        return $this->getCoordonnees()->getTelMobile();
    }


    public function isDroitImage(): ?bool
    {
        return $this->droitImage;
    }

    public function setDroitImage(bool $droitImage): static
    {
        $this->droitImage = $droitImage;

        return $this;
    }

    public function getCoordonnees(): ?Coordonnees
    {
        return $this->coordonnees;
    }

    public function setCoordonnees(Coordonnees $coordonnees): static
    {
        // set the owning side of the relation if necessary
        if ($coordonnees->getPersonne() !== $this) {
            $coordonnees->setPersonne($this);
        }

        $this->coordonnees = $coordonnees;

        return $this;
    }

    public function getAdherent(): ?Adherent
    {
        return $this->adherent;
    }

    public function estAdherentValide(DateTime $date = null): bool
    {
        if ($date == null) {
            $date = new DateTime();
        }
        if ($this->getAdherent()) {
            if (
                $this->getAdherent()->getStatut() == Adherent::STATUT_ADHERENT_OK
                && $this->getAdherent()->getAdhesionCouranteActive($date) !== null
            ) {
                return true;
            }
        }

        return false;
    }

    public function getAdhesionValide(): ?Adhesion
    {
        if ($this->getAdherent()) {
            if (
                $this->getAdherent()->getStatut() == Adherent::STATUT_ADHERENT_OK
            ) {
                return $this->getAdherent()->getAdhesionCouranteActive();
            }
        }

        return null;
    }

    public function setAdherent(Adherent $adherent): static
    {
        // set the owning side of the relation if necessary
        if ($adherent->getPersonne() !== $this) {
            $adherent->setPersonne($this);
        }

        $this->adherent = $adherent;

        return $this;
    }

    public function getEmploye(): ?Employe
    {
        return $this->employe;
    }

    public function setEmploye(Employe $employe): static
    {
        // set the owning side of the relation if necessary
        if ($employe->getPersonne() !== $this) {
            $employe->setPersonne($this);
        }

        $this->employe = $employe;

        return $this;
    }


    public function getContratTravailValide(DateTime $date = null): ?ContratTravail
    {
        if (!$date) {
            $date = new DateTime();
        }
        if ($this->getEmploye()) {

            return $this->getEmploye()->getContratTravailCourantActif($date);
        }

        return null;
    }

    /**
     * @return Collection<int, Fonction>
     */
    public function getFonctions(): Collection
    {
        return $this->fonctions;
    }


    public function getFonctionsActives(?DateTime $date = null): Collection
    {
        if (!$date) {
            $date = new DateTime();
        }


        $fonctionsActives = $this->fonctions->filter(function (Fonction $fonction) use ($date) {

            return ($fonction->getDateDebut() <= $date
                && ($fonction->getDateFin() == null || $fonction->getDateFin() >= $date)
            );
        });

        return $fonctionsActives;
    }

    public function addFonction(Fonction $fonction): static
    {
        if (!$this->fonctions->contains($fonction)) {
            $this->fonctions->add($fonction);
            $fonction->setPersonne($this);
        }

        return $this;
    }

    public function removeFonction(Fonction $fonction): static
    {
        if ($this->fonctions->removeElement($fonction)) {
            // set the owning side to null (unless already changed)
            if ($fonction->getPersonne() === $this) {
                $fonction->setPersonne(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CompteRenduInstance>
     */
    public function getComptesRendusSoumis(): Collection
    {
        return $this->comptesRendusSoumis;
    }

    public function addComptesRendusSoumi(CompteRenduInstance $comptesRendusSoumi): static
    {
        if (!$this->comptesRendusSoumis->contains($comptesRendusSoumi)) {
            $this->comptesRendusSoumis->add($comptesRendusSoumi);
            $comptesRendusSoumi->setDernierePersonneSoummetteur($this);
        }

        return $this;
    }

    public function removeComptesRendusSoumi(CompteRenduInstance $comptesRendusSoumi): static
    {
        if ($this->comptesRendusSoumis->removeElement($comptesRendusSoumi)) {
            // set the owning side to null (unless already changed)
            if ($comptesRendusSoumi->getDernierePersonneSoummetteur() === $this) {
                $comptesRendusSoumi->setDernierePersonneSoummetteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CompteRenduInstance>
     */
    public function getCompteRendusValides(): Collection
    {
        return $this->compteRendusValides;
    }

    public function addCompteRendusValide(CompteRenduInstance $compteRendusValide): static
    {
        if (!$this->compteRendusValides->contains($compteRendusValide)) {
            $this->compteRendusValides->add($compteRendusValide);
            $compteRendusValide->setPersonneValideuse($this);
        }

        return $this;
    }

    public function removeCompteRendusValide(CompteRenduInstance $compteRendusValide): static
    {
        if ($this->compteRendusValides->removeElement($compteRendusValide)) {
            // set the owning side to null (unless already changed)
            if ($compteRendusValide->getPersonneValideuse() === $this) {
                $compteRendusValide->setPersonneValideuse(null);
            }
        }

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function hasRole($role): bool
    {
        return in_array($role, $this->roles);
    }

    public function removeRole($role): array
    {
        if (($key = array_search($role, $this->roles)) !== false) {
            unset($this->roles[$key]);
        }
        return $this->roles;
    }

    public function addRole($role): self
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * The public representation of the user (e.g. a username, an email address, etc.)
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->getUuid();
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Droit>
     */
    public function getDroits(): Collection
    {
        return $this->droits;
    }

    public function addDroit(Droit $droit): static
    {
        if (!$this->droits->contains($droit)) {
            $this->droits->add($droit);
            $droit->setPersonne($this);
        }

        return $this;
    }

    public function removeDroit(Droit $droit): static
    {
        if ($this->droits->removeElement($droit)) {
            // set the owning side to null (unless already changed)
            if ($droit->getPersonne() === $this) {
                $droit->setPersonne(null);
            }
        }

        return $this;
    }

    public function aDroit(String $droitATester, Structure $structure, ?DateTime $date = null): bool
    {
        if (!$date) {
            $date = new DateTime();
        }

        $droitsCorrespondants = $this->droits->filter(function (Droit $droit) use ($date, $structure, $droitATester) {

            return (

                $droit->estActif($date)
                && ($droit->getDroit() == $droitATester)
                && ($droit->getStructure() == $structure)
            );
        });
        return ($droitsCorrespondants->count() > 0);
    }

    public function getDroitsPourStructure(Structure $structure, ?DateTime $date = null): Collection
    {
        if (!$date) {
            $date = new DateTime();
        }
        $droitsCorrespondants = $this->droits->filter(function (Droit $droit) use ($date, $structure) {

            return (

                $droit->estActif($date)
                && ($droit->getStructure() == $structure)
            );
        });
        return ($droitsCorrespondants);
    }


    public function getDroitsActifs(?DateTime $date = null): Collection
    {
        if (!$date) {
            $date = new DateTime();
        }
        $droitsCorrespondants = $this->droits->filter(function (Droit $droit) use ($date) {

            return (

                $droit->estActif($date)
            );
        });
        return ($droitsCorrespondants);
    }


    public function getStatutAdherent(): String
    {
        if ($this->getAdherent()) {
            if (
                $this->getAdherent()->getStatut() == Adherent::STATUT_ADHERENT_OK
                && $this->getAdherent()->getAdhesionCouranteActive() !== null
            ) {
                return Adherent::ADHERENT;
            } else {
                return Adherent::ANCIEN_ADHERENT;
            }
        } else {
            return Adherent::NON_ADHERENT;
        }
    }

    public function estSalarieValide(?DateTime $date = null): bool
    {
        if ($this->getEmploye()) {
            return $this->getEmploye()->hasContratActif($date);
        }

        return false;
    }

    public function getAffichagePourRechercheAuto(): String
    {
        $estAdherent = false;
        $result = $this->getPrenom() . " " . $this->getNom() . " (";
        if ($this->getStatutAdherent() != Adherent::NON_ADHERENT) {
            $result .= $this->getStatutAdherent() . " n°" . $this->getAdherent()->getNumeroAdherent();
            $estAdherent = true;
        }
        if ($this->estSalarieValide()) {
            if ($estAdherent) {
                $result .= " - ";
            }
            $result .= "Salarié·e n°" . $this->getEmploye()->getNumeroSalarie();
        }
        $result .= ")";
        return $result;
    }


    public function getNumeroAdherentOuMatricule(): ?String
    {
        $estSalarie = false;
        $numeroAdherentOuMatriculeSalarie = "";
        if ($this->getEmploye() && $this->getEmploye()->hasContratActif()) {
            $numeroAdherentOuMatriculeSalarie = $this->getEmploye()->getNumeroSalarie();
            $estSalarie = true;
        }

        if ($this->getAdherent()) {
            if ($estSalarie) {
                $numeroAdherentOuMatriculeSalarie .= " - ";
            }
            $numeroAdherentOuMatriculeSalarie .= $this->getAdherent()->getNumeroAdherent();
        }
        return $numeroAdherentOuMatriculeSalarie;
    }


    public function getPerimetresDisponibles(): array
    {
        $result = array();
        $structuresPerimetre = array();
        //Les périmètres disponibles d'une personne sont toutes les structures pour lesquelles la personne est adhérente active, salariée active, et si l'un ou l'autre, a une fonction active
        $adhesionValide = $this->getAdhesionValide();
        $contratDeTravailVailde = $this->getContratTravailValide();

        if ($adhesionValide !== null) {
            $structuresPerimetre[] = $adhesionValide->getStructure();
        }
        if (($adhesionValide !== null) || ($contratDeTravailVailde !== null)) {
            /** @var Fonction $fonctionActive  */
            foreach ($this->getFonctionsActives() as $fonctionActive) {
                $structuresPerimetre[] = $fonctionActive->getEquipe()->getStructure();
            }
        }
        //Et toutes les structures pour lesquelles les personne dont elle est responsable légale est adhérente ou a une fonction active
        $personnesDontEstResponsableLegal = array_merge($this->personnesDontEstResponsableLegal1->toArray(), $this->personnesDontEstResponsableLegal2->toArray());
        /** @var Personne $personneDontEstResponsableLegal  */
        foreach ($personnesDontEstResponsableLegal as $personneDontEstResponsableLegal) {
            $adhesionValide = $personneDontEstResponsableLegal->getAdhesionValide();
            if ($adhesionValide !== null) {
                $structuresPerimetre[] = $adhesionValide->getStructure();
                /** @var Fonction $fonctionActive  */
                foreach ($personneDontEstResponsableLegal->getFonctionsActives() as $fonctionActive) {
                    $structuresPerimetre[] = $fonctionActive->getEquipe()->getStructure();
                }
            }
        }

        //Cas spécial du rôle administrateur
        if ($this->hasRole(self::ROLE_ADMIN)) {
            $result[self::TYPE_PERIMETRE_ADMIN] =  self::ROLE_ADMIN;
        }

        foreach ($structuresPerimetre as $structurePerimetre) {
            if ($structurePerimetre->estNational()) {
                $result[self::TYPE_PERIMETRE_NATIONAL] = $structurePerimetre->getId();
            } else {
                $result[$structurePerimetre->getNom()] = $structurePerimetre->getId();
            }
        }
        $result = array_unique($result);
        return $result;
    }

    public function getDateDerniereConnexion(): ?\DateTimeInterface
    {
        return $this->dateDerniereConnexion;
    }

    public function setDateDerniereConnexion(?\DateTimeInterface $dateDerniereConnexion): static
    {
        $this->dateDerniereConnexion = $dateDerniereConnexion;

        return $this;
    }

    /**
     * @return Collection<int, RoleEquipeElue>
     */
    public function getRolesEquipeElue(): Collection
    {
        return $this->rolesEquipeElue;
    }

    public function addRolesEquipeElue(RoleEquipeElue $rolesEquipeElue): static
    {
        if (!$this->rolesEquipeElue->contains($rolesEquipeElue)) {
            $this->rolesEquipeElue->add($rolesEquipeElue);
            $rolesEquipeElue->setPersonne($this);
        }

        return $this;
    }

    public function removeRolesEquipeElue(RoleEquipeElue $rolesEquipeElue): static
    {
        if ($this->rolesEquipeElue->removeElement($rolesEquipeElue)) {
            // set the owning side to null (unless already changed)
            if ($rolesEquipeElue->getPersonne() === $this) {
                $rolesEquipeElue->setPersonne(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AttributionFonctionElective>
     */
    public function getAttributionsFonctionElective(): Collection
    {
        return $this->attributionsFonctionElective;
    }

    public function addAttributionsFonctionElective(AttributionFonctionElective $attributionsFonctionElective): static
    {
        if (!$this->attributionsFonctionElective->contains($attributionsFonctionElective)) {
            $this->attributionsFonctionElective->add($attributionsFonctionElective);
            $attributionsFonctionElective->setPersonne($this);
        }

        return $this;
    }

    public function removeAttributionsFonctionElective(AttributionFonctionElective $attributionsFonctionElective): static
    {
        if ($this->attributionsFonctionElective->removeElement($attributionsFonctionElective)) {
            // set the owning side to null (unless already changed)
            if ($attributionsFonctionElective->getPersonne() === $this) {
                $attributionsFonctionElective->setPersonne(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, AttributionFonctionElective>
     */

    public function getAttributionsFonctionElectiveActives(DateTime $date = null): Collection
    {
        if (!$date) {
            $date = new DateTime();
        }

        $attributionsActives = $this->attributionsFonctionElective->filter(function (AttributionFonctionElective $attributionsFonctionElective) use ($date) {

            return ($attributionsFonctionElective->estActive());
        });

        return $attributionsActives;
    }


    /**
     * @return Collection<int, ElectionNominative>
     */
    public function getElectionsNominatives(): Collection
    {
        return $this->electionsNominatives;
    }

    public function addElectionsNominative(ElectionNominative $electionsNominative): static
    {
        if (!$this->electionsNominatives->contains($electionsNominative)) {
            $this->electionsNominatives->add($electionsNominative);
            $electionsNominative->setPersonne($this);
        }

        return $this;
    }

    public function removeElectionsNominative(ElectionNominative $electionsNominative): static
    {
        if ($this->electionsNominatives->removeElement($electionsNominative)) {
            // set the owning side to null (unless already changed)
            if ($electionsNominative->getPersonne() === $this) {
                $electionsNominative->setPersonne(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Notification>
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): static
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications->add($notification);
            $notification->setPersonne($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): static
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getPersonne() === $this) {
                $notification->setPersonne(null);
            }
        }

        return $this;
    }

    public function hasNotificationsNonLues(): bool
    {
        foreach ($this->notifications as $notification) {
            /** @var Notification $notification */
            if (!$notification->isLu()) {
                return true;
            }
        }
        return false;
    }

    public function getNombreNotificationsNonLues(): int
    {
        $result = 0;
        foreach ($this->notifications as $notification) {
            /** @var Notification $notification */
            if (!$notification->isLu()) {
                $result++;
            }
        }
        return $result;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getAge(DateTime $date = null): ?int
    {
        $age = null;
        if (!$date) {
            $date = new DateTime();
        }
        if ($this->getDateNaissance() !== null) {
            $age = $date->diff($this->getDateNaissance())->y;
        }
        return $age;
    }
    public function __toString()
    {
        return $this->getUuid();
    }

    public function fusionDoublon(Personne $doublon): void
    {

        foreach ($doublon->getNotifications() as $notification) {
            $notification->setPersonne($this);
        }
        foreach ($doublon->getElectionsNominatives() as $electionNominative) {
            $electionNominative->setPersonne($this);
        }
        foreach ($doublon->getAttributionsFonctionElective() as $attributionFonctionElective) {
            $attributionFonctionElective->setPersonne($this);
        }
        foreach ($doublon->getRolesEquipeElue() as $roleEquipeElue) {
            $roleEquipeElue->setPersonne($this);
        }
        foreach ($doublon->getDroits() as $droit) {
            $droit->setPersonne($this);
        }
        foreach ($doublon->getCompteRendusValides() as $compteRenduValide) {
            $compteRenduValide->setPersonneValideuse($this);
        }
        foreach ($doublon->getComptesRendusSoumis() as $compteRenduSoumis) {
            $compteRenduSoumis->setDernierePersonneSoummetteur($this);
        }
    }
}
