<?php

namespace App\Entity;

use App\Repository\ElectionNominativeRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity(repositoryClass: ElectionNominativeRepository::class)]
class ElectionNominative
{

    const POSTE_ORGANISATION_SLA = "Mission d'organisation";
    const POSTE_COORDINATION_SLA = "Mission de coordination";
    const POSTE_REPRESENTATION_SLA = "Mission de représentation";
    const POSTE_TRESORERIE_SLA = "Mission de trésorerie";

    const POSTE_ORGANISATION_REGION = "Mission d'organisation";
    const POSTE_COORDINATION_REGION = "Mission de coordination";
    const POSTE_REPRESENTATION_REGION = "Mission de représentation";
    const POSTE_TRESORERIE_REGION = "Mission de trésorerie";


    // Missions Pré réforme des statuts (AG 2024)
    /** @deprecated (depuis AG 2024 utile pour historisation) */
    const POSTE_RESLA = "Responsable de la structure Locale d'activités";
    /** @deprecated (depuis AG 2024 utile pour historisation) */
    const POSTE_TSLA = "Trésorier·e de la structure Locale d'activités";
    /** @deprecated (depuis AG 2024 utile pour historisation) */
    const POSTE_RR = "Responsable régional·e";
    /** @deprecated (depuis AG 2024 utile pour historisation) */
    const POSTE_TR = "Trésorier·e régional·e";

    const POSTE_DELEGUE_AG = "Délégué·e à l'Assemblée Générale";
    const POSTE_SUPPLEANT_AG = "Délégué·e suppléant·e à l'Assemblée Générale";

    const LISTE_POSTES_ELECTIFS_PLUSIEURS_ANNEES = [
        self::POSTE_ORGANISATION_SLA,
        self::POSTE_COORDINATION_SLA,
        self::POSTE_REPRESENTATION_SLA,
        self::POSTE_TRESORERIE_SLA,

        self::POSTE_ORGANISATION_REGION,
        self::POSTE_COORDINATION_REGION,
        self::POSTE_REPRESENTATION_REGION,
        self::POSTE_TRESORERIE_REGION,

        // Missions Pré réforme des statuts (AG 2024)
        self::POSTE_RESLA,
        self::POSTE_TSLA,
        self::POSTE_RR,
        self::POSTE_TR
    ];
    const LISTE_POSTES_ELECTIFS_PLUS_18_ANS = [

        self::POSTE_ORGANISATION_SLA,
        self::POSTE_TRESORERIE_SLA,

        self::POSTE_ORGANISATION_REGION,
        self::POSTE_TRESORERIE_REGION,

        // Missions Pré réforme des statuts (AG 2024)
        self::POSTE_RESLA,
        self::POSTE_TSLA,
        self::POSTE_RR,
        self::POSTE_TR
    ];

    const STATUS_EN_ATTENTE = "En attente";
    const STATUS_VALIDE = "Validé";
    const STATUS_REFUSE = "Refusé";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'electionsNominatives')]
    #[ORM\JoinColumn(nullable: false)]
    private ?CompteRenduInstance $compteRendu = null;

    #[ORM\ManyToOne(inversedBy: 'electionsNominatives')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Personne $personne = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $status = self::STATUS_EN_ATTENTE;

    #[Assert\Valid()]
    #[ORM\OneToOne(inversedBy: 'electionNominative', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?Vote $vote = null;

    #[ORM\Column(length: 255)]
    private ?string $poste = null;

    #[ORM\Column(nullable: true)]
    private ?bool $dejaElu = null;

    #[ORM\Column(nullable: true)]
    private ?bool $pasDElection = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteRendu(): ?CompteRenduInstance
    {
        return $this->compteRendu;
    }

    public function setCompteRendu(?CompteRenduInstance $compteRendu): static
    {
        $this->compteRendu = $compteRendu;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): static
    {
        $this->personne = $personne;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getVote(): ?Vote
    {
        return $this->vote;
    }

    public function setVote(?Vote $vote): static
    {
        $this->vote = $vote;

        return $this;
    }

    public function getPoste(): ?string
    {
        return $this->poste;
    }

    public function setPoste(string $poste): static
    {
        $this->poste = $poste;

        return $this;
    }
    #[Assert\Callback]
    protected function verificationVote(ExecutionContextInterface $context, mixed $payload)
    {
        if (!$this->getCompteRendu()->isNaPasEuLieu()) {
            //Si c'est un congrès, la catégorie du nombre de votants spécifiques pour les délégues et suppléants est au pluriel
            if ($this->getCompteRendu()->getType() == CompteRenduInstance::TYPE_CONGRES) {
                if ($this->getPoste() == self::POSTE_DELEGUE_AG) {
                    $nombreVotants = $this->getCompteRendu()->getNombreVotantsPourCategorie(NombreVotantsSpecifique::CATEGORIE_DELEGUES_AG);
                } else if ($this->getPoste() == self::POSTE_SUPPLEANT_AG) {
                    $nombreVotants = $this->getCompteRendu()->getNombreVotantsPourCategorie(NombreVotantsSpecifique::CATEGORIE_SUPPLEANTS_AG);
                } else {
                    $nombreVotants = $this->getCompteRendu()->getNombreVotantsPourCategorie($this->getPoste());
                }
            } else { // Sinon c'est l'intitulé du poste
                $nombreVotants = $this->getCompteRendu()->getNombreVotantsPourCategorie($this->getPoste());
            }


            if (!$this->getPersonne() && !$this->dejaElu && !$this->pasDElection) {
                $context->buildViolation("La personne élue doit être indiquée")
                    ->addViolation();
            } else if ($this->getPersonne() && ((!$this->getVote()) || (!$this->getVote()->getNombreVoixFavorables()))) {
                $context->buildViolation("Le nombre de voix doit être indiqué")
                    ->addViolation();
            } else if ($this->getVote() && ($this->getVote()->getNombreVoixFavorables() + $this->getVote()->getNombreVoixDefavorables()) > $nombreVotants) {
                $context->buildViolation("Le nombre de voix favorables doit être inférieur ou égal au nombre total de votant·e·s")
                    ->addViolation();
            } else if ($this->getPersonne()  && $this->getVote() && $this->getVote()->getNombreVoixFavorables() < (floor($nombreVotants / 2) + 1)) {
                $context->buildViolation("Le nombre de voix favorables doit être supérieur à la moitié du nombre total de votant·e·s (50%) +1")
                    ->addViolation();
            }
            //Vérification des limites d'âge
            if ($this->getPersonne() !== null && $this->getPersonne()->getDateNaissance() !== null) {
                $age = $this->getPersonne()->getAge($this->getCompteRendu()->getDateInstance());
                if ($age < 18 && in_array($this->getPoste(), self::LISTE_POSTES_ELECTIFS_PLUS_18_ANS)) {
                    $context->buildViolation("La personne élue à la mission de " . $this->getPoste() . " doit avoir 18 ans révolus au moment de l'instance.")
                        ->addViolation();
                } else if ($age < 16) {
                    $context->buildViolation("La personne élue à la mission de " . $this->getPoste() . " doit avoir 16 ans révolus au moment de l'instance.")
                        ->addViolation();
                }
            }
        }
    }

    public function isDejaElu(): ?bool
    {
        return $this->dejaElu;
    }

    public function setDejaElu(?bool $dejaElu): static
    {
        $this->dejaElu = $dejaElu;

        return $this;
    }

    public function isPasDElection(): ?bool
    {
        return $this->pasDElection;
    }

    public function setPasDElection(?bool $pasDElection): static
    {
        $this->pasDElection = $pasDElection;

        return $this;
    }
}
