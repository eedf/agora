<?php

namespace App\Entity;

use App\Repository\AdhesionRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: AdhesionRepository::class)]
class Adhesion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateDebut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateFin = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateModification = null;

    #[ORM\ManyToOne(inversedBy: 'adhesions', fetch: 'EAGER')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Structure $structure = null;

    #[ORM\Column]
    private ?bool $annule = null;

    #[ORM\Column(type: 'uuid', unique: true)]
    private ?Uuid $uuid = null;

    #[ORM\ManyToOne(inversedBy: 'adhesions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Adherent $adherent = null;

    #[ORM\OneToMany(mappedBy: 'adhesion', targetEntity: Droit::class, orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $droitsAssocies;



    public function __construct()
    {
        $this->droitsAssocies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): static
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): static
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): static
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): static
    {
        $this->structure = $structure;

        return $this;
    }

    public function isAnnule(): ?bool
    {
        return $this->annule;
    }

    public function setAnnule(bool $annule): static
    {
        $this->annule = $annule;

        return $this;
    }

    public function getAdherent(): ?Adherent
    {
        return $this->adherent;
    }

    public function setAdherent(?Adherent $adherent): static
    {
        $this->adherent = $adherent;

        return $this;
    }

    public function estActive(?DateTime $date = null)
    {

        if ($date == null) {
            $date = new DateTime();
        }
        return ($this->getDateDebut() <= $date
            && $this->getDateFin() >= $date
            && !$this->isAnnule()
        );
    }

    /**
     * @return Collection<int, Droit>
     */
    public function getDroitsAssocies(): Collection
    {
        return $this->droitsAssocies;
    }

    public function addDroitAssocie(Droit $droitAssocie): static
    {
        if (!$this->droitsAssocies->contains($droitAssocie)) {
            $this->droitsAssocies->add($droitAssocie);
            $droitAssocie->setAdhesion($this);
        }

        return $this;
    }

    public function removeDroitAssocie(Droit $droitAssocie): static
    {
        if ($this->droitsAssocies->removeElement($droitAssocie)) {
            // set the owning side to null (unless already changed)
            if ($droitAssocie->getAdhesion() === $this) {
                $droitAssocie->setAdhesion(null);
            }
        }

        return $this;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }
}
