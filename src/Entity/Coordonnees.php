<?php

namespace App\Entity;

use App\Repository\CoordonneesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;

#[ORM\Entity(repositoryClass: CoordonneesRepository::class)]
class Coordonnees
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $nom = null;

    #[ORM\Column(length: 50)]
    private ?string $prenom = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[Assert\Valid]
    private ?AdressePostale $adressePostale = null;

    #[ORM\OneToOne(inversedBy: 'coordonnees', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Personne $personne = null;

    #[ORM\Column(length: 100)]
    #[Assert\Email(
        message: "Cette adresse mail '{{ value }}' n'est pas valide."
    )]
    private ?string $adresseMail = null;

    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[AssertPhoneNumber(defaultRegion: 'FR')]
    private $telMobile = null;

    #[ORM\Column(type: 'phone_number', nullable: true)]
    #[AssertPhoneNumber(defaultRegion: 'FR')]
    private $telDomicile = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): static
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAdressePostale(): ?AdressePostale
    {
        return $this->adressePostale;
    }

    public function setAdressePostale(?AdressePostale $adressePostale): static
    {
        $this->adressePostale = $adressePostale;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(Personne $personne): static
    {
        $this->personne = $personne;

        return $this;
    }

    public function getAdresseMail(): ?string
    {
        return $this->adresseMail;
    }

    public function setAdresseMail(string $adresseMail): static
    {
        $this->adresseMail = $adresseMail;

        return $this;
    }

    public function getTelMobile()
    {
        return $this->telMobile;
    }

    public function setTelMobile($telMobile): static
    {
        $this->telMobile = $telMobile;

        return $this;
    }

    public function getTelDomicile()
    {
        return $this->telDomicile;
    }

    public function setTelDomicile($telDomicile): static
    {
        $this->telDomicile = $telDomicile;

        return $this;
    }
}
