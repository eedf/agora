<?php

namespace App\Repository;

use App\Entity\ElectionEquipe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ElectionEquipe>
 *
 * @method ElectionEquipe|null find($id, $lockMode = null, $lockVersion = null)
 * @method ElectionEquipe|null findOneBy(array $criteria, array $orderBy = null)
 * @method ElectionEquipe[]    findAll()
 * @method ElectionEquipe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElectionEquipeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ElectionEquipe::class);
    }

//    /**
//     * @return ElectionEquipe[] Returns an array of ElectionEquipe objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ElectionEquipe
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
