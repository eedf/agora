<?php

namespace App\Repository;

use App\Entity\DocumentRessource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DocumentRessource>
 *
 * @method DocumentRessource|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentRessource|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentRessource[]    findAll()
 * @method DocumentRessource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentRessourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DocumentRessource::class);
    }

//    /**
//     * @return DocumentRessource[] Returns an array of DocumentRessource objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?DocumentRessource
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
