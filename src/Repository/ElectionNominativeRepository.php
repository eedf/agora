<?php

namespace App\Repository;

use App\Entity\ElectionNominative;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ElectionNominative>
 *
 * @method ElectionNominative|null find($id, $lockMode = null, $lockVersion = null)
 * @method ElectionNominative|null findOneBy(array $criteria, array $orderBy = null)
 * @method ElectionNominative[]    findAll()
 * @method ElectionNominative[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElectionNominativeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ElectionNominative::class);
    }

//    /**
//     * @return ElectionNominative[] Returns an array of ElectionNominative objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ElectionNominative
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
