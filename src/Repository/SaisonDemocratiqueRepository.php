<?php

namespace App\Repository;

use App\Entity\CompteRenduInstance;
use App\Entity\SaisonDemocratique;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SaisonDemocratique>
 *
 * @method SaisonDemocratique|null find($id, $lockMode = null, $lockVersion = null)
 * @method SaisonDemocratique|null findOneBy(array $criteria, array $orderBy = null)
 * @method SaisonDemocratique[]    findAll()
 * @method SaisonDemocratique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaisonDemocratiqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SaisonDemocratique::class);
    }

    //    /**
    //     * @return SaisonDemocratique[] Returns an array of SaisonDemocratique objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?SaisonDemocratique
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }


}
