<?php

namespace App\Repository;

use App\Entity\AttributionFonctionElective;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AttributionFonctionElective>
 *
 * @method AttributionFonctionElective|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttributionFonctionElective|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttributionFonctionElective[]    findAll()
 * @method AttributionFonctionElective[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttributionFonctionElectiveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AttributionFonctionElective::class);
    }


    public function deleteWhereId(int $id)
    {
        return $this->createQueryBuilder('a')
            ->delete()
            ->andWhere('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
    //    /**
    //     * @return AttributionFonctionElective[] Returns an array of AttributionFonctionElective objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?AttributionFonctionElective
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
