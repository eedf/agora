<?php

namespace App\Repository;

use App\Entity\Droit;
use App\Entity\Structure;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Droit>
 *
 * @method Droit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Droit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Droit[]    findAll()
 * @method Droit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DroitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Droit::class);
    }

    public function findDroitsExpires(DateTime $date, String $type)
    {
        return $this->createQueryBuilder('d')
            ->andWhere("d.type = :type")
            ->andwhere("d.dateExpiration < :date")
            ->setParameter('type', $type)
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }

    public function findDroitsManuelsActifsForStructureEtDroits(DateTime $date = null, Structure $structure, array $arrayDroits)
    {
        if ($date == null) {
            $date = new DateTime();
        }
        return $this->createQueryBuilder('d')
            ->andWhere("d.type = :type")
            ->andWhere("d.structure = :structure")
            ->andwhere("d.dateExpiration > :date")
            ->andWhere('d.droit in (:arrayDroits)')
            ->setParameter('type', Droit::TYPE_DROIT_MANUEL)
            ->setParameter('structure', $structure)
            ->setParameter('arrayDroits', $arrayDroits)
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }

    public function deleteTouslesDroitsAutoPourAdhesion()
    {
        return $this->createQueryBuilder('d')->delete()
            ->andWhere("d.type = :type")
            ->andWhere("d.adhesion IS NOT NULL")
            ->setParameter('type', Droit::TYPE_DROIT_AUTO)
            ->getQuery()->getResult();
    }

    public function deleteTouslesDroitsAutoPourFonction()
    {
        return $this->createQueryBuilder('d')->delete()
            ->andWhere("d.type = :type")
            ->andWhere("d.fonction IS NOT NULL")
            ->setParameter('type', Droit::TYPE_DROIT_AUTO)
            ->getQuery()->getResult();
    }

    //    /**
    //     * @return Droit[] Returns an array of Droit objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('d.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Droit
    //    {
    //        return $this->createQueryBuilder('d')
    //            ->andWhere('d.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
