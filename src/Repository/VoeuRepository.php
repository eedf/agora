<?php

namespace App\Repository;

use App\Entity\Voeu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Voeu>
 *
 * @method Voeu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Voeu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Voeu[]    findAll()
 * @method Voeu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoeuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Voeu::class);
    }

//    /**
//     * @return Voeu[] Returns an array of Voeu objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Voeu
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
