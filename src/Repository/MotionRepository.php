<?php

namespace App\Repository;

use App\Entity\Motion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Motion>
 *
 * @method Motion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Motion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Motion[]    findAll()
 * @method Motion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Motion::class);
    }

//    /**
//     * @return Motion[] Returns an array of Motion objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Motion
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
