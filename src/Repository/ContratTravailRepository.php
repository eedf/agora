<?php

namespace App\Repository;

use App\Entity\ContratTravail;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

/**
 * @extends ServiceEntityRepository<ContratTravail>
 *
 * @method ContratTravail|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContratTravail|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContratTravail[]    findAll()
 * @method ContratTravail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContratTravailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContratTravail::class);
    }

    //    /**
    //     * @return ContratTravail[] Returns an array of ContratTravail objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ContratTravail
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }

    public function findLastUpdatedBefore(DateTime $date)
    {
        return $this->createQueryBuilder('s')
            ->where("s.dateModification < :date")
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }

    public function findWhereUuidIn(array $uuids)
    {
        return $this->createQueryBuilder('s')
            ->where("s.uuid in (:uuids)")
            ->setParameters([
                'uuids' => array_map(function ($uuid) {
                    return Uuid::fromString($uuid)->toBinary();
                }, $uuids)
            ])
            ->getQuery()
            ->getResult();
    }
}
