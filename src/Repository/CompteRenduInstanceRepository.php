<?php

namespace App\Repository;

use App\Entity\CompteRenduInstance;
use App\Entity\SaisonDemocratique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CompteRenduInstance>
 *
 * @method CompteRenduInstance|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompteRenduInstance|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompteRenduInstance[]    findAll()
 * @method CompteRenduInstance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompteRenduInstanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompteRenduInstance::class);
    }

    //    /**
    //     * @return CompteRenduInstance[] Returns an array of CompteRenduInstance objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?CompteRenduInstance
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }


    public function findByStatusBySaison(String $status, SaisonDemocratique $saisonDemocratique, String $type): array
    {
        $query_builder = $this->createQueryBuilder('c')
            ->where("c.saison =:saison")
            ->andWhere("c.type =:type")
            ->setParameter("saison", $saisonDemocratique)
            ->setParameter("type", $type);
        $query_builder = $this->filterOnStatusList($query_builder, [$status]);


        return $query_builder->getQuery()
            ->getResult();
    }
    public function findByStructuresBySaisons(array $structures, array $saisons): array
    {
        $query_builder = $this->createQueryBuilder('c')
            ->andWhere('c.saison in (:saisons)')
            ->join("c.structure", "structure")
            ->setParameter("saisons", $saisons)
            ->andWhere('c.structure in (:structures)')
            ->setParameter("structures", $structures)
            ->orderBy("structure.nom", "ASC");;


        return $query_builder->getQuery()
            ->getResult();
    }

    public function findByStructuresForSaisonActive(array $structures): array
    {
        $query_builder = $this->createQueryBuilder('c')
            ->join("c.saison", "saison")
            ->join("c.structure", "structure")
            ->andWhere('saison.actif=true')
            ->andWhere('c.structure in (:structures)')
            ->setParameter("structures", $structures)
            ->orderBy("structure.nom", "ASC");


        return $query_builder->getQuery()
            ->getResult();
    }

    public function findByStatus(String $status): array
    {
        $query_builder = $this->createQueryBuilder('c');
        $query_builder = $this->filterOnStatusList($query_builder, [$status]);

        return $query_builder->getQuery()
            ->getResult();
    }

    public function filterOnStatusList(QueryBuilder $query_builder, array $statusList): QueryBuilder
    {
        $orStatusCondition = "";
        foreach ($statusList as $index => $statut) {
            if ($orStatusCondition != "") {
                $orStatusCondition .= " OR ";
            }
            $orStatusCondition .= "(c.status LIKE :status_" . $index . ")";
            $query_builder->setParameter("status_" . $index, '%' . $statut . '%');
        }
        $query_builder->andWhere($orStatusCondition);

        return $query_builder;
    }
}
