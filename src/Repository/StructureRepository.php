<?php

namespace App\Repository;

use App\Entity\Structure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

/**
 * @extends ServiceEntityRepository<Structure>
 *
 * @method Structure|null find($id, $lockMode = null, $lockVersion = null)
 * @method Structure|null findOneBy(array $criteria, array $orderBy = null)
 * @method Structure[]    findAll()
 * @method Structure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StructureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Structure::class);
    }

    public function findWhereUuidIn(array $uuids)
    {
        return $this->createQueryBuilder('s')
            ->where("s.uuid in (:uuids)")
            ->setParameters([
                'uuids' => array_map(function ($uuid) {
                    return Uuid::fromString($uuid)->toBinary();
                }, $uuids)
            ])
            ->getQuery()
            ->getResult();
    }

    public function findStructuresSLA(): array
    {
        return $this->createQueryBuilder('s')
            ->innerJoin("s.structureParent", "structureParent")
            ->where("structureParent.echelon = :echelonRegional")
            ->andWhere('s.statut in (:statutsActifs)')
            ->andWhere("s.type in (:types)")
            ->setParameter('types', Structure::LISTE_TYPES_STRUCTURES_SLA_OU_SLAN)
            ->setParameter('echelonRegional', Structure::ECHELON_STRUCTURE_REGIONAL)
            ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])

            ->getQuery()
            ->getResult();
    }


    public function findStructuresFillesSLAActives($structureParent): array
    {
        return $this->createQueryBuilder('s')
            ->innerJoin("s.structureParent", "structureParent")
            ->where("structureParent.echelon = :echelonRegional")
            ->andWhere("s.structureParent = :structureParent")
            ->andWhere("s.type in (:types)")
            ->andWhere('s.statut in (:statutsActifs)')
            ->setParameter('types', Structure::LISTE_TYPES_STRUCTURES_SLA_OU_SLAN)
            ->setParameter('echelonRegional', Structure::ECHELON_STRUCTURE_REGIONAL)
            ->setParameter("structureParent", $structureParent)
            ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])
            ->orderBy('s.nom', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findStructuresSLAN(): array
    {
        return $this->createQueryBuilder('s')
            ->innerJoin("s.structureParent", "structureParent")
            ->where("structureParent.echelon = :echelonNational")
            ->andWhere("s.type in (:types)")
            ->andWhere('s.statut in (:statutsActifs)')
            ->setParameter('types', Structure::LISTE_TYPES_STRUCTURES_SLA_OU_SLAN)
            ->setParameter('echelonNational', Structure::ECHELON_STRUCTURE_NATIONAL)
            ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])

            ->getQuery()
            ->getResult();
    }

    public function findStructuresResponsabiliteRegionale(): array
    {
        return $this->createQueryBuilder('s')
            ->Where("s.responsabiliteRegionale = TRUE")
            ->andWhere('s.statut in (:statutsActifs)')
            ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])
            ->orderBy('s.nom', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findStructuresRegion(): array
    {
        return $this->createQueryBuilder('s')
            ->innerJoin("s.structureParent", "structureParent")
            ->where("structureParent.echelon = :echelonNational")
            ->andWhere("s.type = :type")
            ->andWhere('s.statut in (:statutsActifs)')
            ->setParameter('echelonNational', Structure::ECHELON_STRUCTURE_NATIONAL)
            ->setParameter('type', Structure::TYPE_STRUCTURE_REGION)
            ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])
            ->orderBy('s.nom', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findAllStructuresRegionalesActives(): array
    {
        return $this->createQueryBuilder('s')
            ->innerJoin("s.structureParent", "structureParent")
            ->where("structureParent.echelon = :echelonNational")
            ->andWhere("s.type = :type")
            ->andWhere('s.statut in (:statutsActifs)')
            ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])
            ->setParameter('echelonNational', Structure::ECHELON_STRUCTURE_NATIONAL)
            ->setParameter('type', Structure::TYPE_STRUCTURE_REGION)
            ->orderBy('s.nom', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findAllStructuresSLAActives(): array
    {

        return $this->createQueryBuilder('s')

            ->innerJoin("s.structureParent", "structureParent")
            ->where("structureParent.echelon = :echelonRegional")
            ->andWhere('s.statut in (:statutsActifs)')
            ->andWhere("s.type in (:types)")
            ->setParameter('types', Structure::LISTE_TYPES_STRUCTURES_SLA_OU_SLAN)
            ->setParameter('echelonRegional', Structure::ECHELON_STRUCTURE_REGIONAL)
            ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])


            ->orderBy('s.nom', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findAllStructuresActives(bool $withParent = false): array
    {

        $queryBuilder = $this->createQueryBuilder('s')

            ->andWhere('s.statut in (:statutsActifs)')
            ->setParameter("statutsActifs", [Structure::STATUT_STRUCTURE_AUTONOME, Structure::STATUT_STRUCTURE_RATTACHEE])


            ->orderBy('s.nom', 'ASC');

        if ($withParent) {
            $queryBuilder->andWhere('s.structureParent IS NOT NULL');
        }

        return $queryBuilder->getQuery()
            ->getResult();
    }
    //    /**
    //     * @return Structure[] Returns an array of Structure objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Structure
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
