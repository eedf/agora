<?php

namespace App\Command;

use App\Entity\Droit;
use App\Repository\DroitRepository;
use App\Service\NotificationHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'agora:gestion-expiration-droits-fin',
    description: "Supprime les droits manuels lorsqu'ils sont arrivés à expiration",
)]
class AgoraGestionExpirationDroitsFinCommand extends Command
{

    private DroitRepository $droitRepository;
    private $entityManager;
    private $notificationHelper;

    public function __construct(DroitRepository $droitRepository, EntityManagerInterface $entityManager, NotificationHelper $notificationHelper)
    {
        $this->droitRepository = $droitRepository;
        $this->entityManager = $entityManager;
        $this->notificationHelper = $notificationHelper;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $dateCourante = new DateTime();
        $nbDroitsSupprimes = 0;
        $io->info("Recherche des droits exiprés...");
        $droitsAsupprimer = $this->droitRepository->findDroitsExpires($dateCourante, Droit::TYPE_DROIT_MANUEL);
        $io->info("Suppression des droits exiprés...");
        foreach ($droitsAsupprimer as $droitAsupprimer) {
            $nbDroitsSupprimes++;
            $this->notificationHelper->gestionNotification(NotificationHelper::NOTIFICATION_009, [
                NotificationHelper::PERIMETRE_DROIT => $droitAsupprimer
            ]);
            $this->entityManager->remove($droitAsupprimer);
        }

        $this->entityManager->flush();

        $io->success("Terminé. " . $nbDroitsSupprimes . " ont été supprimés.");

        return Command::SUCCESS;
    }
}
