<?php

namespace App\Command;

use App\Entity\Droit;
use App\Repository\DroitRepository;
use App\Repository\NotificationRepository;
use App\Service\NotificationHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'agora:suppression-notifications',
    description: "Supprime les notrifications vieilles de plus de deux mois",
)]
class AgoraSuppressionsNotificationsCommand extends Command
{

    private NotificationRepository $notificationRepository;
    private $entityManager;

    public function __construct(NotificationRepository $notificationRepository, EntityManagerInterface $entityManager, NotificationHelper $notificationHelper)
    {
        $this->notificationRepository = $notificationRepository;
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure(): void
    {
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $date = new DateTime("-2 months");
        $nbNotifsSupprimes = 0;
        $io->info("Recherche des notifications vieilles de plus de 2 mois...");
        $notificationsASupprimer = $this->notificationRepository->findNotificationsExpirees($date);
        $io->info("Suppression des notifications vieilles de plus de 2 mois...");
        foreach ($notificationsASupprimer as $notificationASupprimer) {
            $nbNotifsSupprimes++;
            $this->entityManager->remove($notificationASupprimer);
        }

        $this->entityManager->flush();

        $io->success("Terminé. " . $nbNotifsSupprimes . " notifications ont été supprimées.");

        return Command::SUCCESS;
    }
}
