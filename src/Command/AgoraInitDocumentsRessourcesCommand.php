<?php

namespace App\Command;

use App\Entity\DocumentRessource;
use App\Repository\DocumentRessourceRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'agora:init-documents_ressources',
    description: 'Crée et supprime les documents ressources',
)]
class AgoraInitDocumentsRessourcesCommand extends Command
{

    private DocumentRessourceRepository $documentRessourceRepository;
    private EntityManager $entityManager;
    public function __construct(DocumentRessourceRepository $documentRessourceRepository, EntityManagerInterface $entityManager)
    {
        $this->documentRessourceRepository = $documentRessourceRepository;
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->info("Recherche des documents ressources existants ...");
        $documentsRessourceExistants = $this->documentRessourceRepository->findAll();
        $documentsRessourceASupprimer = array();
        $documentsRessourceACreer = DocumentRessource::LISTE_DOCUMENTS_RESSOURCES;
        foreach ($documentsRessourceExistants as $documentRessourceExistant) {
            $categorie = $documentRessourceExistant->getCategorie();
            $nom = $documentRessourceExistant->getNom();

            if (array_key_exists($categorie, $documentsRessourceACreer) && (array_key_exists($nom, $documentsRessourceACreer[$categorie]))) {
                unset($documentsRessourceACreer[$categorie][$nom]);
            } else {
                $documentsRessourceASupprimer[] = $documentRessourceExistant;
            }
        }

        $io->success("Terminé");

        $io->info("Ajout des nouveaux documents ressources ...");
        //Création
        $nbDocAjoutes = 0;
        foreach ($documentsRessourceACreer as $categorie => $documents) {
            foreach ($documents as $id => $titre) {
                $doc = new DocumentRessource();
                $doc->setNom($id);
                $doc->setTitre($titre);
                $doc->setCategorie($categorie);

                $this->entityManager->persist($doc);
                $this->entityManager->flush();
                $nbDocAjoutes++;
            }
        }
        $io->success("Ajout terminé");

        $io->info("Suppression des documents ressources obsolètes ...");

        //Suppression
        $nbDocSupprimes = 0;
        foreach ($documentsRessourceASupprimer as $documentRessourceASupprimer) {
            $this->entityManager->remove($documentRessourceASupprimer);
            $this->entityManager->flush();
            $nbDocSupprimes++;
        }

        $io->success("Suppression terminée");

        $io->success("Terminé: " . $nbDocAjoutes . " documents ressources ont été rajoutés et " . $nbDocSupprimes . " ont été supprimés");
        return Command::SUCCESS;
    }
}
