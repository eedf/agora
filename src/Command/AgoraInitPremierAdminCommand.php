<?php

namespace App\Command;

use App\Entity\Adherent;
use App\Entity\Employe;
use App\Entity\Personne;
use App\Repository\AdherentRepository;
use App\Repository\EmployeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsCommand(
    name: 'agora:init-premier-admin',
    description: 'Permet de positionner le premier administrateur, et de l\'adresse support',
)]
class AgoraInitPremierAdminCommand extends Command
{

    private $entityManager;
    private $validatorInterface;
    private AdherentRepository $adherentRepository;
    private EmployeRepository $employeRepository;
    public function __construct(EntityManagerInterface $entityManager, AdherentRepository $adherentRepository, EmployeRepository $employeRepository, ValidatorInterface $validatorInterface)
    {
        $this->entityManager = $entityManager;
        $this->adherentRepository = $adherentRepository;
        $this->validatorInterface = $validatorInterface;
        $this->employeRepository = $employeRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $numAdh = $io->ask("Saisissez le numéro d'adhérent·e ou de salarié·e de la personne à nommer comme administrateur", null, function ($numAdh) {
            // use the validator to validate the value
            $errors = $this->validatorInterface->validate(
                $numAdh,
                [
                    new GreaterThan([
                        'message' => "Le numéro d'adhérent·e ou de salarié·e doit être compris entre 000000 et 999999",
                        'value' => 0
                    ]),
                    new LessThan([
                        'message' => "Le numéro d'adhérent·e ou de salarié·e doit être compris entre 000000 et 999999",
                        'value' => 1000000
                    ])
                ],
            );
            if (count($errors) > 0) {
                throw new \RuntimeException($errors[0]->getMessage());
            }

            return $numAdh;
        });
        $io->info("Recherche de l'existence de la personne en base...");

        /** @var Adherent $adherent */
        $adherent = $this->adherentRepository->findOneBy(['numeroAdherent' => $numAdh]);
        if (!$adherent) {
            /** @var Employe $employe */
            $employe = $this->employeRepository->findOneBy(['numeroSalarie' => $numAdh]);
            if (!$employe) {
                $io->error("La personne avec le numéro " . $numAdh . " n'existe pas!");
                return Command::FAILURE;
            } else {
                $personne = $employe->getPersonne();
                $io->success("Salarié·e trouvé·e!");
                if ($personne->getContratTravailValide() == null) {
                    $io->error("Le salarié·e avec le numéro " . $numAdh . " n'est pas actif!");
                    return Command::FAILURE;
                }
            }
        } else {
            $personne = $adherent->getPersonne();


            $io->success("Personne trouvée!");
            if (!$personne->estAdherentValide()) {
                $io->error("La personne avec le numéro " . $numAdh . " n'est pas à jour de sa cotisation ou n'est pas valide!");
                return Command::FAILURE;
            }
            $io->success("Personne à jour de sa cotisation!");
        }
        $personne->addRole(Personne::ROLE_ADMIN);

        $this->entityManager->persist($personne);
        $this->entityManager->flush();

        $io->success('La personne ' . $personne->getPrenom() . ' ' . $personne->getNom() . ' a bien été nommée comme administratrice');
        return Command::SUCCESS;
    }
}
