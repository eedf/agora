<?php

namespace App\Command;

use App\Entity\CompteRenduInstance;
use DateTime;
use App\Entity\SaisonDemocratique;
use App\Entity\Structure;
use App\Repository\CompteRenduInstanceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use App\Repository\SaisonDemocratiqueRepository;
use App\Repository\StructureRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Workflow\WorkflowInterface;

#[AsCommand(
    name: 'agora:changement-saison-democratique',
    description: 'Permet de créer un nouvelle saison démocratique',
)]
class AgoraChangementSaisonDemocratiqueCommand extends Command
{
    private SaisonDemocratiqueRepository $saisonDemocratiqueRepository;
    private StructureRepository $structureRepository;
    private CompteRenduInstanceRepository $compteRenduInstanceRepository;
    private WorkflowInterface $compteRenduInstanceWorkflow;
    private $entityManager;
    public function __construct(WorkflowInterface $compteRenduInstanceWorkflow, SaisonDemocratiqueRepository $saisonDemocratiqueRepository, StructureRepository $structureRepository, CompteRenduInstanceRepository $compteRenduInstanceRepository, EntityManagerInterface $entityManager)
    {
        $this->saisonDemocratiqueRepository = $saisonDemocratiqueRepository;
        $this->entityManager = $entityManager;
        $this->structureRepository = $structureRepository;
        $this->compteRenduInstanceRepository = $compteRenduInstanceRepository;
        $this->compteRenduInstanceWorkflow = $compteRenduInstanceWorkflow;
        parent::__construct();
    }

    protected function configure(): void
    {


        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $creerNouvelleSaison = false;

        $currentDate = new DateTime('now');

        $io->section('Début changement-saison-democratique');
        //On vérifie s'il existe une saison démocratique active, si c'est le cas, on vérifie que la date de changement n'est pas atteinte:
        $saisonActive = $this->saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        if (!$saisonActive) {
            $io->text("Pas de saison démocratique active");
            $creerNouvelleSaison = true;
        } else {

            $io->info("Saison démocratique active \"" . $saisonActive->getNom() . "\" trouvée");

            //Si la saison active est expirée
            if ($saisonActive->getDateFin() <= $currentDate) {
                $io->text("La saison démocratique active \"" . $saisonActive->getNom() . "\" est obsolète");

                //On passe tous les compte-rendus qui sont à saison passée à archivé
                $io->info('Archivage des comptes-rendus en "saison passée"...');
                $nbCRArchives = 0;
                $comptesRendusAArchiver = $this->compteRenduInstanceRepository->findByStatus(CompteRenduInstance::STATUT_CR_INSTANCE_SAISON_PASSEE);
                foreach ($comptesRendusAArchiver as $compteRenduAArchiver) {

                    if ($this->compteRenduInstanceWorkflow->can($compteRenduAArchiver, 'archivage')) {

                        $this->compteRenduInstanceWorkflow->apply($compteRenduAArchiver, 'archivage');
                        $compteRenduAArchiver->setDateModification($currentDate);
                        $this->entityManager->persist($compteRenduAArchiver);


                        $nbCRArchives++;
                    }
                }
                $this->entityManager->flush();
                $io->text($nbCRArchives . " comptes-rendus archivés");


                //On passe tous les compte-rendus de la saison active à "Saison passée"
                $io->info('Passage des comptes-rendus de la saison courante en "saison passée"...');
                $nbCRValides = 0;
                $nbCRNonValides = 0;
                $nbCRAttenteValidation = 0;
                $nbCRNonSaisi = 0;
                $nbCRPasDInstance = 0;
                foreach ($saisonActive->getComptesRendusInstances() as $compteRenduAForcerASaisonPassee) {

                    if ($this->compteRenduInstanceWorkflow->can($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_VALIDE_SAISON_PASSEE)) {

                        $this->compteRenduInstanceWorkflow->apply($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_VALIDE_SAISON_PASSEE);
                        $compteRenduAForcerASaisonPassee->setDateModification($currentDate);
                        $this->entityManager->persist($compteRenduAForcerASaisonPassee);
                        $this->entityManager->flush();

                        $nbCRValides++;
                    } else if ($this->compteRenduInstanceWorkflow->can($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_NON_VALIDE_SAISON_PASSEE)) {
                        $this->compteRenduInstanceWorkflow->apply($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_NON_VALIDE_SAISON_PASSEE);
                        $compteRenduAForcerASaisonPassee->setDateModification($currentDate);
                        $compteRenduAForcerASaisonPassee->supprimerValeurs();

                        $this->entityManager->persist($compteRenduAForcerASaisonPassee);

                        $nbCRNonValides++;
                    } else if ($this->compteRenduInstanceWorkflow->can($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_ATTENTE_VALIDATION_SAISON_PASSEE)) {
                        $this->compteRenduInstanceWorkflow->apply($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_ATTENTE_VALIDATION_SAISON_PASSEE);
                        $compteRenduAForcerASaisonPassee->setDateModification($currentDate);
                        $compteRenduAForcerASaisonPassee->supprimerValeurs();

                        $this->entityManager->persist($compteRenduAForcerASaisonPassee);

                        $nbCRAttenteValidation++;
                    } else if ($this->compteRenduInstanceWorkflow->can($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_NON_SAISI_SAISON_PASSEE)) {
                        $this->compteRenduInstanceWorkflow->apply($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_NON_SAISI_SAISON_PASSEE);
                        $compteRenduAForcerASaisonPassee->setDateModification($currentDate);
                        $compteRenduAForcerASaisonPassee->supprimerValeurs();

                        $this->entityManager->persist($compteRenduAForcerASaisonPassee);

                        $nbCRNonSaisi++;
                    } else if ($this->compteRenduInstanceWorkflow->can($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_PAS_D_INSTANCE_SAISON_PASSEE)) {
                        $this->compteRenduInstanceWorkflow->apply($compteRenduAForcerASaisonPassee, CompteRenduInstance::TRANSITION_NON_SAISI_SAISON_PASSEE);
                        $compteRenduAForcerASaisonPassee->setDateModification($currentDate);

                        $this->entityManager->persist($compteRenduAForcerASaisonPassee);

                        $nbCRPasDInstance++;
                    }
                }
                $this->entityManager->flush();
                $io->text($nbCRValides . " compte-rendus validés ont été passés à l'état \"saison passée\"");
                $io->text($nbCRNonSaisi . " compte-rendus non saisis ont été passés à l'état \"saison passée\"");
                $io->text($nbCRAttenteValidation . " compte-rendus en attente de validation ont été passés à l'état \"saison passée\"");
                $io->text($nbCRNonValides . " compte-rendus non validés ont été passés à l'état \"saison passée\"");
                $io->text($nbCRPasDInstance . " compte-rendus pour des instance n'ayant pas eu lieu ont été passés à l'état \"saison passée\"");


                //On désactive la saison active
                $io->info("Désactivation  de la saison courante ...");
                $saisonActive->setActif(false);
                $this->entityManager->persist($saisonActive);
                $this->entityManager->flush();
                $creerNouvelleSaison = true;
                $io->text("Terminé");
            }
        }

        if ($creerNouvelleSaison) {
            $io->info("Création d'une nouvelle saison...");
            $nouvelleSaison = new SaisonDemocratique();
            $nouvelleSaison->setSaisonPrecedente($saisonActive);
            $nouvelleSaison->setActif(true);
            $nouvelleSaison->setDateDebut(new DateTime(SaisonDemocratique::DEFAULT_FORMAT_DATE_DEBUT_SAISON));
            $nouvelleSaison->setDateFin(new DateTime(SaisonDemocratique::DEFAULT_FORMAT_DATE_FIN_SAISON));
            $nouvelleSaison->setNom($nouvelleSaison->getDateDebut()->format('Y') . "/" . $nouvelleSaison->getDateFin()->format('Y'));
            $this->entityManager->persist($nouvelleSaison);
            $this->entityManager->flush();
            $io->success("Nouvelle saison active \"" . $nouvelleSaison->getNom() . "\" créée");
            $saisonActive = $nouvelleSaison;

            $io->info("Création des compte-rendus d'instance pour cette saison ...");
            //Recherche des SLA
            $nbCRSLA = 0;
            $structuresSLA = $this->structureRepository->findStructuresSLA();
            /** @var Structure $structureSLA */
            foreach ($structuresSLA as $structureSLA) {
                if ($structureSLA->getStatut() != Structure::STATUT_STRUCTURE_FERMEE) // on ne crée le compte rendu que si la structure n'est pas fermée
                {
                    $compteRenduInstance = new CompteRenduInstance(CompteRenduInstance::TYPE_APL_SLA);
                    $this->compteRenduInstanceWorkflow->getMarking($compteRenduInstance);
                    $compteRenduInstance->setSaison($saisonActive);
                    $compteRenduInstance->setStructure($structureSLA);
                    $this->entityManager->persist($compteRenduInstance);
                    $nbCRSLA++;
                }
            }

            $this->entityManager->flush();
            $io->text($nbCRSLA . " comptes-rendus d'APL de SLA créés");

            //Recherche des SLAN
            $structuresSLAN = $this->structureRepository->findStructuresSLAN();
            $nbCRSLAN = 0;
            /** @var Structure $structureSLAN */
            foreach ($structuresSLAN as $structureSLAN) {
                if ($structureSLAN->getStatut() != Structure::STATUT_STRUCTURE_FERMEE) // on ne crée le compte rendu que si la structure n'est pas fermée
                {
                    $compteRenduInstance = new CompteRenduInstance(CompteRenduInstance::TYPE_APL_SLAN);
                    $this->compteRenduInstanceWorkflow->getMarking($compteRenduInstance);
                    $compteRenduInstance->setSaison($saisonActive);
                    $compteRenduInstance->setStructure($structureSLAN);
                    $this->entityManager->persist($compteRenduInstance);
                    $nbCRSLAN++;
                }
            }
            $io->text($nbCRSLAN . " comptes-rendus d'APL de SLAN créés");
            $this->entityManager->flush();

            //Recherche des Régions
            $structuresRegion = $this->structureRepository->findStructuresRegion();
            $nbCRCongres = 0;
            /** @var Structure $structureRegion */
            foreach ($structuresRegion as $structureRegion) {
                if ($structureRegion->getStatut() != Structure::STATUT_STRUCTURE_FERMEE) // on ne crée le compte rendu que si la structure n'est pas fermée
                {
                    $compteRenduInstance = new CompteRenduInstance(CompteRenduInstance::TYPE_CONGRES);
                    $this->compteRenduInstanceWorkflow->getMarking($compteRenduInstance);
                    $compteRenduInstance->setSaison($saisonActive);
                    $compteRenduInstance->setStructure($structureRegion);
                    $this->entityManager->persist($compteRenduInstance);
                    $nbCRCongres++;
                }
            }
            $io->text($nbCRCongres . " comptes-rendus de congrès créés");
            $this->entityManager->flush();
        }

        $io->success('Fin changement-saison-democratique');


        return Command::SUCCESS;
    }
}
