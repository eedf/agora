<?php

namespace App\Command;

use App\Repository\SaisonDemocratiqueRepository;
use App\Service\EcritureFonctionJeitoHelper;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\MailerInterface;

#[AsCommand(
    name: 'agora:ecriture-fonctions-electives-et-autonomie-structure',
    description: "Met à jour les attributions des missions électives et d'autonomie de structure de la saison démocratique en cours et celle de l'année précédente dans Jéito",
)]
class AgoraEcritureFonctionsElectivesEtAutonomieStructureCommand extends Command
{

    const NIVEAU_MESSAGE_SUCCESS = 0;
    const NIVEAU_MESSAGE_INFO = 1;
    const NIVEAU_MESSAGE_WARNING = 2;
    const NIVEAU_MESSAGE_TEXT = 3;
    const NIVEAU_MESSAGE_SECTION = 4;

    private $infosForSupport;

    public function __construct(private SaisonDemocratiqueRepository $saisonDemocratiqueRepository, private EcritureFonctionJeitoHelper $ecritureFonctionJeitoHelper, private MailerInterface $mailer, private String $adresseMailSupport)
    {
        $this->infosForSupport = array();
        parent::__construct();
    }

    protected function configure(): void {}

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $saisonActive = $this->saisonDemocratiqueRepository->findOneBy(['actif' => true]);
        if (!$saisonActive) {
            $io->warning("Pas de saison démocratique active");
            return Command::FAILURE;
        } else {
            //On commence par la saison précédente
            $this->afficherMessage("Mise à jour des missions électives et de l'autonomie des structures vers Jéito pour la saison précédente", self::NIVEAU_MESSAGE_SECTION, $io);
            if (!$saisonActive->getSaisonPrecedente()) {
                $this->afficherMessage("Pas de saison démocratique précédente", self::NIVEAU_MESSAGE_WARNING, $io);
            } else {

                $nb_compte_rendus_traites = 0;
                $nb_compte_rendus_erreur = 0;
                $compteRendusSaisonPrecedente = $saisonActive->getSaisonPrecedente()->getComptesRendusInstances();
                $io->progressStart(count($compteRendusSaisonPrecedente));
                foreach ($compteRendusSaisonPrecedente as $compteRendu) {
                    try {
                        $this->ecritureFonctionJeitoHelper->ecritureCompteRendu($compteRendu);
                    } catch (Exception $ex) {
                        $this->afficherMessage("Erreur lors de l'envoi des attribution lors pour le compte rendu de " . $compteRendu->getStructure()->getNom() . ": " . $ex->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                        $nb_compte_rendus_erreur++;
                    }
                    $nb_compte_rendus_traites++;
                    $io->progressAdvance();
                }
                $io->progressFinish();
                $this->afficherMessage('Mise à jour des missions électives de ' . $nb_compte_rendus_traites . ' compte-rendus pour la saison précédente, dont ' . $nb_compte_rendus_erreur . ' en erreur.', self::NIVEAU_MESSAGE_SUCCESS, $io);
            }
            //On finit par la saison en cours
            $this->afficherMessage("Mise à jour des missions électives et de l'autonomie des structures vers Jéito pour la saison active", self::NIVEAU_MESSAGE_SECTION, $io);

            $nb_compte_rendus_traites = 0;
            $nb_compte_rendus_erreur = 0;
            $compteRendusSaisonActive = $saisonActive->getComptesRendusInstances();
            $io->progressStart(count($compteRendusSaisonActive));
            foreach ($compteRendusSaisonActive as $compteRendu) {
                try {
                    $this->ecritureFonctionJeitoHelper->ecritureCompteRendu($compteRendu);
                } catch (Exception $ex) {
                    $this->afficherMessage("Erreur lors de l'envoi des attribution lors pour le compte rendu de " . $compteRendu->getStructure()->getNom() . ": " . $ex->getMessage(), self::NIVEAU_MESSAGE_WARNING, $io);
                    $nb_compte_rendus_erreur++;
                }
                $nb_compte_rendus_traites++;
                $io->progressAdvance();
            }
            $io->progressFinish();
            $this->afficherMessage('Mise à jour des missions électives de ' . $nb_compte_rendus_traites . ' compte-rendus pour la saison courante, dont ' . $nb_compte_rendus_erreur . ' en erreur.', self::NIVEAU_MESSAGE_SUCCESS, $io);
        }
        $this->envoiMailRecapSupport($io);



        return Command::SUCCESS;
    }


    function envoiMailRecapSupport(SymfonyStyle $io)
    {
        //envoi du mail à l'adresse support:
        if ($this->adresseMailSupport) {
            $message = (new TemplatedEmail())

                // On attribue le destinataire
                ->to($this->adresseMailSupport)
                ->subject("[Support Agora] Récapitulatif de l'écriture des missions électives et des statuts d'autonomie de structures")
                ->htmlTemplate('mails_monitoring_support/recap_ecriture_fonctions_email.html.twig')
                ->context([
                    'infos' => $this->infosForSupport
                ]);

            $this->mailer->send($message);
        } else {
            $io->warning('L\'adresse de support n\'existe pas encore, aucun mail ne sera envoyé, n\'oubliez pas d\'initialiser le premier administrateur');
        }
    }

    function afficherMessage(String $message, int $niveauInformation, SymfonyStyle $io)
    {
        $afficherDansMail = true;
        switch ($niveauInformation) {
            case self::NIVEAU_MESSAGE_SUCCESS:
                $io->success($message);
                break;
            case self::NIVEAU_MESSAGE_INFO:
                $io->info($message);
                break;
            case self::NIVEAU_MESSAGE_WARNING:
                $io->warning($message);
                $message = "Attention: " . $message;
                break;
            case self::NIVEAU_MESSAGE_TEXT:
                $io->text($message);
                $afficherDansMail = false;
                break;
            case self::NIVEAU_MESSAGE_SECTION:
                $io->section($message);
                break;
        }
        if ($afficherDansMail) {
            $this->infosForSupport[] = $message;
        }
    }
}
