#!/bin/bash

set -ex

echo "======== BEGIN `date` ========"

# Vérification de l'utilisateur
if [ "$(id -un)" != "www-data" ]; then
    echo "Ce script doit être lancé en tant qu'utilisateur www-data."
    exit 1
fi

# Rotation des logs
touch /var/www/html/var/log/logrotate-state
logrotate --state /var/www/html/var/log/logrotate-state logrotate.conf

# Supprime les droits fins expirés
php bin/console agora:gestion-expiration-droits-fin --env=prod -vvv

# Synchronisation quotidienne avec Jéito
php bin/console agora:synchronisation-jeito --env=prod -vvv

# Gestion du changement de saison démocratique et changement d'état des compte rendus
php bin/console agora:changement-saison-democratique --env=prod -vvv

# Gestion de l'écriture des missions électives et de l'autonomie des structures vers Jéito
php bin/console agora:ecriture-fonctions-electives-et-autonomie-structure

# Suppression des notifications vieilles de plus de deux mois
php bin/console agora:suppression-notifications --env=prod -vvv

echo "======== END `date` ========"
