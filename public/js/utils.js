function desactiverEtMasquerDivForm($div) {
    $div.hide();
    $div.find("select").prop('disabled', true);
    $div.find("input").prop('disabled', true);
    $div.find("textarea").prop('disabled', true);
}

function activerEtAfficherDivForm($div) {
    $div.show();
    $div.find("select").prop('disabled', false);
    $div.find("input").prop('disabled', false);
    $div.find("textarea").prop('disabled', false);
}


