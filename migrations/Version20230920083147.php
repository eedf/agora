<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230920083147 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE compte_rendu_instance (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, derniere_personne_soummetteur_id INT DEFAULT NULL, personne_valideuse_id INT DEFAULT NULL, saison_id INT NOT NULL, type VARCHAR(20) NOT NULL, date_modification DATETIME DEFAULT NULL, date_validation DATETIME DEFAULT NULL, INDEX IDX_B2F845712534008B (structure_id), INDEX IDX_B2F845715C2D9856 (derniere_personne_soummetteur_id), INDEX IDX_B2F84571FBC25E50 (personne_valideuse_id), INDEX IDX_B2F84571F965414C (saison_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE saison_democratique (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, date_limite_aplsla DATETIME NOT NULL, date_limite_aplslan DATETIME NOT NULL, date_limite_congres DATETIME NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, actif TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F845712534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F845715C2D9856 FOREIGN KEY (derniere_personne_soummetteur_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571FBC25E50 FOREIGN KEY (personne_valideuse_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571F965414C FOREIGN KEY (saison_id) REFERENCES saison_democratique (id)');
        $this->addSql('ALTER TABLE contrat_travail CHANGE date_fin date_fin DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F845712534008B');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F845715C2D9856');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571FBC25E50');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571F965414C');
        $this->addSql('DROP TABLE compte_rendu_instance');
        $this->addSql('DROP TABLE saison_democratique');
        $this->addSql('ALTER TABLE contrat_travail CHANGE date_fin date_fin DATETIME NOT NULL');
    }
}
