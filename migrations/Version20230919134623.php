<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230919134623 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE adherent (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, numero_adherent INT NOT NULL, date_modification DATETIME NOT NULL, statut VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_90D3F06031C43700 (numero_adherent), UNIQUE INDEX UNIQ_90D3F060A21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adhesion (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, adherent_id INT NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, date_modification DATETIME NOT NULL, annule TINYINT(1) NOT NULL, id_jeito BIGINT NOT NULL, UNIQUE INDEX UNIQ_C50CA65A2685EE64 (id_jeito), INDEX IDX_C50CA65A2534008B (structure_id), INDEX IDX_C50CA65A25F06C53 (adherent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adresse_postale (id INT AUTO_INCREMENT NOT NULL, adresse VARCHAR(255) DEFAULT NULL, complement_adresse VARCHAR(255) DEFAULT NULL, code_postal VARCHAR(10) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, pays VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contrat_travail (id INT AUTO_INCREMENT NOT NULL, employe_id INT NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, date_modification DATETIME NOT NULL, type VARCHAR(255) NOT NULL, nom VARCHAR(255) DEFAULT NULL, id_jeito BIGINT NOT NULL, UNIQUE INDEX UNIQ_394729CD2685EE64 (id_jeito), INDEX IDX_394729CD1B65292 (employe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE coordonnees (id INT AUTO_INCREMENT NOT NULL, adresse_postale_id INT DEFAULT NULL, personne_id INT NOT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) NOT NULL, adresse_mail VARCHAR(100) NOT NULL, tel_mobile VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', tel_domicile VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', UNIQUE INDEX UNIQ_BC8EC7AC96EEC07 (adresse_postale_id), UNIQUE INDEX UNIQ_BC8EC7AA21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employe (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, numero_salarie BIGINT NOT NULL, date_modification DATETIME NOT NULL, email VARCHAR(255) DEFAULT NULL, telephone VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', UNIQUE INDEX UNIQ_F804D3B944DA24BB (numero_salarie), UNIQUE INDEX UNIQ_F804D3B9A21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipe (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, date_creation DATETIME DEFAULT NULL, date_modification DATETIME DEFAULT NULL, type VARCHAR(255) NOT NULL, date_desactivation DATETIME DEFAULT NULL, nom VARCHAR(255) NOT NULL, id_jeito BIGINT NOT NULL, UNIQUE INDEX UNIQ_2449BA152685EE64 (id_jeito), INDEX IDX_2449BA152534008B (structure_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fonction (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, equipe_id INT NOT NULL, id_jeito BIGINT NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, nom VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, statut VARCHAR(255) NOT NULL, date_modification DATETIME NOT NULL, UNIQUE INDEX UNIQ_900D5BD2685EE64 (id_jeito), INDEX IDX_900D5BDA21BD112 (personne_id), INDEX IDX_900D5BD6D861B89 (equipe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parametre_global (id INT AUTO_INCREMENT NOT NULL, nom_parametre VARCHAR(50) NOT NULL, valeur_parametre VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_B2A496F83836E100 (nom_parametre), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personne (id INT AUTO_INCREMENT NOT NULL, responsable_legal1_id INT DEFAULT NULL, responsable_legal2_id INT DEFAULT NULL, id_jeito BIGINT NOT NULL, date_modification DATETIME NOT NULL, nom VARCHAR(100) NOT NULL, prenom VARCHAR(100) NOT NULL, date_naissance DATE DEFAULT NULL, genre VARCHAR(20) NOT NULL, adresse_mail VARCHAR(254) NOT NULL, droit_image TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_FCEC9EF2685EE64 (id_jeito), INDEX IDX_FCEC9EF39FF2EE9 (responsable_legal1_id), INDEX IDX_FCEC9EF2B4A8107 (responsable_legal2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structure (id INT AUTO_INCREMENT NOT NULL, structure_parent_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, type VARCHAR(100) NOT NULL, date_modification DATETIME NOT NULL, id_jeito BIGINT NOT NULL, statut VARCHAR(255) NOT NULL, echelon VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_6F0137EA2685EE64 (id_jeito), INDEX IDX_6F0137EAE422675C (structure_parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adherent ADD CONSTRAINT FK_90D3F060A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE adhesion ADD CONSTRAINT FK_C50CA65A2534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE adhesion ADD CONSTRAINT FK_C50CA65A25F06C53 FOREIGN KEY (adherent_id) REFERENCES adherent (id)');
        $this->addSql('ALTER TABLE contrat_travail ADD CONSTRAINT FK_394729CD1B65292 FOREIGN KEY (employe_id) REFERENCES employe (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7AC96EEC07 FOREIGN KEY (adresse_postale_id) REFERENCES adresse_postale (id)');
        $this->addSql('ALTER TABLE coordonnees ADD CONSTRAINT FK_BC8EC7AA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE employe ADD CONSTRAINT FK_F804D3B9A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE equipe ADD CONSTRAINT FK_2449BA152534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE fonction ADD CONSTRAINT FK_900D5BDA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE fonction ADD CONSTRAINT FK_900D5BD6D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id)');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF39FF2EE9 FOREIGN KEY (responsable_legal1_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF2B4A8107 FOREIGN KEY (responsable_legal2_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE structure ADD CONSTRAINT FK_6F0137EAE422675C FOREIGN KEY (structure_parent_id) REFERENCES structure (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adherent DROP FOREIGN KEY FK_90D3F060A21BD112');
        $this->addSql('ALTER TABLE adhesion DROP FOREIGN KEY FK_C50CA65A2534008B');
        $this->addSql('ALTER TABLE adhesion DROP FOREIGN KEY FK_C50CA65A25F06C53');
        $this->addSql('ALTER TABLE contrat_travail DROP FOREIGN KEY FK_394729CD1B65292');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7AC96EEC07');
        $this->addSql('ALTER TABLE coordonnees DROP FOREIGN KEY FK_BC8EC7AA21BD112');
        $this->addSql('ALTER TABLE employe DROP FOREIGN KEY FK_F804D3B9A21BD112');
        $this->addSql('ALTER TABLE equipe DROP FOREIGN KEY FK_2449BA152534008B');
        $this->addSql('ALTER TABLE fonction DROP FOREIGN KEY FK_900D5BDA21BD112');
        $this->addSql('ALTER TABLE fonction DROP FOREIGN KEY FK_900D5BD6D861B89');
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF39FF2EE9');
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF2B4A8107');
        $this->addSql('ALTER TABLE structure DROP FOREIGN KEY FK_6F0137EAE422675C');
        $this->addSql('DROP TABLE adherent');
        $this->addSql('DROP TABLE adhesion');
        $this->addSql('DROP TABLE adresse_postale');
        $this->addSql('DROP TABLE contrat_travail');
        $this->addSql('DROP TABLE coordonnees');
        $this->addSql('DROP TABLE employe');
        $this->addSql('DROP TABLE equipe');
        $this->addSql('DROP TABLE fonction');
        $this->addSql('DROP TABLE parametre_global');
        $this->addSql('DROP TABLE personne');
        $this->addSql('DROP TABLE structure');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
