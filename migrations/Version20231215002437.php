<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231215002437 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Correction relation RoeEquipeELue <-> Personne';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_equipe_elue DROP INDEX UNIQ_38FF3B7AA21BD112, ADD INDEX IDX_38FF3B7AA21BD112 (personne_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_equipe_elue DROP INDEX IDX_38FF3B7AA21BD112, ADD UNIQUE INDEX UNIQ_38FF3B7AA21BD112 (personne_id)');
    }
}
