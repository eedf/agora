<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241209095219 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MODIF INSTANCE 3. pouvoir mettre plusieurs représentants de l'échelon supérieur";
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE compte_rendu_instance_personne (compte_rendu_instance_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_A7385150D1A068B (compte_rendu_instance_id), INDEX IDX_A7385150A21BD112 (personne_id), PRIMARY KEY(compte_rendu_instance_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE compte_rendu_instance_personne ADD CONSTRAINT FK_A7385150D1A068B FOREIGN KEY (compte_rendu_instance_id) REFERENCES compte_rendu_instance (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE compte_rendu_instance_personne ADD CONSTRAINT FK_A7385150A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571B1FF0CC7');


        //Migration  des representants
        $this->addSql('INSERT INTO compte_rendu_instance_personne (compte_rendu_instance_id, personne_id)
            SELECT id, representant_echelon_superieur_id
            FROM compte_rendu_instance
            WHERE representant_echelon_superieur_id IS NOT NULL');

        $this->addSql('DROP INDEX IDX_B2F84571B1FF0CC7 ON compte_rendu_instance');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP representant_echelon_superieur_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance_personne DROP FOREIGN KEY FK_A7385150D1A068B');
        $this->addSql('ALTER TABLE compte_rendu_instance_personne DROP FOREIGN KEY FK_A7385150A21BD112');
        $this->addSql('DROP TABLE compte_rendu_instance_personne');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD representant_echelon_superieur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571B1FF0CC7 FOREIGN KEY (representant_echelon_superieur_id) REFERENCES personne (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_B2F84571B1FF0CC7 ON compte_rendu_instance (representant_echelon_superieur_id)');
    }
}
