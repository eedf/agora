<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231212171826 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE compte_rendu_instance set autonomie_structure=1 WHERE autonomie_structure="Autonome"');
        $this->addSql('UPDATE compte_rendu_instance set autonomie_structure=2 WHERE autonomie_structure="Rattachée"');
        $this->addSql('ALTER TABLE compte_rendu_instance CHANGE autonomie_structure autonomie_structure INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance CHANGE autonomie_structure autonomie_structure VARCHAR(255) DEFAULT NULL');
    }
}
