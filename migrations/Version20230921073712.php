<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230921073712 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance ADD representant_echelon_superieur_id INT DEFAULT NULL, ADD date_instance DATETIME DEFAULT NULL, ADD nombre_votants_total INT DEFAULT NULL, ADD nombre_voix_pour_budget_previsionnel INT DEFAULT NULL, ADD nombre_voix_pour_rapport_moral INT DEFAULT NULL, ADD nombre_voix_pour_par INT DEFAULT NULL, ADD nombre_voix_pour_rapport_financier INT DEFAULT NULL');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571B1FF0CC7 FOREIGN KEY (representant_echelon_superieur_id) REFERENCES personne (id)');
        $this->addSql('CREATE INDEX IDX_B2F84571B1FF0CC7 ON compte_rendu_instance (representant_echelon_superieur_id)');
        $this->addSql('ALTER TABLE fonction CHANGE date_fin date_fin DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571B1FF0CC7');
        $this->addSql('DROP INDEX IDX_B2F84571B1FF0CC7 ON compte_rendu_instance');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP representant_echelon_superieur_id, DROP date_instance, DROP nombre_votants_total, DROP nombre_voix_pour_budget_previsionnel, DROP nombre_voix_pour_rapport_moral, DROP nombre_voix_pour_par, DROP nombre_voix_pour_rapport_financier');
        $this->addSql('ALTER TABLE fonction CHANGE date_fin date_fin DATETIME NOT NULL');
    }
}
