<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231120173621 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE election_equipe (id INT AUTO_INCREMENT NOT NULL, compte_rendu_id INT NOT NULL, vote_id INT NOT NULL, UNIQUE INDEX UNIQ_29F641E14BC44A10 (compte_rendu_id), UNIQUE INDEX UNIQ_29F641E172DCDAFC (vote_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE election_nominative (id INT AUTO_INCREMENT NOT NULL, compte_rendu_id INT NOT NULL, personne_id INT NOT NULL, vote_id INT NOT NULL, status VARCHAR(255) NOT NULL, poste VARCHAR(255) NOT NULL, INDEX IDX_268DC1834BC44A10 (compte_rendu_id), UNIQUE INDEX UNIQ_268DC183A21BD112 (personne_id), UNIQUE INDEX UNIQ_268DC18372DCDAFC (vote_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_equipe_elue (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, election_equipe_id INT NOT NULL, fonction_specifique VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_38FF3B7AA21BD112 (personne_id), INDEX IDX_38FF3B7A73DD56E3 (election_equipe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE election_equipe ADD CONSTRAINT FK_29F641E14BC44A10 FOREIGN KEY (compte_rendu_id) REFERENCES compte_rendu_instance (id)');
        $this->addSql('ALTER TABLE election_equipe ADD CONSTRAINT FK_29F641E172DCDAFC FOREIGN KEY (vote_id) REFERENCES vote (id)');
        $this->addSql('ALTER TABLE election_nominative ADD CONSTRAINT FK_268DC1834BC44A10 FOREIGN KEY (compte_rendu_id) REFERENCES compte_rendu_instance (id)');
        $this->addSql('ALTER TABLE election_nominative ADD CONSTRAINT FK_268DC183A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE election_nominative ADD CONSTRAINT FK_268DC18372DCDAFC FOREIGN KEY (vote_id) REFERENCES vote (id)');
        $this->addSql('ALTER TABLE role_equipe_elue ADD CONSTRAINT FK_38FF3B7AA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE role_equipe_elue ADD CONSTRAINT FK_38FF3B7A73DD56E3 FOREIGN KEY (election_equipe_id) REFERENCES election_equipe (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE election_equipe DROP FOREIGN KEY FK_29F641E14BC44A10');
        $this->addSql('ALTER TABLE election_equipe DROP FOREIGN KEY FK_29F641E172DCDAFC');
        $this->addSql('ALTER TABLE election_nominative DROP FOREIGN KEY FK_268DC1834BC44A10');
        $this->addSql('ALTER TABLE election_nominative DROP FOREIGN KEY FK_268DC183A21BD112');
        $this->addSql('ALTER TABLE election_nominative DROP FOREIGN KEY FK_268DC18372DCDAFC');
        $this->addSql('ALTER TABLE role_equipe_elue DROP FOREIGN KEY FK_38FF3B7AA21BD112');
        $this->addSql('ALTER TABLE role_equipe_elue DROP FOREIGN KEY FK_38FF3B7A73DD56E3');
        $this->addSql('DROP TABLE election_equipe');
        $this->addSql('DROP TABLE election_nominative');
        $this->addSql('DROP TABLE role_equipe_elue');
    }
}
