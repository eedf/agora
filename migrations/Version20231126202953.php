<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231126202953 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE droit (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, personne_id INT NOT NULL, fonction_id INT DEFAULT NULL, adhesion_id INT DEFAULT NULL, date_expiration DATETIME DEFAULT NULL, droit VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_CB7AA7512534008B (structure_id), INDEX IDX_CB7AA751A21BD112 (personne_id), INDEX IDX_CB7AA75157889920 (fonction_id), INDEX IDX_CB7AA751F68139D7 (adhesion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE droit ADD CONSTRAINT FK_CB7AA7512534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE droit ADD CONSTRAINT FK_CB7AA751A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE droit ADD CONSTRAINT FK_CB7AA75157889920 FOREIGN KEY (fonction_id) REFERENCES fonction (id)');
        $this->addSql('ALTER TABLE droit ADD CONSTRAINT FK_CB7AA751F68139D7 FOREIGN KEY (adhesion_id) REFERENCES adhesion (id)');
        $this->addSql('ALTER TABLE droit_manuel DROP FOREIGN KEY FK_1E11CE7F2534008B');
        $this->addSql('ALTER TABLE droit_manuel DROP FOREIGN KEY FK_1E11CE7FA21BD112');
        $this->addSql('DROP TABLE droit_manuel');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE droit_manuel (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, personne_id INT NOT NULL, date_expiration DATETIME DEFAULT NULL, droit VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_1E11CE7F2534008B (structure_id), INDEX IDX_1E11CE7FA21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE droit_manuel ADD CONSTRAINT FK_1E11CE7F2534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE droit_manuel ADD CONSTRAINT FK_1E11CE7FA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE droit DROP FOREIGN KEY FK_CB7AA7512534008B');
        $this->addSql('ALTER TABLE droit DROP FOREIGN KEY FK_CB7AA751A21BD112');
        $this->addSql('ALTER TABLE droit DROP FOREIGN KEY FK_CB7AA75157889920');
        $this->addSql('ALTER TABLE droit DROP FOREIGN KEY FK_CB7AA751F68139D7');
        $this->addSql('DROP TABLE droit');
    }
}
