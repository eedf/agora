<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231208143020 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_6F0137EA2685EE64 ON structure');
        $this->addSql('ALTER TABLE structure ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');

        $this->addSql('DROP INDEX UNIQ_900D5BD2685EE64 ON fonction');
        $this->addSql('ALTER TABLE fonction ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');

        $this->addSql('DROP INDEX UNIQ_FCEC9EF2685EE64 ON personne');
        $this->addSql('ALTER TABLE personne ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');

        $this->addSql('DROP INDEX UNIQ_2449BA152685EE64 ON equipe');
        $this->addSql('ALTER TABLE equipe ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');

        $this->addSql('DROP INDEX UNIQ_C50CA65A2685EE64 ON adhesion');
        $this->addSql('ALTER TABLE adhesion ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');

        $this->addSql('DROP INDEX UNIQ_394729CD2685EE64 ON contrat_travail');
        $this->addSql('ALTER TABLE contrat_travail ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');

        $this->addSql('ALTER TABLE adherent ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');

        $this->addSql('ALTER TABLE employe ADD uuid BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\'');
    }

    public function postUp(Schema $schema): void
    {
        //Commande de migration
        $namespace_uuid = Uuid::v5(Uuid::fromString(Uuid::NAMESPACE_DNS), "jeito.eedf.fr");

        $this->connection->getConfiguration()->setMiddlewares([]);
        $this->connection->getConfiguration()->setSQLLogger(null);


        $this->migrerTable($namespace_uuid, "structure", "structure");
        $this->migrerTable($namespace_uuid, "person", "personne");
        $this->migrerTable($namespace_uuid, "team", "equipe");
        $this->migrerTable($namespace_uuid, "adhesion", "adhesion");
        $this->migrerTable($namespace_uuid, "employment", "contrat_travail");
        $this->migrerTable($namespace_uuid, "function", "fonction");
        $this->migrerTable($namespace_uuid, "adherent", "adherent", "numero_adherent");
        $this->migrerTable($namespace_uuid, "employee", "employe", "numero_salarie");


        $stmt = $this->connection->prepare('ALTER TABLE contrat_travail DROP id_jeito');
        $stmt->execute();
        $stmt = $this->connection->prepare('ALTER TABLE adhesion DROP id_jeito');
        $stmt->execute();
        $stmt = $this->connection->prepare('ALTER TABLE personne DROP id_jeito');
        $stmt->execute();
        $stmt = $this->connection->prepare('ALTER TABLE fonction DROP id_jeito');
        $stmt->execute();
        $stmt = $this->connection->prepare('ALTER TABLE structure DROP id_jeito');
        $stmt->execute();
        $stmt = $this->connection->prepare('ALTER TABLE equipe DROP id_jeito');
        $stmt->execute();

        $stmt = $this->connection->prepare('CREATE UNIQUE INDEX UNIQ_90D3F060D17F50A6 ON adherent (uuid)');
        $stmt->execute();
        $stmt = $this->connection->prepare('CREATE UNIQUE INDEX UNIQ_C50CA65AD17F50A6 ON adhesion (uuid)');
        $stmt->execute();
        $stmt = $this->connection->prepare('CREATE UNIQUE INDEX UNIQ_394729CDD17F50A6 ON contrat_travail (uuid)');
        $stmt->execute();
        $stmt = $this->connection->prepare('CREATE UNIQUE INDEX UNIQ_F804D3B9D17F50A6 ON employe (uuid)');
        $stmt->execute();
        $stmt = $this->connection->prepare('CREATE UNIQUE INDEX UNIQ_2449BA15D17F50A6 ON equipe (uuid)');
        $stmt->execute();
        $stmt = $this->connection->prepare('CREATE UNIQUE INDEX UNIQ_900D5BDD17F50A6 ON fonction (uuid)');
        $stmt->execute();
        $stmt = $this->connection->prepare('CREATE UNIQUE INDEX UNIQ_FCEC9EFD17F50A6 ON personne (uuid)');
        $stmt->execute();
        $stmt = $this->connection->prepare('CREATE UNIQUE INDEX UNIQ_6F0137EAD17F50A6 ON structure (uuid)');
        $stmt->execute();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fonction DROP uuid');
    }
    private function migrerTable($namespace_uuid, $nomModeleUuid, $nomTable, $nomIdJeito = "id_jeito")
    {
        $stmt = $this->connection->prepare("SELECT id, " . $nomIdJeito . " FROM " . $nomTable);
        $objets = $stmt->executeQuery();

        while (($objet = $objets->fetchAssociative()) !== false) {

            /** @var Uuid $uuid */
            $uuid = Uuid::v5($namespace_uuid, $nomModeleUuid . "/" . $objet[$nomIdJeito]);
            $this->connection->update(
                $nomTable,
                [
                    'uuid' => $uuid->toBinary()
                ],
                [
                    'id' => $objet['id']
                ]
            );
        }
    }
}
