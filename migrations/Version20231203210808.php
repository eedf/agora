<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231203210808 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE document_ressource (id INT AUTO_INCREMENT NOT NULL, document_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, lien VARCHAR(255) DEFAULT NULL, type_document VARCHAR(255) NOT NULL, categorie VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_81A76DE5C33F7837 (document_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE document_ressource ADD CONSTRAINT FK_81A76DE5C33F7837 FOREIGN KEY (document_id) REFERENCES document_file (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE document_ressource DROP FOREIGN KEY FK_81A76DE5C33F7837');
        $this->addSql('DROP TABLE document_ressource');
    }
}
