<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241213092109 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MODIF INSTANCE 2. Ajouter possibilité pour quelqu\'un avec droit de validation de saisir "aucune APL n\'a eu lieu cette année pour cette structure" → pas d\'équipe + rattachement de la structure';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance ADD na_pas_eu_lieu TINYINT(1) DEFAULT NULL');
        $this->addSql('UPDATE election_nominative SET poste="Mission de coordination" WHERE poste="Mission de coodination"');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance DROP na_pas_eu_lieu');
    }
}
