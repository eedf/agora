<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240412214639 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Suppression des mission de délégué à l\'AG';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM attribution_fonction_elective WHERE type="Délégué·e à l\'AG";');
    }

    public function down(Schema $schema): void {}
}
