<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231118111900 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE saison_democratique ADD saison_precedente_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE saison_democratique ADD CONSTRAINT FK_A90E0872CB16C629 FOREIGN KEY (saison_precedente_id) REFERENCES saison_democratique (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A90E0872CB16C629 ON saison_democratique (saison_precedente_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE saison_democratique DROP FOREIGN KEY FK_A90E0872CB16C629');
        $this->addSql('DROP INDEX UNIQ_A90E0872CB16C629 ON saison_democratique');
        $this->addSql('ALTER TABLE saison_democratique DROP saison_precedente_id');
    }
}
