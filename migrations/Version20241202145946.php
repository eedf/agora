<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241202145946 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MODIF VOTES 2. Ajouter vote blanc';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE structure CHANGE responsabilite_regionale responsabilite_regionale TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE vote ADD nombre_voix_blancs INT DEFAULT 0');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE structure CHANGE responsabilite_regionale responsabilite_regionale TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE vote DROP nombre_voix_blancs');
    }
}
