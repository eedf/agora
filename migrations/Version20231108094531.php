<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231108094531 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE vote (id INT AUTO_INCREMENT NOT NULL, nombre_voix_favorables INT NOT NULL, nombre_voix_defavorables INT DEFAULT NULL, nombre_de_voix_abstention INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vote_instance (id INT AUTO_INCREMENT NOT NULL, instance_id INT NOT NULL, vote_id INT NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_BC6F18F73A51721D (instance_id), UNIQUE INDEX UNIQ_BC6F18F772DCDAFC (vote_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vote_instance ADD CONSTRAINT FK_BC6F18F73A51721D FOREIGN KEY (instance_id) REFERENCES compte_rendu_instance (id)');
        $this->addSql('ALTER TABLE vote_instance ADD CONSTRAINT FK_BC6F18F772DCDAFC FOREIGN KEY (vote_id) REFERENCES vote (id)');
        $this->addSql('DROP TABLE messenger_messages');
        $this->addSql('ALTER TABLE adresse_postale CHANGE code_postal code_postal VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE personne ADD date_derniere_connexion DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, headers LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, queue_name VARCHAR(190) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE vote_instance DROP FOREIGN KEY FK_BC6F18F73A51721D');
        $this->addSql('ALTER TABLE vote_instance DROP FOREIGN KEY FK_BC6F18F772DCDAFC');
        $this->addSql('DROP TABLE vote');
        $this->addSql('DROP TABLE vote_instance');
        $this->addSql('ALTER TABLE adresse_postale CHANGE code_postal code_postal VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE personne DROP date_derniere_connexion');
    }
}
