<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241203151606 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MODIF VOTES 1. Force de vote qui change en cours d'instance";
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE nombre_votants_specifique (id INT AUTO_INCREMENT NOT NULL, compte_rendu_id INT NOT NULL, nombre_votants INT DEFAULT NULL, categorie VARCHAR(255) NOT NULL, INDEX IDX_369DB4644BC44A10 (compte_rendu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE nombre_votants_specifique ADD CONSTRAINT FK_369DB4644BC44A10 FOREIGN KEY (compte_rendu_id) REFERENCES compte_rendu_instance (id)');
        $this->addSql('ALTER TABLE motion ADD vote_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE motion ADD CONSTRAINT FK_F5FEA1E872DCDAFC FOREIGN KEY (vote_id) REFERENCES vote (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F5FEA1E872DCDAFC ON motion (vote_id)');
        $this->addSql('ALTER TABLE voeu ADD vote_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE voeu ADD CONSTRAINT FK_147EB61072DCDAFC FOREIGN KEY (vote_id) REFERENCES vote (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_147EB61072DCDAFC ON voeu (vote_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE motion DROP FOREIGN KEY FK_F5FEA1E872DCDAFC');
        $this->addSql('DROP INDEX UNIQ_F5FEA1E872DCDAFC ON motion');
        $this->addSql('ALTER TABLE motion DROP vote_id');
        $this->addSql('ALTER TABLE voeu DROP FOREIGN KEY FK_147EB61072DCDAFC');
        $this->addSql('DROP INDEX UNIQ_147EB61072DCDAFC ON voeu');
        $this->addSql('ALTER TABLE voeu DROP vote_id');
        $this->addSql('ALTER TABLE nombre_votants_specifique DROP FOREIGN KEY FK_369DB4644BC44A10');
        $this->addSql('DROP TABLE nombre_votants_specifique');
    }
}
