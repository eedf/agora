<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230928091641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE document_file (id INT AUTO_INCREMENT NOT NULL, updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', document_size INT DEFAULT NULL, document_name VARCHAR(255) DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE droit_manuel (id INT AUTO_INCREMENT NOT NULL, structure_id INT NOT NULL, personne_id INT NOT NULL, date_expiration DATETIME DEFAULT NULL, droit VARCHAR(255) NOT NULL, INDEX IDX_1E11CE7F2534008B (structure_id), INDEX IDX_1E11CE7FA21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE droit_manuel ADD CONSTRAINT FK_1E11CE7F2534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE droit_manuel ADD CONSTRAINT FK_1E11CE7FA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD budget_previsionnel_id INT DEFAULT NULL, ADD projet_pedagogique_id INT DEFAULT NULL, ADD plan_action_id INT DEFAULT NULL, ADD calendrier_previsionnel_id INT DEFAULT NULL, ADD rapport_moral_id INT DEFAULT NULL, ADD rapport_activites_id INT DEFAULT NULL, ADD rapport_financier_id INT DEFAULT NULL, ADD fonctionnement_structure_id INT DEFAULT NULL, ADD liste_emargement_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571396CD707 FOREIGN KEY (budget_previsionnel_id) REFERENCES document_file (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571B47D6FC3 FOREIGN KEY (projet_pedagogique_id) REFERENCES document_file (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F845712DEC0D20 FOREIGN KEY (plan_action_id) REFERENCES document_file (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571B008EC49 FOREIGN KEY (calendrier_previsionnel_id) REFERENCES document_file (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F845718DBA1023 FOREIGN KEY (rapport_moral_id) REFERENCES document_file (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571FF7A5E24 FOREIGN KEY (rapport_activites_id) REFERENCES document_file (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571EEC35FE6 FOREIGN KEY (rapport_financier_id) REFERENCES document_file (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571B326AD26 FOREIGN KEY (fonctionnement_structure_id) REFERENCES document_file (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571FD1376AE FOREIGN KEY (liste_emargement_id) REFERENCES document_file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2F84571396CD707 ON compte_rendu_instance (budget_previsionnel_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2F84571B47D6FC3 ON compte_rendu_instance (projet_pedagogique_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2F845712DEC0D20 ON compte_rendu_instance (plan_action_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2F84571B008EC49 ON compte_rendu_instance (calendrier_previsionnel_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2F845718DBA1023 ON compte_rendu_instance (rapport_moral_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2F84571FF7A5E24 ON compte_rendu_instance (rapport_activites_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2F84571EEC35FE6 ON compte_rendu_instance (rapport_financier_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2F84571B326AD26 ON compte_rendu_instance (fonctionnement_structure_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B2F84571FD1376AE ON compte_rendu_instance (liste_emargement_id)');
        $this->addSql('ALTER TABLE personne ADD roles JSON NOT NULL, ADD password VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE saison_democratique DROP date_limite_aplsla, DROP date_limite_aplslan, DROP date_limite_congres');
        $this->addSql('ALTER TABLE structure ADD structure_regionale_affiliee_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE structure ADD CONSTRAINT FK_6F0137EA25A41551 FOREIGN KEY (structure_regionale_affiliee_id) REFERENCES structure (id)');
        $this->addSql('CREATE INDEX IDX_6F0137EA25A41551 ON structure (structure_regionale_affiliee_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571396CD707');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571B47D6FC3');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F845712DEC0D20');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571B008EC49');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F845718DBA1023');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571FF7A5E24');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571EEC35FE6');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571B326AD26');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571FD1376AE');
        $this->addSql('ALTER TABLE droit_manuel DROP FOREIGN KEY FK_1E11CE7F2534008B');
        $this->addSql('ALTER TABLE droit_manuel DROP FOREIGN KEY FK_1E11CE7FA21BD112');
        $this->addSql('DROP TABLE document_file');
        $this->addSql('DROP TABLE droit_manuel');
        $this->addSql('DROP INDEX UNIQ_B2F84571396CD707 ON compte_rendu_instance');
        $this->addSql('DROP INDEX UNIQ_B2F84571B47D6FC3 ON compte_rendu_instance');
        $this->addSql('DROP INDEX UNIQ_B2F845712DEC0D20 ON compte_rendu_instance');
        $this->addSql('DROP INDEX UNIQ_B2F84571B008EC49 ON compte_rendu_instance');
        $this->addSql('DROP INDEX UNIQ_B2F845718DBA1023 ON compte_rendu_instance');
        $this->addSql('DROP INDEX UNIQ_B2F84571FF7A5E24 ON compte_rendu_instance');
        $this->addSql('DROP INDEX UNIQ_B2F84571EEC35FE6 ON compte_rendu_instance');
        $this->addSql('DROP INDEX UNIQ_B2F84571B326AD26 ON compte_rendu_instance');
        $this->addSql('DROP INDEX UNIQ_B2F84571FD1376AE ON compte_rendu_instance');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP budget_previsionnel_id, DROP projet_pedagogique_id, DROP plan_action_id, DROP calendrier_previsionnel_id, DROP rapport_moral_id, DROP rapport_activites_id, DROP rapport_financier_id, DROP fonctionnement_structure_id, DROP liste_emargement_id');
        $this->addSql('ALTER TABLE personne DROP roles, DROP password');
        $this->addSql('ALTER TABLE saison_democratique ADD date_limite_aplsla DATETIME NOT NULL, ADD date_limite_aplslan DATETIME NOT NULL, ADD date_limite_congres DATETIME NOT NULL');
        $this->addSql('ALTER TABLE structure DROP FOREIGN KEY FK_6F0137EA25A41551');
        $this->addSql('DROP INDEX IDX_6F0137EA25A41551 ON structure');
        $this->addSql('ALTER TABLE structure DROP structure_regionale_affiliee_id');
    }
}
