<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231122112558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE attribution_fonction_elective (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, equipe_id INT NOT NULL, compte_rendu_id INT NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME DEFAULT NULL, status VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_2DB3F3AA21BD112 (personne_id), INDEX IDX_2DB3F3A6D861B89 (equipe_id), INDEX IDX_2DB3F3A4BC44A10 (compte_rendu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attribution_fonction_elective ADD CONSTRAINT FK_2DB3F3AA21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE attribution_fonction_elective ADD CONSTRAINT FK_2DB3F3A6D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id)');
        $this->addSql('ALTER TABLE attribution_fonction_elective ADD CONSTRAINT FK_2DB3F3A4BC44A10 FOREIGN KEY (compte_rendu_id) REFERENCES compte_rendu_instance (id)');
        $this->addSql('ALTER TABLE nomination_elu DROP FOREIGN KEY FK_7BED9DA23A51721D');
        $this->addSql('ALTER TABLE nomination_elu DROP FOREIGN KEY FK_7BED9DA2A21BD112');
        $this->addSql('ALTER TABLE nomination_elu DROP FOREIGN KEY FK_7BED9DA257889920');
        $this->addSql('ALTER TABLE nomination_elu DROP FOREIGN KEY FK_7BED9DA26D861B89');
        $this->addSql('DROP TABLE nomination_elu');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE nomination_elu (id INT AUTO_INCREMENT NOT NULL, instance_id INT NOT NULL, personne_id INT NOT NULL, equipe_id INT NOT NULL, fonction_id INT DEFAULT NULL, nombre_voix_pour INT DEFAULT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, type VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, statut VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, nomn VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_7BED9DA23A51721D (instance_id), INDEX IDX_7BED9DA26D861B89 (equipe_id), INDEX IDX_7BED9DA257889920 (fonction_id), INDEX IDX_7BED9DA2A21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE nomination_elu ADD CONSTRAINT FK_7BED9DA23A51721D FOREIGN KEY (instance_id) REFERENCES compte_rendu_instance (id)');
        $this->addSql('ALTER TABLE nomination_elu ADD CONSTRAINT FK_7BED9DA2A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE nomination_elu ADD CONSTRAINT FK_7BED9DA257889920 FOREIGN KEY (fonction_id) REFERENCES fonction (id)');
        $this->addSql('ALTER TABLE nomination_elu ADD CONSTRAINT FK_7BED9DA26D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id)');
        $this->addSql('ALTER TABLE attribution_fonction_elective DROP FOREIGN KEY FK_2DB3F3AA21BD112');
        $this->addSql('ALTER TABLE attribution_fonction_elective DROP FOREIGN KEY FK_2DB3F3A6D861B89');
        $this->addSql('ALTER TABLE attribution_fonction_elective DROP FOREIGN KEY FK_2DB3F3A4BC44A10');
        $this->addSql('DROP TABLE attribution_fonction_elective');
    }
}
