<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231128174746 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE attribution_fonction_elective ADD fonction_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE attribution_fonction_elective ADD CONSTRAINT FK_2DB3F3A57889920 FOREIGN KEY (fonction_id) REFERENCES fonction (id)');
        $this->addSql('CREATE INDEX IDX_2DB3F3A57889920 ON attribution_fonction_elective (fonction_id)');
        $this->addSql('ALTER TABLE election_nominative ADD deja_elu TINYINT(1) DEFAULT NULL, ADD pas_delection TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE attribution_fonction_elective DROP FOREIGN KEY FK_2DB3F3A57889920');
        $this->addSql('DROP INDEX IDX_2DB3F3A57889920 ON attribution_fonction_elective');
        $this->addSql('ALTER TABLE attribution_fonction_elective DROP fonction_id');
        $this->addSql('ALTER TABLE election_nominative DROP deja_elu, DROP pas_delection');
    }
}
