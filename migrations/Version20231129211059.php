<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231129211059 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE election_nominative DROP INDEX UNIQ_268DC183A21BD112, ADD INDEX IDX_268DC183A21BD112 (personne_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE election_nominative DROP INDEX IDX_268DC183A21BD112, ADD UNIQUE INDEX UNIQ_268DC183A21BD112 (personne_id)');
    }
}
