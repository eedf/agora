<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231129212140 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571FBC25E50');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F845715C2D9856');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571FBC25E50 FOREIGN KEY (personne_valideuse_id) REFERENCES personne (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F845715C2D9856 FOREIGN KEY (derniere_personne_soummetteur_id) REFERENCES personne (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F845715C2D9856');
        $this->addSql('ALTER TABLE compte_rendu_instance DROP FOREIGN KEY FK_B2F84571FBC25E50');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F845715C2D9856 FOREIGN KEY (derniere_personne_soummetteur_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE compte_rendu_instance ADD CONSTRAINT FK_B2F84571FBC25E50 FOREIGN KEY (personne_valideuse_id) REFERENCES personne (id)');
    }
}
