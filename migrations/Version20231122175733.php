<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231122175733 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE motion (id INT AUTO_INCREMENT NOT NULL, compte_rendu_id INT NOT NULL, titre VARCHAR(255) NOT NULL, contenu LONGTEXT NOT NULL, INDEX IDX_F5FEA1E84BC44A10 (compte_rendu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voeu (id INT AUTO_INCREMENT NOT NULL, compte_rendu_id INT NOT NULL, titre VARCHAR(255) NOT NULL, contenu LONGTEXT NOT NULL, INDEX IDX_147EB6104BC44A10 (compte_rendu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE motion ADD CONSTRAINT FK_F5FEA1E84BC44A10 FOREIGN KEY (compte_rendu_id) REFERENCES compte_rendu_instance (id)');
        $this->addSql('ALTER TABLE voeu ADD CONSTRAINT FK_147EB6104BC44A10 FOREIGN KEY (compte_rendu_id) REFERENCES compte_rendu_instance (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE motion DROP FOREIGN KEY FK_F5FEA1E84BC44A10');
        $this->addSql('ALTER TABLE voeu DROP FOREIGN KEY FK_147EB6104BC44A10');
        $this->addSql('DROP TABLE motion');
        $this->addSql('DROP TABLE voeu');
    }
}
