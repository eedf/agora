<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241210113836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'DOCUMENTS 2 . Documents APL/Congrès : permettre documents hors documents type';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE autre_document (id INT AUTO_INCREMENT NOT NULL, document_id INT NOT NULL, compte_rendu_instance_id INT NOT NULL, titre VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_C1DE9D3AC33F7837 (document_id), INDEX IDX_C1DE9D3AD1A068B (compte_rendu_instance_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE autre_document ADD CONSTRAINT FK_C1DE9D3AC33F7837 FOREIGN KEY (document_id) REFERENCES document_file (id)');
        $this->addSql('ALTER TABLE autre_document ADD CONSTRAINT FK_C1DE9D3AD1A068B FOREIGN KEY (compte_rendu_instance_id) REFERENCES compte_rendu_instance (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE autre_document DROP FOREIGN KEY FK_C1DE9D3AC33F7837');
        $this->addSql('ALTER TABLE autre_document DROP FOREIGN KEY FK_C1DE9D3AD1A068B');
        $this->addSql('DROP TABLE autre_document');
    }
}
