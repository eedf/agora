<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231129211932 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF2B4A8107');
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF39FF2EE9');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF2B4A8107 FOREIGN KEY (responsable_legal2_id) REFERENCES personne (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF39FF2EE9 FOREIGN KEY (responsable_legal1_id) REFERENCES personne (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF39FF2EE9');
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF2B4A8107');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF39FF2EE9 FOREIGN KEY (responsable_legal1_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF2B4A8107 FOREIGN KEY (responsable_legal2_id) REFERENCES personne (id)');
    }
}
