<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240917154807 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'PRAGA 3. Changer "rapport moral et d\'activités" en "rapport moral"';
    }

    public function up(Schema $schema): void
    {
        $rapportMoral = "Rapport Moral";
        $rapportMoralEtActivites = "Rapport Moral et d'Activités";
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE vote_instance SET type="' . $rapportMoral . '" WHERE id in 
            (SELECT vi.id 
                from (select * from vote_instance) vi 
                INNER JOIN compte_rendu_instance cr 
                ON (
                    (vi.type="' . $rapportMoralEtActivites . '") 
                    AND (cr.id = vi.instance_id)
                    ) 
                INNER JOIN saison_democratique s 
                ON (s.id = cr.saison_id AND s.actif IS TRUE)
            );');
    }

    public function down(Schema $schema): void
    {

        $rapportMoral = "Rapport Moral";
        $rapportMoralEtActivites = "Rapport Moral et d'Activités";
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE vote_instance SET type="' . $rapportMoralEtActivites . '" WHERE id in 
            (SELECT vi.id 
                from (select * from vote_instance) vi 
                INNER JOIN compte_rendu_instance cr 
                ON (
                    (vi.type="' . $rapportMoral . '") 
                    AND (cr.id = vi.instance_id)
                    ) 
                INNER JOIN saison_democratique s 
                ON (s.id = cr.saison_id AND s.actif IS TRUE)
            );');
    }
}
