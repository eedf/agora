<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240224165039 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("UPDATE vote_instance SET type=\"Plan d'Action Régional\" WHERE id in (select vi.id from (select * from vote_instance) vi INNER JOIN compte_rendu_instance cr ON (cr.id = vi.instance_id AND vi.type = \"Plan d'Action Local\" AND cr.type=\"congres\"));");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("UPDATE vote_instance SET type=\"Plan d'Action Local\" WHERE id in (select vi.id from (select * from vote_instance) vi INNER JOIN compte_rendu_instance cr ON (cr.id = vi.instance_id AND vi.type = \"Plan d'Action Régional\" AND cr.type=\"congres\"));");
    }
}
